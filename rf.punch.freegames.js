/* eslint-disable no-magic-numbers */
( function() {
    "use strict";
    var m_state = game.state.getCu2rrentState();
    var bkg = m_state.getRFLoadScenes().getPhaserBackgrounds().zzbasegame;

    var crop = new Phaser.Rectangle( 0, 0, bkg.width, bkg.height );
    game.add.tween( crop ).to( { height: 0 }, 600, Phaser.Easing.Quartic.Out, true );
    bkg.crop( crop );

    var updateCrop = function() {
        bkg.updateCrop();
    };

    game.time.events.loop( Phaser.Timer.SECOND / 1000, updateCrop );

    function onShutdown() {
        m_state = null;
        bkg = null;
        updateCrop = null;
    }

    m_state.onShutdown.addOnce( onShutdown );
} )();
