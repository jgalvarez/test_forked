/* eslint-disable no-magic-numbers */
/* eslint-disable no-implicit-globals */
/* jshint -W087 */ // Ignore debugger
// test
/**
 * @version 0.1.0
 * CAMBIOS:
 * 0.1.0 - Soporte para inicio de automático de la selección.
 * 0.0.1 - Escena inicial
 */

// Un bello cerdo.
// Un bello cochino.

// añado comentario
var PickAndWinConsts = {
    MS_DELAY_EXPLOSION_ANIM: 0,
    MS_DELAY_SHINE_ANIM: 1000,
    MS_DELAY_SHOW_PRIZE: 1050,
    MS_DELAY_SHOW_NON_WINNING_PRIZES: 2000,
    MS_TW_ALPHA_PRIZE: 2500,
    MS_TW_SCALE_PRIZE: 2500,
    MS_TW_HALO_MIN: 1000,
    MS_TW_HALO_MAX: 2000,
    MS_DELAY_HALO_MIN: 700,
    MS_DELAY_HALO_MAX: 1000,
    MS_ALPHA_MIN_HALO_MIN: 0,
    MS_ALPHA_MIN_HALO_MAX: 0.3,
    MS_DELAY_PLAY_WIN_SOUND: 1000,
    MS_TW_FADEOUT_TITLE: 2000,
    MS_DELAY_TO_EXIT_TO_BASEGAME: 7500,
    MS_TO_START_AUTO_BONUS: _.get( RFGameConfig, "game.msToStartAutoBonus", Phaser.Timer.MINUTE )
};

var Stone = function( game ) {
    "use strict";
    this.base = undefined;
    this.halo = undefined;
    this.gem = undefined;
    var isValid = "kaka" !== "pepe";
    if( isValid ) {
        console.log( "hola" );
    }

    this.brokenAnimation = undefined;
    this.shineAnimation = undefined;
    this.soundBrokenAnimation = undefined;

    this.playPrizeAnim = function() {
        this.base.visible = false;
        this.gem.visible = true;
        this.prizeLabel.visible = true;
        this.prizeLabel.alpha = 0;

        this.playBrokenAnimation( PickAndWinConsts.MS_DELAY_EXPLOSION_ANIM );
        this.playPrizeLabel( PickAndWinConsts.MS_DELAY_SHOW_PRIZE );
        this.playShineAnimation( PickAndWinConsts.MS_DELAY_SHINE_ANIM );
    };

    this.playBrokenAnimation = function( delayTime ) {
        playAnimation( this.brokenAnimation, delayTime, this.soundBrokenAnimation );
    };

    this.playShineAnimation = function( delayTime ) {
        playAnimation( this.shineAnimation, delayTime );
    };

    this.playPrizeLabel = function( delayTime ) {
        delayTime = delayTime > 0 ? delayTime : 1000;
        var repeat = 0;
        var yoyo = false;
        this.prizeLabel.scale.x = 0;
        this.prizeLabel.scale.y = 0;
        m_game.add
            .tween( this.prizeLabel )
            .to(
                { alpha: 1 },
                PickAndWinConsts.MS_TW_ALPHA_PRIZE,
                Phaser.Easing.Quartic.Out,
                true,
                delayTime,
                repeat,
                yoyo,
                delayTime,
                repeat,
                yoyo,
                delayTime,
                repeat,
                yoyo,
                delayTime,
                repeat,
                yoyo
            );
        m_game.add
            .tween( this.prizeLabel.scale )
            .to(
                { x: 1, y: 1 },
                PickAndWinConsts.MS_TW_SCALE_PRIZE,
                Phaser.Easing.Quartic.Out,
                true,
                delayTime,
                repeat,
                yoyo
            );
    };

    this.playHalo = function() {
        var twTime = _.random( PickAndWinConsts.MS_TW_HALO_MIN, PickAndWinConsts.MS_TW_HALO_MAX );
        var delayTime = _.random(
            PickAndWinConsts.MS_DELAY_HALO_MIN,
            PickAndWinConsts.MS_DELAY_HALO_MAX
        );
        var autoStart = true;
        var repeat = -1;
        var yoyo = true;
        var alphaMin = _.random(
            PickAndWinConsts.MS_ALPHA_MIN_HALO_MIN,
            PickAndWinConsts.MS_ALPHA_MIN_HALO_MAX
        );
        this.halo.alpha = alphaMin;
        this.stopHalo();
        m_tweenHalo = m_game.add
            .tween( this.halo )
            .to(
                { alpha: 1 },
                twTime,
                Phaser.Easing.Linear.None,
                autoStart,
                delayTime,
                repeat,
                yoyo
            );
    };

    this.stopHalo = function() {
        m_game.tweens.remove( m_tweenHalo );
    };

    this.fadeOutHalo = function( twTime ) {
        twTime = twTime > 0 ? twTime : Phaser.Timer.SECOND;
        var autoStart = true;
        var delayTime = 0;
        var repeat = 0;
        var yoyo = false;
        m_game.add
            .tween( this.halo )
            .to(
                { alpha: 0 },
                twTime,
                Phaser.Easing.Linear.None,
                autoStart,
                delayTime,
                repeat,
                yoyo
            );
    };

    var playAnimAndSound = function( anim, sound ) {
        if( anim ) {
            anim.play();
        }
        if( sound ) {
            sound.play();
        }
    };

    var playAnimation = function( animSprite, delayTime, sound ) {
        var playInternal = function() {
            animSprite.visible = true;
            playAnimAndSound( anim, sound );
        };

        delayTime = delayTime > 0 ? delayTime : 0;

        var animName = _.get( animSprite, [ "animations", "currentAnim", "name" ] );
        if( !animName ) {
            console.error( "Invalid animation", animName );
            return;
        }

        var anim = animSprite.animations.getAnimation( animName );
        if( !anim ) {
            console.warn( "I haven't animation from", animSprite );
            return;
        }

        if( delayTime === 0 ) {
            playInternal();
        }
        else{
            m_game.time.events.add( delayTime, playInternal );
        }
    };

    var m_game = game;
    var m_tweenHalo;
};

// eslint-disable-next-line max-statements
var PickAndWinScene = function( game ) {
    "use strict";

    var m_myScene = eCustomSceneNames.CUSTOM_GAME_1;

    // Call to base Scene Constructor
    var _protected = BaseScene.call( this, game, m_myScene );

    var m_super = this;
    var SceneUtils = _protected.SceneUtils;

    var self = this;
    var m_rfLoader = null;
    var m_rffsm = null;
    var m_phaserButtons = null;
    var m_phaserLabels = null;
    var m_phaserAnimations = null;
    var m_phaserImages = null;

    var m_timers = {
        startAutoBonusTimer: {
            msTime: PickAndWinConsts.MS_TO_START_AUTO_BONUS,
            timer: null
        }
    };
    var m_labelAutoGame;

    var m_audioSounds = {
        bkg: new RFAudioSound( game ),
        win: new RFAudioSound( game ),
        pickSelection: new RFAudioSound( game )
    };

    var m_stones = [ new Stone( game ), new Stone( game ), new Stone( game ), new Stone( game ), new Stone( game ), new Stone( game ) ];

    var m_pickButtonSelected;

    var MS_TO_STOP_COUNTER = 4000;

    this.shutdown = function() {
        // call to parent
        m_super.shutdownBase();
    };

    /**
     *
     */
    this.preload = function() {
        // call to parent
        m_super.preloadBase();
    };

    /**
     *
     */
    this.create = function() {
        // call to parent
        m_super.createBase();

        // create the state machine
        m_rffsm = new RFFsm();

        // initialize the state machine
        m_rffsm.initialize( m_fsmInitData );

        RFGlobalData.game.lastScene = m_myScene;

        // Start with initial state
        m_rffsm.changeState( eStateNames.INIT );
    };

    // ========================================================================== PRIVATE AREA ===

    var loadScene = function() {
        // Load all the scene
        m_rfLoader = m_super.getRFLoadScenes();
        m_rfLoader.loadScene( m_myScene );

        m_phaserLabels = m_rfLoader.getPhaserLabels();
        m_phaserButtons = m_rfLoader.getPhaserButtons();
        m_phaserImages = m_rfLoader.getPhaserImages();
        m_phaserAnimations = m_rfLoader.getPhaserAnimations();

        m_pickButtonSelected = null;

        SceneUtils.initSound( m_audioSounds.win, "WIN" );
        SceneUtils.initSound( m_audioSounds.pickSelection, "PICK_SELECTION" );
        SceneUtils.initSound( m_audioSounds.bkg, "BKG" );

        m_audioSounds.bkg.playLooped();

        SceneUtils.initLabels();

        addCounterSoundsToLabels();
        addCounterTweenToLabels();

        loadStones();

        drawWinPrize( 0 );
    };

    var getStoneByIdAndTag = function( map, tag, stoneId ) {
        var filtered = SceneUtils.filterByParameters( map, "tag", tag );
        return _.head( SceneUtils.filterByParameters( filtered, "stoneId", stoneId ) );
    };

    var getStoneBaseById = function( stoneId ) {
        return getStoneByIdAndTag( m_phaserImages, "STONE", stoneId );
    };
    var getStoneHaloById = function( stoneId ) {
        return getStoneByIdAndTag( m_phaserImages, "STONE_HALO", stoneId );
    };
    var getGemById = function( stoneId ) {
        return getStoneByIdAndTag( m_phaserImages, "GEM", stoneId );
    };
    var getStonePrizeById = function( stoneId ) {
        return _.head( SceneUtils.filterByParameters( m_phaserLabels, "stoneId", stoneId ) );
    };
    var getBrokenStoneAnimById = function( stoneId ) {
        return getStoneByIdAndTag( m_phaserAnimations, "BROKEN_STONE", stoneId );
    };
    var getShineStoneAnimById = function( stoneId ) {
        return getStoneByIdAndTag( m_phaserAnimations, "SHINE_STONE", stoneId );
    };

    var loadStones = function() {
        _.forEach( m_stones, function( stone, stoneId ) {
            stone.base = getStoneBaseById( stoneId );
            stone.halo = getStoneHaloById( stoneId );
            stone.gem = getGemById( stoneId );
            stone.prizeLabel = getStonePrizeById( stoneId );
            stone.soundBrokenAnimation = m_audioSounds.pickSelection;
            stone.brokenAnimation = getBrokenStoneAnimById( stoneId );
            stone.shineAnimation = getShineStoneAnimById( stoneId );
        } );
    };

    // ----------------------------------------------------------------------- BEGIN INIT STATE ---
    var startInit = function() {
        // scene creation
        loadScene();

        // Set action functions to the buttons
        addActionsButtons( m_rfLoader.getPhaserButtons() );

        // Keyboard bindings
        addKeyBindings();

        // Colocamos el texto de "Juego automático" si se está en autojuego
        m_labelAutoGame = _.find( m_phaserLabels, function( o ) {
            return o.parameters.tag === "AUTO_PLAY";
        } );
        m_labelAutoGame.visible = false;

        m_rffsm.changeState( eStateNames.IDLE );
    };
    var isStateIdle = function() {
        return m_rffsm.getState() === eStateNames.IDLE;
    };

    var exitInit = function() {
        console.log( "exitInit" );
    };
    // ----------------------------------------------------------------------- END INIT STATE ---

    // ----------------------------------------------------------------------- BEGIN IDLE STATE ---
    var startIdle = function() {
        console.log( "startIdle" );

        if( !isTimerActive( m_timers.startAutoBonusTimer ) ) {
            startTimer( m_timers.startAutoBonusTimer, autoPressButton );
        }

        playHalos();
    };

    var exitIdle = function() {
        console.log( "exitIdle" );
    };
    // ----------------------------------------------------------------------- END IDLE STATE ---

    // ----------------------------------------------------------------------- BEGIN EXIT STATE ---
    var startExit = function( oParams ) {
        if( _.has( oParams, "sceneName" ) ) {
            var timeToWait = 0;
            if( _.has( oParams, "msToWait" ) ) {
                timeToWait = oParams.msToWait;
            }
            if( timeToWait > 0 ) {
                self.game.time.events.add( timeToWait, function() {
                    stopAllSounds();
                    self.game.state.start( oParams.sceneName );
                } );
            }
            else{
                stopAllSounds();
            }
        }
        else{
            console.warn( "We are on EXIT state without no scene to go next" );
        }
    };
    // ----------------------------------------------------------------------- END EXIT STATE ---

    var startWaitingResponse = function( params ) {
        sendPickSelection( params );
    };

    var addCounterSoundsToLabels = function() {
        var soundType = eSoundTypes.COUNTER;
        var rfSounds = m_rfLoader.getRFSounds();
        var filteredSounds = SceneUtils.filterByType( rfSounds, soundType );
        if( filteredSounds.length > 0 ) {
            // init RFAudioSound
            var soundKeys = _.head( filteredSounds ).soundKeys;
            var rfAudioSound = new RFAudioSound( game );
            rfAudioSound.setSounds( soundKeys );

            var creditLabels = SceneUtils.filterByType( m_phaserLabels, eLabelsNames.msgWin );
            var label = _.head( creditLabels );
            label.setRFAudioSound( rfAudioSound );

            if( filteredSounds.length > 1 ) {
                console.warn( "Multiple sounds defined for this type:", soundType );
            }
        }
        else{
            console.warn( "No sounds found for counters", soundType );
        }
    };

    var addCounterTweenToLabels = function() {
        var applyTimeToFinish = function( oLabels, timeToFinish ) {
            _.forEach( oLabels, function( oLabel ) {
                oLabel.setTimeToFinish( timeToFinish );
            } );
        };
        applyTimeToFinish( SceneUtils.filterByType( m_phaserLabels, eLabelsNames.msgCredits ), 500 );
        applyTimeToFinish( SceneUtils.filterByType( m_phaserLabels, eLabelsNames.msgWin ), 500000 );
    };

    // ==============================================================
    // ON KEYB
    // ==============================================================

    var addKeyBindings = function() {
        if( _.get( RFGameConfig, [ "game", "useKeyBindings" ], true ) ) {
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.spin" ), onSpin );
        }
    };

    var onSpin = function() {
        console.log( "onSpin!" );
    };

    // ==============================================================
    // ON KEYB END
    // ==============================================================

    // ==============================================================
    // ON BUTTON CLICKS
    // ==============================================================

    var autoPressButton = function() {
        var button = getRandomPickSelection();
        onBtnPickSelection( button, true );
    };

    /**
     * @param  {object} phaserButtons
     */
    var addActionsButtons = function( phaserButtons ) {
        _.forEach( phaserButtons, function( oPhaserButton ) {
            var clickFunction = getOnClickFunction( oPhaserButton.action );
            oPhaserButton.onInputUp.add( clickFunction );
        } );
    };

    /**
     * @param   {string} strFunctionName Name of the action in gameDefinition for the buttons
     * @returns {function} the function to call when the button is clicked
     */
    var getOnClickFunction = function( strFunctionName ) {
        switch( strFunctionName ) {
            case"PICK_SELECTION":
                return onBtnPickSelection;

            default:
                console.warn( "Function ", strFunctionName, " not implemented in this scene" );
                return onBtnNotDefined;
        }
    };

    var onBtnPickSelection = function( button, autoPlay ) {
        autoPlay = _.isBoolean( autoPlay ) ? autoPlay : false;

        if( !autoPlay ) {
            clearTimer( m_timers.startAutoBonusTimer );
        }

        if( isStateIdle() ) {
            m_pickButtonSelected = button;

            m_labelAutoGame.visible = autoPlay;

            disableInput( m_phaserButtons );
            stopHalos();

            var params = {
                pickButton: button
            };
            m_rffsm.changeState(
                eStateNames.WAITING_RESPONSE,
                params,
                "Server Playing Game: PICK_SELECTION"
            );
        }
    };

    var onBtnNotDefined = function( button ) {
        console.warn( "Action not defined for this button", button );
    };

    // ==============================================================
    // ON BUTTON CLICKS END
    // ==============================================================

    // ==============================================================
    // BEGIN SERVER REQUEST
    // ==============================================================

    var sendPickSelection = function() {
        var serverParams = {};
        RFGlobalData.net.wrapper.sendGenericRequest(
            "pickAndWin",
            serverParams,
            onPickSelectionResponse,
            SceneUtils.fERROR
        );
    };

    // ==============================================================
    // END SERVER REQUEST
    // ==============================================================

    // ==============================================================
    // BEGIN SERVER RESPONSES
    // ==============================================================

    var onPickSelectionResponse = function( wsResponse ) {
        console.log( "PICK AND WIN received", wsResponse );

        var prize = _.get( wsResponse, [ "response", "pickAndWinState", "prize" ], 0 );
        var nonWinnerPrizesArray = _.get(
            wsResponse,
            [ "response", "pickAndWinState", "nonWinnerPrizesArray" ],
            []
        );

        // Update the residual prize to draw it in the basegame scene
        RFGlobalData.game.residualPrize = prize;

        var stone = m_stones[m_pickButtonSelected.parameters.stoneId];
        stone.prizeLabel.setMoneyOrCredits( prize, RFGlobalData.locale.currencyISO, 2000 );
        stone.playPrizeAnim();

        m_audioSounds.bkg.stop();

        m_game.time.events.add( PickAndWinConsts.MS_DELAY_SHINE_ANIM, function() {
            m_audioSounds.win.play();
        } );
        m_game.time.events.add( PickAndWinConsts.MS_DELAY_SHOW_NON_WINNING_PRIZES, function() {
            showNonWinningPrizes( nonWinnerPrizesArray );
        } );

        var title = _.head( SceneUtils.filterByParameters( m_phaserLabels, "tag", "BONUS_INFO" ) );
        if( title ) {
            SceneUtils.fadeOut( title, PickAndWinConsts.MS_TW_FADEOUT_TITLE );
        }

        m_game.time.events.add( Phaser.Timer.SECOND * 3, function() {
            drawWinPrize( prize, MS_TO_STOP_COUNTER );
        } );

        var serverSceneId = _.get(
            wsResponse,
            [ "response", "player", "currentServerSceneId" ],
            eSceneNames.BASEGAME
        );
        changeStateByServerSceneId( serverSceneId );
    };

    // ==============================================================
    //   END SERVER RESPONSES
    // ==============================================================

    // ==============================================================
    //   START UPDATE DATA
    // ==============================================================

    // ==============================================================
    //   END UPDATE DATA
    // ==============================================================

    // ==============================================================
    //   START DRAW DATA
    // ==============================================================

    var drawWinPrize = function( prize, twTime ) {
        twTime = twTime >= 0 ? twTime : 0;
        var labels = SceneUtils.filterByType( m_phaserLabels, eLabelsNames.msgWin );
        _.forEach( labels, function( oPhaserLabel ) {
            oPhaserLabel.setMoneyOrCredits( prize, RFGlobalData.locale.currencyISO, twTime );
        } );
    };

    // ==============================================================
    //   END DRAW DATA
    // ==============================================================

    var changeStateByServerSceneId = function( serverSceneId ) {
        if( !_.hasValue( eServerSceneId, serverSceneId ) ) {
            console.error( "Invalid server scene id: '" + serverSceneId + "'" );
            debugger;
        }

        var timeToWait = 0;
        switch( serverSceneId ) {
            case eServerSceneId.BASE_GAME:
                timeToWait = PickAndWinConsts.MS_DELAY_TO_EXIT_TO_BASEGAME;
                m_rffsm.changeState(
                    eStateNames.EXIT,
                    { sceneName: eSceneNames.BASEGAME, msToWait: timeToWait },
                    "Server Playing Game: BASEGAME"
                );
                break;
            default:
                console.warn( "Server state not controlled: '" + serverSceneId + "'" );
                break;
        }
    };

    // ==============================================================
    //  UTILS AND HELPERS
    // ==============================================================

    var getRandomPickSelection = function() {
        return _.sample(
            _.filter( m_phaserButtons, function( o ) {
                return o.action === "PICK_SELECTION";
            } )
        );
    };

    // #region Timers
    var startTimer = function( timerObj, fnCallBack ) {
        // console.info( "startTimer", timerObj );
        if( !_.isFunction( fnCallBack ) ) {
            console.error( "startTimer: fnCallBack is not a function", fnCallBack );
            return;
        }
        if( _.has( timerObj, "msTime" ) && _.has( timerObj, "timer" ) ) {
            if( timerObj.timer !== null ) {
                console.warn( "Previous timer was active. Creating anoyther timer", timerObj );
                clearTimer( timerObj );
            }
            timerObj.timer = game.time.events.add( timerObj.msTime, function() {
                timerObj.timer = null;
                if( _.isFunction( fnCallBack ) ) {
                    fnCallBack();
                }
            } );
        }
        else{
            console.warn( "Invalid timer", timerObj );
        }
    };
    var clearTimer = function( timerObj ) {
        console.info( "clearTimer", timerObj );
        var timer = _.get( timerObj, "timer" );
        if( _.has( timerObj, "timer" ) ) {
            if( timerObj.timer !== null ) {
                game.time.events.remove( timerObj.timer );
                timerObj.timer = null;
            }
        }
        else{
            console.warn( "Invalid timer", timer );
        }
    };
    var isTimerActive = function( timerObj ) {
        return Boolean( _.get( timerObj, [ "timer" ], false ) );
    };
    // #endregion Timers

    var playHalos = function() {
        _.forEach( m_stones, function( stone ) {
            stone.playHalo();
        } );
    };

    var stopHalos = function() {
        _.forEach( m_stones, function( stone ) {
            stone.stopHalo();
            stone.fadeOutHalo();
        } );
    };

    var showNonWinningPrizes = function( nonWiningPrizes ) {
        if( _.size( nonWiningPrizes ) < _.size( m_stones ) ) {
            console.error( "Less non wining prizes than stones", nonWiningPrizes );
            return;
        }

        _.forEach( m_stones, function( stone, stoneId ) {
            if( stoneId !== m_pickButtonSelected.parameters.stoneId ) {
                var prizeLabel = stone.prizeLabel;
                prizeLabel.visible = true;
                prizeLabel.alpha = 0;
                var nonWinningPrize = nonWiningPrizes.shift( 0 );
                prizeLabel.setMoneyOrCredits( nonWinningPrize, RFGlobalData.locale.currencyISO );

                var twTime = _.random( 1000, 2000 );
                var delayTime = _.random( 100, 500 );
                var autoStart = true;
                var repeat = 0;
                var yoyo = false;
                var alphaTo = 0.5;

                m_game.add.tween( prizeLabel ).to(
                    { alpha: alphaTo },

                    twTime,
                    Phaser.Easing.Linear.None,
                    autoStart,
                    delayTime,
                    repeat,
                    yoyo
                );
            }
        } );
    };

    var disableInput = function( map ) {
        _.forEach( map, function( element ) {
            element.inputEnabled = false;
        } );
    };

    var stopAllSounds = function() {
        _.forEach( m_audioSounds, function( audioSound ) {
            audioSound.stop();
        } );
        self.game.sound.stopAll();
    };

    // ===================================== FSM ===
    var eStateNames = {
        INIT: "INIT",
        IDLE: "IDLE",
        WAITING_RESPONSE: "WAITING_RESPONSE",
        WAITING_RESPONSE2: "WAITING_RESPONSE2",
        EXIT: "EXIT"
    };
    Object.freeze( eStateNames );
    Object.freeze( eStateNames );
    Object.freeze( eStateNames );

    var m_fsmInitData = {
        game: game,
        debug: true,
        states: {
            INIT: { onStart: startInit, onExit: exitInit },
            IDLE: { onStart: startIdle, onExit: exitIdle },
            WAITING_RESPONSE: {
                onStart: startWaitingResponse,
                onExit: null
            },
            EXIT: { onStart: startExit, onExit: null }
        }
    };
    // ===================================== END FSM ===

    var m_game = game;
};

PickAndWinScene.prototype = Object.create( PickAndWinScene.prototype );
PickAndWinScene.prototype.constructor = PickAndWinScene;
