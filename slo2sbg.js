/*! slo2sbg - v1.3.1 ( Build date: 2019-04-04 16:04:32 ) */


//------------------------------------------------------
// Source: src/engine/libs/xmltojson.js
//------------------------------------------------------

// Changes XML to JSON
// eslint-disable-next-line
function xmlToJson( xml ) {
    "use strict";
    // Create the return object
    var obj = {};

    var ELEMENT = 1;
    var TEXT = 3;

    if( xml.nodeType === ELEMENT ) { // element
        // do attributes
        if( xml.attributes.length > 0 ) {
            obj[ "@attributes" ] = {};
            for( var j = 0; j < xml.attributes.length; j++ ) {
                var attribute = xml.attributes.item( j );
                obj[ "@attributes" ][ attribute.nodeName ] = attribute.nodeValue;
            }
        }
    }
    else if( xml.nodeType === TEXT ) { // text
        obj = xml.nodeValue;
    }

    // do children
    if( xml.hasChildNodes() ) {
        for( var i = 0; i < xml.childNodes.length; i++ ) {
            var item = xml.childNodes.item( i );
            var nodeName = item.nodeName;
            if( typeof ( obj[ nodeName ] ) === "undefined" ) {
                obj[ nodeName ] = xmlToJson( item );
            }
            else {
                if( typeof ( obj[ nodeName ].push ) === "undefined" ) {
                    var old = obj[ nodeName ];
                    obj[ nodeName ] = [];
                    obj[ nodeName ].push( old );
                }
                obj[ nodeName ].push( xmlToJson( item ) );
            }
        }
    }
    return obj;
}


//------------------------------------------------------
// Source: src/engine/libs/filesaver.min.js
//------------------------------------------------------

/* @source http://purl.eligrey.com/github/FileSaver.js/blob/master/FileSaver.js */
/* jshint ignore:start */
/* eslint-disable */
var saveAs=saveAs||function(e){"use strict";if(typeof e==="undefined"||typeof navigator!=="undefined"&&/MSIE [1-9]\./.test(navigator.userAgent)){return}var t=e.document,n=function(){return e.URL||e.webkitURL||e},r=t.createElementNS("http://www.w3.org/1999/xhtml","a"),o="download"in r,i=function(e){var t=new MouseEvent("click");e.dispatchEvent(t)},a=/constructor/i.test(e.HTMLElement),f=/CriOS\/[\d]+/.test(navigator.userAgent),u=function(t){(e.setImmediate||e.setTimeout)(function(){throw t},0)},d="application/octet-stream",s=1e3*40,c=function(e){var t=function(){if(typeof e==="string"){n().revokeObjectURL(e)}else{e.remove()}};setTimeout(t,s)},l=function(e,t,n){t=[].concat(t);var r=t.length;while(r--){var o=e["on"+t[r]];if(typeof o==="function"){try{o.call(e,n||e)}catch(i){u(i)}}}},p=function(e){if(/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(e.type)){return new Blob([String.fromCharCode(65279),e],{type:e.type})}return e},v=function(t,u,s){if(!s){t=p(t)}var v=this,w=t.type,m=w===d,y,h=function(){l(v,"writestart progress write writeend".split(" "))},S=function(){if((f||m&&a)&&e.FileReader){var r=new FileReader;r.onloadend=function(){var t=f?r.result:r.result.replace(/^data:[^;]*;/,"data:attachment/file;");var n=e.open(t,"_blank");if(!n)e.location.href=t;t=undefined;v.readyState=v.DONE;h()};r.readAsDataURL(t);v.readyState=v.INIT;return}if(!y){y=n().createObjectURL(t)}if(m){e.location.href=y}else{var o=e.open(y,"_blank");if(!o){e.location.href=y}}v.readyState=v.DONE;h();c(y)};v.readyState=v.INIT;if(o){y=n().createObjectURL(t);setTimeout(function(){r.href=y;r.download=u;i(r);h();c(y);v.readyState=v.DONE});return}S()},w=v.prototype,m=function(e,t,n){return new v(e,t||e.name||"download",n)};if(typeof navigator!=="undefined"&&navigator.msSaveOrOpenBlob){return function(e,t,n){t=t||e.name||"download";if(!n){e=p(e)}return navigator.msSaveOrOpenBlob(e,t)}}w.abort=function(){};w.readyState=w.INIT=0;w.WRITING=1;w.DONE=2;w.error=w.onwritestart=w.onprogress=w.onwrite=w.onabort=w.onerror=w.onwriteend=null;return m}(typeof self!=="undefined"&&self||typeof window!=="undefined"&&window||this.content);if(typeof module!=="undefined"&&module.exports){module.exports.saveAs=saveAs}else if(typeof define!=="undefined"&&define!==null&&define.amd!==null){define([],function(){return saveAs})}
/* eslint-enable */
/* jshint ignore:end */

//------------------------------------------------------
// Source: src/engine/libs/rf/rf.loader.asyncloader.js
//------------------------------------------------------

/**
 * Carga recursos en Phaser de forma asíncrona.
 * @author Lolo
 * @version 0.1.3
 * @constructor
 * @param {object} game  El juego de Phaser en el que se incluye la librería.
 * @requires phaser.min.js
 * @requires lodash.js
 * CAMBIOS:
 * 0.1.3 - Optimizados los accesos por lodash
 */
// eslint-disable-next-line
function RFAsyncLoader(game) {
    "use strict";

    this.VERSION = "0.1.2";

    var MS_WAIT_TO_START_FIRST_LOAD = 5000;

    /**
     * Valores por defecto de las opciones
     */
    var m_options = {
        msToWaitNextLoad : 100,
        debug: false
    };
    this.setOptions = function( oOptions ) {
        m_options.debug = _.get( oOptions, [ "debug" ], m_options.debug );
        m_options.msToWaitNextLoad = _.get( oOptions, [ "msToWaitNextLoad" ], m_options.msToWaitNextLoad );
    };

    /**
     * Añade a la lista de carga imágenes estáticas.
     * @author Lolo
     * @param {RFAsyncObject} asyncObject El objeto que incluye toda la información para cargar la imagen.
     * @throws Si no se ha definido una key en asyncObject
     * @throws Si no se ha definido una url en asyncObject
     * @throws Si ya existe otro objeto en la caché de Phaser con la misma key de asyncObject
     */
    this.addImage = function( asyncObject ) {
        if( _.isObject( asyncObject ) ) {
            if( _.isEmpty( asyncObject.key ) ) { throw new Error( "No se ha definido una key en el objeto RFAsyncObject" ); }
            if( _.isEmpty( asyncObject.url ) ) { throw new Error( "No se ha definido una url en el objeto RFAsyncObject con key " + asyncObject.key ); }
            // if (m_game.cache.checkImageKey(asyncObject.key)) console.warn("Ya existe otra imagen en Phaser con la misma key que "+asyncObject.key);

            if( !m_game.cache.checkImageKey( asyncObject.key ) ) {
                if( ( m_game.device.desktop ) || ( asyncObject.forMobile ) ) {
                    addToQueue( asyncObject, eAsyncObjectTypes.IMAGE );
                }
            }

        }
        else {
            console.warn( "No se ha facilitado ningún objeto RFAsyncObject para cargar." );
        }
    };

    // @todo: Eliminar en una futura versión
    this.loadSpriteSheet = function( asyncObject ) {
        console.warn( "RFAsyncLoader::loadSpriteSheet is deprecated. Use 'addSpriteSheet' method and then 'startLoad'", asyncObject );
        self.addSpriteSheet( asyncObject );
    };

    /**
     * Añade a la lista de carga animaciones como SpriteSheet.
     * @author Lolo
     * @param {RFAsyncObject} asyncObject El objeto que incluye toda la información para cargar la imagen.
     * @throws Si no se ha definido una key en asyncObject
     * @throws Si no se ha definido una url en asyncObject
     * @throws Si ya existe otro objeto en la caché de Phaser con la misma key de asyncObject
     * @throws Si no se ha especificado el ancho del fotograma
     * @throws Si no se ha especificado el alto del fotograma
     */
    this.addSpriteSheet = function( asyncObject ) {
        if( _.isObject( asyncObject ) ) {

            /* eslint-disable */
            if( _.isEmpty( asyncObject.key ) ) { throw new Error( "No se ha definido una key en el objeto RFAsyncObject" ); }
            if( _.isEmpty( asyncObject.url ) ) { throw new Error( "No se ha definido una url en el objeto RFAsyncObject con key " + asyncObject.key ); }
            if( !_.isNumber( asyncObject.frameWidth ) ) { throw new Error( "No se ha definido el ancho del fotograma como número en el objeto RFAsyncObject con key " + asyncObject.key ); }
            if( !_.isNumber( asyncObject.frameHeight ) ) { throw new Error( "No se ha definido el alto del fotograma como número en el objeto RFAsyncObject con key " + asyncObject.key ); }
            if( !_.isNumber( asyncObject.totalFrames ) ) { throw new Error( "No se ha definido el total de fotogramas en el objeto RFAsyncObject con key " + asyncObject.key ); }
            // if (m_game.cache.checkImageKey(asyncObject.key)) console.warn("Ya existe otra imagen en Phaser con la misma key que "+asyncObject.key);
            /* eslint-enable */

            if( !m_game.cache.checkImageKey( asyncObject.key ) ) {
                if( ( m_game.device.desktop ) || ( asyncObject.forMobile ) ) {
                    addToQueue( asyncObject, eAsyncObjectTypes.SPRITE_SHEET );
                }
            }
        }
        else {
            console.warn( "No se ha facilitado ningún objeto RFAsyncObject para cargar." );
        }
    };

    /**
     * Añade a la lista de carga Atlas XML.
     * @author Lolo
     * @param {RFAsyncObject} asyncObject El objeto que incluye toda la información para cargar el atlas.
     * @throws Si no se ha definido una key en asyncObject
     * @throws Si no se ha definido una url en asyncObject
     * @throws Si ya existe otro objeto en la caché de Phaser con la misma key de asyncObject
     * @throws Si no se ha especificado una ruta a un fichero XML de atlas
     */
    this.addAtlas = function( asyncObject ) {
        if( _.isObject( asyncObject ) ) {
            if( _.isEmpty( asyncObject.key ) ) { throw new Error( "No se ha definido una key en el objeto RFAsyncObject" ); }
            if( _.isEmpty( asyncObject.url ) ) { throw new Error( "No se ha definido una url en el objeto RFAsyncObject con key " + asyncObject.key ); }
            if( _.isEmpty( asyncObject.xmlData ) ) { throw new Error( "No se ha definido el fichero XML del atlas en " + asyncObject.key ); }
            // if (m_game.cache.checkImageKey(asyncObject.key)) console.warn("Ya existe otro recurso en Phaser con la misma key que " + asyncObject.key);

            if( !m_game.cache.checkImageKey( asyncObject.key ) ) {
                if( ( m_game.device.desktop ) || ( asyncObject.forMobile ) ) {
                    addToQueue( asyncObject, eAsyncObjectTypes.ATLAS );
                }
            }
        }
        else {
            console.warn( "No se ha facilitado ningún objeto RFAsyncObject para cargar." );
        }
    };

    /**
     * Inicia la descarga asíncrona sencuencial
     * @param {string=} strAssetsDesc texto para identificar las cargas en los logs
     */
    this.startLoad = function( strAssetsDesc ) {
        if( !m_isLoading ) {
            if( _.size( m_loadQueue ) > 0 ) {
                m_strAssetsDesc = strAssetsDesc;
                if( m_options.debug ) { console.log( m_strAssetsDesc, ": Start async load", m_loadQueue ); }
                loadNext( MS_WAIT_TO_START_FIRST_LOAD ); // Espera un tiempo prudencial para iniciar la primera carga.
                m_isLoading = true;
            }
        }
    };

    /**
     * Deja de descargar la secuencia de archivos ( se puede reanundad nuevamente con startLoad ).
     */
    this.stopLoad = function() {
        if( m_isLoading ) {
            if( m_options.debug ) { console.log( m_strAssetsDesc, ": Stop async load" ); }
            clearTimeout( m_timer );
            m_timer = null;
            m_isLoading = false;
        }
    };

    /**
     * Realiza una pausa para la descarga. Se usa para pausar cargas asíncronas en cambio de escenas
     */
    this.pauseLoad = function() {
        if( m_isLoading ) {
            if( m_options.debug ) { console.log( m_strAssetsDesc, ": Pause async load" ); }
            m_pauseLoad = true;
        }
    };

    /**
     * Continúa la descarga donde la había dejado. Se usa para cantinuar cargas asíncronas en cambio de escenas
     */
    this.continueLoad = function() {
        if( m_isLoading ) {
            if( m_options.debug ) { console.log( m_strAssetsDesc, ": Continue async load" ); }
            m_pauseLoad = false;
        }
    };

    /**
     * Borra la cola de descarga
     */
    this.clearQueue = function() {
        if( _.size( m_loadQueue ) > 0 ) {
            this.stopLoad();
            if( m_options.debug ) { console.log( m_strAssetsDesc, ": Clear async queue load. Total load time elapsed " + m_totalTime + " ms" ); }
            m_loadQueue = [];
            m_totalTime = 0;
        }
    };

    this.isLoading = function() {
        return m_isLoading;
    };


    var addToQueue = function( asyncObject, type ) {
        if( _.hasValue( eAsyncObjectTypes, type ) ) {
            m_loadQueue.push( {
                type: type,
                obj: asyncObject
            } );
        }
    };

    var loadElement = function( element ) {

        if( element ) {

            assertAllKeys( element, [ "type", "obj" ] );

            var asyncObject = element.obj;
            if( m_game.cache.checkImageKey( asyncObject.key ) ) {
                loadNext( 0 ); // load now
            }
            else {
                switch( element.type ) {
                    case eAsyncObjectTypes.ATLAS:
                        m_loader.atlasXML( asyncObject.key, asyncObject.url, asyncObject.atlasXML );
                        startLoad( asyncObject );
                        break;

                    case eAsyncObjectTypes.IMAGE:
                        m_loader.image( asyncObject.key, asyncObject.url );
                        startLoad( asyncObject );
                        break;

                    case eAsyncObjectTypes.SPRITE_SHEET:
                        m_loader.spritesheet( asyncObject.key, asyncObject.url, asyncObject.frameWidth, asyncObject.frameHeight, asyncObject.totalFrames );
                        startLoad( asyncObject );
                        break;

                    default:
                        console.warn( "Invalid eAsyncObjectTypes" );
                }

            }
        }
    };

    var startLoad = function( asyncObject ) {
        m_loader.onFileComplete.addOnce( function() { imageLoaded( asyncObject ); }, self );
        if( m_options.debug ) { console.debug( "\t", m_strAssetsDesc, ": Async start load with key", asyncObject.key ); }
        m_startTime = _.now();
        m_loader.start();
    };

    var imageLoaded = function( asyncObject ) {
        var loadTime = _.now() - m_startTime;
        m_totalTime += loadTime;
        if( m_options.debug ) {
            console.debug( "\t", m_strAssetsDesc, ": Image loaded with key " + asyncObject.key +
                ", forMobile: " + asyncObject.forMobile + " ( " + loadTime + " ms )" );
        }
        asyncObject._loaded = true;
        if( _.isFunction( asyncObject.onLoaded ) ) {
            asyncObject.onLoaded();
        }
        loadNext();
    };

    var loadNext = function( msToWaitNextLoad ) {
        if( _.size( m_loadQueue ) > 0 ) {
            msToWaitNextLoad = _.isNumber( msToWaitNextLoad ) ? msToWaitNextLoad : m_options.msToWaitNextLoad;
            var nextElement = m_loadQueue.shift();
            scheduleNextLoad( nextElement, msToWaitNextLoad );
        }
        else {
            if( m_options.debug ) { console.info( m_strAssetsDesc, ": Async load complete. Total load time elapsed " + m_totalTime + " ms" ); }
            m_isLoading = false;
            m_timer = null;
        }
    };

    var scheduleNextLoad = function( nextElement, msToWaitNextLoad ) {

        msToWaitNextLoad = ( _.isNumber( msToWaitNextLoad ) && msToWaitNextLoad <= 0 ) ? 1 : msToWaitNextLoad;
        m_timer = setTimeout( function() {

            if( !m_pauseLoad ) {
                // Previene cargar elementos mientras que la escena no haya sido creada, ya que Phaser tiene
                // un problema si se cargan elementos mientras se están cargando los suyos o está en el método
                // create, ya que sus callbacks de carga de fichero, pueden ser los asociados por la escena
                // o los asociados por el asyncloader, lo que provoca inconsistencias.
                m_autoPauseLoad = m_autoPauseLoad && !m_game.state.created;

                if( !m_autoPauseLoad ) {
                    loadElement( nextElement );
                }
                else {
                    // reintentamos, ya que estamos pausamos (por cambio de escena) y aún no se ha creado la escena actual
                    if( m_options.debug ) {
                        console.debug( "\t", m_strAssetsDesc, ": The async load is paused (auto). Retrying for", MS_TO_RETRY_PAUSED_LOADS, "ms" );
                    }
                    scheduleNextLoad( nextElement, MS_TO_RETRY_PAUSED_LOADS );
                }
            }
            else {
                // reintentamos, ya que estamos pausamos (manualmente). Reintentamos posteriormente
                if( m_options.debug ) {
                    console.debug( "\t", m_strAssetsDesc, ": The async load is paused (manualy). Retrying for", MS_TO_RETRY_PAUSED_LOADS, "ms" );
                }
                scheduleNextLoad( nextElement, MS_TO_RETRY_PAUSED_LOADS );
            }

        }, msToWaitNextLoad );

    };

    var onStateChange = function( /* newState, oldState */ ) {
        if( m_isLoading ) {
            if( m_options.debug ) { console.debug( "\t", m_strAssetsDesc, ": Auto paused by state change" ); }
            m_autoPauseLoad = true;
        }
    };


    if( !isValidGame( game ) ) {
        throw new Error( "El objeto Game es incorrecto." );
    }

    var eAsyncObjectTypes = {
        IMAGE: 0,
        SPRITE_SHEET: 1,
        ATLAS: 2
    };
    Object.freeze( eAsyncObjectTypes );

    var MS_TO_RETRY_PAUSED_LOADS = 1000;

    var self = this;
    var m_game = game;

    var m_timer = null;
    var m_isLoading = false;
    var m_autoPauseLoad = false; // auto pause when onStateChange
    var m_pauseLoad = false; // paused manually
    var m_startTime = -1;
    var m_totalTime = 0;

    var m_loadQueue = [];
    var m_strAssetsDesc = "";

    var m_loader = m_game.load;

    m_game.state.onStateChange.add( onStateChange, this );

}

/**
 * Objeto que se cargará con RFAsyncLoader.
 * @author Lolo
 * @version 0.0.2
 * @constructor
 * @param {string} key El nombre del recurso dentro de la caché de Phaser
 * @param {string} url La ruta hacia el recurso que se quiere cargar
 * @param {function} onLoaded Función que será llamada cuando el recurso haya sido cargado
 * @param {boolean} forMobile Si el recurso se cargará también en móviles o no
 * @param {number} frameWidth Si es un spritesheet, el tamaño en píxeles del ancho del fotograma (necesario solo para carga de animaciones)
 * @param {number} frameHeight Si es un spritesheet, el tamaño en píxeles del alto del fotograma (necesario solo para carga de animaciones)
 * @param {number} totalFrames Si es un spritesheet, el número total de fotogramas de la animación (opcional)
 * @param {xml} xmlData URL con el fichero xml que define el atlas (necesario solo para carga de atlas)
 */
// eslint-disable-next-line
function RFAsyncObject( params ) {
    "use strict";
    this.key = params.key || null;
    this.url = params.url || null;
    this.frameWidth = params.frameWidth || null;
    this.frameHeight = params.frameHeight || null;
    this.totalFrames = params.totalFrames || 0;
    this.xmlData = params.xmlData || null;
    if( params.forMobile === null ) {
        this.forMobile = true;
    }
    else { this.forMobile = params.forMobile; }
    this.onLoaded = params.onLoaded || null;
    this._loaded = false;

    /**
     * Indica si el recurso ya se ha cargado o no.
     * @author Lolo
     * @returns {Boolean} true si ya se ha cargado el recurso, false en caso contrario.
     */
    this.isLoaded = function() {
        return this._loaded;
    };
}

//------------------------------------------------------
// Source: src/engine/libs/rf/rf.loader.webfontloader.js
//------------------------------------------------------

/**
 * Carga de fuentes tipográficas desde el loader de Phaser
 *
 * @example
 * // Carga de una fuente en el preload
 * game.load.webfont('KEY', 'URL');
 *
 * @example
 * // Uso de la fuente dentro de un label
 * game.add.text(0,0,'Coño ya', { font: '120px KEY'});
 *
*/
Phaser.Loader.prototype.webfont = function( key, url ) {
    "use strict";

    var LOAD_TIMEOUT = 10000;
    var file = { key: key, url: url };
    $( "head" ).prepend( "<style type='text/css'>" +
        "@font-face {\n" +
        "\tfont-family: '" + file.key + "';\n" +
        "\tsrc: local('☺'), url('" + file.url + "');\n" +
        "}\n" +
        "\tp.myClass {\n" +
        "\tfont-family: '" + file.key + "' !important;\n" +
        "}\n" +
        "</style>" );
    var self = this;
    var font = new FontFaceObserver( file.key );
    font.load( null, LOAD_TIMEOUT ).then( function() {
        self.asyncComplete( file );
    }, function() {
        self.asyncComplete( file, "Error con " + file.url );
    } );
};

// ------------------------------------------------------------ //
// FONTFACE OBSERVER (¡No tocar!)
// ------------------------------------------------------------ //
/* jshint ignore:start */
/* eslint-disable */
(function(){'use strict';var f,g=[];function l(a){g.push(a);1==g.length&&f()}function m(){for(;g.length;)g[0](),g.shift()}f=function(){setTimeout(m)};function n(a){this.a=p;this.b=void 0;this.f=[];var b=this;try{a(function(a){q(b,a)},function(a){r(b,a)})}catch(c){r(b,c)}}var p=2;function t(a){return new n(function(b,c){c(a)})}function u(a){return new n(function(b){b(a)})}function q(a,b){if(a.a==p){if(b==a)throw new TypeError;var c=!1;try{var d=b&&b.then;if(null!=b&&"object"==typeof b&&"function"==typeof d){d.call(b,function(b){c||q(a,b);c=!0},function(b){c||r(a,b);c=!0});return}}catch(e){c||r(a,e);return}a.a=0;a.b=b;v(a)}}
function r(a,b){if(a.a==p){if(b==a)throw new TypeError;a.a=1;a.b=b;v(a)}}function v(a){l(function(){if(a.a!=p)for(;a.f.length;){var b=a.f.shift(),c=b[0],d=b[1],e=b[2],b=b[3];try{0==a.a?"function"==typeof c?e(c.call(void 0,a.b)):e(a.b):1==a.a&&("function"==typeof d?e(d.call(void 0,a.b)):b(a.b))}catch(h){b(h)}}})}n.prototype.g=function(a){return this.c(void 0,a)};n.prototype.c=function(a,b){var c=this;return new n(function(d,e){c.f.push([a,b,d,e]);v(c)})};
function w(a){return new n(function(b,c){function d(c){return function(d){h[c]=d;e+=1;e==a.length&&b(h)}}var e=0,h=[];0==a.length&&b(h);for(var k=0;k<a.length;k+=1)u(a[k]).c(d(k),c)})}function x(a){return new n(function(b,c){for(var d=0;d<a.length;d+=1)u(a[d]).c(b,c)})};window.Promise||(window.Promise=n,window.Promise.resolve=u,window.Promise.reject=t,window.Promise.race=x,window.Promise.all=w,window.Promise.prototype.then=n.prototype.c,window.Promise.prototype["catch"]=n.prototype.g);}());

(function(){var k=!!document.addEventListener;function l(a,b){k?a.addEventListener("scroll",b,!1):a.attachEvent("scroll",b)}function v(a){document.body?a():k?document.addEventListener("DOMContentLoaded",a):document.attachEvent("onreadystatechange",function(){"interactive"!=document.readyState&&"complete"!=document.readyState||a()})};function w(a){this.a=document.createElement("div");this.a.setAttribute("aria-hidden","true");this.a.appendChild(document.createTextNode(a));this.b=document.createElement("span");this.c=document.createElement("span");this.h=document.createElement("span");this.f=document.createElement("span");this.g=-1;this.b.style.cssText="max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";this.c.style.cssText="max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";
this.f.style.cssText="max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";this.h.style.cssText="display:inline-block;width:200%;height:200%;font-size:16px;max-width:none;";this.b.appendChild(this.h);this.c.appendChild(this.f);this.a.appendChild(this.b);this.a.appendChild(this.c)}
function y(a,b){a.a.style.cssText="max-width:none;min-width:20px;min-height:20px;display:inline-block;overflow:hidden;position:absolute;width:auto;margin:0;padding:0;top:-999px;left:-999px;white-space:nowrap;font:"+b+";"}function z(a){var b=a.a.offsetWidth,c=b+100;a.f.style.width=c+"px";a.c.scrollLeft=c;a.b.scrollLeft=a.b.scrollWidth+100;return a.g!==b?(a.g=b,!0):!1}function A(a,b){function c(){var a=m;z(a)&&null!==a.a.parentNode&&b(a.g)}var m=a;l(a.b,c);l(a.c,c);z(a)};function B(a,b){var c=b||{};this.family=a;this.style=c.style||"normal";this.weight=c.weight||"normal";this.stretch=c.stretch||"normal"}var C=null,D=null,H=!!window.FontFace;function I(){if(null===D){var a=document.createElement("div");try{a.style.font="condensed 100px sans-serif"}catch(b){}D=""!==a.style.font}return D}function J(a,b){return[a.style,a.weight,I()?a.stretch:"","100px",b].join(" ")}
B.prototype.load=function(a,b){var c=this,m=a||"BESbswy",x=b||3E3,E=(new Date).getTime();return new Promise(function(a,b){if(H){var K=new Promise(function(a,b){function e(){(new Date).getTime()-E>=x?b():document.fonts.load(J(c,c.family),m).then(function(c){1<=c.length?a():setTimeout(e,25)},function(){b()})}e()}),L=new Promise(function(a,c){setTimeout(c,x)});Promise.race([L,K]).then(function(){a(c)},function(){b(c)})}else v(function(){function q(){var b;if(b=-1!=f&&-1!=g||-1!=f&&-1!=h||-1!=g&&-1!=
h)(b=f!=g&&f!=h&&g!=h)||(null===C&&(b=/AppleWebKit\/([0-9]+)(?:\.([0-9]+))/.exec(window.navigator.userAgent),C=!!b&&(536>parseInt(b[1],10)||536===parseInt(b[1],10)&&11>=parseInt(b[2],10))),b=C&&(f==r&&g==r&&h==r||f==t&&g==t&&h==t||f==u&&g==u&&h==u)),b=!b;b&&(null!==d.parentNode&&d.parentNode.removeChild(d),clearTimeout(G),a(c))}function F(){if((new Date).getTime()-E>=x)null!==d.parentNode&&d.parentNode.removeChild(d),b(c);else{var a=document.hidden;if(!0===a||void 0===a)f=e.a.offsetWidth,g=n.a.offsetWidth,
h=p.a.offsetWidth,q();G=setTimeout(F,50)}}var e=new w(m),n=new w(m),p=new w(m),f=-1,g=-1,h=-1,r=-1,t=-1,u=-1,d=document.createElement("div"),G=0;d.dir="ltr";y(e,J(c,"sans-serif"));y(n,J(c,"serif"));y(p,J(c,"monospace"));d.appendChild(e.a);d.appendChild(n.a);d.appendChild(p.a);document.body.appendChild(d);r=e.a.offsetWidth;t=n.a.offsetWidth;u=p.a.offsetWidth;F();A(e,function(a){f=a;q()});y(e,J(c,'"'+c.family+'",sans-serif'));A(n,function(a){g=a;q()});y(n,J(c,'"'+c.family+'",serif'));A(p,function(a){h=
a;q()});y(p,J(c,'"'+c.family+'",monospace'))})})};window.FontFaceObserver=B;window.FontFaceObserver.prototype.check=window.FontFaceObserver.prototype.load=B.prototype.load;"undefined"!==typeof module&&(module.exports=window.FontFaceObserver);}());
/* eslint-enable */
/* jshint ignore:end */

//------------------------------------------------------
// Source: src/engine/libs/rf/rf.net.webservice.js
//------------------------------------------------------

/**
 * Módulo de envío asíncrono de peticiones SOAP
 *
 * @example
 *
 *   // Creación de la instancia que se encargará de los envíos
 *   var rfWService = new RFWebServiceModule();
 *
 *   // Creamos el objeto de inicialización para el módulo
 *   var initParams = {
 *       url         : "http://xx.xx.xx.xx:yyyy/server.php",
 *       timeout     : 1000,
 *       supportCors : false
 *   };
 *
 *   // Se inicializa el módulo
 *   rfWService.initialize(initParams);
 *
 *   // Creación de callback para resultados correctos
 *   function fOK( result ) {
 *       console.log( "OK", result );
 *   }
 *
 *   // Creación de callback para resultados incorrectos
 *   function fERROR( result ) {
 *       console.log( "ERROR", result );
 *   }
 *
 *   // Creamos los parámetros de un envío estándar
 *   var params = {
 *       requestMethod : 'start',
 *       requestParams: {
 *           user : 2
 *       }
 *   };
 *
 *   // Realizamos la petición al servidor
 *   rfWService.send( params, fOK, fERROR );
 *
 *
 * @version 0.2.0
 * @constructor
 * @requires jquery.js
 * @requires lodash.js
 * @requires rf.utils.js ( para getJSON )
 *
 * CAMBIOS:
 * 0.1.4 - Optimizados los accesos por lodash
 * 0.1.5 - Se eliminan dependencias externas para la personalización de los colores.
 * 0.1.6 - Se añade el tipo para la petición ajax.
 * 0.2.0 - Se añade el contenteType a las opciones.
 */
// eslint-disable-next-line
function RFWebServiceModule() {

    "use strict";

    // ---============================================================= PUBLIC ===---

    /**
     * Versión del módulo
     * @type {string}
     * @readonly
     */
    this.VERSION = "0.2.0";

    /**
     * Inicializa el uso del módulo. Este método tiene que ser llamado antes de usar
     * cualquier otra función
     * @param {object=} params Parámetros de inicialización.
     * @param {string=} params.url='' Específica la URL para todas las peticiones
     * @param {boolean=} params.supportCors=false Indica si se soporta cors.
     * @param {integer=} params.timeout=10000 Tiempo máximo en ms para esperar respuesta.
     */
    this.initialize = function( initParams ) {
        if( !m_initialized ) {
            m_initialized = true;
            if( _.isObject( initParams ) ) {
                m_instanceName = _.get( initParams, [ "name" ], m_instanceName );
                if( _.has( initParams, [ "url" ] ) ) {
                    this.setURL( initParams.url );
                }
                if( _.has( initParams, [ "supportCors" ] ) ) {
                    this.setSupportCors( initParams.supportCors );
                }
                if( _.has( initParams, [ "timeout" ] ) ) {
                    this.setTimeout( initParams.timeout );
                }
                if( _.has( initParams, [ "contentType" ] ) ) {
                    this.setContentType( initParams.contentType );
                }
                if( _.has( initParams, [ "crossDomain" ] ) ) {
                    this.setCrossDomain( initParams.crossDomain );
                }
            }
        }
    };

    /**
     * Finaliza el uso del módulo
     */
    this.shutdown = function() {
        if( m_initialized ) {
            m_options.url = "";
            m_options.supportCors = false;
            m_initialized = false;
        }
    };

    /**
     * Establece el tráfico 'sniffado' para poder reproducir la secuencia de peticiones en modo offline
     * @param {array} aSniffedTraffic Array de objetos 'sniffados'. Los objetos han de contener "request" y "response" como propiedades
     */
    this.setSniffedTraffic = function( aSniffedTraffic ) {

        var isValid = true;
        if( _.isArray( aSniffedTraffic ) ) {
            for( var i = 0; i < _.size( aSniffedTraffic ) && isValid; ++i ) {
                isValid = _.hasAllKeys( aSniffedTraffic[ i ], [ "request", "response" ] );
            }
        }

        if( isValid ) {
            m_sniffedTraffic = aSniffedTraffic;
            console.debug( "Sniffed traffic setted", m_sniffedTraffic );
        }
        else {
            console.warn( "Invalid sniffed traffic array", aSniffedTraffic );
        }

        return isValid;
    };

    /**
     * Modifica las opciones del módulo, mediante un objeto
     * @param {object=} oOptions opciones a modificar
     * @param {string=} oOptions.url Especifica la URL para todos los envíos genéricos.
     * @param {boolean=} oOptions.supportCors - Especifica si se soporta Cors
     * @param {integer=} oOptions.timeout Especifica el timeout en milisegundos para esperar respuesta en la petición.
     * @param {boolean=} oOptions.debug Traza el envío de peticiones
     * @param {string=} oOptions.sendLabelColor Color de la etiqueta en el envío de peticiones
     * @param {string=} oOptions.sendMessageColor Color del mensaje en el envío de peticiones
     * @throws {Error} Si la URL es incorrecta.
     */
    this.setOptions = function( oOptions ) {
        if( _.has( oOptions, [ "url" ] ) ) {
            this.setURL( oOptions.url );
        }
        if( _.has( oOptions, [ "supportCors" ] ) ) {
            this.setSupportCors( oOptions.supportCors );
        }
        if( _.has( oOptions, [ "timeout" ] ) ) {
            this.setTimeout( oOptions.timeout );
        }
        if( _.has( oOptions, [ "contentType" ] ) ) {
            this.setContentType( oOptions.contentType );
        }
        m_options.offline = _.get( oOptions, [ "offine" ], m_options.offline );
        m_options.debug = _.get( oOptions, [ "debug" ], m_options.debug );
        m_options.sendLabelColor = _.get( oOptions, [ "sendLabelColor" ], m_options.sendLabelColor );
        m_options.sendMessageColor = _.get( oOptions, [ "sendMessageColor" ], m_options.sendMessageColor );
    };

    /**
     * @param {string} url Especifica la URL para todos los envíos genéricos.
     * @throws {Error} Si la URL es incorrecta.
     */
    this.setURL = function( url ) {
        assertInitialized();
        if( isUrlValid( url ) ) {
            m_options.url = url;
        }
        else {
            throw new Error( _ERROR_MESSAGES.INVALID_URL );
        }
    };

    /**
     * @return {string} URL de los envíos genéricos.
     */
    this.getURL = function() {
        assertInitialized();
        return m_options.url;
    };

    /**
     * @param {string} Tipo de petición, "POST", "GET"...
     * @throws {Error} Si no es un string
     */
    this.setType = function( type ) {
        assertInitialized();
        if( _.isString( type ) ) {
            m_options.type = type;
        }
        else {
            throw new Error( _ERROR_MESSAGES.INVALID_TYPE );
        }
    };

    /**
     * @return {string} URL de los envíos genéricos.
     */
    this.getURL = function() {
        assertInitialized();
        return m_options.url;
    };

    /**
     * @param {boolean} bSupport - Especifica si se soporta Cors
     */
    this.setSupportCors = function( bSupport ) {
        assertInitialized();
        if( _.isBoolean( bSupport ) ) {
            m_options.supportCors = bSupport;
        }
    };

    /**
     * @return {boolean} Devuelve si se soporta cors
     */
    this.getSupportCors = function() {
        assertInitialized();
        return m_options.supportCors;
    };

    /**
     * @param {integer} timeout Especifica el timeout en milisegundos para esperar respuesta en la petición.
     */
    this.setTimeout = function( timeout ) {
        assertInitialized();
        if( _.isInteger( timeout ) ) {
            m_options.timeout = timeout;
        }
    };

    /**
     * @return {integer} Devuelve el timeout para esperar respuesta en la petición
     */
    this.getTimeout = function() {
        assertInitialized();
        return m_options.timeout;
    };

    /**
     * @return {object} Devielve una copia de la cabecera establecida para todas las peticiones
     */
    this.getHeaders = function() {
        return _.clone( m_headers );
    };

    /**
     * Establece la pareja clave-valor en el objeto de cabecera
     * @param {string} key La clave a consultar dentro del objeto de cabecera
     * @param {string} value El valor de la cabecera
     */
    this.setHeader = function( key, value ) {
        m_headers[ key ] = value;
    };

    /**
     * @param {string} key La clave a consultar dentro del objeto de cabecera
     * @return {undefined|string} El valor de la clave a consultar. Si no existe, devuelve undefined
     */
    this.getHeader = function( key ) {
        return m_headers[ key ];
    };

    /**
     * @param {string} key La clave a consultar dentro del objeto de cabecera
     * @return {boolean} Si existe o no la clave en el objeto de cabecera
     */
    this.hasHeader = function( key ) {
        return _.has( m_headers, key );
    };

    /**
     * @param {string} el contentType que se usará para las peticiones
     */
    this.setContentType = function( contentType ) {
        m_options.contentType = _.isString( contentType ) ? contentType : m_options.contentType;
    };

    /**
     * @return {string} el contentType que se se está usando para las peticiones
     */
    this.getContentType = function() {
        return m_options.contentType;
    };


    /**
     * @param {string} el crossDomain que se usará para las peticiones
     */
    this.setCrossDomain = function( crossDomain ) {
        m_options.crossDomain = Boolean( crossDomain );
    };

    /**
     * @return {string} el crossDomain que se se está usando para las peticiones
     */
    this.getCrossDomain = function() {
        return m_options.crossDomain;
    };


    /**
     * @param {string} el DataType que se usará para las peticiones
     */
    this.setDataType = function( dataType ) {
        m_options.dataType = _.isString( dataType ) ? dataType : m_options.dataType;
    };

    /**
     * @return {string} el DataType que se se está usando para las peticiones
     */
    this.getDataType = function() {
        return m_options.dataType;
    };


    /**
     * Envía una petición URI genérica. Ver parámetros.
     * Normalmente se enviará el método y los parámetros asociados ( <tt>params.requestMethod</tt> junto con <tt>params.requestParams</tt>),
     * lo que acabará formando automáticamente la petición a enviar.
     * En caso de que se quiera enviar una petición específica, se usará {params.soapRequest}.
     *
     * @param  {object} params Parámetros de la petición.
     *     @param {string=} params.url Url específica para la petición actual.
     *     @param {boolean=} params.supportCors Indica si se soporta cors para la petición actual.
     *     @param {integer=} params.timeout Indica el valor del timeout en milisegundos para la petición actual.
     *     @param {object=} params.headers Headers que se enviarán en la petición actual.
     *     @param {object=} params.requestParams Parámetros de la petición asociada al método <tt>params.requestMethod</tt>.
     *
     * @param  {function=} fCallbackSuccess Función de callback si la petición es correcta. Pasa como parámetro el resultado de la operación.
     * @param  {function=} fCallbackError Función de callback si la petición es incorrecta.Pasa como parámetro el resultado de la operación.
     *
     * @throws {Error} Si la <tt>URL</tt> es incorrecta.
     * @throws {Error} Si <tt>urlRequest</tt> formada es incorrecta.
     * @throws {Error} Si el módulo <tt>RFWebServiceModule</tt> no ha sido inicializado.
     */
    this.sendURLEncoded = function( params, fCallbackSuccess, fCallbackError ) {

        var resultSend;

        if( m_options.offline ) {
            resultSend = sendURLEncodedOffline( params, fCallbackSuccess, fCallbackError );
        }
        else {
            resultSend = sendURLEncodedOnline( params, fCallbackSuccess, fCallbackError );
        }

        return resultSend;
    };


    /**
     * Envía una petición SOAP genérica. Ver parámetros.
     * Normalmente se enviará el método y los parámetros asociados ( <tt>params.requestMethod</tt> junto con <tt>params.requestParams</tt>),
     * lo que acabará formando automáticamente la petición a enviar.
     * En caso de que se quiera enviar una petición específica, se usará {params.soapRequest}.
     *
     * @param  {object} params Parámetros de la petición.
     *     @param {string=} params.url Url específica para la petición actual.
     *     @param {boolean=} params.supportCors Indica si se soporta cors para la petición actual.
     *     @param {integer=} params.timeout Indica el valor del timeout en milisegundos para la petición actual.
     *     @param {string=} params.requestMethod Método que se enviará al servidor junto con los parámetros <tt>params.requestParams</tt>.
     *     @param {object=} params.requestParams Parámetros de la petición asociada al método <tt>params.requestMethod</tt>.
     *     @param {string=} params.soapRequest Si se quiere usar un XML específico de la petición a enviar.
     *                                         Si se define, ignora los valores <tt>params.requestMethod</tt> y <tt>params.requestParams</tt>
     *
     * @param  {function=} fCallbackSuccess Función de callback si la petición es correcta. Pasa como parámetro el resultado de la operación.
     * @param  {function=} fCallbackError Función de callback si la petición es incorrecta.Pasa como parámetro el resultado de la operación.
     *
     * @throws {Error} Si la <tt>URL</tt> es incorrecta.
     * @throws {Error} Si <tt>soapRequest</tt> formada es incorrecta.
     * @throws {Error} Si el módulo <tt>RFWebServiceModule</tt> no ha sido inicializado.
     */
    this.sendSOAP = function( params, fCallbackSuccess, fCallbackError ) {

        assertInitialized();
        var result = false;

        // custom params
        params = _.isObject( params ) ? params : {};
        params.data = makeSoapRequest( params );

        // make the sendParams
        var sendParams = makeSendParams( params );

        // and overwrite it
        if( sendParams.contentType === undefined && sendParams.dataType === undefined ) {
            sendParams.contentType = _.get( sendParams, [ "contentType" ], "text/xml; charset=\"utf-8\"" );
            sendParams.dataType    = _.get( sendParams, [ "dataType" ], "xml" );
        }

        // send
        if( sendParams.url !== "" && sendParams.data !== "" ) {
            sendInternal( sendParams, fCallbackSuccess, fCallbackError );
            result = true;
        }
        else {
            if( sendParams.url === "" ) { throw new Error( _ERROR_MESSAGES.EMPTY_URL ); }
            if( sendParams.data === "" ) { throw new Error( _ERROR_MESSAGES.EMPTY_SOAP_REQUEST ); }
        }

        return result;
    };

    /**
     * Envía una petición genérica. Ver parámetros.
     * Normalmente se enviará el método y los parámetros asociados ( <tt>params.requestMethod</tt> junto con <tt>params.requestParams</tt>),
     * lo que acabará formando automáticamente la petición a enviar.
     * En caso de que se quiera enviar una petición específica, se usará {params.soapRequest}.
     *
     * @param  {object} params Parámetros de la petición.
     *     @param {string=} params.url Url específica para la petición actual.
     *     @param {boolean=} params.supportCors Indica si se soporta cors para la petición actual.
     *     @param {integer=} params.timeout Indica el valor del timeout en milisegundos para la petición actual.
     *     @param {string=} params.data Datos que se enviarán
     *
     * @param  {function=} fCallbackSuccess Función de callback si la petición es correcta. Pasa como parámetro el resultado de la operación.
     * @param  {function=} fCallbackError Función de callback si la petición es incorrecta.Pasa como parámetro el resultado de la operación.
     *
     * @throws {Error} Si la <tt>URL</tt> es incorrecta.
     * @throws {Error} Si el módulo <tt>RFWebServiceModule</tt> no ha sido inicializado.
     */
    this.send = function( params, fCallbackSuccess, fCallbackError ) {

        assertInitialized();
        var result = false;

        params = _.isObject( params ) ? params : {};

        // override the params
        var sendParams = makeSendParams( params );

        if( sendParams.url !== "" ) {
            sendInternal( sendParams, fCallbackSuccess, fCallbackError );
            result = true;
        }
        else {
            throw new Error( _ERROR_MESSAGES.EMPTY_URL );
        }

        return result;
    };


    // ---============================================================= PRIVATE ===---

    var makeSendParams = function( params ) {

        var url         = _.has( params, [ "url" ] ) && isUrlValid( params.url ) ? params.url : m_options.url;
        var crossDomain = _.has( params, [ "crossDomain" ] ) && _.isBoolean( params.crossDomain ) ? params.crossDomain : m_options.crossDomain;
        var headers     = _.get( params, [ "headers" ], m_headers );
        var supportCors = _.has( params, [ "supportCors" ] ) && _.isBoolean( params.supportCors ) ? params.supportCors : m_options.supportCors;
        var timeout     = _.has( params, [ "timeout" ] ) && _.isInteger( params.timeout ) ? params.timeout : m_options.timeout;
        var type        = _.get( params, [ "type" ], m_options.type );
        var contentType = _.get( params, [ "contentType" ], m_options.contentType );
        var dataType    = _.get( params, [ "dataType" ], m_options.dataType );
        var data        = _.get( params, [ "data" ] );

        return {
            url         : url,
            crossDomain : crossDomain,
            supportCors : supportCors,
            headers     : headers,
            timeout     : timeout,
            type        : type,
            contentType : contentType,
            dataType    : dataType,
            data        : data
        };
    };

    var sendInternal = function( sendParams, fCallbackSuccess, fCallbackError ) {

        assertAllKeys( sendParams, [ "type", "url", "crossDomain", "data", "contentType", "dataType", "timeout" ] );

        var seqNumber = m_seqNumber++;
        var startTime = new Date();

        if( m_options.debug ) {
            var instanceTxt = m_instanceName ? m_instanceName + " " : "";
            console.log(
                "%c" + instanceTxt + "SEND (" + seqNumber + ") >> %c" + sendParams.data,
                m_options.sendLabelColor,
                m_options.sendMessageColor,
                sendParams
            );
        }

        var _onSuccess = function() {
            var argsArray = argumentsToArray( arguments );
            logSeqNumberResponse( seqNumber, ( new Date() - startTime ) );
            onSendSuccess( argsArray, fCallbackSuccess );
        };

        var _onError = function() {
            var argsArray = argumentsToArray( arguments );
            logSeqNumberResponse( seqNumber, ( new Date() - startTime ) );
            onSendError( argsArray, fCallbackError );
        };

        $.support.cors = sendParams.supportCors;
        $.ajax( {
            type        : sendParams.type,
            url         : sendParams.url,
            crossDomain : sendParams.crossDomain,
            headers     : sendParams.headers,
            data        : sendParams.data,
            contentType : sendParams.contentType,
            dataType    : sendParams.dataType,
            timeout     : sendParams.timeout,
            success     : _onSuccess,
            error       : _onError
        } );
    };


    var sendURLEncodedOffline = function( params, fCallbackSuccess, fCallbackError ) {

        var result = false;
        if( _.size( m_sniffedTraffic ) > 0 ) {

            var sendElement = m_sniffedTraffic.shift();
            if( sendElement ) {

                // 1 - SEND: dont send anything ( only log )
                if( m_options.debug ) {
                    console.log( "OFFLINE ELEMENT:", sendElement );
                    console.log( "%cSEND (OFFLINE [" + sendElement.seqId + "]) >> %c",
                        m_options.sendLabelColor, m_options.sendMessageColor, sendElement.request );
                }

                // 2 - RECEIVE: simulate asynchronous response
                var delayResponseTime = _.get( sendElement, [ "elapsedTime" ], 1 );
                var startTime = _.now();
                _.delay( function() {

                    logSeqNumberResponse( "OFFLINE [" + sendElement.seqId + "]", _.now() - startTime );

                    var resultType = _.get( sendElement, [ "result" ], "success" );
                    var strJSONResponse = JSON.stringify( sendElement.response );
                    switch( resultType ) {
                        case "success":
                            onSendSuccess( strJSONResponse, fCallbackSuccess );
                            break;
                        case "error":
                            onSendError( strJSONResponse, fCallbackError );
                            break;
                        default:
                            console.warn( "Offline response result type not controlled state:", resultType, "Calling 'success' function" );
                            onSendSuccess( strJSONResponse, fCallbackSuccess );
                            break;
                    }

                }, delayResponseTime );
            }

            result = true;
        }
        else {
            onSendError( "Offline send: Sniffed traffic hasn't elements to send", fCallbackError );
        }

        return result;
    };

    var sendURLEncodedOnline = function( params, fCallbackSuccess, fCallbackError ) {

        assertInitialized();

        var result = false;

        params = _.isObject( params ) ? params : {};

        var urlEncodedRequest = makeURLEncodedRequest( params.requestParams );
        params.data = urlEncodedRequest;

        // override the params
        var sendParams = makeSendParams( params );

        if( sendParams.contentType === undefined && sendParams.dataType === undefined ) {
            sendParams.contentType = _.get( sendParams, [ "contentType" ], "application/x-www-form-urlencoded; charset=UTF-8" );
            sendParams.dataType    = _.get( sendParams, [ "dataType" ], "text" );
        }

        if( m_options.debug ) {
            console.log( "REQUEST PARAMS: ", params.requestParams );
        }

        var startTime = new Date();
        var seqNumber = m_seqNumber;
        var sniffObject = m_options.sniffTraffic ? new RFSniffObject( seqNumber ) : undefined;
        sniffStart( sniffObject, params );

        var _handleResult = function( resultOperation, resultType ) {
            var elapsedTime = new Date() - startTime;
            sniffEnd( sniffObject, resultOperation, resultType, elapsedTime );
            logSeqNumberResponse( seqNumber, elapsedTime );
        };

        var _onSuccess = function( resultOperation ) {
            var argsArray = argumentsToArray( arguments );
            _handleResult( resultOperation, "success" );
            onSendSuccess( argsArray, fCallbackSuccess );
        };

        var _onError = function( resultOperation ) {
            var argsArray = argumentsToArray( arguments );
            _handleResult( resultOperation, "error" );
            onSendError( argsArray, fCallbackError );
        };

        if( sendParams.url !== "" && urlEncodedRequest !== "" ) {
            sendInternal( sendParams, _onSuccess, _onError );
            result = true;
        }
        else {
            if( sendParams.url === "" ) { throw new Error( _ERROR_MESSAGES.EMPTY_URL ); }
            if( urlEncodedRequest === "" ) { throw new Error( _ERROR_MESSAGES.EMPTY_URL_ENCODED_REQUEST ); }
        }


        return result;
    };

    var assertInitialized = function() {
        if( !m_initialized ) {
            throw new Error( _ERROR_MESSAGES.NOT_INITIALIZED );
        }
    };

    /**
     * Método que invoca a la función de callback aplicándole los parámetros de argsArray
     * @param {array} argsArray - Array de argumentos para aplicar en la función de callback
     * @param {function} callback - Función que se llamará pasando el array de argumentos
     */
    var applyFunction = function( argsArray, callback ) {
        if( _.isFunction( callback ) ) {
            argsArray = _.isArray( argsArray ) ? argsArray : [ argsArray ];
            callback.apply( null, argsArray );
        }
    };

    /**
     * Este método se llama ante una petición correcta e invoca el callback pasado.
     * Actualmente no hace nada, pero en un futuro podría anotar estadísticas de peticiones
     * correctas, incorrectas, tiempo por petición...
     * @param {array} argsArray - Array de argumentos con los resultados de operación para aplicar en la función de callback
     * @param {function} callback - Función que se llamará una vez procesada la petición
     */
    var onSendSuccess = applyFunction;

    /**
     * Este método se llama ante una petición incorrecta e invoca el callback pasado.
     * Actualmente no hace nada, pero en un futuro podría anotar estadísticas de peticiones
     * correctas, incorrectas, tiempo por petición...
     * @param {array} argsArray - Array de argumentos con los resultados de operación incorrecta para aplicar en la función de callback
     * @param {function} callback - Función que se llamará una vez procesada la petición
     */
    var onSendError = applyFunction;

    var sniffStart = function( rfSniffObject, request ) {
        if( m_options.sniffTraffic && rfSniffObject instanceof RFSniffObject ) {
            rfSniffObject.request = request;
            m_sniffedTraffic.push( rfSniffObject );
        }
    };

    var sniffEnd = function( rfSniffObject, response, result, elapsedTime ) {
        if( m_options.sniffTraffic && rfSniffObject instanceof RFSniffObject ) {
            rfSniffObject.result = result;
            rfSniffObject.elapsedTime = elapsedTime || rfSniffObject.elapsedTime;
            rfSniffObject.response = getJSON( response ) || response;
        }
    };

    /**
     * Crea un un string con la petición SOAP bien formada. Tiene 2 modos de funcionamiento:
     * <ol><li>Si en los parámetros existe una petición SOAP (soapRequest), se comprueba que sea un XML válido. </li>
     * <li>En caso de que no llegue ninguna petición SOAP, se forma la petición mediante <tt>params.requestMethod</tt> y <tt>params.requestParams</tt></li></ol>
     * @param {object} params Parámetros del envío de la petición SOAP
     *     @param {string=} params.requestMethod Método que se enviará al servidor junto con los parámetros <tt>params.requestParams</tt>.
     *     @param {object=} params.requestParams Parámetros de la petición asociada al método <tt>params.requestMethod</tt>.
     *     @param {string=} params.soapRequest Si se quiere usar un XML específico de la petición a enviar.
     *                                         Si se define, ignora los valores <tt>params.requestMethod</tt> y <tt>params.requestParams</tt>.
     * @return {string} Devuelve la petición SOAP en formato string. En caso de error, devuelve un string vacío.
     */
    var makeSoapRequest = function( params ) {
        var soapRequest = "";
        if( _.has( params, [ "soapRequest" ] ) ) {  // if it's forced custom soapRequest
            if( isValidXML( params.soapRequest ) ) {
                soapRequest = params.soapRequest;
            }
            else {
                console.warn( "params.soapRequest is invalid XML" );
            }
        }
        // soapRequest with method and params
        else if( _.has( params, [ "requestMethod" ] ) && _.isString( params.requestMethod ) ) {
            var xmlParams = makeStringXML( params.requestParams );
            var soapBody = "<" + params.requestMethod + " xmlns=\"http://www.xamarin.com/webservices/\">" + xmlParams + "</" + params.requestMethod + ">";
            soapRequest = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" " +
                "xmlns:soap =\"http://schemas.xmlsoap.org/soap/envelope/\"> <soap:Body>" + soapBody + "</soap:Body></soap:Envelope>";
        }
        else {
            console.warn( "params.method is not defined or is not a string" );
        }
        return soapRequest;
    };

    /**
     * Crea un un string con la petición URL Encoded bien formada.
     * @param {object} params Parámetros del envío de la petición.
     * @return {string} Devuelve la petición URL Encoded en formato string. En caso de error, devuelve un string vacío.
     */
    var makeURLEncodedRequest = function( params ) {
        var urlRequest = "";
        var count = 0;
        _.forEach( params, function( paramValue, paramKey ) {
            paramValue = _.isObject( paramValue ) ? encodeURIComponent( JSON.stringify( paramValue ) ) : encodeURIComponent( paramValue );
            // paramValue = _.isObject( paramValue ) ? JSON.stringify( paramValue ) : paramValue;
            urlRequest += ( count > 0 ? "&" : "" ) + paramKey + "=" + paramValue;
            count++;
        } );
        return urlRequest;
    };

    /**
     * Crea un XML (string) a partir del objeto pasado (sin nodo raiz).
     * @param {object} obj Objeto a representar en XML.
     * @param {integer=} maxDeep=100 Maxima profundidad de iteración en el objeto
     * @param {integer=} _iterationDepthLeft=maxDepth (uso interno) Profundidad restante en la iteración recursiva actual.
     * @return {string} El XML en formato string.
     * @throws Si se alcanza el valor máximo de profundidad.
     */
    var makeStringXML = function( obj, maxDepth, _iterationDepthLeft ) {

        var strXML = "";
        var MAX_DEPTH = 100;

        maxDepth = _.isInteger( maxDepth ) && maxDepth > 0 ? maxDepth : MAX_DEPTH;
        _iterationDepthLeft =  _.isInteger( _iterationDepthLeft ) && _iterationDepthLeft <= maxDepth ? _iterationDepthLeft :  maxDepth;

        if( _iterationDepthLeft > 0 ) {
            _.forEach( obj, function( value, key ) {
                if( _.isObject( value ) ) {
                    value = makeStringXML( value, maxDepth, _iterationDepthLeft - 1 );
                }
                if( _.isArray( obj ) ) {
                    strXML += value;
                }
                else {
                    strXML += "<" + key + ">" + value +  "</" + key + ">";
                }
            } );
        }
        else {
            throw new Error( "makeStringXML: Max deep iteration level reached (" + maxDepth + ")" );
        }

        return strXML;
    };

    /**
     * Valida un xml en formato string.
     * @param {string} stringXML El string que conforma el XML a validar.
     * @return {boolean} Indica si es un XML válido
     */
    var isValidXML = function( stringXML ) {
        var result = false;
        try {
            $.parseXML( stringXML );
            result = true;
        } catch( ex ) { } // eslint-disable-line

        return result;
    };

    /**
     * @param {string} url URL a testear (de momento absoluta)
     * @return {boolean} Indica si es una URL válida
     * @todo Permitir URL relativas
     * @todo Si se pasa "localhost" no funciona. Se soluciona poniendo 127.0.0.1 como host
     */
    var isUrlValid = function( /* url */ ) {
        return true;
        // eslint-disable-next-line
        // return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|([a-z])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|(\x6c\x6f\x63\x61\x6c\x68\x6f\x73\x74)|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
    };

    /**
     * Traza por consola el tiempo de la petición y su número de secuencia
     * @param {integer} seqNumber Número de secuencia de la petición
     * @param {integer} msTime Milisegundos que ha tardado la petición en recibir respuesta
     */
    var logSeqNumberResponse = function( seqNumber, msTime ) {
        if( m_options.debug ) {
            var str = "RECEIVE (" + seqNumber + ") : " + msTime + "ms.";
            if( msTime >= _MS_TO_WARN ) {
                console.warn( str + " Performance warning." );
            }
            else {
                console.log( str );
            }
        }
    };

    // ---============================================================= INTERNAL CLASS ===---
    function RFSniffObject( seqId ) {
        this.seqId = seqId || 0;
        this.request = undefined;
        this.response = undefined;
        this.result = undefined; // "success" or "error"
        this.elapsedTime = 0;
        this.timeStamp = _.now();
    }

    // ---============================================================= ATTRIBUTES ===---
    var self = this;
    var m_initialized = false;
    var m_seqNumber  = 0;

    var m_options = {
        url              : "",
        supportCors      : true,
        type             : "POST",
        contentType      : undefined,
        dataType         : undefined,
        crossDomain      : true,
        timeout          : 10000,
        debug            : true,
        sendLabelColor   : "font-weight:bold; color:green;",
        sendMessageColor : "color:lightgray;",
        offline          :  false,
        sniffTraffic: false /* replaced for dist version */  // only if we are online
    };

    var m_headers = {};
    var m_instanceName;

    var _ERROR_MESSAGES = {
        NOT_INITIALIZED           : "RFWebServiceModule is not initialized",
        EMPTY_SOAP_REQUEST        : "Empty soapRequest",
        EMPTY_URL_ENCODED_REQUEST : "Empty URL encoded request",
        EMPTY_URL                 : "Empty URL",
        INVALID_URL               : "Invalid URL",
        INVALID_TYPE              : "Type must be and string ('POST', 'GET'...)"
    };
    Object.freeze( _ERROR_MESSAGES );

    var _MS_TO_WARN = 1000; // Si las peticiones tardan más de este tiempo, se saca una advertencia.

    var m_sniffedTraffic = [];

    // xxx temporal... solo para facilitar los datos desde consola
    /* commented for dist version: window._RF_sniffedTraffic = m_sniffedTraffic */

    // xxx temporal... carga rápida
    if( m_options.offline ) {
        var url = "js/sniffed.json";
        $.ajax( {
            url: url,
            async: false,
            dataType: "json",
            success: function( response ) {
                self.setSniffedTraffic( response );
            }
        } );
    }
}


//------------------------------------------------------
// Source: src/engine/libs/rf/rf.net.websocket.js
//------------------------------------------------------

/**
 * Módulo de comunicación por websockets usando Stomp y SocksJS
 *
 * @version 0.1.0
 *
 * @constructor
 *
 * @requires lodash.js
 * @requires Stomp.js
 * @requires SockJS.js
 *
 * CAMBIOS:
 * 0.1.0 - Versión inicial
 */

/* globals Stomp SockJS */
// eslint-disable-next-line no-unused-vars
var RFWebSocketModule = function() {
    "use strict";

    var self = this;

    var m_options = {
        debug: true,
        verbose: false,
        allowDuplicateSubscriptions: false,
        stringifyData: true
    };
    this.setOptions = function( oOptions ) {
        _.forEach( oOptions, function( value, keyOption ) {
            self.setOption( keyOption, value );
        } );
        return this;
    };
    this.setOption = function( keyOption, value ) {
        if( _.has( m_options, keyOption ) ) {
            _.set( m_options, keyOption, value );
        }
        setDebugToStompClient();
        return this;
    };
    this.getOptions = function() {
        return _.clone( m_options );
    };
    this.getOption = function( keyOption ) {
        return _.get( m_options, keyOption );
    };

    this.connect = function( serverURL, headers, connectCallback, errorCallback ) {
        if( !m_stompClient ) {

            m_serverURL = serverURL;

            // create the socket and applies to Stomp client
            m_socket = new SockJS( m_serverURL );
            m_socket.onclose = onClose;

            m_stompClient = Stomp.over( m_socket );

            setDebugToStompClient();

            m_stompClient.connect(
                headers,
                function( frame ) {
                    if( m_options.debug ) { console.log( "WS: client connected", frame ); }
                    if( _.isFunction( connectCallback ) ) {
                        connectCallback( frame );
                    }
                },
                function( error ) {
                    reset();
                    if( m_options.debug ) { console.error( "WS connection error", error ); }
                    if( _.isFunction( errorCallback ) ) {
                        errorCallback( error );
                    }
                }
            );
        }
        else {
            console.warn( this.isConnected() ? "Already connected" : "Still connecting..." );
        }
    };

    this.disconnect = function( disconnectCallback ) {
        if( checkConnection() ) {
            m_stompClient.disconnect( function() {
                reset();
                if( m_options.debug ) { console.log( "WS: client disconnect" ); }
                if( _.isFunction( disconnectCallback ) ) {
                    disconnectCallback();
                }
            } );
        }
        else {
            console.warn( "Client not connected" );
        }
    };

    // headers is optional
    this.subscribe = function( destination, callback, headers ) {
        var subscription;
        if( checkConnection() ) {
            if( !hasSubscribed( destination, callback ) ) {
                subscription = subscribeInternal( destination, callback, headers );
            }
            else if( m_options.allowDuplicateSubscriptions ) {
                subscription = subscribeInternal( destination, callback, headers );
            }
            else{
                console.warn( "Already subscribed (", destination, callback, ")" );
            }
        }
        return subscription;
    };

    this.unsubscribe = function( subscription ) {
        var destination = getDestination( subscription );
        if( destination ) {
            if( _.isFunction( _.get( subscription, "unsubscribe" ) ) ) {
                if( m_options.debug ) { console.log( "WS: unsubscribe from", destination, "with this subscription", subscription ); }
                subscription.unsubscribe();
                removeSubscription( subscription );
            }
            else {
                console.error( "WS: Invalid Subscription" );
            }
        }
        else {
            console.warn( "WS: Subscription not found", subscription );
        }
    };

    this.unsubscribeAll = function( subscriptions ) {
        if( subscriptions === undefined ) {
            subscriptions = getAllSubscriptions();
        }
        if( _.isArray( subscriptions ) ) {
            _.forEach( subscriptions, function( subcription ) {
                self.unsubscribe( subcription );
            } );
        }
    };

    this.send = function( destination, headers, data ) {
        if( checkConnection() ) {
            if( m_options.debug ) { console.log( ">>> WS SENDING data to (" + destination + ")", data ); }
            data = m_options.stringifyData ? JSON.stringify( data ) : data;
            m_stompClient.send( destination, headers, data );
        }
    };

    // - sendAll( headers, data ) => broadcast
    // - sendAll( destinations, headers, data ) => destinations: array of destinations
    this.sendAll = function() {

        if( arguments.length >= 2 ) {
            var data, destinations, headers;
            switch( arguments.length ) {
                case 2:
                    destinations = _.keys( m_subscriptions );
                    headers = arguments[ 0 ];
                    data = arguments[ 1 ];
                    break;
                case 3: // eslint-disable-line no-magic-numbers
                    destinations = arguments[ 0 ];
                    headers = arguments[ 1 ];
                    data = arguments[ 2 ];
                    break;
                default:
                    break;
            }

            _.forEach( destinations, function( destination ) {
                self.send( destination, headers, data );
            } );
        }
        else {
            console.error( "WS sendAll: invalid number of arguments" );
        }
    };

    this.isConnected = function() {
        return Boolean( _.get( m_stompClient, "connected" ) );
    };

    this.getServerURL = function() {
        return m_serverURL;
    };

    // headers is optional
    var subscribeInternal = function( destination, callback, headers ) {
        var subscription = m_stompClient.subscribe(
            destination,
            function( data ) {
                if( m_options.debug ) { console.log( "<<< WS RECEIVED data (" + destination + ")", data ); }
                if( _.isFunction( callback ) ) {
                    callback( data );
                }
            },
            headers
        );
        addSubscription( subscription, destination, callback );
        if( m_options.debug ) { console.log( "WS: subscribed to", destination, "with this callback function", callback, "Subscription:", subscription ); }
        return subscription;
    };

    // Helpers ----------------------

    var getAllSubscriptions = function() {
        var subscriptions = [];
        _.forEach( m_subscriptions, function( list ) {
            _.forEach( list, function( obj ) {
                subscriptions.push( obj.subscription );
            } );
        } );
        return subscriptions;
    };


    var hasSubscribed = function( destination, callback ) {
        var list = _.get( m_subscriptions, destination );
        var isSubscribed = _.isObject( _.find( list, function( o ) { return o.listener === callback; } ) );
        return isSubscribed;
    };

    var addSubscription = function( subscription, destination, callback ) {
        if( !hasSubscribed( destination, callback ) ) {
            var list = _.get( m_subscriptions, destination );
            if( list === undefined ) {
                list = [];
                _.set( m_subscriptions, destination, list );
            }
            list.push( { subscription: subscription, listener: callback } );
        }
    };

    var getDestination = function( subscription ) {
        var destination;
        _.forEach( m_subscriptions, function( list, dest ) {
            if( !destination ) {
                var sub = _.find( list, function( o ) { return o.subscription === subscription; } );
                if( _.isObject( sub ) ) {
                    destination = dest;
                }
            }
        } );
        return destination;
    };

    var removeSubscription = function( subscription ) {
        var destination = getDestination( subscription );
        var list = _.get( m_subscriptions, destination );
        var sub = _.find( list, function( o ) { return o.subscription === subscription;  } );
        _.pull( list, sub );
        if( _.size( list ) === 0 ) {
            delete m_subscriptions[ destination ];
        }
    };

    var checkConnection = function() {
        var isConnected = self.isConnected();
        if( !isConnected ) {
            console.error( "Not connected" );
        }
        return isConnected;
    };

    // Helpers -------------------------

    var reset = function() {
        self.unsubscribeAll();
        m_stompClient = undefined;
        m_socket = undefined;
        m_serverURL = undefined;
    };

    var onClose = function() {
        console.error( "WebSocket closed" );
    };

    var setDebugToStompClient = function() {
        if( m_stompClient ) {
            m_stompClient.debug = ( m_options.debug && m_options.verbose ? console.log : false );
        }
    };

    var m_serverURL;
    var m_socket;
    var m_stompClient;
    var m_subscriptions = {

        // Schema of this object
        // destination: [
        //     {
        //         subscription: object,
        //         listener: function
        //     }
        // ]

    };
};


//------------------------------------------------------
// Source: src/engine/libs/rf/rf.plugins.transitions.js
//------------------------------------------------------

/* version 0.0.1 */

Phaser.Plugin.Transition = function( game, parent ) {
    "use strict";
    Phaser.Plugin.call( this, game, parent );
    this.capture = null;
};

Phaser.Plugin.Transition.prototype = Object.create( Phaser.Plugin.prototype );
Phaser.Plugin.Transition.prototype.constructor = Phaser.Plugin.SamplePlugin;

/* Captura la imagen */
Phaser.Plugin.Transition.prototype.screenShot = function( parameters ) {
    "use strict";
    var DEFAULT_QUALITY = 0.2;
    if( ( game.renderer instanceof PIXI.WebGLRenderer ) && ( game.preserveDrawingBuffer === false ) ) {
        console.warn( "No se ha puesto preserveDrawingBuffer a true en el index" );
    }
    this.quality = parameters.quality || DEFAULT_QUALITY;
    this.capture = game.canvas.toDataURL( "image/jpeg", this.quality );
};

/* Devuelve la imagen capturada */
Phaser.Plugin.Transition.prototype.getScreenShot = function() {
    "use strict";
    return this.capture;
};

/* Capure FadeOut */
Phaser.Plugin.Transition.prototype.captureFadeOut = function( parameters ) {
    "use strict";
    if( this.capture !== null ) {
        var img = new Image();

        img.onload = function() {
            var base = new PIXI.BaseTexture( this ),
                texture = new PIXI.Texture( base );
            this.overlay = game.add.sprite( 0, 0, texture );
            this.overlay.setTexture( texture );
            this.overlay.alpha = 1;
            this.time = parameters.time || 1;
            if( typeof parameters.rotation !== "undefined" ) {
                this.overlay.anchor.set( 0.5 );
                this.overlay.x += this.overlay.width / 2;
                this.overlay.y += this.overlay.height / 2;
                var fadeTweenRotation = game.add.tween( this.overlay );
                fadeTweenRotation.to( { angle: parameters.rotation }, this.time * Phaser.Timer.SECOND );
                fadeTweenRotation.start();
            }
            if( typeof parameters.scale !== "undefined" ) {
                var fadeTweenScale = game.add.tween( this.overlay.scale );
                fadeTweenScale.to( { x: parameters.scale, y: parameters.scale }, this.time * Phaser.Timer.SECOND );
                fadeTweenScale.start();
            }
            var fadeTween = game.add.tween( this.overlay );
            fadeTween.to( { alpha: 0 }, this.time * Phaser.Timer.SECOND );
            fadeTween.onComplete.add( function() {
                if( parameters.onComplete ) { parameters.onComplete(); }
                if( parameters.nextScene ) { game.state.start( parameters.nextScene ); }
            }, this );
            fadeTween.start();
        };

        img.src = this.getScreenShot();
    }
    else {
        if( parameters.onComplete ) { parameters.onComplete(); }
        if( parameters.nextScene ) { game.state.start( parameters.nextScene ); }
    }
};

/* FadeOut */
Phaser.Plugin.Transition.prototype.fadeOut = function( parameters ) {
    "use strict";
    if( !parameters.image ) {
        this.crossFadeBitmap = this.game.make.bitmapData( this.game.width, this.game.height );
        this.crossFadeBitmap.rect( 0, 0, this.game.width, this.game.height, parameters.color );
    }
    else {
        this.crossFadeBitmap = parameters.image;
    }
    this.overlay = this.game.add.sprite( 0, 0, this.crossFadeBitmap );
    this.overlay.alpha = 0;
    var fadeTween = this.game.add.tween( this.overlay );
    fadeTween.to( { alpha: 1 }, parameters.time * Phaser.Timer.SECOND );
    fadeTween.onComplete.add( function() {
        if( parameters.onComplete ) { parameters.onComplete(); }
        if( parameters.nextScene ) { game.state.start( parameters.nextScene ); }
    }, this );
    fadeTween.start();
};

/* FadeIn */
Phaser.Plugin.Transition.prototype.fadeIn = function( parameters ) {
    "use strict";
    if( !parameters.image ) {
        this.crossFadeBitmap = this.game.make.bitmapData( this.game.width, this.game.height );
        this.crossFadeBitmap.rect( 0, 0, this.game.width, this.game.height, parameters.color );
    }
    else {
        this.crossFadeBitmap = parameters.image;
    }
    this.overlay = this.game.add.sprite( 0, 0, this.crossFadeBitmap );
    this.overlay.alpha = 1;
    var fadeTween = this.game.add.tween( this.overlay );
    fadeTween.to( { alpha: 0 }, parameters.time * Phaser.Timer.SECOND );
    fadeTween.onComplete.add( function() {
        if( parameters.onComplete ) { parameters.onComplete(); }
        if( parameters.nextScene ) { game.state.start( parameters.nextScene ); }
    }, this );
    fadeTween.start();
};

/* Fade In and zoom */
Phaser.Plugin.Transition.prototype.fadeInZoom = function( parameters ) {
    "use strict";
    this.crossFadeBitmap = parameters.image;
    this.overlay = this.game.add.sprite( game.world.centerX, game.world.centerY, this.crossFadeBitmap );
    this.overlay.anchor.set( 0.5 );
    this.scale = parameters.scale || 2;
    this.overlay.scale.set( this.scale );
    this.overlay.alpha = 1;
    var zoomTween = this.game.add.tween( this.overlay.scale );
    zoomTween.to( { x: 1, y: 1 }, parameters.time * Phaser.Timer.SECOND ).start();
    var fadeTween = this.game.add.tween( this.overlay );
    fadeTween.to( { alpha: 0 }, parameters.time * Phaser.Timer.SECOND );
    fadeTween.onComplete.add( function() {
        if( parameters.onComplete ) { parameters.onComplete(); }
        if( parameters.nextScene ) { game.state.start( parameters.nextScene ); }
    }, this );
    fadeTween.start();
};

/* Fade Out and zoom */
Phaser.Plugin.Transition.prototype.fadeOutZoom = function( parameters ) {
    "use strict";
    this.crossFadeBitmap = parameters.image;
    this.overlay = this.game.add.sprite( game.world.centerX, game.world.centerY, this.crossFadeBitmap );
    this.overlay.anchor.set( 0.5 );
    this.scale = parameters.scale || 2;
    this.overlay.scale.set( 1 );
    this.overlay.alpha = 0;
    var zoomTween = this.game.add.tween( this.overlay.scale );
    zoomTween.to( { x: this.scale, y: this.scale }, parameters.time * Phaser.Timer.SECOND ).start();
    var fadeTween = this.game.add.tween( this.overlay );
    fadeTween.to( { alpha: 1 }, parameters.time * Phaser.Timer.SECOND );
    fadeTween.onComplete.add( function() {
        if( parameters.onComplete ) { parameters.onComplete(); }
        if( parameters.nextScene ) { game.state.start( parameters.nextScene ); }
    }, this );
    fadeTween.start();
};

/* Animation transition */
Phaser.Plugin.Transition.prototype.animation = function( parameters ) {
    "use strict";
    var DEFAULT_FPS = 12;
    if( game.cache.checkImageKey( parameters.animation ) === true ) {
        var overlay = game.add.sprite( 0, 0, parameters.animation );
        var fps = parameters.fps || DEFAULT_FPS;
        var scale = parameters.scale || 1;
        overlay.scale.set( scale );
        var anim = overlay.animations.add( "anm" );
        anim.play( fps, false );
        if( parameters.killOnComplete ) { anim.killOnComplete = true; }
        anim.onComplete.add( function() {
            if( parameters.onComplete ) { parameters.onComplete(); }
        }, this );
    }
    else if( parameters.onComplete ) {
        parameters.onComplete();
    }
};


//------------------------------------------------------
// Source: src/engine/defs/rf.defs.js
//------------------------------------------------------

/**
 * @version 0.1.0
 * CAMBIOS:
* 0.1.0 - Se Añade el enumerado ePlatformIDs que contiene posibles valores de distintas configuraciones de plataformas.
        - Se añade al engine, por que éste también usa de estos valores para hacer distinto código dependiendo de plataforma.
 */

var ePlatformIDs = {
    ONLINE: "ONLINE",
    BARTOP: "BARTOP"
};
Object.freeze( ePlatformIDs );

//------------------------------------------------------
// Source: src/engine/utils/base/rf.utils.base.interface.js
//------------------------------------------------------

/*
 * Constructor that creates a new Interface object for checking a function implements the required methods.
 * @param objectName | String | the instance name of the Interface
 * @param methods | Array | methods that should be implemented by the relevant function
 * @version 0.0.2
 * CAMBIOS:
 * 0.0.2 - Renombra la variable interface para solucionar problema con el grunt de producción.
 */
// eslint-disable-next-line
var Interface = function( objectName, methods ) {
    "use strict";
    // Check that the right amount of arguments are provided
    if( arguments.length !== 2 ) {
        throw new Error( "Interface constructor called with " + arguments.length + "arguments, but expected exactly 2." );
    }

    // Create the public properties
    this.name = objectName;
    this.methods = [];

    // Loop through provided arguments and add them to the 'methods' array
    for( var i = 0, len = methods.length; i < len; i++ ) {
        // Check the method name provided is written as a String
        if( typeof methods[ i ] !== "string" ) {
            throw new Error( "Interface constructor expects method names to be passed in as a string." );
        }

        // If all is as required then add the provided method name to the method array
        this.methods.push( methods[ i ] );
    }
};

/*
 * Adds a static method to the 'Interface' constructor
 * @param object | Object Literal | an object literal containing methods that should be implemented
 */
Interface.ensureImplements = function( object ) {
    "use strict";
    // Check that the right amount of arguments are provided
    if( arguments.length < 2 ) {
        throw new Error( "Interface.ensureImplements was called with " + arguments.length + "arguments, but expected at least 2." );
    }

    // Loop through provided arguments (notice the loop starts at the second argument)
    // We start with the second argument on purpose so we miss the data object (whose methods we are checking exist)
    for( var i = 1, len = arguments.length; i < len; i++ ) {
        // Check the object provided as an argument is an instance of the 'Interface' class
        var iFace = arguments[ i ];
        if( iFace.constructor !== Interface ) {
            throw new Error( "Interface.ensureImplements expects the second argument to be an instance of the 'Interface' constructor." );
        }

        // Otherwise if the provided argument IS an instance of the Interface class then
        // loop through provided arguments (object) and check they implement the required methods
        for( var j = 0, methodsLen = iFace.methods.length; j < methodsLen; j++ ) {

            var method = iFace.methods[ j ];

            // Check method name exists and that it is a function (e.g. test[getTheDate])

            // if false is returned from either check then throw an error
            if( !object[ method ] || typeof object[ method ] !== "function" ) {
                throw new Error( "This Class does not implement the '" + iFace.name + "' interface correctly. The method '" + method + "' was not found." );
            }
        }
    }
};


//------------------------------------------------------
// Source: src/engine/utils/jobs/rf.utils.jobs.ijob.js
//------------------------------------------------------

/**
 * Definición de la interfaz de un Job
 * @interface
 */
// eslint-disable-next-line
var iRFJob = new Interface( "iRFJob", [ "execute", "stop", "onFinishJob" ] );

//------------------------------------------------------
// Source: src/engine/utils/jobs/rf.utils.jobs.iretryjob.js
//------------------------------------------------------

/**
 * Definición de la interfaz de un 'reintentador' de Jobs
 * @interface
 */
// eslint-disable-next-line
var iRFRetryJob = new Interface( "iRFRetryJob", [ "hasToRetryFunction" ] );

//------------------------------------------------------
// Source: src/engine/utils/jobs/rf.utils.jobs.retryjob.js
//------------------------------------------------------

/**
 * Reintentador de Jobs que se encarga de reintentar un job hasta que hasToRetryFunction, indique no se ha de reintentar más veces.
 * Se pueden configurar los tiempos de espera entre intentos fallidos y el número máximo de intentos.
 * @class
 * @implements {iRFRetryJob}
 */
// eslint-disable-next-line no-unused-vars
function RFRetryJob( rfJob ) {
    "use strict";

    this.rfJob = rfJob;  // The implementation on iRFJob
    this.maxRetries = 3;
    this.msToWaitBeforeRetries = 5000;
    this.hasToRetryFunction = _.noop; // Need to be implemented
    this.onSuccess = _.noop;
    this.onError = _.noop;
    this.onRetry = _.noop;
    this.debug = false;

    this.execute = function() {

        Interface.ensureImplements( this, iRFRetryJob );

        if( !isIdleState() ) {
            console.warn( getBaseText(), "invalid status: ", m_state, "you can't execute the job manually several times" );
            return;
        }

        // bind onFinishJob event
        this.rfJob.onFinishJob = onFinishJob;

        Interface.ensureImplements( self.rfJob, iRFJob );
        m_state = STATES.WORKING;

        m_currentNumberRetries = 0;
        executeInternal();
    };

    this.stop = function() {
        if( !isIdleState() ) {
            console.log( getBaseText(), "Stopped job", self.rfJob );
            clearTimeout( m_retryTimer );
            this.rfJob.stop();
            this.rfJob.onFinishJob = undefined;
            m_state = STATES.NONE;
        }
        else {
            console.log( getBaseText(), "already stopped", self.rfJob );
        }
    };

    this.getState = function() {
        return m_state;
    };

    this.getCurrentNumberRetries = function() {
        return m_currentNumberRetries;
    };

    var isIdleState = function() {
        return !( m_state === STATES.WORKING || m_state === STATES.WAITING_TO_RETRY );
    };

    var executeInternal = function() {
        self.rfJob.execute();
    };

    var waitToRetry = function() {
        m_currentNumberRetries++;
        if( m_currentNumberRetries <= self.maxRetries ) {
            m_state = STATES.WAITING_TO_RETRY;
            if( self.msToWaitBeforeRetries > 0 ) {
                if( self.debug ) { console.log( getBaseText(), "waiting", self.msToWaitBeforeRetries, "to retrying rfJob", self.rfJob ); }
                m_retryTimer = setTimeout( retry, self.msToWaitBeforeRetries );
            }
            else {
                retry();
            }
        }
        else {
            m_state = STATES.FINISHED_ERROR;
            if( self.debug ) { console.error( getBaseText(), "max attempts reached (" + self.maxRetries + ") for this rfJob", self.rfJob ); }
            var argsArray = argumentsToArray( arguments );
            getAFunction( self.onError ).apply( null, argsArray );
        }
    };

    var retry = function() {
        m_state = STATES.WORKING;
        if( self.debug ) { console.log( getBaseText(), "retrying (" + m_currentNumberRetries + "/" + self.maxRetries + ") rfJob", self.rfJob ); }
        executeInternal();
        getAFunction( self.onRetry )();
    };

    var onFinishJob = function() {

        if( m_state !== STATES.WORKING ) {
            console.warn( getBaseText(), "invalid state", m_state, "for this job", rfJob );
            debugger;
        }

        if( !_.isFunction( self.hasToRetryFunction ) ) {
            console.warn( getBaseText(), "hasToRetryFunction is not a function" );
            debugger;
            return;
        }

        var argsArray = argumentsToArray( arguments );
        var hasToRetry = Boolean( self.hasToRetryFunction.apply( null, argsArray ) );
        if( hasToRetry ) {
            if( self.debug ) { console.warn( getBaseText(), "not passed the retry function. We need to retry...", rfJob  ); }
            waitToRetry.apply( null, argsArray );
        }
        else {
            if( self.debug ) { console.log( getBaseText(), "passed the retry function.", rfJob  ); }
            m_state = STATES.FINISHED_OK;
            getAFunction( self.onSuccess ).apply( null, argsArray );
        }
    };

    var getBaseText = function() {
        return "RFRetryJob( " + m_retryId + " ): ";
    };

    var STATES = {
        NONE: "NONE",
        WORKING: "WORKING",
        WAITING_TO_RETRY: "WAITING_TO_RETRY",
        FINISHED_OK: "FINISHED_OK",
        FINISHED_ERROR: "FINISHED_ERROR"
    };
    Object.freeze( STATES );

    var m_retryId = _.uniqueId();
    var m_retryTimer;
    var m_state = STATES.NONE;
    var m_currentNumberRetries = 0;
    var self = this;
}

//------------------------------------------------------
// Source: src/engine/utils/jobs/rf.utils.jobs.sendrequest.js
//------------------------------------------------------

/**
 * Job que se encarga del envío de peticiones http.
 * Se necesita inyectar como dependencia el módulo RFWebServiceModule que se usará para el envío de las peticiones.
 * @class
 * @implements {iRFJob}
 */
// eslint-disable-next-line no-unused-vars
function RFSendRequestJob() {

    "use strict";

    var self = this;

    this.name = RFSendRequestJob.name;
    this.http = undefined;  // Dependency: the RFWebServiceModule to use.
    this.request = undefined;

    this.execute = function() {
        send();
    };

    this.stop = _.noop;

    this.onFinishJob = function() {
        console.error( getBaseText(), "you need to overwrite 'onFinishJob' method." );
    };

    var send = function() {
        if( !( self.http instanceof RFWebServiceModule ) ) {
            console.error( getBaseText(), "missing the 'RFWebServiceModule' module to send" );
            return;
        }
        self.http.send( self.request, handleResult, handleResult );
    };

    var handleResult = function() {
        var argsArray = argumentsToArray( arguments );
        self.onFinishJob.apply( null, argsArray );
    };

    var getBaseText = function() {
        return self.name + ": ";
    };

    Interface.ensureImplements( this, iRFJob );
}

//------------------------------------------------------
// Source: src/engine/utils/rf.utils.currency.js
//------------------------------------------------------

/**
 * @version 0.0.2
 * CAMBIOS:
 * 0.0.1 - Se añade Colombia
 * 0.0.2 - Se añade XXX como currency iso (Sin divisa). Se usará para gestionar los créditos
 */
// eslint-disable-next-line
function RFCurrency() {
    "use strict";

    var DEFAULT_LENGTH_OF_WHOLE_PART = 3;

    this.symbol = "";
    this.currencyISO = "";
    this.numberOfDecimals = 0;
    this.template = "{0}{1}{2}"; // 0: value , 1: separator between value and symbol , 2: currency symbol
    this.valueSymbolSeparator = "";
    this.decimalSeparator = ",";
    this.thousandSeparator = ".";

    this.format = function( value, valueSymbolSeparator ) {
        valueSymbolSeparator = _.isString( valueSymbolSeparator ) ? valueSymbolSeparator : this.valueSymbolSeparator;
        value = value.format( this.numberOfDecimals, DEFAULT_LENGTH_OF_WHOLE_PART, this.decimalSeparator, this.thousandSeparator );
        return String.format( this.template, value, valueSymbolSeparator, this.symbol );
    };
}

// eslint-disable-next-line
var RFCurrencyUtils = {
    getRFCurrency : function( currencyISO ) {
        "use strict";

        var CURRENCYISO_STR_LENGTH = 3;

        if( !_.isString( currencyISO ) ) { throw new Error( "Invalid currencyISO: " + currencyISO ); }
        currencyISO = _.trim( _.toUpper( currencyISO ) );
        if( _.size( currencyISO ) !== CURRENCYISO_STR_LENGTH ) { throw new Error( "Invalid currencyISO: " + currencyISO ); }

        var rfCurrency = new RFCurrency();

        // common defaults values
        rfCurrency.currencyISO = currencyISO;
        rfCurrency.numberOfDecimals = 2;
        rfCurrency.decimalSeparator = ",";
        rfCurrency.thousandSeparator = ".";
        rfCurrency.template = "{0}{1}{2}";
        switch( currencyISO ) {
            case "EUR":
                rfCurrency.symbol = "€";
                break;
            case "GBP":
                rfCurrency.symbol = "£";
                break;
            case "USD":
            case "MXN":
            case "COP":
                rfCurrency.symbol = "$";
                rfCurrency.decimalSeparator = ".";
                rfCurrency.thousandSeparator = ",";
                rfCurrency.template = "{2}{1}{0}";
                break;
            case "XXX":
                rfCurrency.numberOfDecimals = 0;
                break;
            default:
                rfCurrency.symbol = currencyISO;
                break;
        }
        return rfCurrency;
    },

    decimalSeparator : ".", // only to format function
    thousandSeparator: ",", // only to format function

    // TODO: Fix this
    // eslint-disable-next-line max-params
    format : function( value, currencyISO, valueSymbolSeparator, decimalSeparator, thousandSeparator ) {
        "use strict";
        decimalSeparator = decimalSeparator || this.decimalSeparator;
        thousandSeparator = thousandSeparator || this.thousandSeparator;
        var rfCurrency = this.getRFCurrency( currencyISO );

        // override separator
        rfCurrency.decimalSeparator = decimalSeparator;
        rfCurrency.thousandSeparator = thousandSeparator;
        return rfCurrency.format( value, valueSymbolSeparator );
    }
};



//------------------------------------------------------
// Source: src/engine/utils/rf.utils.debug.js
//------------------------------------------------------

/**
 * @version 0.0.1
 * CAMBIOS:
 * 0.0.1 - Se agregan colores para debug de reels.
 */

// eslint-disable-next-line
function RFDebug() {
    "use strict";

    var self = this;

    this.consoleColors = {
        REELS: "background:#b9810d; color:#000000; padding: 0.5em;",
        ERROR: "background:#800000; color:#ffffff; padding: 0.5em;",
        ROUND: "background:#0080c0; color:#ffffff; padding: 1em; font-size: 24;",
        INFO: "background:#008080; color:#80ffff; padding: 1.8em; font-size: 24; border-radius: 0.25em;",
        INFO_GAMECONFIG: "background: #6666ff; color:#ffffff; padding: 0.25em; font-size: 24; border-radius: 0.25em;",
        INFO_ENGINE: "background: #00cc99; color:#99ffcc; padding: 0.25em; font-size: 24; border-radius: 0.25em;",
        INFO_SLO2: "background: #00cc66; color:#ccffcc; padding: 0.25em; font-size: 24; border-radius: 0.25em;",
        INFO_PLATFORM: "background: #9999ff; color:#ffffff; padding: 0.25em; font-size: 24; border-radius: 0.25em;",
        INFO_GAME_VERSION: "background: #cc0066; color:#ffcccc; padding: 0.35em; font-size: 24; border-radius: 0.25em;",
        OVERWRITED: "background: #9933ff; color:#ffffff; padding: 0.5em; font-size: 24; border-radius: 0.25em;",
        WARNING: "background:#ffff00; color:#bf6000; padding: 0.25em; font-size: 24; border-radius: 0.25em;"
    };

    this.getConsoleColors = function( ) {
        return self.consoleColors;
    };
}

// eslint-disable-next-line
var RFDebugUtils = new RFDebug();



//------------------------------------------------------
// Source: src/engine/utils/rf.utils.js
//------------------------------------------------------

/**
 * @version 0.2.3
 * @required lodash.js
 * CAMBiOS:
 * 0.2.3 - Evita que el método "format" de la clase String falle en caso de no recibir un string
 */

/**
 * Indica si existen en el objeto todas las claves pasadas en el array
 * @param {object} obj objecto a comprobar
 * @param {array} requiredKeys lista de claves a chequear
 * @return {boolean} True si están todas las claves. False en caso contrario.
 * @required lodash.js
 */
_.mixin( {
    "hasAllKeys": function( obj, requiredKeys ) {
        "use strict";
        return _.every( requiredKeys, _.partial( _.has, obj ) );
    }
} );

/**
 * Devuelve un array con las claves de un objeto, ordenadas en orden ascendente
 * @param {object} obj objecto
 * @return {array} Claves del objeto ordenadas en orden ascendente.
 * @required lodash.js
 */
_.mixin( {
    "keysOrdered": function( obj ) {
        "use strict";
        return _.sortBy( _.keys( obj ) );
    }
} );

/**
 * Devuelve true si el valor pasado, se encuentra en el objeto, dentro de alguna de sus claves
 * @param {object} obj objecto en el que buscar
 * @param {object|number|string} Valor a buscar
 * @return {boolean} true si el valor pasado, se encuentra en el objeto.
 * @example
 * _.hasValue( {a:1, b:"hola"}, "hola" ); // → true
 * _.hasValue( {a:1, b:"hola"}, "Hola" ); // → false
 */
_.mixin( {
    "hasValue": function( obj, value ) {
        "use strict";
        return( _.indexOf( _.values(  obj ),  value ) >= 0 );
    }
} );

/**
 * Devuelve true si el valor pasado, está en minúsculas.
 * @param {string} str cadena a comprobar
 * @return {boolean} true si el valor pasado, está en minúsculas.
 */
_.mixin( {
    "isLowerCase": function( str ) {
        "use strict";
        return( _.isString( str ) && str.toLowerCase() === str );
    }
} );

/**
 * Devuelve true si el valor pasado, está en mayúsculas.
 * @param {string} str cadena a comprobar
 * @return {boolean} true si el valor pasado, está en mayúsculas.
 */
_.mixin( {
    "isUpperCase": function( str ) {
        "use strict";
        return( _.isString( str ) && str.toUpperCase() === str );
    }
} );

/**
 * Devuelve un valor por defecto en caso de que no sea un numérico.
 * Si lo es, devuelve el mismo valor. Útil para usarlo en inicializaciones.
 * @param {integer} value valor numérico a comprobar
 * @return {integer|defaultValue} Devuelve el entero el valor por defecto en caso de que no sea un número.
 */
_.mixin( {
    "getNumber": function( value, defaultValue ) {
        "use strict";
        return( _.isNumber( value ) ? value : defaultValue );
    }
} );

/**
 * Devuelve un valor por defecto en caso de que no sea un booleano.
 * Si lo es, devuelve el mismo valor. Útil para usarlo en inicializaciones.
 * @param {boolean} value valor numérico a comprobar
 * @return {boolean|defaultValue} Devuelve el valor por defecto en caso de que no sea un booleano.
 */
_.mixin( {
    "getBoolean": function( value, defaultValue ) {
        "use strict";
        return( _.isBoolean( value ) ? value : defaultValue );
    }
} );

/**
 * Método que devuelve todos los parámetros de la url
 */
// eslint-disable-next-line
var QueryParameters = ( function() {
    "use strict";
    var result = {};

    if ( window.location.search ) {
        // split up the query string and store in an associative array
        var params = window.location.search.slice( 1 ).split( "&" );
        for ( var i = 0; i < params.length; i++ ) {
            var tmp = params[i].split( "=" );
            result[tmp[0]] = unescape( tmp[1] );
        }
    }

    return result;
}() );


/**
 * Se añade el método format a la clase String
 */
if ( !String.format ) {
    String.format = function( strToFormat ) {
        "use strict";
        if( _.isString( strToFormat ) ) {
            var args = Array.prototype.slice.call( arguments, 1 );
            return strToFormat.replace( /{(\d+)}/g, function( match, number ) {
                return typeof args[ number ] !== "undefined" ? args[ number ] : match;
            } );
        }
        console.error( "String.format: this is not a string", strToFormat );
        return strToFormat;
    };
}

/**
 * Indica si el dispositivo es móvil o no.
 * Conviene no usar el userAgent para saber si es móvil o no. No existe una técnica clara, pero de momento usaremos esta:
 */
// eslint-disable-next-line no-implicit-globals, no-unused-vars
function isMobile() {
    "use strict";
    // eslint-disable-next-line
    return ( /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4)) );
}

/**
 * Number.prototype.format(n, x, s, c)
 *
 * @param integer n: length of decimal
 * @param integer x: length of whole part
 * @param mixed   s: sections delimiter
 * @param mixed   c: decimal delimiter
 */
/* eslint-disable */
Number.prototype.format = function( n, x, s, c ) {
    "use strict";
    var re = "\\d(?=(\\d{" + (x || 3) + "})+" + (n > 0 ? "\\D" : "$") + ")",
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace(".", c) : num).replace(new RegExp(re, "g"), "$&" + (s || ","));
};
/* eslint-enable */

/**
 * Chequea si el storage indicado existe o no
 */
// eslint-disable-next-line no-implicit-globals, no-unused-vars
function storageAvailable( type ) {
    "use strict";
    try {
        var storage = window[type];
        var varTest = "__storage_test__";
        storage.setItem( varTest, varTest );
        storage.removeItem( varTest );
        return true;
    }
    catch( ex ) {
        return false;
    }
}

/**
 * Indica si es un objeto de tipo Phaser.Game
 */
// eslint-disable-next-line no-implicit-globals, no-unused-vars
function isValidGame( game ) {
    "use strict";
    return game instanceof Phaser.Game;
}

/**
 * Comprueba si exiten todas las claves pasadas. En caso de que no exista, lanza una excepción
 * @param {object} objToCheck objecto a chequear
 * @param {array} aRequiredKeys Las claves que tienen que estar dentro del objecto.
 * @example
 */
// eslint-disable-next-line no-implicit-globals, no-unused-vars
function assertAllKeys( objToCheck, aRequiredKeys ) {
    "use strict";
    if( !_.hasAllKeys( objToCheck, aRequiredKeys ) ) {
        if( _.isObject( objToCheck ) && _.isArray( aRequiredKeys ) ) {
            var objKeys = _.keys( objToCheck );
            throw new Error( "assertAllKeys failed. Missing keys [" + _.difference( aRequiredKeys, objKeys ).toString() +
                "]. Keys object to check (" + objKeys.toString() + "). Necesary Keys (" + aRequiredKeys.toString() + ")" );
        }
        else {
            throw new Error( "assertAllKeys failed." );
        }
    }
}

/**
 * Devuelve un JSON si puede parsearlo o "false" en caso contrario. Útil para inicializaciones con el operador OR
 * @params {string} string a chequear
 * @params {object|false}
 */
// eslint-disable-next-line no-implicit-globals, no-unused-vars
function getJSON( str ) {
    "use strict";
    var json = false;
    try { json = JSON.parse( str ); }
    catch( e ) { } // eslint-disable-line
    return json;
}

/**
 * Indica si el string pasado es un JSON o no
 * @params {string} string a chequear
 * @return {boolean}
 */
// eslint-disable-next-line no-implicit-globals, no-unused-vars
function isJSON( str ) {
    "use strict";
    return ( getJSON( str ) !== false );
}

/**
 * Retorna la función pasada o una función vacía en caso de que el argumento pasado no sea una función.
 * Esto permite realizar invocaciones a funciones sin temor a que no sean funciones.
 * @param {*} aFunction
 * @returns Una función
 */
// eslint-disable-next-line no-implicit-globals, no-unused-vars
function getAFunction( aFunction ) {
    "use strict";
    return _.isFunction( aFunction ) ? aFunction : _.noop;
}

/**
 * @param {Arguments} args Los argumentos que se quieran convertir.
 * Convierte los argumentos pasados en un array de argumentos (recordemos que los argumentos no es un Array)
 */
// eslint-disable-next-line no-implicit-globals, no-unused-vars
function argumentsToArray( args ) {
    "use strict";
    return ( args.length === 1 ? [ args[ 0 ] ] : Array.apply( null, args ) );
}

/**
 * Asocia la clave del escena a la instancia de escena creada.
 * Se ha de llamar antes de iniciar la primera escena, posiblemente en el index.html
 * @param {object} game Phaser.game instance
 */
// eslint-disable-next-line no-implicit-globals, no-unused-vars
function addSceneKeysToSceneInstances( game ) {
    "use strict";
    if( isValidGame( game ) ) {
        _.forEach( game.state.states, function( sceneInstance, sceneKey ) {
            if( sceneInstance ) {
                sceneInstance.myScene = sceneKey;
            }
        } );
    }
}

/**
 * Asocia la clave del escena a la instancia de escena creada.
 * Se ha de llamar antes de iniciar la primera escena, posiblemente en el index.html
 * @param {object} game Phaser.game instance
 */
// eslint-disable-next-line no-implicit-globals, no-unused-vars
function existsSceneKeyInGame( game, sceneKey ) {
    "use strict";
    if( !isValidGame( game ) ) { return false; }

    return _.indexOf( _.keys( game.state.states ), sceneKey ) >= 0;
}

//------------------------------------------------------
// Source: src/engine/utils/rf.utils.persists.js
//------------------------------------------------------

/**
 * @version 0.0.1
 * @requires lodash.js
 * @requires LZString.js
 * CAMBIOS:
 * 0.0.1 - Versión inicial
 */

/**
 * obj = {
 *   key: "clave1", // localStorageKey
 *   value: {object|string|number}
 * }
 */
// eslint-disable-next-line no-implicit-globals, no-unused-vars
function RFPersists( obj, autoSave ) {

    "use strict";

    this.getKey = function() {
        return m_obj.key;
    };

    this.get = function() {
        return this.getValue();
    };

    this.getValue = function() {
        return m_obj.value;
    };

    this.setValue = function( value ) {
        m_obj.value = value;
        if( m_autoSave ) {
            this.save();
        }
        return this;
    };

    this.setAutoSave = function( bValue ) {
        m_autoSave = _.isBoolean( bValue ) ? bValue : false;
        return this;
    };

    this.setCompress = function( bValue ) {
        m_compress = _.isBoolean( bValue ) ? bValue : false;
        return this;
    };

    this.save = function() {
        try {
            var processedValue = JSON.stringify( obj.value );
            if( m_compress ) {
                processedValue = LZString.compress( processedValue );
            }
            localStorage.setItem( obj.key, processedValue );
        }
        catch( ex ) {
            console.error( "RFPersists Error. Failed to save", obj, ex );
        }
        return this;
    };

    this.load = function() {
        try {
            var processedValue;
            if( m_compress ) {
                processedValue = JSON.parse( LZString.decompress( localStorage.getItem( obj.key ) ) );
            }
            else {
                JSON.parse( localStorage.getItem( obj.key ) );
            }
            obj.value = processedValue;
        }
        catch( ex ) {
            console.error( "RFPersists Error. Failed to load from localStorage", obj, ex );
        }
        return this;
    };

    this.remove = function() {
        try {
            obj.value = undefined;
            localStorage.removeItem( obj.key );
        }
        catch( ex ) {
            console.error( "RFPersists Error. Failed to remove from localStorage", obj, ex );
        }
        return this;
    };

    // ----------------------

    var m_obj = obj;
    var m_autoSave = false;
    var m_compress = true;

    assertAllKeys( obj, [ "key", "value" ] );
    this.setAutoSave( autoSave );

    return this;
}

//------------------------------------------------------
// Source: src/engine/utils/rf.utils.random.js
//------------------------------------------------------

/**
 * Abstraction of a random element. This item will contain a reference to the creating object and
 * the weight of this element in the random algorithm.
 */
function RFWeightedElement( object, weight ) {
    "use strict";

    this.object = object;
    this.weight = weight;
}

/**
 * Encapsulates the generation of random elements using weights. You can add elements to the pool with an associated weight,
 * this weight will determine the probability of that element to be selected from the pool.
 */
// eslint-disable-next-line
function RFWeightedRandom() {
    "use strict";

    var self = this;
    self.elements = undefined;
    self.randomGenerationList = undefined;
    self.sumOfWeights = undefined;

    this.initialize = function() {
        self.elements = [];
        self.randomGenerationList = [];
        self.sumOfWeights = 0;
    };

    /**
     * Sets the pool elements for the random generation.
     * @param {Array<RFWeightedElement>} elements The weighted elements to be added to the pool.
     */
    this.setElements = function( elements ) {
        self.elements = elements;
        self.generateRandomGenerationList();
    };

    /**
     * Retrieve the next random generated entry.
     * @param {Number} count The ammount of random entries to retrieve.
     * @returns {Array<RFWeightedElement> || RFWeightedElement} The random element/s generated.
     */
    this.next = function( count ) {
        if( count === undefined || count < 1 ) {
            count = 1;
        }

        var selectedElements = [];
        for( var countI = 0; countI < count; countI++ ) {
            var random = ( Math.random() * ( self.sumOfWeights - 1 ) ) + 1;
            var shuffledList = _.shuffle( self.randomGenerationList );
            for( var i = 0; i < shuffledList.length; i++ ) {
                random -= shuffledList[ i ].weight;

                if( random <= 0 ) {
                    selectedElements.push( shuffledList[ i ] );
                    break;
                }
            }
        }

        if( count === 1 ) {
            return selectedElements[ 0 ];
        }

        return selectedElements;
    };

    /**
     * Generates the random selection pool.
     */
    this.generateRandomGenerationList = function() {
        self.randomGenerationList = [];

        for( var i = 0; i < self.elements.length; i++ ) {
            if( self.elements[ i ] instanceof RFWeightedElement ) {
                self.sumOfWeights += self.elements[ i ].weight;

                for( var j = 0; j < self.elements[ i ].weight; j++ ) {
                    self.randomGenerationList.push( self.elements[ i ] );
                }
            }
            else {
                console.error( "%c The WeightedRandom needs all elements to be WeightedElements." +
                    " One or more of the elements in the list are not WeightedElements", RFDebugUtils.getConsoleColors().ERROR );
            }
        }
    };

    self.initialize();
}

//------------------------------------------------------
// Source: src/engine/utils/rf.utils.watchdog.js
//------------------------------------------------------

/* jshint -W087 */ // Ignore debugger
/**
 * Módulo que realizará llamadas a la función a vigilar.
 * En caso de fallar y pasar el número de intentos, se llamará a la función de Fail y comenzará a reintentar.
 *
 * La función a vigilar ha de recibir 3 parámetros:
 *   1) objecto con parámetros
 *   2) Callback si la respuesta es correcta
 *   3) Callback si la respuesta es incorrecta
 *
 * @requires lodash.js
 * @version 0.0.2
 * CAMBIOS
 * 0.0.2 - Optimizados los accesos por lodash
 *
 */
// eslint-disable-next-line
var eWatchdogStates = {
    UNKNOWN : 0,
    OK      : 1,
    ERROR   : 0
};
Object.freeze( eWatchdogStates );

// eslint-disable-next-line
function RFWatchdog() {
    "use strict";

    /**
     * Options to this module
     */
    var m_options = {
        msToPool                 : 1000,
        maxAttempts              : 5,
        retryOnFail              : true,
        watchFunction            : null,  // must be defined with 3 arguments like: watchFunction ( oParameters, onOkCallback, onErrorCallback )
        watchFunctionOParameters : {},
        onErrorCallback          : null,  // on first Fail.
        onOkCallback             : null   // on first OK. If fails, on next Ok if we retry.
    };
    this.setOptions = function( oOptions ) {
        m_options.msToPool                 = _.get( oOptions, [ "msToPool" ],                 m_options.msToPool );
        m_options.maxAttempts              = _.get( oOptions, [ "maxAttempts" ],              m_options.maxAttempts );
        m_options.retryOnFail              = _.get( oOptions, [ "retryOnFail" ],              m_options.retryOnFail );
        m_options.watchFunction            = _.get( oOptions, [ "watchFunction" ],            m_options.watchFunction );
        m_options.watchFunctionOParameters = _.get( oOptions, [ "watchFunctionOParameters" ], m_options.watchFunctionOParameters );
        m_options.onErrorCallback          = _.get( oOptions, [ "onErrorCallback" ],          m_options.onErrorCallback );
        m_options.onOkCallback             = _.get( oOptions, [ "onOkCallback" ],             m_options.onOkCallback );
    };

    /**
     * Start pooling
     */
    this.start = function( bCallWatchFunctionImmediately ) {
        if( !m_isPooling ) {
            m_isPooling = true;

            // First call
            if( bCallWatchFunctionImmediately ) {
                callWatchFunction();
            }

            // Next
            m_timer = setInterval( function() { callWatchFunction(); }, m_options.msToPool );
        }
    };

    /**
     * Stop wathdog pooling
     */
    this.stop = function() {
        if( m_isPooling ) {

            m_isPooling = false;
            m_retrying = false;
            m_attempts = m_options.maxAttempts;
            m_wdState = eWatchdogStates.UNKNOWN;

            clearInterval( m_timer );
            m_timer = null;
        }
    };

    /**
     * Is pooling
     */
    this.isPooling = function() {
        return m_isPooling;
    };

    /**
     * Indicates what is the connection state of the module
     */
    this.getWatchdogState = function() {
        return m_wdState;
    };

    var callWatchFunction = function() {
        if( _.isFunction( m_options.watchFunction ) ) {
            m_options.watchFunction( m_options.watchFunctionOParameters, onOkCallback, onErrorCallback );
        }
        else {
            throw new Error( "RFWatchdog: Invalid watchFunction" );
        }
    };

    var onOkCallback = function() {
        if( m_isPooling ) {
            m_retrying = false;
            m_attempts = m_options.maxAttempts;

            if( m_wdState !== eWatchdogStates.OK ) {
                if( _.isFunction( m_options.onOkCallback ) ) {
                    _.delay( m_options.onOkCallback, 1 ); // call asyncronously
                }
                m_wdState = eWatchdogStates.OK;
            }

        }
    };

    var onErrorCallback = function() {
        if( m_isPooling ) {

            if( !m_retrying ) {
                if( m_options.retryOnFail ) {
                    m_retrying = true;
                }
                else {
                    self.stop();
                    return;
                }

            }

            m_attempts--;
            if( m_attempts < 0 ) {

                m_attempts = 0;
                m_wdState = eWatchdogStates.ERROR;

                if( _.isFunction( m_options.onErrorCallback ) ) {
                    _.delay( m_options.onErrorCallback, 1 );  // call asyncronously
                }

                clearInterval( m_timer );
                m_timer = null;

            }
        }
    };

    var self = this;
    var m_isPooling = false;
    var m_retrying = false;
    var m_timer = null;
    var m_attempts = m_options.maxAttempts;
    var m_wdState = eWatchdogStates.UNKNOWN;
}

//------------------------------------------------------
// Source: src/engine/components/phaser-plugin-behavior.min.js
//------------------------------------------------------

/* jshint ignore:start */
/* eslint-disable */
!function (e) { if ("object" == typeof exports && "undefined" != typeof module) module.exports = e(); else if ("function" == typeof define && define.amd) define([], e); else { var t; t = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this, t.BehaviorPlugin = e() } }(function () { return function e(t, r, i) { function n(s, h) { if (!r[s]) { if (!t[s]) { var a = "function" == typeof require && require; if (!h && a) return a(s, !0); if (o) return o(s, !0); var c = new Error("Cannot find module '" + s + "'"); throw c.code = "MODULE_NOT_FOUND", c } var u = r[s] = { exports: {} }; t[s][0].call(u.exports, function (e) { var r = t[s][1][e]; return n(r ? r : e) }, u, u.exports, e, t, r, i) } return r[s].exports } for (var o = "function" == typeof require && require, s = 0; s < i.length; s++) n(i[s]); return n }({ 1: [function (e, t, r) { "use strict"; var i = e("behavior-system"), n = function (e, t) { Phaser.Plugin.call(this, e, t), this._system = new i }; n.prototype = Object.create(Phaser.Plugin), n.prototype.enable = function (e) { this._system.enable(e) }, n.prototype.disable = function (e) { this._system.disable(e) }, n.prototype.preUpdate = function () { this._system.processAll("preUpdate") }, n.prototype.update = function () { this._system.processAll("update") }, n.prototype.render = function () { this._system.processAll("render") }, n.prototype.postRender = function () { this._system.processAll("postRender") }, window.Phaser && (window.Phaser.Plugin.Behavior = n), t.exports = n }, { "behavior-system": 3 }], 2: [function (e, t, r) { function i(e) { this._constructor(e) } var n, o = e("./helper/extend"), s = e("./helper/recycler"); i.prototype = { _constructor: function (e) { return this.gameObject = e, this._behaviorList = new s, this._behaviorMap = {}, this }, set: function (e, t, r) { if (this.has(e)) throw new Error('"' + e + '" already in use'); if (t === n) throw new Error("invalid behavior"); var i = Object.create(t), s = o({}, t.options); return i.options = o(s, r), i.options.$key = e, this._behaviorMap[e] = this._behaviorList.getNextIndex(), this._behaviorList.push(i), i.create !== n && i.create(this.gameObject, i.options), t }, remove: function (e) { var t = this._behaviorMap[e]; if (t >= 0) { var r = this._behaviorList.iterable[t]; r.destroy !== n && r.destroy(this.gameObject, r.options), this._behaviorMap[e] = n, this._behaviorList.remove(t) } }, has: function (e) { return this._behaviorMap[e] !== n }, get: function (e) { var t = this._behaviorMap[e]; return t >= 0 ? this._behaviorList.iterable[t] : n }, process: function (e) { for (var t = 0, r = this._behaviorList.iterable, i = r.length; i > t; t++) { var o = r[t]; o !== n && "function" == typeof o[e] && o[e](this.gameObject, o.options) } return this } }, t.exports = i }, { "./helper/extend": 4, "./helper/recycler": 5 }], 3: [function (e, t, r) { function i() { this._constructor() } var n, o = e("./behavior-container"), s = e("./helper/recycler"); i.prototype = { _constructor: function () { return this._children = new s, this }, enable: function (e) { if (e.behaviors === n) { var t = new o(e), r = this._children.getNextIndex(); t.id = r, this._children.push(t), e.behaviors = t } return e }, disable: function (e) { return e.behaviors !== n && (this._children.remove(e.behaviors.id), e.behaviors = n), e }, processAll: function (e) { for (var t = 0, r = this._children.iterable, i = r.length; i > t; t++) { var o = r[t]; o !== n && o.process(e) } } }, t.exports = i }, { "./behavior-container": 2, "./helper/recycler": 5 }], 4: [function (e, t, r) { function i(e, t) { for (var r in t) t.hasOwnProperty(r) && (e[r] = t[r]); return e } t.exports = i }, {}], 5: [function (e, t, r) { function i(e) { this._constructor(e) } var n; i.prototype = { _constructor: function (e) { return this.iterable = Array.isArray(e) ? e.slice() : [], this._recycledIndexes = [], this }, push: function (e) { return e === n ? new TypeError("undefined value") : (this._set(e), this.iterable.length) }, remove: function (e) { if (e === n || e >= this.iterable.length || 0 > e) throw new Error("invalid index"); var t; return e === this.iterable.length - 1 ? t = this.pop() : (t = this.iterable[e], this.iterable[e] = n, this._recycleIndex(e)), t }, pop: function () { return this.iterable.pop() }, getNextIndex: function () { var e = this._recycledIndexes, t = e.length; return t > 0 ? e[t - 1] : this.iterable.length }, _set: function (e) { var t = this._recycledIndexes.pop(); t === n && (t = this.iterable.length), this.iterable[t] = e }, _recycleIndex: function (e) { this._recycledIndexes.push(e) } }, t.exports = i }, {}] }, {}, [1])(1) });
/* eslint-enable */
/* jshint ignore:end */

//------------------------------------------------------
// Source: src/engine/components/rf.comp.button.js
//------------------------------------------------------

/**
 * Componente para añadir más opciones a los botones de Phaser.
 * @param  {float} alphaDisabled Transparencia del botón cuando está desactivado
 * @param  {boolean} enabled Si el botón está activo o no
 */

// eslint-disable-next-line
var RFButtonComp = {
    options: {
        _obj: null,
        _origTexture: null,
        alphaDisabled: 0.3,
        enabled: true,
        disabledTexture: null,
        enable: function() {
            "use strict";
            if( this._obj !== null ) {
                this.enabled = true;
                this._obj.inputEnabled = true;
                // this._obj.frame = 0;
                this._obj.input.useHandCursor = true;
                if( this.disabledTexture ) {
                    this._obj.loadTexture( this._origTexture );
                }
                else {
                    this._obj.alpha = 1;
                }
            }
        },
        disable: function() {
            "use strict";
            this.enabled = false;
            if( this.disabledTexture ) {
                this._obj.frameName = this.disabledTexture;
            }
            else {
                this._obj.alpha = this.alphaDisabled;
            }

            this._obj.inputEnabled = false;
        }
    },

    create: function( object, options ) {
        "use strict";
        options._obj = object;
        options._origTexture = object.texture.baseTexture.source.name;
        if( !options.enabled ) {
            options.disable();
        }
    }
};

//------------------------------------------------------
// Source: src/engine/components/rf.comp.counter.js
//------------------------------------------------------

/**
 * Componente para contadores numéricos.
 * @param  {integer} initIntegerValue: Valor int con el que empieza el contador
 * @param  {integer} valueInteger Valor número hacia donde irá el contador
 * @param  {integer} timeToFinish Valor número en milisegundos que indica cuánto tardará el contador en llegar a su valor final
 * @param  {audio} soundMeter Sonido que se reproducirá al cambiar el contador
 * @version 0.1.7
 * CAMBIOS:
 * 0.1.3 - En el caso de que el label haya sido modificado, no realiza un tween.
 * 0.1.4 - Almacenea el tween y lo resetea cada vez que haya un cambio.
 * 0.1.5 - Cambia la llamada .text por .setString para forzar la actualización de los gradientes en los textos.
 * 0.1.6 - Se realiza "manualmente" el texto (usando .text) en lugar de llamar al "setString", para evitar que se cambie "forceUpdate" desde dicho método.
 * 0.1.7 - Siempre se realiza el formateo, aunque el número de decimales sea 0, para evitar que se trunque el valor cuando se juega con créditos
 */
// eslint-disable-next-line
var RFCounterComp = {
    options: {
        _obj: null,
        _changing: false,
        _firstChange: true,
        _tween: null,
        _finalValue: "", // Este es el último valor que se pondrá en la etiqueta
        game: null,
        value: 0,
        initValue: 0,
        timeToFinish: 1000,
        rfAudioSoundMeter: null,
        numberOfDecimals: 0,
        forceRFCurrencyDecimals: true, // solo para cuando se llame a "moneyChange"
        forceUpdate: false,  // fuerza una actualización del contador, aunque sea el primer cambio o aunque el objeto contenga el mismo valor en value

        moneyChange: function( value, currencyISO, time, bPlayAudio ) {
            "use strict";
            this._change( value, currencyISO, time, bPlayAudio );
        },
        valueChange: function( value, time, bPlayAudio ) {
            "use strict";
            this._change( value, undefined, time, bPlayAudio );
        },
        _getValue: function( value, rfCurrency ) {
            "use strict";
            var LENGTH_OF_WHOLE_PART = 3;
            var decimals = ( !_.isEmpty( rfCurrency ) && this.forceRFCurrencyDecimals ) ? rfCurrency.numberOfDecimals : this.numberOfDecimals;
            if( !_.isEmpty( rfCurrency ) ) {
                decimals = Math.min( decimals, rfCurrency.numberOfDecimals );
            }
            return parseFloat( value ).format( decimals, LENGTH_OF_WHOLE_PART, RFCurrencyUtils.decimalSeparator, RFCurrencyUtils.thousandSeparator );
        },
        _format: function( value, rfCurrency ) {
            "use strict";
            if( !_.isEmpty( rfCurrency ) ) {
                return String.format( rfCurrency.template, this._getValue( value, rfCurrency ), rfCurrency.valueSymbolSeparator, rfCurrency.symbol );
            }
            return this._getValue( value );
        },

        // TODO: Fix this
        // eslint-disable-next-line complexity
        _change: function( value, currencyISO, time, bPlayAudio ) {
            "use strict";
            if( this._firstChange || value !== this._obj.value || this.forceUpdate ) {
                this._firstChange = false;

                var val = ( _.isNumber( value ) ? value : this.value );
                var tim = ( _.isNumber( time ) ? time : this.timeToFinish );
                var aud = ( _.isBoolean( bPlayAudio ) ? bPlayAudio : true );
                var rfCurrency = _.isEmpty( currencyISO ) ? null : RFCurrencyUtils.getRFCurrency( currencyISO );

                // Paramos tweens previos
                game.tweens.remove( this._tween );
                if( this.rfAudioSoundMeter ) {
                    this.rfAudioSoundMeter.stop();
                }

                // Controla si se ha modificado el objeto y no contiene el mismo valor (esto se da en modificaciones entre cadenas y
                // valores numéricos, ya que esta modificación no pasa por el componente)
                tim = ( this._obj.text === this._finalValue ? tim : 0 );

                if( tim > 0 ) {
                    this._changing = true;
                    if( ( aud === true ) && ( this.rfAudioSoundMeter ) ) {
                        if( this.loop ) {
                            this.rfAudioSoundMeter.playLooped();
                        }
                        else {
                            this.rfAudioSoundMeter.play();
                        }
                    }
                    var self = this;
                    this._tween = game.add.tween( this._obj ).to( { value: val }, tim, Phaser.Easing.Linear.None );
                    this._tween.onUpdateCallback( function() {
                        self._obj.text = self._format( self._obj.value, rfCurrency );
                        // If we need to update the gradient every time, uncomment this or change the ".text" assing to "setString" method.
                        // this._obj._updateGradient();
                    }, this );
                    this._tween.onComplete.addOnce( function() {
                        if( self.rfAudioSoundMeter ) {
                            self.rfAudioSoundMeter.stop();
                        }
                        self._obj.value = val;

                        // Se realiza "manualmente", en lugar de llamar al "setString", para evitar que se cambie "forceUpdate" desde ese método.
                        self._obj.text = self._format( val, rfCurrency );
                        self._obj._updateGradient();

                        self._finalValue = self._obj.text;
                        self._changing = false;
                    }, this );
                    this._tween.start();
                }
                else {
                    this._obj.value = val;

                    // Se realiza "manualmente", en lugar de llamar al "setString", para evitar que se cambie "forceUpdate" desde ese método.
                    this._obj.text = this._format( val, rfCurrency );
                    this._obj._updateGradient();

                    this._finalValue = this._obj.text;
                }
            }
        }
    },
    create: function( object, options ) {
        "use strict";
        if( !options.game ) { throw new Error( "No se ha definido game dentro de options en Counter." ); }
        options._obj = object;
        object.value = options.initValue;
    }
};

//------------------------------------------------------
// Source: src/engine/components/rf.comp.dynamicReel.js
//------------------------------------------------------

/**
 * The states the reel can be in.
 * @member IDLE The reel is not spinning.
 * @member SPINNING The reel is spinning.
 * @member BOUNCING The reel is bouncing.
 * @member STOPPING The reel is stopping but has not finished animating.
 * @member FINISHED The reel has stopped and has finished animating.
 */
// eslint-disable-next-line
var RFDynamicReelStates = {
    IDLE: "IDLE",
    SPINNING: "SPINNING",
    BOUNCING: "BOUNCING",
    STOPPING: "STOPPING",
    FINISHED: "FINISHED"
};

/**
 * This component represents a reel object that can use dynamic symbols,
 * this means that the blur animation is not an animated sprite but actual sprite symbols moving.
 * @param {object array} symbols The contained RFSymbols.
 * @param  {integer} bounceSumY Amount of pixels the symbols are displaced during the bounce tween.
 * @param  {integer} bounceTime The amount of time that the bounce tween takes to complete.
 * @requires rf.gamecore.symbol.js
 * @requires lodash.js
 * @version 0.0.1
 */
// eslint-disable-next-line
var RFDynamicReelComp = {
    options: {
        _state: RFDynamicReelStates.IDLE,
        _symbolContainer: null,
        _verticalOffset: 0,
        _poolHeight: 0,

        _deltaTime: 0,
        _lastTime: 0,
        _stoppingToBounce: false,

        _weightedRandom: null,

        startSpinOffset: 0,
        spinSpeedOffset: 0,
        spinSpeed: 300,

        _finalSpinSpeed: 0,
        _finalStartOffset: 0,

        id: undefined,
        game: null,
        object: null,
        bounceSumY: 30,
        bounceTime: 1000,
        stopSound: null,
        playSounds: null,
        symbols: null,
        blurSymbols: null,
        gameSymbols: null,
        gameSymbolKeys: null,
        blurGroups: null,
        nextSymbols: null,
        finalSymbolsSet: false,
        endingGroup: 0,
        onStop: null
    },

    /**
     * Handles the bounce Tween for the reel symbol group.
     * @param {Phaser.Group} group The group that will be animated.
     * @param {Phaser.Group} context Self reference of the DynamicReel component.
     */
    bounce: function( group, context ) {
        "use strict";
        context.setState( RFDynamicReelStates.BOUNCING, context );

        if( context.options.playSounds ) {
            context.options.stopSound.play();
        }

        var bounceTween = context.options.game.add.tween( group );
        // bounceTween.frameBased = true;

        if( context.options.bounceTime === 0 ) {
            context.options.bounceTime = 1;
        }

        bounceTween.from(
            { y: context.options.bounceSumY },
            context.options.bounceTime,
            Phaser.Easing.Elastic.Out
        );
        bounceTween.onComplete.addOnce( function() {
            context.setState( RFDynamicReelStates.FINISHED, context );
        }, context );
        bounceTween.start();
    },

    /**
     * Returns the symbols container.
     * @returns {Phaser.Group} The group that contains the displayed symbols.
     */
    getReelGroup: function() {
        "use strict";
        return this.options._symbolContainer;
    },

    /**
     * State check function for the bouncing state.
     * @returns {Boolean} [True] when _state is BOUNCING, otherwise [false].
     */
    isBouncing: function() {
        "use strict";
        return this.options._state === RFDynamicReelStates.BOUNCING;
    },

    /**
     * State check function for the spinning state.
     * @returns {Boolean} [True] when _state is SPINNING, otherwise [false].
     */
    isSpinning: function() {
        "use strict";
        return this.options._state === RFDynamicReelStates.SPINNING;
    },

    /**
     * Init the options.blurSymbols with init default from ReelColum.js - updateRFGraphicSymbolsArray
     * @param {Object} rfSymbol symbol to initialize the blurSymbol by index
     */
    initBlurSymbolbyIndex: function( index, rfSymbol ) {
        "use strict";
        this.options.blurSymbols[ index ].setRFSymbol( rfSymbol );
    },

    /**
     * API function. Starts the spin process for the reel.
     */
    spinStart: function() {
        "use strict";
        // Check if the reel is not already spinning.
        if( this.options._state !== RFDynamicReelStates.SPINNING ) {
            this.options._stoppingToBounce = false;

            // Calculate the final spin speed, taking into account the speed offset and main speed values.
            this.options._finalSpinSpeed = this.options.spinSpeed + Math.floor( Math.random() * this.options.spinSpeedOffset );

            // Calculate the delay that will occur on starting to spin the reel.
            this.options._finalStartOffset = Math.floor( Math.random() * this.options.startSpinOffset );

            // Sets the state to spinning.
            this.setState( RFDynamicReelStates.SPINNING, this );

            // Clear the final symbols set flag.
            this.options.finalSymbolsSet = false;

            // Create a self reference for the lodash function.
            var self = this;

            /**
             * Selects a random symbol to be rendered for all the symbols in the blur groups that are not replacing the
             * main symbols on screen.
             */
            _.forEach( this.options.blurSymbols, function( o ) {
                if( o.getSprite().id > _.size( self.options.symbols ) - 1 ) {
                    self.setRandomSymbol( o, self );
                }
            } );

            /**
             * Creates a timer for the start spin process. The timer will execute using the previously calculated
             * *_finalStartOffset* value. This value differs in all reels by a *startSpinOffset* value.
             */
            var timer = this.options.game.time.create( false );
            timer.add( this.options._finalStartOffset, function() {
                // Set all main symbols to invisible.
                _.forEach( self.options.symbols, function( o ) {
                    o.getSprite().visible = false;
                } );

                // Set all dynamic symbols to visible and reposition the groups before starting the spin animation.
                _.forEach( self.options.blurGroups, function( o ) {
                    o.visible = true;

                    /**
                     * The ending group is set to position 0. The ending group is the one visible at the end of
                     * the animation sequence rendering the actual symbols that have been generated for the spin.
                     */
                    if( o.id === self.options.endingGroup ) {
                        o.position.y = 0;
                    }
                    else {
                        o.position.y = -self.options._poolHeight;
                    }
                } );
            }, this );
            timer.start();
        }
    },

    /**
     * This function handles pooling the groups and replacing the symbols with random ones.
     *
     * The random algorithm used is a *RFWeightedRandom* implemented in that class.
     *
     * When a group reaches the _poolHeight it must be pooled to be reused by the animation sequence.
     *
     * @param {Phaser.Group} group The group that will be animated.
     * @param {Phaser.Group} context Self reference of the DynamicReel component.
     */
    poolGroup: function( group, context ) {
        "use strict";

        var position = {
            x: group.position.x,
            y: -context.options._poolHeight
        };
        group.position.x = position.x;
        group.position.y = position.y;

        if( context.options._state !== RFDynamicReelStates.STOPPING && !context.finalSymbolsSet ) {
            context.setRandomSymbol( group, context );
        }
        else if( context.options._state === RFDynamicReelStates.STOPPING && !context.options.finalSymbolsSet ) {
            context.options.finalSymbolsSet = true;
            context.options.endingGroup = group.id;
            context.options._stoppingToBounce = true;
            for( var index = 0; index < context.options.symbols.length; index++ ) {
                group.children[ index ].data._symbol.setRFSymbol( context.options.nextSymbols[ index ] );
            }
        }
    },

    /**
     * Handles setting and getting random symbols from the Weighted Random.
     *
     * When this function receives a group as the symbol parameter, it will recursively call itself with
     * the child symbols as the symbol parameter.
     *
     * When it receives those symbols as the symbol parameter, it will generate random symbols to assign to
     * them to be rendered during the animation.
     *
     * @param {Phaser.Group || RFGraphicSymbol} symbol The group that will be animated or a RFGraphicSymbol.
     * @param {Phaser.Group} context Self reference of the DynamicReel component.
     */
    setRandomSymbol: function( symbol, context ) {
        "use strict";
        if( symbol instanceof RFGraphicSymbol ) {
            symbol.setRFSymbol( context.getRandomSymbol( context ) );
        }
        else {
            _.forEach( symbol.children, function( o ) {
                o.data._symbol.setRFSymbol( context.getRandomSymbol( context ) );
            } );
        }
    },

    /**
     * Generates a random symbol key to be used during the animation sequence.
     *
     * @param {Phaser.Group} context Self reference of the DynamicReel component.
     * @returns {string} The resulting symbol.
     */
    getRandomSymbol: function( context ) {
        "use strict";
        return context.options.gameSymbols[ context.options._weightedRandom.next().object.symbol ];
    },

    /**
     * API. Stops animating the reels and displays the resulting symbols.
     *
     * @param {Array<RFSymbol>} rfSymbolsArray The resulting symbols for the current spin.
     * @param {function} fOnStop The callback function. Invoked when the animation stops.
     */
    spinStop: function( fOnStop, rfSymbolsArray ) {
        "use strict";
        this.options.nextSymbols = rfSymbolsArray;
        this.options.onStop = fOnStop;

        this.setState( RFDynamicReelStates.STOPPING, this );
    },

    /**
     * Handles state setting for the reel.
     *
     * @param {RFDynamicReelStates} state The state to transition to.
     * @param {Phaser.Group} context Self reference of the DynamicReel component.
     */
    setState: function( state, context ) {
        "use strict";
        context.options._state = state;

        context.onStateChanged( state, context );
    },

    /**
     * Event. This event is dispatched when the reels state has been changed.
     *
     * @param {RFDynamicReelStates} state The new state.
     * @param {Phaser.Group} context Self reference of the DynamicReel component.
     */
    onStateChanged: function( state, context ) {
        "use strict";

        // Check if the state is FINISHED.
        if( context.options._state === RFDynamicReelStates.FINISHED ) {

            /**
             * Hide the dynamic symbols and show the main symbols.
             */
            _.forEach( context.options.blurGroups, function( o ) { o.visible = false; } );
            _.forEach( context.options.symbols, function( o ) { o.getSprite().visible = true; } );

            // Sets the state to idle.
            context.setState( RFDynamicReelStates.IDLE, context );

            // Check if the onStop callback is a function and invoke it.
            if( _.isFunction( context.options.onStop ) ) {
                context.options.onStop();
            }
        }
    },

    /**
     * Generates a new WeightedRandom object and its parameters.
     *
     * @param {Object} symbolWeights The symbols and linked weights to be used for the random algorithm.
     */
    setSymbolWeights: function( symbolWeights ) {
        "use strict";

        if( symbolWeights === null ) {
            return;
        }

        // Generate a new WeightedRandom object.
        this.options._weightedRandom = new RFWeightedRandom();

        var weightedElements = [];
        var symbols = symbolWeights.symbols;
        var weights = symbolWeights.weights;

        // Generate WeightedElements with the weights for the symbols and the symbols info.
        for( var i = 0; i < symbols.length; i++ ) {
            weightedElements.push( new RFWeightedElement( { symbol: symbols[ i ], weight: weights[ i ] }, weights[ i ] ) );
        }

        // Set the elements in the WeightedRandom object.
        this.options._weightedRandom.setElements( weightedElements );
    },
    update: function( object, options ) {
        "use strict";
        var opt = options;
        options = opt;

        var miliToSeconds = 0.001;
        this.options._deltaTime = this.options.game.time.elapsed * miliToSeconds;

        if( this.options._state === RFDynamicReelStates.STOPPING || this.options._state === RFDynamicReelStates.SPINNING ) {

            var fpsBase = 60.0;
            var baseDeltatime = ( 1 / fpsBase );
            var currentFPS = ( this.options._deltaTime * fpsBase ) / baseDeltatime;

            // eslint-disable-next-line
            var spinSpeed = ( this.options._finalSpinSpeed * currentFPS ) / fpsBase;

            var self = this;
            _.forEach( this.options.blurGroups, function( o ) {
                o.position.y += spinSpeed * self.options._deltaTime;

                if( self.options._state !== RFDynamicReelStates.BOUNCING ) {
                    if( o.position.y >= self.options._poolHeight && !( o.id !== self.options.endingGroup && self.options._stoppingToBounce ) ) {
                        var skipDistance = ( o.position.y - self.options._poolHeight );
                        self.poolGroup( o, self );
                        o.position.y += ( skipDistance % ( self.options._poolHeight * 2 ) );
                    }
                }

                if( self.options._state === RFDynamicReelStates.STOPPING && o.id === self.options.endingGroup && self.options.finalSymbolsSet ) {
                    if( o.position.y >= 0 ) {
                        o.position.y = 0;
                        self.bounce( o, self );
                    }
                }
            } );
        }
    },
    create: function( object, options ) {
        "use strict";

        this.options._lastTime = _.now() / Phaser.Timer.SECOND;

        // Initialize the reel with idle state.
        this.setState( RFDynamicReelStates.IDLE, this );

        if( !options.game ) { throw new Error( "No se ha definido game dentro de options en Reels." ); }

        options.object = object;

        // Generate a symbol container group. (Main Symbols).
        options._symbolContainer = options.game.add.group();
        for( var i = 0; i < options.symbols.length; i++ ) {
            options._symbolContainer.add( options.symbols[ i ].getSprite() );
        }

        // Load all the symbols that could be used during the game.
        options.gameSymbols = options.game.state.getCurrentState().getRFLoadScenes()
            .getRFSymbols();

        var symbolCount = options.symbols.length;
        options.gameSymbolKeys = [];

        _.forEach( options.gameSymbols, function( o ) {
            options.gameSymbolKeys.push( o.id );
        } );

        // Generate a default WeightedRandom object in case no weights are provided.
        options._weightedRandom = new RFWeightedRandom();
        var weightedElements = [];
        for( var wi = 0; wi < options.gameSymbolKeys.length; wi++ ) {
            weightedElements.push( new RFWeightedElement( { symbol: options.gameSymbolKeys[ wi ], weight: 1 }, 1 ) );
        }
        options._weightedRandom.setElements( weightedElements );

        options.blurSymbols = [];
        options.blurGroups = [];

        // Calculate the vertical offset of the symbols.
        options._verticalOffset = ( ( options.symbols[ 1 ].getSprite().position.y - options.symbols[ 0 ].getSprite().position.y ) );

        // Calculate the y position at which the groups must be pooled.
        options._poolHeight = ( options._verticalOffset * options.symbols.length );

        /**
         * Generate 2 sets of symbols for the dynamic reel spin animation sequence.
         *
         * These two sets will be contained in separate groups and animated as such. The disposition is loaded
         * from the GameDefinition > Reels > Symbols definition.
         */
        for( var index = 0; index < 2; index++ ) {
            var blurGroup = options.game.add.group();
            blurGroup.id = index;

            for( var j = 0; j < symbolCount; j++ ) {
                var oSymbol = this.getRandomSymbol( this );

                var rfGraphicSymbol = new RFGraphicSymbol( options.game );
                rfGraphicSymbol.setAnchor( 0.5, 0.5 );
                var graphicSprite = rfGraphicSymbol.getSprite();
                graphicSprite.id = j;

                var position = {
                    x: options.symbols[ j ].getPosition().x,
                    y: options.symbols[ j ].getPosition().y
                };

                rfGraphicSymbol.setXY( position.x, position.y );
                rfGraphicSymbol.setRFSymbol( oSymbol );

                options.blurSymbols.push( rfGraphicSymbol );
                rfGraphicSymbol.getSprite().data._symbol = rfGraphicSymbol;
                blurGroup.add( rfGraphicSymbol.getSprite() );
            }

            options._symbolContainer.add( blurGroup );
            options.blurGroups.push( blurGroup );

            options.blurGroups[ index ].position.y = options._verticalOffset * options.symbols.length * index;
            options.blurGroups[ index ].visible = false;

            options.blurGroups[ index ]._tweenDelta = 0;
        }

        // options._poolHeight = ( options.blurGroups[ 0 ].height );
    }
};

//------------------------------------------------------
// Source: src/engine/components/rf.comp.flash.js
//------------------------------------------------------

/**
 *
 * @version 0.0.1
 */
// eslint-disable-next-line
var RFFlashComp = {
    options: {
        _obj: null,
        _running: false,
        _tw: null,
        game: null,
        initAlpha: 1,
        endAlpha: 0,
        finishTime: 350,
        autoStart: false,
        onComplete: null,
        start: function() {
            "use strict";
            if( this._running === false ) {
                this._running = true;
                this._tw = this.game.add.tween( this._obj ).to( { alpha: 0.1 }, this.finishTime, "Linear", true, 0, -1 );
                this._tw.yoyo( true, Math.random() * this.finishTime );
            }
        },
        stop: function() {
            "use strict";
            if( this._running === true ) {
                this._running = false;
                this._tw.stop();
                this._obj.alpha = this.initAlpha;
            }
        }
    },

    create: function( object, options ) {
        "use strict";
        if( options.game === null ) { throw Error( "Se debe definir el game en el componente RFFlashComp" ); }
        options._obj = object;
        if( options.autoStart ) {
            options.start();
        }
    }

};

//------------------------------------------------------
// Source: src/engine/components/rf.comp.flipcard.js
//------------------------------------------------------

/**
 * @version 0.0.3
 * @todo: Se mantiene el nombre flipCard para que sea retrocompatible. En futuras versiones debería eliminarse
 * @todo: Implementar funcionalidad startFlipped ( actualmente no se está contemplando ).
 * CAMBIOS:
 * 0.0.2 - Se almacena el estado de giro de la carta para evitar giros si la carta ya está girada.
 * 0.0.3 - Elimina el método update, ya que no funcionaba y no se estaba usando en ningún juego.
 */

/* eslint-disable */
var flipCard;
var RFFlipCardComp;
/* eslint-enable */

RFFlipCardComp = {
    options: {
        _obj: null,
        _origTexture: null,
        _tw: null,
        _twBack: null,
        _running: false,
        _finishTW: false,
        _flashTW: null,
        _isFlipped: false,

        backTexture: null,
        timeToFinish: 1000,
        autoStart: false,
        onComplete: null,
        onBackComplete: null,
        atlas: null,
        game: null,
        startFlipped: true,
        flipSound: null,
        tweenEasingFirstTurn: Phaser.Easing.Linear.None,
        tweenEasingSecondTurn: Phaser.Easing.Linear.None,

        /** Gira la carta */
        flip: function() {
            "use strict";
            if( this._running === false && this._isFlipped === false ) {
                this._running = true;

                if( this.flipSound ) {
                    this.flipSound.play();
                }

                var self = this;
                this._tw = this.game.add.tween( this._obj.scale ).to( { x: 0 }, this.timeToFinish / 2, this.tweenEasingFirstTurn, true );
                this._tw.onComplete.addOnce( function() {
                    if( self.atlas !== null ) {
                        self._obj.loadTexture( self.atlas, self.backTexture );
                    }
                    else {
                        self._obj.loadTexture( self.backTexture );
                    }
                    self._twBack = self.game.add.tween( self._obj.scale ).to( { x: 1 }, self.timeToFinish / 2, self.tweenEasingSecondTurn, true );
                    self._twBack.onComplete.addOnce( function() {
                        self._running = false;
                        if( self.onComplete ) { self.onComplete(); }
                    }, self );
                }, this );
                this._isFlipped = true;
            }
        },

        /** Gira la carta hasta su posición inicial */
        flipBack: function() {
            "use strict";
            if( this._running === false && this._isFlipped ) {
                this._running = true;

                if( this.flipSound ) {
                    this.flipSound.play();
                }

                var self = this;
                this._tw = this.game.add.tween( this._obj.scale ).to( { x: 0 }, this.timeToFinish / 2, this.tweenEasingFirstTurn, true );
                this._tw.onComplete.addOnce( function() {
                    if( self.atlas !== null ) {
                        self._obj.loadTexture( self.atlas, self._origTexture );
                    }
                    else {
                        self._obj.loadTexture( self._origTexture );
                    }
                    self._twBack = self.game.add.tween( self._obj.scale ).to( { x: 1 }, self.timeToFinish / 2, self.tweenEasingSecondTurn, true );
                    self._twBack.onComplete.addOnce( function() {
                        self._running = false;
                        if( self.onBackComplete ) { self.onBackComplete(); }
                    }, self );
                }, this );
                this._isFlipped = false;
            }
        },
        isRunning: function() {
            "use strict";
            return this._running;
        },

        /** Hace que flashee la carta */
        flashStart: function() {
            "use strict";
            var MS_TWEEN = 500;
            var REPEAT_FOREVER = -1;
            var MS_DELAY_TO_START = 0;
            this._flashTW = this.game.add.tween( this._obj ).to(
                { alpha: 0.2 },
                MS_TWEEN,
                Phaser.Easing.Linear.None,
                true,
                MS_DELAY_TO_START,
                REPEAT_FOREVER,
                true
            );
        },

        /** Hace deje de parpadear la carta */
        flashStop: function() {
            "use strict";
            if( this._flashTW ) {
                this._flashTW.stop();
            }
            this._obj.alpha = 1;
        }
    },

    create: function( object, options ) {
        "use strict";
        if( options.game === null ) { throw Error( "Se debe especificar game en el componente RFFlipCardComp" ); }

        options._obj = object;
        // if (options.backTexture===null) console.warn("No se ha especificado una textura para el retira en el componente flipCards");
        options._origTexture = object.frameName; // No se ha probado si funciona con atlas
        object.anchor.setTo( 0.5 );
        object.position.x += ( object.width / 2 );
        object.position.y += ( object.height / 2 );

        if( options.autoStart === true ) {
            options.flip();
        }
        if( !options.game.device.desktop ) {
            options.overEffect = false;
        } // Evitamos que funcione el efecto over en móviles
    }
};
flipCard = RFFlipCardComp;

//------------------------------------------------------
// Source: src/engine/components/rf.comp.followCursor.js
//------------------------------------------------------

/** Movimiento aleatorio de sprites, asignando una posición x e y final*/
// eslint-disable-next-line
var RFCompFollowCursor = {
    options: {
        speed: 600,
        anchorX: 0.5,
        anchorY: 0.5,
        follow: false,
        game: null
    },

    create: function( object, options ) {
        "use strict";
        if( options.game === null ) { throw Error( "No se ha definido un game en el componente RFCompFollowCursor" ); }

        object.anchor.set( options.anchorX, options.anchorY );
        if( options.follow ) {
            options.game.physics.enable( object, Phaser.Physics.ARCADE );
        }
    },

    update: function( object, options ) {
        "use strict";
        if( options.follow ) {
            options.game.physics.arcade.moveToPointer( object, options.speed );
            if( Phaser.Rectangle.contains( object.body, options.game.input.x, options.game.input.y ) ) {
                object.body.velocity.setTo( 0, 0 );
            }
        }
        else {
            object.position.x = options.game.input.x;
            object.position.y = options.game.input.y;
        }
    }

};

//------------------------------------------------------
// Source: src/engine/components/rf.comp.reel.js
//------------------------------------------------------

/**
 * Componente para crear los rodillos en el juego
 * @param  {object} blur  Sprite que tiene el blur gráfico del rodillo
 * @param  {integer} blurFPS  Fotogramas por segundo a los que irá la animación del blur
 * @param  {Array.<number> | Array.<string>} frames Array de frames de la animación del blur
 * @param  {object array} symbols Array con los RFSymbols que componen el rodillo
 * @param  {integer} bounceSumY Cuánto se desplaza en píxeles la figura cuando se produce el rebote
 * @param  {integer} bounceTime Cuánto tarda el rodillo en colocarse en su posición de inicio (en milisegundos)
 * @param  {object} audioStop Sonido que se reproducirá al parar el rodillo
 * @requires rf.gamecore.symbol.js
 * @requires lodash.js
 * @version 0.1.4
 */
// eslint-disable-next-line
var RFReelComp = {
    options: {
        _obj: null,
        _bouncing: false,
        _spinning: false,
        _symbolsGroup: null,
        _reelGroup: null,

        game: null,
        blur: null,
        blurFPS: 15,
        frames: [],
        bounceSumY: 30,
        bounceTime: 1000,
        audioStop: null,
        symbols: null
    },
    bounce: function( fOnStop ) {
        "use strict";
        if( this.options._bouncing === false ) {
            if( this.options.bounceTime > 0 ) {
                var bouncetw = this.options.game.add.tween( this.options._symbolsGroup );
                bouncetw.from( { y: this.options._symbolsGroup.y + this.options.bounceSumY }, this.bounceTime, Phaser.Easing.Elastic.Out );

                var self = this;
                bouncetw.onComplete.addOnce( function() {
                    self.options._bouncing = false;
                    if( _.isFunction( fOnStop ) ) {
                        fOnStop();
                    }
                }, this );
                bouncetw.start();
            }
            else if( _.isFunction( fOnStop ) ) {
                fOnStop();
            }
        }
    },
    getSymbolsGroup: function() {
        "use strict";
        return this.options._symbolsGroup;
    },
    getReelGroup: function() {
        "use strict";
        return this.options._reelGroup;
    },
    isBouncing: function() {
        "use strict";
        return this.options._bouncing;
    },
    isSpinning: function() {
        "use strict";
        return this.options._spinning;
    },
    spinStart: function() {
        "use strict";
        if( this.options._spinning === false ) {

            this.options._symbolsGroup.visible = false;
            this.options.blur.visible = true;
            this.options.blur.animations.play( "bluranm" );
            this.options.blur.animations.getAnimation( "bluranm" ).frame =
                Math.floor( Math.random() * this.options.blur.animations.getAnimation( "bluranm" ).frameTotal );

            this.options._spinning = true;
        }
    },
    spinStop: function( fOnStop ) {
        "use strict";
        if( this.options._spinning === true ) {
            if( this.options.audioStop ) {
                this.options.audioStop.play();
            }
            this.options._symbolsGroup.visible = true;

            var self = this;
            this.bounce( function() {
                self.options._spinning = false;
                if( _.isFunction( fOnStop ) ) {
                    fOnStop();
                }
            } );

            this.options.blur.animations.stop();
            this.options.blur.visible = false;
        }
    },

    /**
     * Init the options.blurSymbols with initi default from ReelColum.js - updateRFGraphicSymbolsArray
     *  @param {Object} initRfSymbols symbol to initialize the blurSymbol by index
     */
    initBlurSymbolbyIndex: function( index, initRfSymbols ) {
        "use strict";
        this.options.blurSymbols[ index ].setRFSymbol( initRfSymbols );
    },
    create: function( object, options ) {
        "use strict";

        if( !options.game ) { throw new Error( "No se ha definido game dentro de options en Reels." ); }
        options._obj = object;

        options._symbolsGroup = options.game.add.group();
        for( var i = 0; i < options.symbols.length; i++ ) {
            options._symbolsGroup.add( options.symbols[ i ].getSprite() );
        }

        if( options.blur !== null ) {
            options.blur.visible = false;
            var frames = _.isArray( options.frames ) ? options.frames : [];
            options.blur.animations.add( "bluranm", frames, options.blurFPS, true );
        }

        options._reelGroup = options.game.add.group();
        options._reelGroup.add( options._symbolsGroup );
        options._reelGroup.add( options.blur );
    }
};

//------------------------------------------------------
// Source: src/engine/components/rf.comp.scaleappear.js
//------------------------------------------------------

/**
 * Hace aparecer un sprite escalándose
 * @version 0.1.0
 */

/* eslint-disable */
var scaleAppear;
var RFScaleAppearComp;
/* eslint-enable */

scaleAppear = {
    options: {
        _obj: null,
        _running: false,
        _tw: null,
        game: null,
        initScale: 0.1,
        endScale: 1,
        finishTime: 1000,
        autoStart: false,
        onComplete: null,
        start: function() {
            "use strict";
            if( this._running === false ) {
                this._running = true;
                this._tw = this.game.add.tween( this._obj.scale ).to( { x: this.endScale, y: this.endScale }, this.finishTime, Phaser.Easing.Back.Out, true );

                var self = this;
                this._tw.onComplete.addOnce( function() {
                    if( self.onComplete ) { self.onComplete(); }
                    self._running = false;
                }, this );
            }
        }
    },

    create: function( object, options ) {
        "use strict";
        if( options.game === null ) { throw Error( "Se debe definir el game en el componente scaleAppear" ); }
        options._obj = object;
        object.anchor.setTo( 0.5 );
        object.position.x += ( object.width / 2 );
        object.position.y += ( object.height / 2 );
        object.scale.setTo( options.initScale );
        if( options.autoStart ) {
            options.start();
            // var tw = game.add.tween(object.scale).to( { x: options.endScale, y:options.endScale }, options.finishTime, Phaser.Easing.Back.Out, true);
            // if (options.onComplete) tw.onComplete.add(options.onComplete());
        }
    }
};

RFScaleAppearComp = scaleAppear;

//------------------------------------------------------
// Source: src/engine/components/rf.comp.starfield.js
//------------------------------------------------------

/**
 * Se crea un maravilloso campo de estrellas que parpadean
 * @version 0.1.0
 */
// eslint-disable-next-line
var RFStarFieldComp = {
    options: {
        _obj: null,
        _angle: 1,
        game: null,
        starsNum: 10,
        starsAlpha: { min: 0.1, max: 1 },
        time: { min: 1000.0, max: 2000.0 },
        texture: null,
        blendMode: PIXI.blendModes.ADD,
        atlas: null,
        angleTime: { min: 5000.0, max: 10000.0 },
        rect: { x: 0, y: 0, width: 100, height: 100 },
        scale: { min: 0.1, max: 1 }
    },

    create: function( object, options ) {
        "use strict";
        options._obj = object;
        if( !options.game ) { throw Error( "No has indicado el game en el componente starField" ); }
        for( var i = 0; i < options.starsNum; i++ ) {
            // Creación de cada estrella
            var star = null;
            if( !options.atlas ) {
                star = game.add.sprite( 0, 0, options.texture );
            }
            else {
                star = game.add.sprite( 0, 0, options.atlas, options.texture );
            }
            star.anchor.set( 0.5 );
            var position = {
                x: options.game.rnd.integerInRange( options.rect.x, options.rect.x + options.rect.width - star.width ),
                y: options.game.rnd.integerInRange( options.rect.y, options.rect.y + options.rect.height - star.height )
            };
            star.position = position;
            star.scale.set( options.game.rnd.realInRange( options.scale.min, options.scale.max ) );
            star.blendMode = options.blendMode;
            // Tween del brillito
            star.alpha = options.starsAlpha.max;
            var timernd = options.game.rnd.realInRange( options.time.min, options.time.max );
            options.game.add.tween( star ).to( { alpha: options.starsAlpha.min }, timernd, Phaser.Easing.Linear.None, true, 0, timernd, true );
            options.game.add.tween( star.scale ).to( {
                x: options.scale.min,
                y: options.scale.min
            }, timernd, Phaser.Easing.Linear.None, true, 0, timernd, true );
            // Ángulo
            var timerndAng = options.game.rnd.realInRange( options.angleTime.min, options.angleTime.max );
            options.game.add.tween( star ).to( { angle: 359 }, timerndAng, null, true, 0, Infinity );
        }
    }

};



//------------------------------------------------------
// Source: src/engine/components/rf.comp.symbolanim.js
//------------------------------------------------------

/**
 * Componente para crear animaciones a partir de los iconos
 * @param  {float} alphaDisabled Transparencia del skittle cuando no está activado
 * @param  {texture} animTexture Textura que compone la animación
 * @param  {integer} animFPS Fotogramas por segundo de la animación
 * @param  {function} func Función que se ejecutará cuando termine la animación
 * @param  {boolean} autoHide Si la figura padre debe desaparecer o no cuando se ejecuta la animación
 * @param  {boolean} repeat Si la animación se reproduce en bucle o no. Si se reproduce en bucle, no se cargará la función func
 * @param  {boolean} autoStart Si la animación se reproduce nada más cargarse el componente.
 * @param  {boolean} autoCenter NO USADO: Se usa la posición (x, y) y anchor del sprite original.
 * @param  {integer} msAnimTimeWithNoTexture Número de mílisegundos en el que se realizará el tween en caso de no tener una textura de animación
 * @param  {integer} scaleTween Escala en x e y, del símbolo a la que realizará el tween, en caso de no tener textura de animación.
 *                              Al finalizar el tween, dejará el la escala en 1
 * @version 0.4.0
 * CAMBIOS:
 * 0.2.0 - Se añaden los frames de la animación del símbolo.
 *       - Se deja un tween Quadratic para los símbolos en lugar de lineal y se parametriza el número de repeticiones.
 * 0.3.0 - Añadido animación de rotacion para cuando no hay animaciones claim
 *       - Añadida comprobación "isFunction" a la función pasada para añadir al tween cuando
 *         éste se completa
 * 0.4.0 - Se añade una nueva variable "useAnimation" que controla la ejecución de las animaciones o tweens.
 */
// eslint-disable-next-line
var RFSymbolAnimComp = {
    options: {
        _obj: null,
        _position: null,
        _keyObj: "",
        _frameObj: 0,
        _anim: null,
        _tween: null,
        game: null,
        animTexture: null,
        animFrames: null,
        animFPS: 30,
        autoHide: true,
        repeat: false,
        func: null,
        autoStart: false,
        restoreOriginalSymbolAtEnd: false,
        useAnimation: false,
        //  autoCenter: true,  NOT USED (remove in version 0.1.0)
        msAnimTimeWithNoTexture: 1500,
        numRepAnimTimeWithNoTexture: 1,
        scaleTween: 0.7,
        claimAnim: false,
        /* eslint-disable complexity */
        start: function() {
            "use strict";
            this.stop();

            if( this.useAnimation && !_.isEmpty( this.animTexture ) && this.game.cache.checkImageKey( this.animTexture ) ) {

                this._keyObj = this._obj.key;
                this._frameObj = this._obj.frame;

                this._obj.loadTexture( this.animTexture );

                var keyAnim = "__animComp_" + this.animTexture;
                this._anim = this._obj.animations.getAnimation( keyAnim );

                if( this._anim === null ) {
                    var frames = _.isArray( this.animFrames ) ? this.animFrames : null;
                    this._anim = this._obj.animations.add( keyAnim, frames );
                }

                this._anim.play( this.animFPS, this.repeat );
                this._anim.onComplete.add( this._fCallbackOnCompleteAnimation, this );

            }
            else if( this.useAnimation ) {
                var twTime = this.msAnimTimeWithNoTexture;
                if( !this.claimAnim ) { // win animation
                    var twProperties = {
                        x: [ this.scaleTween, 1 ],
                        y: [ this.scaleTween, 1 ]
                    };
                    this._tween = this.game.add.tween( this._obj.scale ).to(
                        twProperties,
                        twTime / ( this.numRepAnimTimeWithNoTexture + 1 ),
                        Phaser.Easing.Quadratic.InOut,
                        true,
                        0,
                        this.numRepAnimTimeWithNoTexture
                    );
                }
                else {
                    var dirAngleaux = _.random( 0, 1 );
                    var dirAngle = dirAngleaux === 1 ? -1 : 1;
                    var newAngle = 15;
                    dirAngle *= newAngle;
                    if( this.repeat ) {
                        this._tween = this.game.add.tween( this._obj ).to(
                            { angle: dirAngle },
                            twTime / ( this.numRepAnimTimeWithNoTexture + 1 ),
                            Phaser.Easing.Quadratic.InOut,
                            true,
                            0,
                            -1,
                            true
                        );
                    }
                    else {
                        this._tween = this.game.add.tween( this._obj ).to(
                            { angle: dirAngle },
                            twTime / ( this.numRepAnimTimeWithNoTexture + 1 ),
                            Phaser.Easing.Quadratic.InOut,
                            true,
                            0,
                            this.numRepAnimTimeWithNoTexture,
                            true
                        );
                    }
                }
                if( _.isFunction( this.func ) ) {
                    this._tween.onComplete.addOnce( this.func, this );
                }
            }
            else if( !this.animation ) {
                // Por si acaso va a este flujo alguna animación que no tiene que hacerse
                if( _.isFunction( this.func ) ) {
                    this.func();
                }
            }
        },
        /* eslint-enable complexity */
        stop: function( bCallToCallback ) {
            "use strict";
            if( this._anim !== null ) {
                if( !_.isEmpty( this.animTexture ) && this.game.cache.checkImageKey( this.animTexture ) ) {

                    this._anim.stop();
                    this._anim = null;

                    this._obj.loadTexture( this._keyObj, this._frameObj );

                    bCallToCallback = _.isBoolean( bCallToCallback ) ? bCallToCallback : false;
                    if( bCallToCallback ) {
                        if( _.isFunction( this.func ) ) {
                            this.func();
                        }
                    }
                }
            }

            if( this._tween ) {
                if( this.claimAnim ) {
                    this._obj.angle = 0;
                }
                else {
                    this._obj.scale.x = 1;
                    this._obj.scale.y = 1;
                }
                this._tween.stop();
            }
        },
        _fCallbackOnCompleteAnimation: function() {
            "use strict";
            if( this.restoreOriginalSymbolAtEnd ) {
                this._obj.loadTexture( this._keyObj, this._frameObj );
            }
            this._anim = null;
            if( _.isFunction( this.func ) ) {
                this.func();
            }
        },
        isPlayingAnimation: function() {
            "use strict";
            var isPlaying = null;
            if( this._anim ) {
                isPlaying =  this._anim.isPlaying;
            }
            return isPlaying;
        }
    },

    create: function( object, options ) {
        "use strict";
        if( !( object instanceof Phaser.Sprite ) ) { throw new Error( "RFSymbolAnimComp can only applies to a Phaser.Sprite" ); }

        options._obj = object;
        options._position = object.position;

        if( options.autoStart ) {
            options.start();
        }
    }

};


//------------------------------------------------------
// Source: src/engine/components/rf.comp.tooltip.js
//------------------------------------------------------

/**
 * Componente para indicar información de un elemento de manera visual
 * @version 0.1.0
 * @param {phaserObj} objToolTip objeto a mostrar cuando se activa el event pointerOver en _obj
 * @param {float} timetoShow tiempo necesario para mostrar el tooltip
 * @param {float} timeToHide tiempo necesario para esconder el tooltip
 */

// eslint-disable-next-line
var RFToolTipComp = {
    options: {
        _obj: null,
        _needToShow: false,
        _enable: true,
        timeToShow: 2000,
        timeToHide: 100,
        objToolTip: null,
        over: function() {
            "use strict";
            var self = this;
            this._needToShow = true;
            setTimeout( function() {
                if( self._needToShow ) {
                    if( self.objToolTip && self._enable ) {
                        self.objToolTip.visible = true;
                        _.forEach( self.objToolTip.children, function( child ) {
                            child.visible = true;
                            child.bringToTop();
                        } );
                    }
                    self._needToShow = false;
                }
            }, this.timeToShow );
        },
        out: function() {
            "use strict";
            var self = this;
            this._needToShow = false;
            setTimeout( function() {
                if( self.objToolTip ) {
                    self.objToolTip.visible = false;
                    _.forEach( self.objToolTip.children, function( child ) {
                        child.visible = false;
                    } );
                }
            }, this._timeToHide );
        },
        enableTip: function() {
            "use strict";
            this._enable = true;
        },
        disableTip: function() {
            "use strict";
            this._enable = false;
        }
    },

    create: function( object, options ) {
        "use strict";
        options._obj = object;
    }
};

//------------------------------------------------------
// Source: src/engine/gamecore/phaser.custom/rf.gamecore.phaser.custom.button.js
//------------------------------------------------------

/**
 * Funcionalidades extra para los botones
 * @version 0.0.1
 */
Phaser.Button.prototype._setState = function( frame ) {
    "use strict";
    if( _.isInteger( frame ) ) {
        this.frame = frame;
    }
    else if( _.isString( frame ) ) {
        this.frameName = frame;
    }
};

Phaser.Button.prototype.setNormal = function() {
    "use strict";
    this._setState( _.get( this, "data.gdObj.normal" ) );
};

Phaser.Button.prototype.setOver = function() {
    "use strict";
    this._setState( _.get( this, "data.gdObj.over" ) );
};

Phaser.Button.prototype.setDown = function() {
    "use strict";
    this._setState( _.get( this, "data.gdObj.down" ) );
};

//------------------------------------------------------
// Source: src/engine/gamecore/phaser.custom/rf.gamecore.phaser.custom.label.js
//------------------------------------------------------

/**
 * Funcionalidades extra para las labels (bitmaptext y text)
 * @version 0.2.0
 * CAMBIOS:
 * 0.1.0 - Se añade la funcionalidad de gradiente para las labels de tipo text.
 * 0.2.0 - Se añade RFLabelUtils como utilidades para los labels.
 *       - Se añade el método updateData para que los labels que lo tengan configurado, se rellenen con los
 *         datos de un objeto recibido (se necesita de ciertas funciones definidas en el slo2game dentro del label)
 */

// #region UTILS AND HELPERS
// eslint-disable-next-line no-unused-vars
var RFLabelUtils = {
    getReplacedText: function( textTemplate, localeTextsMap ) {
        "use strict";
        var finalStr = "";
        if( localeTextsMap === undefined ) {
            finalStr = textTemplate;
        }
        else if( _.isString( textTemplate ) ) {
            var TEXT_TEMPLATE = /\{([^}]+)\}/g;
            var currentIdx = 0;
            var arrayMatch;
            while( ( arrayMatch = TEXT_TEMPLATE.exec( textTemplate ) ) !== null ) {

                // get the locale text
                var matchedText = _.get( arrayMatch, 0, "" );
                var localeKey = _.get( arrayMatch, 1 );
                var localeText = _.get( localeTextsMap, localeKey );

                // append previous text
                finalStr += textTemplate.substr( currentIdx, arrayMatch.index - currentIdx );

                // append the locale texts
                if( localeText !== undefined ) {
                    finalStr += localeText;
                }
                else {
                    finalStr += matchedText;

                    // informs only if the localeKey is not a number to prevent {0},{1}... warnings
                    var isNumber = Number( localeKey ) >= 0;
                    if( !isNumber ) {
                        console.warn( "Locale text " + localeKey + " not found in ", localeText );
                    }
                }

                currentIdx = TEXT_TEMPLATE.lastIndex;

            }
            finalStr += textTemplate.substr( currentIdx, textTemplate.length - currentIdx );
        }
        return finalStr;
    },

    // Example of textSubstitutionObj:
    // {
    //     "path": [ "payTable", "prizeList", "AA", "prizes", "3" ],
    //     "defaultValue": "1",
    //     "type": "moneyOrCredits",
    //     "currencyISO": "EUR"
    // }
    getFinalValue: function( params ) {
        "use strict";


        var phaserLabel = _.get( params, [ "phaserLabel" ] );
        var objToExtractData = _.get( params, [ "objToExtractData" ] );
        var textSubstitutionObj = _.get( params, [ "textSubstitutionObj" ] );
        var finalCurrencyISO = _.get( params, [ "finalCurrencyISO" ] );
        var localeTextsMap = _.get( params, [ "localeTextsMap" ] );

        var value;
        var path = _.get( textSubstitutionObj, [ "path" ] );
        if( path ) {
            var defaultValue = _.get( textSubstitutionObj, [ "defaultValue" ] );
            if( _.isString( defaultValue ) ) {
                defaultValue = RFLabelUtils.getReplacedText( defaultValue, localeTextsMap );
            }

            var type = _.get( textSubstitutionObj, [ "type" ] );

            value = _.get( objToExtractData, path, defaultValue );

            switch( type ) {
                case "number":
                    value = Number( value );
                    break;
                case "string":
                    value = String( value );
                    break;
                case "money":
                    value = RFCurrencyUtils.format( value, finalCurrencyISO );
                    break;
                case "moneyOrCredits":
                    value = Number( value );

                    if( _.isFunction( phaserLabel.formatMoneyOrCredits ) ) {
                        value = phaserLabel.formatMoneyOrCredits( value, finalCurrencyISO );
                    }
                    else {
                        console.warn(
                            "The label", phaserLabel, "hasn't exported the formatMoneyOrCredits function. " +
                            "Maybe you don't call the SceneUtils.initLabels function"
                        );
                    }

                    break;
                default:
                    value = String( value );
                    break;
            }
        }

        return value;
    },
    getFormattedText: function( phaserLabel, objToExtractData, extraParams ) {
        "use strict";

        var currencyISO = _.get( extraParams, [ "currencyISO" ] );
        var localeTextsMap = _.get( extraParams, [ "localeTextsMap" ] );
        var updateData = _.get( phaserLabel, [ "data", "gdObj", "parameters", "updateData" ] );
        var textTemplate = _.get( updateData, [ "textTemplate" ] );
        var textSubstitutions = _.get( updateData, [ "textSubstitutions" ] );

        var replacedText = RFLabelUtils.getReplacedText( textTemplate, localeTextsMap );
        var argsArray = [ replacedText ];
        _.forEach( textSubstitutions, function( textSubstitutionObj ) {
            var finalCurrencyISO = _.get( textSubstitutionObj, [ "currencyISO" ], currencyISO );
            argsArray.push( RFLabelUtils.getFinalValue( {
                phaserLabel: phaserLabel,
                objToExtractData: objToExtractData,
                textSubstitutionObj: textSubstitutionObj,
                finalCurrencyISO: finalCurrencyISO,
                localeTextsMap: localeTextsMap
            } ) );
        } );

        var formattedString = String.format.apply( this, argsArray );
        return formattedString;
    }
};
// #region UTILS AND HELPERS

// #region FORMATS ( color, stroke, fontStyles, fontWeights )
Phaser.BitmapText.prototype.clearColors = function() { "use strict"; };
Phaser.Text.prototype.clearColors = function() {
    "use strict";
    this.colors = [];
    this.addColor();
};

Phaser.BitmapText.prototype.clearStrokeColors = function() { "use strict";  };
Phaser.Text.prototype.clearStrokeColors = function() {
    "use strict";
    this.strokeColors = [];
    this.addStrokeColor();
};

Phaser.BitmapText.prototype.clearFontStyles = function() { "use strict";  };
Phaser.Text.prototype.clearFontStyles = function() {
    "use strict";
    this.fontStyles = [];
    this.addFontStyle();
};

Phaser.BitmapText.prototype.clearFontWeights = function() { "use strict";  };
Phaser.Text.prototype.clearFontWeights = function() {
    "use strict";
    this.fontWeights = [];
    this.addFontWeight();
};

Phaser.BitmapText.prototype.clearFormats = function() { "use strict";  };
Phaser.Text.prototype.clearFormats = function() {
    "use strict";
    this.clearColors();
    this.clearStrokeColors();
    this.clearFontStyles();
    this.clearFontWeights();
};
// #endregion FORMATS ( color, stroke, fontStyles, fontWeights )

// #region FORMATTED STRING
// Tags:
// - [color="..."]
// - [strokeColor="..."]
// - [fontStyle="..."]
// - [fontWeight= "..."]
// The tags has no close tag. It's from the current possition to the end of the string
//
// IMPORTANT: This funcionality INCOMPATIBLE "tabs" options. If you formated the text, you lose the "tabs" setup (if was defined).
//            You must clear all formats ( clearFormats ) to make tabs, works again.
//
Phaser.BitmapText.prototype.setFormattedString = function( ) { "use strict";  };
Phaser.Text.prototype.setFormattedString = function( str ) {
    "use strict";

    var self = this;

    // Regular expresion to get text formated.
    var TEXT_TEMPLATE = /\[color="([^"]*)"\]|\[strokeColor="([^"]*)"\]|\[fontStyle="([^"]*)"\]|\[fontWeight="([^"]*)"\]/g;
    var GROUP_NUMBER = {
        MATCHED_GROUP: 0,
        COLOR: 1,
        STROKE_COLOR: 2,
        FONT_STYLE: 3,
        FONT_WEIGHT: 4
    };
    var NUM_REG_EXP_GROUPS = _.size( GROUP_NUMBER );

    function getType( index ) {
        switch( index ) {
            case GROUP_NUMBER.COLOR:        return "Color";
            case GROUP_NUMBER.STROKE_COLOR: return "StrokeColor";
            case GROUP_NUMBER.FONT_STYLE:   return "FontStyle";
            case GROUP_NUMBER.FONT_WEIGHT:  return "FontWeight";
            default:                        return "";
        }
    }

    function getTypeAndValue( arrayMatch ) {
        if( arrayMatch.length >= NUM_REG_EXP_GROUPS ) { // 5 groups for the regular expresion
            for( var i = 1; i < arrayMatch.length; i++ ) { // 0 is the matched group
                var value = arrayMatch[ i ];
                if( value !== undefined ) {
                    var type = getType( i );
                    if( type ) {
                        return {
                            type: getType( i ),
                            value: arrayMatch[ i ]
                        };
                    }
                    console.warn( "setFormattedString: Invalid type:", type, "with i:", i, "arrayMatch:", arrayMatch );
                }
            }
        }
        return undefined;
    }

    function getNumberOfOccurrences( string, strToMatch ) {
        var numberOfOccurrences = 0;
        if( _.isString( string ) && _.isString( strToMatch ) ) {
            numberOfOccurrences = string.split( strToMatch ).length - 1;
        }
        return numberOfOccurrences;
    }

    function addFormats( indexesObj ) {

        self.clearFormats();

        // calls to addColor, addStrokeColor, addFontStyle and addFontWeight
        _.forEach( indexesObj, function( arrayIndex, type ) {
            _.forEach( arrayIndex, function( obj ) {
                self[ "add" + type ]( obj.value, obj.index );
            } );
        } );
    }

    if( _.isString( str ) ) {
        var finalStr = "";
        var currentIdx = 0;

        var indexesObj = {
            Color: [],
            StrokeColor: [],
            FontStyle: [],
            FontWeight: []
        };

        var numOfEOL = 0;
        var arrayMatch;
        while( ( arrayMatch = TEXT_TEMPLATE.exec( str ) ) !== null ) {
            var subString = str.substr( currentIdx, arrayMatch.index - currentIdx );
            finalStr += subString;
            currentIdx = TEXT_TEMPLATE.lastIndex;

            // Fixed the EOL Phaser's bug when don't use wordWrap (that works well)
            if( !self.wordWrap ) {
                numOfEOL += getNumberOfOccurrences( subString, "\n" );
                numOfEOL += getNumberOfOccurrences( subString, "\r" );
            }

            var typeValue = getTypeAndValue( arrayMatch );
            if( typeValue ) {
                indexesObj[ typeValue.type ].push( {
                    index: finalStr.length - numOfEOL,
                    value: typeValue.value
                } );
            }
        }
        finalStr += str.substr( currentIdx, str.length - currentIdx );

        addFormats( indexesObj );
        self.text = finalStr;
    }
    else {
        self.text = str;
    }
};
// #endregion FORMATTED STRING

// #region GRADIENTS
Phaser.BitmapText.prototype._createGradient = function() { "use strict";  };
Phaser.Text.prototype._createGradient = function( gdObj ) {
    "use strict";
    var self = this;

    if( self._hasGradient === false ) {
        return;
    }

    if( self._hasGradient === undefined ) {

        self._hasGradient = false;

        gdObj = gdObj || _.get( self, [ "data", "gdObj" ] );

        // object without gradients
        var hasGradient = _.has( gdObj, [ "webFont", "gradient" ] );
        if( !hasGradient ) {
            return;
        }

        var linearGradient = _.get( gdObj, [ "webFont", "gradient", "linear" ] );
        var radialGradient = _.get( gdObj, [ "webFont", "gradient", "radial" ] );
        var colorsMap = _.get( gdObj, [ "webFont", "gradient", "colors" ] );

        if( linearGradient && radialGradient ) {
            console.warn( "Multiple gradients for this label", gdObj, self );
            return;
        }
        if( !linearGradient && !radialGradient ) {
            console.warn( "Missing 'radial' or 'linear' gradient definition", gdObj, self );
            return;
        }
        if( !colorsMap ) {
            console.warn( "No colors to define a gradient for this label", gdObj, self );
            return;
        }

        self._linearGradient    = linearGradient;
        self._radialGradient    = radialGradient;
        self._colorsGradientMap = colorsMap;
        self._hasGradient       = true;

        self._updateGradient();

    }
};

Phaser.BitmapText.prototype._createLinearGradient = function() { "use strict";  };
Phaser.Text.prototype._createLinearGradient = function() {
    "use strict";

    var self = this;
    var linearGradientDefinition = self._linearGradient;

    var eGradientLinearType = {
        HORIZONTAL: "HORIZONTAL",
        VERTICAL: "VERTICAL",
        DIAGONAL1: "DIAGONAL1", // from top-left to bottom-right
        DIAGONAL2: "DIAGONAL2"  // from top-right to bottom-left
    };
    Object.freeze( eGradientLinearType );

    var getDefaultParams = function( phaserObj ) {
        return {
            x0: 0,
            y0: 0,
            x1: 0,
            y1: phaserObj.height
        };
    };

    var isValidGradientValues = function( gradientParams ) {
        return (
            _.isNumber( gradientParams.x0 ) && _.isNumber( gradientParams.y0 ) &&
            _.isNumber( gradientParams.x1 ) && _.isNumber( gradientParams.y1 ) );
    };

    var gradient;
    var params = getDefaultParams( self );

    switch( linearGradientDefinition ) {
        case eGradientLinearType.VERTICAL:
            break;
        case eGradientLinearType.HORIZONTAL:
            params.x1 = self.width;
            params.y1 = 0;
            break;
        case eGradientLinearType.DIAGONAL1:
            params.x1 = self.width;
            params.y1 = self.height;
            break;
        case eGradientLinearType.DIAGONAL2:
            params.x0 = self.width;
            params.y1 = self.height;
            break;
        default:
            if( _.isObject( linearGradientDefinition ) ) {
                params.x0 = _.get( linearGradientDefinition, [ "x0" ] );
                params.y0 = _.get( linearGradientDefinition, [ "y0" ] );
                params.x1 = _.get( linearGradientDefinition, [ "x1" ] );
                params.y1 = _.get( linearGradientDefinition, [ "y1" ] );
            }
            else {
                console.warn( "Invalid predefined linear gradient type or custom linear gradient:", linearGradientDefinition );
            }
            break;
    }

    if( !isValidGradientValues( params ) ) {
        console.warn( "Invalid gradient definition. We make a default radial gradient:",
            self, linearGradientDefinition, params.x0, params.y0, params.r0, params.x1, params.y1, params.r1 );
        params = getDefaultParams( self );
    }

    gradient = self.context.createLinearGradient( params.x0, params.y0, params.x1, params.y1 );

    return gradient;
};

Phaser.BitmapText.prototype._createRadialGradient = function() { "use strict";  };
Phaser.Text.prototype._createRadialGradient = function() {
    "use strict";

    var self = this;
    var radialGradientDefinition = self._radialGradient;

    var eGradientRadialType = {
        HORIZONTAL: "HORIZONTAL",
        VERTICAL: "VERTICAL"
    };
    Object.freeze( eGradientRadialType );

    var getDefaultParams = function( phaserObj ) {
        return {
            x0: phaserObj.width / 2,
            y0: phaserObj.height / 2,
            r0: 0,
            x1: phaserObj.width / 2,
            y1: phaserObj.height / 2,
            r1: phaserObj.width
        };
    };

    var isValidGradientValues = function( gradientParams ) {
        return (
            _.isNumber( gradientParams.x0 ) && _.isNumber( gradientParams.y0 ) && _.isNumber( gradientParams.r0 ) &&
            _.isNumber( gradientParams.x1 ) && _.isNumber( gradientParams.y1 ) && _.isNumber( gradientParams.r1 ) );
    };

    var gradient;
    var params = getDefaultParams( self );

    switch( radialGradientDefinition ) {
        case eGradientRadialType.HORIZONTAL:
            break;
        case eGradientRadialType.VERTICAL:
            params.r1 = self.height;
            break;
        default:
            if( _.isObject( radialGradientDefinition ) ) {
                params.x0 = _.get( radialGradientDefinition, [ "x0" ] );
                params.y0 = _.get( radialGradientDefinition, [ "y0" ] );
                params.r0 = _.get( radialGradientDefinition, [ "r0" ] );
                params.x1 = _.get( radialGradientDefinition, [ "x1" ] );
                params.y1 = _.get( radialGradientDefinition, [ "y1" ] );
                params.r1 = _.get( radialGradientDefinition, [ "r1" ] );
            }
            else {
                console.warn( "Invalid predefined radial gradient type or custom radial gradient", radialGradientDefinition );
            }
            break;
    }

    if( !isValidGradientValues( params ) ) {
        console.warn( "Invalid gradient definition. We make a default radial gradient",
            self, radialGradientDefinition, params.x0, params.y0, params.r0, params.x1, params.y1, params.r1 );
        params = getDefaultParams( self );
    }

    gradient = self.context.createRadialGradient( params.x0, params.y0, params.r0, params.x1, params.y1, params.r1 );

    return gradient;
};

Phaser.BitmapText.prototype._updateGradient = function() { "use strict";  };
Phaser.Text.prototype._updateGradient = function() {
    "use strict";

    var self = this;
    var linearGradient = self._linearGradient;
    var radialGradient = self._radialGradient;
    var colorsMap = self._colorsGradientMap;

    if( self._hasGradient ) {
        var gradient;
        if( linearGradient ) {
            gradient = self._createLinearGradient();
        }
        else if( radialGradient ) {
            gradient = self._createRadialGradient();
        }

        // apply colors
        _.forEach( colorsMap, function( color, offset ) {
            offset = Number( offset );
            if( offset >= 0 && offset <= 1 ) {
                gradient.addColorStop( offset, color );
            }
            else {
                console.warn( "Invalid offset color to apply for this label (may be between 0 and 1)", offset, self );
            }
        } );

        // finally, apply the gradient to the label
        self.fill = gradient;
    }
};

Phaser.Text.prototype._setText = Phaser.Text.prototype.setText;
Phaser.Text.prototype.setText = function() {
    "use strict";
    Phaser.Text.prototype._setText.apply( this, arguments );
    this._updateGradient();
};
// #endregion GRADIENTS

// #region UPDATE_DATA


// params:
// - currencyISO: el currencyISO a aplicar en caso de que haya valores monetarios.
// - localeTextsMap: mapa con todas las traducciones de los textos.
// - twTime: valor en milisengundos, que se realizará el efecto de contador, solo si aplica.
// Sobreescribe el valor que se indique en el GameDefinition, si viniera establecido.
// - playAudio: booleano que, en caso de realizar el efecto contador, reproducirá el sonido que esté
// configurado dentro del label, solo en caso de que el tipo sea money o moneyOrCredits.
// Sobreescribe el valor que se indique en el GameDefinition, si viniera establecido.
// eslint-disable-next-line no-implicit-globals, complexity
var _updateData = function( obj, params ) {
    "use strict";

    // eslint-disable-next-line no-invalid-this
    var self = this;
    var updateDataObj = _.get( self, [ "data", "gdObj", "parameters", "updateData" ] );
    if( updateDataObj ) {

        var hasTextSubtitutions = _.get( updateDataObj, [ "textTemplate" ] );
        var localeTextsMap = _.get( params, [ "localeTextsMap" ] );

        // ====================================== TEXT SUBSTITUTIONS  ===
        if( hasTextSubtitutions ) {
            var extraParams = {
                currencyISO: _.get( params, [ "currencyISO" ] ),
                localeTextsMap: localeTextsMap
            };
            var finalString = RFLabelUtils.getFormattedText( self, obj, extraParams );

            var isFormattedString = _.get( updateDataObj, [ "type" ] ) === "formattedString";
            if( isFormattedString ) {
                self.setFormattedString( finalString );
            }
            else {
                self.setString( finalString );
            }
        }

        // ====================================== ONE TEXT ===
        else {

            // Get the gdObj updateData information
            var currencyISO = _.get( updateDataObj, [ "currencyISO" ] );
            var twTime = _.get( updateDataObj, [ "twTime" ] );
            var bPlayAudio = _.get( updateDataObj, [ "playAudio" ] );

            // Override this information
            currencyISO = _.get( params, [ "currencyISO" ], currencyISO );
            twTime = _.get( params, [ "twTime" ], twTime );
            bPlayAudio = _.get( params, [ "playAudio" ], bPlayAudio );

            var path = _.get( updateDataObj, [ "path" ] );
            var defaultValue = _.get( updateDataObj, [ "defaultValue" ] );
            if( _.isString( defaultValue ) ) {
                defaultValue = RFLabelUtils.getReplacedText( defaultValue, localeTextsMap );
            }
            var value = _.get( obj, path, defaultValue );

            var type = _.get( updateDataObj, [ "type" ] );
            switch( type ) {
                case "number":
                    value = Number( value );
                    self.setString( value );
                    break;
                case "string":
                    value = String( value );
                    self.setString( value );
                    break;
                case "formattedString":
                    self.setFormattedString( value );
                    break;
                case "money":
                    value = Number( value );
                    self.setMoney( value, currencyISO, twTime, bPlayAudio );
                    break;
                case "moneyOrCredits":
                    value = Number( value );
                    if( _.isFunction( self.setMoneyOrCredits ) ) {
                        self.setMoneyOrCredits( value, currencyISO, twTime, bPlayAudio );
                    }
                    else {
                        self.setString( value );
                    }
                    break;
                default:
                    value = String( value );
                    self.setString( value );
                    break;
            }
        }


    }
};
Phaser.BitmapText.prototype.updateData = _updateData;
Phaser.Text.prototype.updateData = _updateData;
// #endregion UPDATE_DATA

//------------------------------------------------------
// Source: src/engine/gamecore/rf.gamecore.fsm.js
//------------------------------------------------------

/**
 * Definición de la máquina de estados usada para los juegos
 * @version 0.2.0
 * @constructor
 * @requires rf.utils.js (for isValidGame)
 * @requires lodash.js
 * @requires phaser.js
 * @requires phaser-plugin-behavior.min.js
 * CAMBIOS
 * 0.1.2 - Optimizados los accesos por lodash
 * 0.2.0 - Se añade un objeto de opciones y se eliminan dependencias externas.l
 */
// eslint-disable-next-line
function RFFsm() {
    "use strict";

    /**
     * Inicializa la máquina de estados.
     * @param {object} initData Parámetros de inicialización de la FSM
     * @param {object} initData.game El juego game de phaser.
     * @param {object} initData.game.behaviourPlugin El plugin de comportamientos cargado.
     * @param {object} initData.states Definición de los estados
     * @param {boolean} initData.debug Indica si trazamos los cambios de estado
     * @example
     *        initData = {
     *            game: this.game,
     *            name: "BaseGame",
     *            debug: true,
     *            states: {
     *              starting: {
     *                  onStart: this.startStarting
     *              },
     *              idle': {
     *                  onStart  : this.startIdle,
     *                  onUpdate : this.updateIdle
     *                  onExit   : this.exitIdle
     *              },
     *              spinning: {
     *                  onStart  : this.startSpinning,
     *                  onUpdate : null,
     *              },
     *            }
     *        }
     * @throws Si 'game' no es un objeto
     * @throws Si no está cargado el plugin de comportamiento en 'game.behaviorPlugin'
     * @throws Si no existen los estados en el initData
     */
    this.initialize = function( initData ) {
        if( !m_initialized ) {
            if( !( _.has( initData, [ "game" ] ) && isValidGame( initData.game ) ) ) {
                throw new Error( "FSM: 'game' must be an object" );
            }
            if( !_.has( initData.game, [ "behaviorPlugin" ] ) ) {
                throw new Error( "RFFsm.initialize: We need the Phaser 'game' behavior plugin loaded in 'game.behaviourPlugin' property. " +
                    "Ie.game.behaviorPlugin = game.plugins.add( Phaser.Plugin.Behavior );" );
            }
            if ( !( _.isObject( initData ) && _.has( initData, [ "states" ] ) ) ) {
                throw new Error( "RFFsm.initialize: Missing 'initData.states'" );
            }

            m_fsm = initData.game.add.sprite();
            initData.game.behaviorPlugin.enable( m_fsm );
            m_fsm.behaviors.set( "fsm", m_stateMachine, { states : initData.states } );

            this.setOptions( {
                debug: _.get( initData, [ "debug" ] ),
                name: _.get( initData, [ "name" ] ),
                fromStateColor: _.get( initData, [ "fromStateColor" ] ),
                separatorColor: _.get( initData, [ "separatorColor" ] ),
                toStateColor: _.get( initData, [ "toStateColor" ] ),
                descriptionColor: _.get( initData, [ "descriptionColor" ] )
            } );

            m_initialized = true;
        }

        return m_initialized;
    };

    var m_options = {
        debug: true,
        name: "FSM",
        fromStateColor: "color:blue;",
        separatorColor: "color:black",
        toStateColor: "color:white; background:green; font-weight:bold;",
        descriptionColor: "color:orange;"
    };
    this.setOptions = function( oOptions ) {
        m_options.debug = _.get( oOptions, [ "debug" ], m_options.debug );
        m_options.name = _.get( oOptions, [ "name" ], m_options.name );
        m_options.fromStateColor = _.get( oOptions, [ "fromStateColor" ], m_options.fromStateColor );
        m_options.separatorColor = _.get( oOptions, [ "separatorColor" ], m_options.separatorColor );
        m_options.toStateColor = _.get( oOptions, [ "toStateColor" ], m_options.toStateColor );
        m_options.descriptionColor = _.get( oOptions, [ "descriptionColor" ], m_options.descriptionColor );
    };

    /**
     * Obtiene el estado actual de la FSM
     * @returns {string} Devuelve el estado actual de la FSM
     * @throws Si la fsm no está inicializada
     */
    this.getState = function() {
        assertInitialized();
        return m_fsm.behaviors.get( "fsm" ).options.getState();
    };

    /**
     * Devuelve el tiempo transcurrido en el estado actual.
     * @returns {integer} Tiempo transcurrido en el estado actual.
     * @throws Si la fsm no está inicializada
     */
    this.getElapsedTime = function() {
        assertInitialized();
        return m_fsm.behaviors.get( "fsm" ).options.execTime;
    };

    /**
     * Realiza un cambio de estado
     * @param {string} newState El string que identifica el estado.
     * @param {object} params Los parámetros que se pasarán al inicio del cambio de estado (si fueran necesarios)
     * @param {string} Descripción a sacar por consola, del motivo del cambio de estado.
     * @throws Si la fsm no está inicializada
     */
    this.changeState = function( strNewState, oParams, strDescription ) {
        assertInitialized();
        m_fsm.behaviors.get( "fsm" ).options.changeState( strNewState, oParams, strDescription );
    };

    /**
     * Realiza un cambio de estado (alias de changeState)
     * @param {string} newState El string que identifica el estado.
     * @param {object} params Los parámetros que se pasarán al inicio del cambio de estado (si fueran necesarios)
     * @param {string} Descripción a sacar por consola, del motivo del cambio de estado.
     * @throws Si la fsm no está inicializada
     */
    this.setState = function( strNewState, oParams, strDescription ) {
        this.changeState( strNewState, oParams, strDescription );
    };

    var assertInitialized = function() {
        if( !m_initialized ) {
            throw new Error( _ERROR_MESSAGES.NOT_INITIALIZED );
        }
    };

    var m_stateMachine = {
        options: {
            _obj: null,
            _currentState: null,
            states: null,
            execTime: 0,
            traceChangeStates: true,
            changeState: function( state, params, description ) {
                if( state !== this._currentState ) {

                    if( !_.has( this.states, [ state ] ) ) {
                        throw new Error( m_options.name + ": Invalid state (" + state + ")" );
                    }

                    if( m_options.debug ) {

                        // In new Chrome versions ( current: 55.0.2883.87 m ), the console.trace expands itself
                        console.groupCollapsed(
                            m_options.name + " %c" + this._currentState + "%c >> %c" + state +
                            ( _.isString( description ) ? "%c (" + description + ")" : "%c" ),
                            m_options.fromStateColor, m_options.separatorColor, m_options.toStateColor, m_options.descriptionColor
                        );
                        console.trace();
                        console.groupEnd();
                    }

                    this.execTime = new Date().getTime();

                    // Exit the current state
                    if( this._currentState ) {
                        if( _.isFunction( this.states[ this._currentState ].onExit ) ) {
                            this.states[ this._currentState ].onExit( params );
                        }
                    }

                    // change the state
                    this._currentState = state;

                    // Start the new (current) state
                    if( _.isFunction( this.states[ this._currentState ].onStart ) ) {
                        this.states[ this._currentState ].onStart( params );
                    }

                }
            },
            getState: function() {
                return this._currentState;
            }
        },

        create: function( object, options ) {
            options._obj = object;
            options._lastTime = new Date().getTime();
            if( _.size( options.states ) < 1 ) {
                throw new Error( "No se ha especificado ningún estado." );
            }
        },

        update: function( object, options ) {
            if( options._currentState ) {
                if( _.isFunction( options.states[ options._currentState ].onUpdate ) ) {
                    options.states[ options._currentState ].onUpdate();
                }
            }
        }

    };

    var m_fsm = null;
    var m_initialized = false;

    var _ERROR_MESSAGES = {
        NOT_INITIALIZED : "RFFsm is not initialized"
    };
    Object.freeze( _ERROR_MESSAGES );

}

//------------------------------------------------------
// Source: src/engine/gamecore/utils/rf.gamecore.util.rectangle.js
//------------------------------------------------------

/**
 * Clase para el manejo de rects
 * @param {object} oRect
 * @param {integer} oRect.x
 * @param {integer} oRect.y
 * @param {integer} oRect.width
 * @param {integer} oRect.height
 */
// eslint-disable-next-line
function RFRectangle( oRect ) {
    "use strict";

    assertAllKeys( oRect, [ "x", "y", "width", "height" ] ); // RFRectangle: Invalid rect object

    this.x = oRect.x;
    this.y = oRect.y;
    this.width = oRect.width;
    this.height = oRect.height;
    this.center = {
        x: this.x + ( this.width / 2 ),
        y: this.y + ( this.height / 2 )
    };

}

//------------------------------------------------------
// Source: src/engine/gameaudio/rf.gameaudio.sound.js
//------------------------------------------------------

/**
 * @version 0.0.5
 * CAMBIOS:
 * 0.0.4 - Se añade soporte para modificar el volumen.
 * 0.0.5 - Optimizados los accesos por lodash
 */
// eslint-disable-next-line
function RFAudioSound( game ) {
    "use strict";

    /**
     * @param {array|string} sounds Array de sonidos o un key de sonido
     */
    this.setSounds = function( sounds ) {
        if( _.isArray( sounds ) ) {
            removeSounds();
            _.forEach( sounds, function( soundKey ) {
                if( !_.isEmpty( soundKey ) ) {
                    m_aSounds.push( m_game.add.sound( soundKey ) );
                }
                else {
                    console.error( "RFAudioSound::setSounds empty soundKey:", soundKey );
                }
            } );
        }
        else if( _.isString( sounds ) && !_.isEmpty( sounds ) ) {
            var soundKey = sounds;
            m_aSounds.push( m_game.add.sound( soundKey ) );
        }
        else {
            console.warn( "RFAudioSound::setSounds invalid sounds:", sounds );
        }
    };

    /**
     * Modifica las opciones de reproducción
     */
    var m_options = {
        random    : false,
        loopAtEnd : true,
        reverse   : false
    };
    this.setOptions = function( oOptions ) {
        m_options.random = _.get( oOptions, [ "random" ], m_options.random );
        m_options.loopAtEnd = _.get( oOptions, [ "loopAtEnd" ], m_options.loopAtEnd );

        if( _.has( oOptions, [ "reverse" ] ) ) {
            if( m_options.reverse !== oOptions.reverse ) {
                _.reverse( m_aSounds );
            }
            m_options.reverse = oOptions.reverse;
        }
    };

    /**
     * Reinicia el ciclo de reproducción
     */
    this.resetCycle = function( ) {
        m_currentIdx = 0;
    };

    /**
     * Reproduce en bucle el sonido actual e incrementa la posición para reproducir el siguiente
     * @param {string|integer=} soundId Índice a reproducir en caso de que no se quiera hacer una reproducción en
     *                          cíclica de los sonidos del array o la clave usada al crear los sonidos
     */
    this.playLooped = function( soundId ) {
        return playSingle( soundId, true );
    };

    /**
     * Reproduce el sonido actual e incrementa la posición para reproducir el siguiente
     * @param {string|integer=} soundId Índice a reproducir en caso de que no se quiera hacer una reproducción en
     *                          cíclica de los sonidos del array o la clave usada al crear los sonidos
     */
    this.play = function( soundId ) {
        return playSingle( soundId );
    };

    /**
     * Reproduce los todos los sonidos de la colección según el orden establecido en las opciones.
     * Cuando termina un sonido, reproduce el siguiente
     */
    this.playAll = function( soundId ) {
        stopAll();
        var sound = playSingle( soundId );
        if( sound ) {
            sound.onStop.add( onStopPlayNext );
        }
        return sound;
    };

    /**
     * Para la reproducción de un sonido, o de todos los indicados en setSounds
     * @param {string|integer=} soundId Índice a parar como índice o como clave. En caso de no especificar nada, se paran todos los sonidos.
     */
    this.stop = function( soundId ) {
        if( m_aSounds.length > 0 ) {
            if( soundId === undefined ) {
                stopAll();
            }
            else {
                var idx = getIndex( soundId );
                if( idx >= 0 ) {
                    m_aSounds[ idx ].stop();
                }
            }
        }
    };

    /**
     * Indica si se está reproducciendo algún audio de la secuencia
     */
    this.isPlaying = function() {
        var bPlaying = false;
        for( var i = 0; i < m_aSounds.length && !bPlaying; ++i ) {
            var sound = m_aSounds[ i ];
            bPlaying = sound.isPlaying;
        }
        return bPlaying;
    };

    /**
     * Establece el volumen para todos los sonidos de la secuencia
     * @param {number} volume Volumen a establecer (entre 0 y 1)
     * @param {string|integer=} soundId índice o clave del sonido a modificar su valor. En caso de no especificar nada, se modifican todos los sonidos
     */
    this.setVolume = function( volume, soundId ) {
        if( volume >= 0 && volume <= 1 ) {

            var idx = getIndex( soundId );
            if( idx >= 0 ) {
                m_aSounds[ idx ].volume = volume;
            }
            else {
                _.forEach( m_aSounds, function( sound ) {
                    sound.volume = volume;
                } );
            }
        }
        else {
            console.warn( "Volume must be between 0 and 1", volume );
        }
    };

    // TODO: Fix this
    // eslint-disable-next-line complexity
    var playSingle = function ( soundId, looped ) {
        if( m_aSounds.length > 0 ) {
            var idx = getIndex( soundId );
            if( idx < 0 ) {
                if( m_options.random && m_aSounds.length > 1 ) {

                    var canBeRepeated = false; // if we wants to parametrize in a future
                    if( !canBeRepeated ) {
                        idx = _.sample( _.without( _.range( m_aSounds.length ), m_currentIdx ) );
                    }
                    else {
                        idx = _.random( 0, m_aSounds.length - 1 );
                    }
                    m_currentIdx = idx;

                }
                else {
                    idx = m_currentIdx;
                    if( !m_options.loopAtEnd ) {

                        // TODO: Fix this
                        // eslint-disable-next-line max-depth
                        if( m_currentIdx + 1 >= m_aSounds.length ) {
                            m_currentIdx = m_aSounds.length - 2; // force stay in the last index
                        }
                    }
                    m_currentIdx = ( m_currentIdx + 1 ) % m_aSounds.length;

                }
            }
            if( idx >= 0 ) {
                if( _.isBoolean( looped ) && looped ) {
                    m_aSounds[ idx ].loop = true;
                }
                return m_aSounds[ idx ].play();
            }
        }
        return undefined;
    };

    /**
     * Para todos los sonidos de la coleccion
     */
    var stopAll = function() {
        _.forEach( m_aSounds, function( sound ) {
            sound.loop = false;
            sound.onStop.removeAll();
            sound.stop();
        } );
    };

    var getIndex = function( soundId ) {
        var idx = -1;
        if( _.isInteger( soundId ) ) {
            if( _.has( m_aSounds, [ soundId ] ) ) {
                idx = soundId;
            }
            else {
                console.warn( "Invalid idxSound: ", soundId );
            }
        }
        else if( _.isString( soundId ) ) {
            idx = _.findIndex( m_aSounds, function( o ) { return o.key === soundId; } );
        }
        return idx;
    };

    var onStopPlayNext = function() {
        self.playAll();
    };

    var removeSounds = function() {
        _.forEach( m_aSounds, function( sound ) {
            sound.onStop.removeAll();
            m_game.sound.remove( sound );
        } );
        m_aSounds = [];
        m_currentIdx = 0;
    };

    var constructor = function() {
        if( !isValidGame( m_game ) ) {
            throw new Error( "RFAudioSound: 'game' is not an object" );
        }

        m_game = game;
    };

    var m_game = game;
    var m_currentIdx = 0;
    var m_aSounds = [];
    var self = this;

    constructor();
}

//------------------------------------------------------
// Source: src/engine/gamegraphic/rf.gamegraphic.extendedwild.js
//------------------------------------------------------

/**
 * @version 0.1.0
 * CAMBIOS:
 * 0.1.0 - Se añaden los frames de la animación.
 */
// eslint-disable-next-line
function RFGraphicExtendedWild( game ) {
    "use strict";

    this.setRFExtendedWild = function( rfExtendedWild ) {

        if( !( rfExtendedWild instanceof RFExtendedWild ) ) {
            throw new Error( "Invalid rfExtendedWild" );
        }
        if( _.isEmpty( rfExtendedWild.key ) ) {
            throw new Error( "rfExtendedWild.key is not defined" );
        }

        m_rfExtendedWild = rfExtendedWild;

        m_spriteExtendedWild.loadTexture( rfExtendedWild.key );

        var animationKey = getAnimationKey();
        if( !m_spriteExtendedWild.animations.getAnimation( animationKey ) ) {
            var frames = _.get( rfExtendedWild, [ "frames" ] );
            frames = _.isArray( frames ) ? frames : null;
            m_spriteExtendedWild.animations.add( animationKey, frames );
        }

    };

    this.setVisible = function( bVisible ) {
        m_spriteExtendedWild.visible = bVisible;
    };

    this.setX = function( x ) {
        m_spriteExtendedWild.x = x;
    };

    this.setY = function( y ) {
        m_spriteExtendedWild.y = y;
    };

    this.setXY = function( x, y ) {
        m_spriteExtendedWild.x = x;
        m_spriteExtendedWild.y = y;
    };

    this.setAnchor = function( anchorX, anchorY ) {
        m_spriteExtendedWild.anchor.x = anchorX;
        m_spriteExtendedWild.anchor.y = anchorY;
    };

    this.getSprite = function() {
        return m_spriteExtendedWild;
    };

    this.getPosition = function() {
        return {
            x: m_spriteExtendedWild.x,
            y: m_spriteExtendedWild.y
        };
    };

    this.startAnimation = function( bLoop, fCallback ) {
        this.stopAnimation();
        var anim = m_spriteExtendedWild.animations.play( getAnimationKey(), m_rfExtendedWild.fps, bLoop );
        if( _.isFunction( fCallback ) ) {
            anim.onComplete.removeAll( this ); // remove previous callback funtions
            anim.onComplete.addOnce( fCallback, this );
        }
    };

    this.stopAnimation = function() {
        m_spriteExtendedWild.animations.stop();
    };


    var getAnimationKey = function() {
        return WILD_ANIMATION_KEY + m_rfExtendedWild.key;
    };

    var constructor = function( gameObj ) {

        if( !isValidGame( gameObj ) ) {
            throw new Error( "RFGraphicExtendedWild: 'game' is not a Phaser.Game" );
        }

        m_game = gameObj;
        m_spriteExtendedWild = m_game.add.sprite();
    };

    var m_game               = null;
    var m_rfExtendedWild     = null;
    var m_spriteExtendedWild = null;

    var WILD_ANIMATION_KEY = "__WILD_ANIMATION_KEY_";

    constructor( game );
}

//------------------------------------------------------
// Source: src/engine/gamegraphic/rf.gamegraphic.line.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFGraphicLine( game ) {
    "use strict";
    this.setRFLine = function( rfLine ) {

        if( !( rfLine instanceof RFLine ) ) { throw new Error( "RFGraphicLine::setRFLine rfLine is not an RFLine" ); }

        m_rfLine = rfLine;

        // if key is null, "loadTexture" function erase the texture in the screen
        var key = _.isEmpty( rfLine.line.key ) ? null : rfLine.line.key;
        var atlas = _.isEmpty( rfLine.line.atlas ) ? null : rfLine.line.atlas;

        // Line image
        if( _.isEmpty( atlas ) ) {
            m_SpriteLine.loadTexture( key );
        }
        else {
            m_SpriteLine.loadTexture( atlas, key );
        }

        this.setX( m_rfLine.line.rect.x );
        this.setY( m_rfLine.line.rect.y );
    };

    this.deleteMask = function() {
        m_SpriteLine.mask = null;
    };

    this.setMask = function( oMask ) {
        m_SpriteLine.mask = oMask;
    };

    this.setVisible = function( bVisible ) {
        m_SpriteLine.visible = bVisible;
    };

    this.setX = function( x ) {
        m_SpriteLine.x = x;
    };

    this.setY = function( y ) {
        m_SpriteLine.y = y;
    };

    this.setXY = function( x, y ) {
        m_SpriteLine.x = x;
        m_SpriteLine.y = y;
    };

    this.setAnchor = function( anchorX, anchorY ) {
        m_SpriteLine.anchor.x = anchorX;
        m_SpriteLine.anchor.y = anchorY;
    };

    // only for test proposes
    this._getSprite = function() {
        return m_SpriteLine;
    };

    var constructor = function( gameObj ) {
        if( !isValidGame( gameObj ) ) { throw new Error( "RFGraphicLine: 'game' is not an Phaser Game" ); }

        m_game = gameObj;
        m_SpriteLine = m_game.add.sprite();
    };

    var m_game        = null;
    var m_rfLine      = null;
    var m_SpriteLine  = null;

    constructor( game );
}

//------------------------------------------------------
// Source: src/engine/gamegraphic/rf.gamegraphic.mask.js
//------------------------------------------------------

/**
 * Clase para el manejo máscaras
 * @param {object} game Objeto Game the Phaser
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFMask( game ) {
    "use strict";

    this.getMask = function() {
        return m_mask;
    };

    this.addRectangles = function( aRectangles ) {
        _.forEach( aRectangles, function( rectangle ) {
            self.addRectangle( rectangle );
        } );
    };

    this.addRectangle = function( oRect ) {
        addToMask( oRect );
    };

    this.addSprite = function( oSprite ) {
        if( !_.isObject( oSprite ) ) { throw new Error( "RFMask: oSprite is not an object" ); }
        addToMask( { x: oSprite.x, y: oSprite.y, width: oSprite.width, height: oSprite.height } );
    };

    this.clear = function() {
        if( m_mask ) {
            m_mask.destroy();
            m_mask = null;
        }
    };

    var addToMask = function( oRect ) {

        assertAllKeys( oRect, [ "x", "y", "width", "height" ] );
        if( !( _.isNumber( oRect.x ) && _.isNumber( oRect.y ) && _.isNumber( oRect.width ) && _.isNumber( oRect.height ) ) ) {
            throw new Error( "RFMask::addToMask invalid parameters" );
        }

        if( oRect.width > 0 && oRect.height > 0 ) {
            if( !m_mask ) {
                m_mask = m_game.add.graphics( 0, 0 );
            }
            var HEX_WHITE = 0xffffff;
            m_mask.beginFill( HEX_WHITE, 0 );
            m_mask.drawRect( oRect.x, oRect.y, oRect.width, oRect.height );
            m_mask.endFill();
        }
    };

    var m_game = game;
    var m_mask = null;
    var self = this;
}

//------------------------------------------------------
// Source: src/engine/gamegraphic/rf.gamegraphic.multiplier.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFGraphicMultiplier( game ) {
    "use strict";

    this.setRFMultiplier = function( rfMultiplier ) {

        if( !( rfMultiplier instanceof RFMultiplier ) ) {
            throw new Error( "Invalid rfMultiplier" );
        }
        if( _.isEmpty( rfMultiplier.key ) ) {
            throw new Error( "rfMultiplier.key is not defined" );
        }

        m_rfMultiplier = rfMultiplier;

        m_spriteMultiplier.loadTexture( rfMultiplier.key );

        var animationKey = getAnimationKey();
        if( !m_spriteMultiplier.animations.getAnimation( animationKey ) ) {

            var frames = _.get( rfMultiplier, [ "frames" ] );
            frames = _.isArray( frames ) ? frames : null;
            m_spriteMultiplier.animations.add( animationKey, frames );
        }

    };

    this.setVisible = function( bVisible ) {
        m_spriteMultiplier.visible = bVisible;
    };

    this.setX = function( x ) {
        m_spriteMultiplier.x = x;
    };

    this.setY = function( y ) {
        m_spriteMultiplier.y = y;
    };

    this.setXY = function( x, y ) {
        m_spriteMultiplier.x = x;
        m_spriteMultiplier.y = y;
    };

    this.setAnchor = function( anchorX, anchorY ) {
        m_spriteMultiplier.anchor.x = anchorX;
        m_spriteMultiplier.anchor.y = anchorY;
    };

    this.getSprite = function() {
        return m_spriteMultiplier;
    };

    this.getPosition = function() {
        return {
            x: m_spriteMultiplier.x,
            y: m_spriteMultiplier.y
        };
    };

    this.startAnimation = function( bLoop, fCallback ) {
        this.stopAnimation();
        var anim = m_spriteMultiplier.animations.play( getAnimationKey(), m_rfMultiplier.fps, bLoop );
        if( _.isFunction( fCallback ) ) {
            anim.onComplete.removeAll( this ); // remove previous callback funtions
            anim.onComplete.addOnce( fCallback, this );
        }
    };

    this.stopAnimation = function() {
        m_spriteMultiplier.animations.stop();
    };


    var getAnimationKey = function() {
        return MULTIPLIER_ANIMATION_KEY + m_rfMultiplier.key;
    };

    var constructor = function( gameObj ) {

        if( !isValidGame( gameObj ) ) {
            throw new Error( "RFGraphicMultiplier: 'game' is not a Phaser.Game" );
        }

        m_game = gameObj;
        m_spriteMultiplier = m_game.add.sprite();
    };

    var m_game = null;
    var m_rfMultiplier = null;
    var m_spriteMultiplier = null;

    var MULTIPLIER_ANIMATION_KEY = "__MULTIPLIER_ANIMATION_KEY_";

    constructor( game );
}

//------------------------------------------------------
// Source: src/engine/gamegraphic/rf.gamegraphic.paybox.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFGraphicPaybox( game ) {

    "use strict";

    this.setRFLine = function( rfLine ) {

        if( !( rfLine instanceof RFLine ) ) { throw new Error( "RFGraphicLine::setRFLine rfLine is not an RFLine" ); }

        // if key is null, "loadTexture" function erase the texture in the screen
        var key = _.isEmpty( rfLine.payBox.key ) ? null : rfLine.payBox.key;
        var atlas = _.isEmpty( rfLine.payBox.atlas ) ? null : rfLine.payBox.atlas;

        m_rfLine = rfLine;

        // Paybox image
        if( _.isEmpty( atlas ) ) {
            m_SpritePaybox.loadTexture( key );
        }
        else {
            m_SpritePaybox.loadTexture( atlas, key );
        }

    };

    this.setVisible = function( bVisible ) {
        m_SpritePaybox.visible = bVisible;
    };

    this.setX = function( x ) {
        m_SpritePaybox.x = x;
    };

    this.setY = function( y ) {
        m_SpritePaybox.y = y;
    };

    this.setXY = function( x, y ) {
        m_SpritePaybox.x = x;
        m_SpritePaybox.y = y;
    };

    this.setAnchor = function( anchorX, anchorY ) {
        m_SpritePaybox.anchor.x = anchorX;
        m_SpritePaybox.anchor.y = anchorY;
    };

    this.getSprite = function() {
        return m_SpritePaybox;
    };

    var constructor = function( gameObj ) {
        if( !isValidGame( gameObj ) ) {
            throw new Error( "RFGraphicPaybox: 'game' is not an object" );
        }

        m_game = gameObj;
        m_SpritePaybox = m_game.add.sprite();
    };

    var m_game          = null;
    var m_rfLine        = null;  // eslint-disable-line
    var m_SpritePaybox  = null;

    constructor( game );
}

//------------------------------------------------------
// Source: src/engine/gamegraphic/rf.gamegraphic.reelcolumn.js
//------------------------------------------------------

/**
 * @requires rf.gamecore.util.rectangle
 * @requires rf.gamegraphic.symbol
 * @requires rf.comp.reel
 * @version 0.4.0
 * CAMBIOS:
 * 0.1.4 - Optimizados los accesos por lodash
 * 0.2.0 - Se añade el grupo del multiplicador y métodos para reproducir su animación.
 * 0.3.0 - Añade el gdObj al sprite del símbolo para que slo2maker permita editarlo.
 * 0.4.0 - Modifica la inicialización de la estela de la emoción para que coja valores por defecto
 *       - Arreglado para que ahora la estela de la emoción haga correctamente el Fade In / OUT
 */
// eslint-disable-next-line
function RFGraphicReelColumn( id, game ) {
    "use strict";

    /**
     * @param {object} initData Definición de rodillos, que viene en el GameDefinition
     */
    // TODO: Fix this
    /* eslint-disable complexity, max-statements */
    this.initialize = function( initData ) {
        if( !_.isObject( initData ) ) { throw new Error( "RFGraphicReelColumn::initialize missing initData" ); }
        assertAllKeys( initData, [ "blur", "symbols", "payBoxesOverSymbols" ] );

        // Create the reels
        var oSymbols = initData.symbols;

        m_oExtendedWild.hasExtendedWild = _.has( initData, [ "extendedWild" ] );
        m_oExtendedWild.group = m_game.add.group();
        m_oExtendedWild.group.name = "m_oExtendedWild.group" + ( m_oExtendedWild.hasExtendedWild ? "" : "_NO_EXTENDED" );
        m_oExtendedWild.group.visible = false;

        m_options.prizeHypeEnable = ( _.has( initData, [ "prizeHypeEnable" ] ) ? initData.prizeHypeEnable : m_options.prizeHypeEnable );

        var payBoxesGroup = m_game.add.group();
        payBoxesGroup.name = "payBoxesGroup";

        var multipliersGroup = m_game.add.group();
        multipliersGroup.name = "multipliersGroup";

        _.forEach( _.keysOrdered( oSymbols ), function( keySymbol ) {
            var oSymbol = oSymbols[ keySymbol ];

            assertAllKeys( oSymbol, [ "rect" ] );
            var rectangle = new RFRectangle( oSymbol.rect );

            var rfGraphicSymbol = new RFGraphicSymbol( m_game );
            rfGraphicSymbol.setAnchor( 0.5, 0.5 );
            rfGraphicSymbol.setXY( rectangle.center.x, rectangle.center.y );
            m_reelColumn.push( rfGraphicSymbol );
            _.set( rfGraphicSymbol.getSprite(), [ "data", "gdObj" ], oSymbol );

            var rfGraphicPaybox = new RFGraphicPaybox( m_game );
            rfGraphicPaybox.setAnchor( 0.5, 0.5 );
            rfGraphicPaybox.setXY( rectangle.center.x, rectangle.center.y );
            m_payBoxes.push( rfGraphicPaybox );
            payBoxesGroup.add( rfGraphicPaybox.getSprite() );

            var rfGraphicMultiplier = new RFGraphicMultiplier( m_game );
            rfGraphicMultiplier.setAnchor( 0.5, 0.5 );
            rfGraphicMultiplier.setXY( rectangle.center.x, rectangle.center.y );
            m_multipliers.push( rfGraphicMultiplier );
            multipliersGroup.add( rfGraphicMultiplier.getSprite() );

            if( m_oExtendedWild.hasExtendedWild ) {
                var rfGraphicPayboxExtendedWild = new RFGraphicPaybox( m_game );
                rfGraphicPayboxExtendedWild.setAnchor( 0.5, 0.5 );
                rfGraphicPayboxExtendedWild.setXY( rectangle.center.x, rectangle.center.y );
                m_oExtendedWild.payboxes.push( rfGraphicPayboxExtendedWild );
                m_oExtendedWild.group.add( rfGraphicPayboxExtendedWild.getSprite() );
            }

        } );

        m_numSymbols = _.size( oSymbols );

        if( m_oExtendedWild.hasExtendedWild ) {
            // create the extended wild
            m_oExtendedWild.graphicObject = new RFGraphicExtendedWild( m_game );
            m_oExtendedWild.graphicObject.setXY( initData.extendedWild.rect.x, initData.extendedWild.rect.y );

            // add extended wild to bottom (payboxes are over it).
            m_oExtendedWild.group.addChildAt( m_oExtendedWild.graphicObject.getSprite(), 0 );
        }

        m_options.isDynamic = _.get( initData, "isDynamic", false );
        if( !m_options.isDynamic ) {
            // Add Blur and component RFReel to the reel
            var blurSprite = m_game.add.sprite( initData.blur.rect.x, initData.blur.rect.y, initData.blur.animation.key );

            // Set the blur's scale
            var frameData = m_game.cache.getFrameData( initData.blur.animation.key );
            var frameWidth = frameData.getFrame( 0 ).width;
            var frameHeight = frameData.getFrame( 0 ).height;
            var scaleX = initData.blur.rect.width / frameWidth;
            var scaleY = initData.blur.rect.height / frameHeight;
            var isScaled = _.isNumber( scaleX ) && _.isNumber( scaleY ) && ( scaleX !== 1 || scaleY !== 1 ) && scaleX !== 0 && scaleY !== 0;
            if( isScaled ) {
                console.info( "Blur scaled to x=" + scaleX + ", y=" + scaleY );
                blurSprite.scale.setTo( scaleX, scaleY );
            }

            m_reelColumn.component = m_game.add.sprite();
            m_game.behaviorPlugin.enable( m_reelColumn.component );
            m_reelColumn.component.behaviors.set( "reelComp", RFReelComp, {
                game: m_game,
                blur: blurSprite,
                frames: initData.blur.animation.frames,
                blurFPS: initData.blur.animation.fps,
                symbols: m_reelColumn
                // xxx audioStop: initData.stopSound.soundKey
            } );
        }
        else {
            m_reelColumn.component = m_game.add.sprite();
            m_game.behaviorPlugin.enable( m_reelColumn.component );
            m_reelColumn.component.behaviors.set( "reelComp", RFDynamicReelComp, {
                game: m_game,
                symbols: m_reelColumn,
                stopAudio: ( _.has( initData, [ "sounds", "reelStop" ] ) ? initData.sounds.reelStop : undefined )
            } );
        }

        m_reelComponent = m_reelColumn.component.behaviors.get( "reelComp" );

        // Set prizeHypeAnimation
        if( _.has( initData, [ "prizeHypeAnimation" ] ) ) {
            var m_prizeHypeAnims_data = _.get( initData, [ "prizeHypeAnimation" ], m_prizeHypeAnims_data );
            if( m_prizeHypeAnims_data ) {
                // Inicialización de variables, si no siempre un valor por defecto.
                var rectPrizeHypeAnimX = _.get( m_prizeHypeAnims_data, [ "rect", "x" ], DEFAULTS.PRIZEHYPE_RECT_X );
                var rectPrizeHypeAnimY = _.get( m_prizeHypeAnims_data, [ "rect", "y" ], DEFAULTS.PRIZEHYPE_RECT_Y );
                var scalePrizeHypeAnimX = _.get( m_prizeHypeAnims_data, [ "scale", "x" ], DEFAULTS.PRIZEHYPE_SCALE_X );
                var scalePrizeHypeAnimY = _.get( m_prizeHypeAnims_data, [ "scale", "y" ], DEFAULTS.PRIZEHYPE_SCALE_Y );
                var keyAtlasPrizeHypeAnim = _.get( m_prizeHypeAnims_data, [ "atlas" ], DEFAULTS.PRIZEHYPE_ATLAS );
                var namePrizeHypeAnim = _.get( m_prizeHypeAnims_data, [ "name" ], DEFAULTS.PRIZEHYPE_NAME );
                var framesPrizeHypeAnim = _.get( m_prizeHypeAnims_data, [ "frames" ], DEFAULTS.PRIZEHYPE_FRAMES );
                var fpsPrizeHypeAnim = _.get( m_prizeHypeAnims_data, [ "fps" ], DEFAULTS.PRIZEHYPE_FPS );
                var loopPrizeHypeAnim = _.get( m_prizeHypeAnims_data, [ "loop" ], DEFAULTS.PRIZEHYPE_LOOP );
                var visiblePrizeHypeAnim = _.get( m_prizeHypeAnims_data, [ "visible" ], DEFAULTS.PRIZEHYPE_VISIBLE );

                m_prizeHypeAnim = m_game.add.sprite( rectPrizeHypeAnimX, rectPrizeHypeAnimY, keyAtlasPrizeHypeAnim );
                m_prizeHypeAnim.scale.set( scalePrizeHypeAnimX, scalePrizeHypeAnimY );
                m_prizeHypeAnim.animations.add( namePrizeHypeAnim, framesPrizeHypeAnim,
                    fpsPrizeHypeAnim, loopPrizeHypeAnim );
                m_prizeHypeAnim.visible = visiblePrizeHypeAnim;
            }
        }
        else {
            console.warn( "RFGraphicReelColumn::Initialize NOT prizeHype Animations found" );
        }

        // Layering (por orden de abajo a arriba).
        //
        // A) Si payBoxesOverSymbols === true:
        //     1 - Símbolos
        //     2 - Multiplicadores
        //     3 - Payboxes
        //     4 - Grupo con Wild extendido
        //         4.1 - Wild extendido
        //         4.2 - Paybox (otra instancia)
        //     5 - prizeHypeAnim (estela de emocion)
        // B) Si payBoxesOverSymbols === false:
        //     1 - Payboxes
        //     2 - Símbolos
        //     3 - Multiplicadores
        //     4 - Grupo con Wild extendido
        //         4.1 - Wild extendido
        //         4.2 - Paybox (otra instancia)
        //     5 - prizeHypeAnim (estela de emocion)

        var graphicColumnGroup = m_game.add.group();
        graphicColumnGroup.name = "graphicColumnGroup";

        var reelGroup = m_reelComponent.getReelGroup();

        reelGroup.name = "reelGroup";
        if( initData.payBoxesOverSymbols ) {
            graphicColumnGroup.add( reelGroup );
            graphicColumnGroup.add( multipliersGroup );
            graphicColumnGroup.add( payBoxesGroup );
        }
        else {
            graphicColumnGroup.add( payBoxesGroup );
            graphicColumnGroup.add( reelGroup );
            graphicColumnGroup.add( multipliersGroup );
        }
        if( m_oExtendedWild.hasExtendedWild ) {
            graphicColumnGroup.add( m_oExtendedWild.group );
        }

        // create the mask for the reel column
        var mask = new RFMask( m_game );
        var maskRect;
        if( _.has( initData, [ "mask", "rect" ] ) ) {
            maskRect = initData.mask.rect;
        }
        else {
            maskRect = initData.blur.rect;
            console.info( "No mask.rect in initData for this RFGraphicReelColumn", initData, "Use the rect of the blur" );
        }
        mask.addRectangle( maskRect );
        graphicColumnGroup.mask = mask.getMask();

        // Init sounds
        if( _.has( initData, [ "sounds", "reelStart" ] ) ) {
            m_sounds.start.setSounds( initData.sounds.reelStart );
        }
        else {
            console.info( "RFGraphicReelColumn::initialize no sounds.reelStart" );
        }

        if( _.has( initData, [ "sounds", "reelStop" ] ) ) {
            m_sounds.stop.setSounds( initData.sounds.reelStop );
        }
        else {
            console.info( "RFGraphicReelColumn::initialize no sounds.reelStop" );
        }

        if( m_options.isDynamic ) {
            if( _.has( initData, [ "sounds", "prizeHype" ] ) ) {
                m_sounds.prizeHype.setSounds( initData.sounds.prizeHype );
            }
            else {
                console.info( "%c RFGraphicReelColumn::initialize no sounds.prizeHype. ", RFDebugUtils.getConsoleColors().REELS );
            }
        }
    };
    /* eslint-enable complexity, max-statements */

    /**
     * Modifica las opciones de giro de rodillos
     * @param {object} options Objeto con las opciones a modificar
     * @param {integer=} options.msStopReel=1000 Milisegundos que tardará la en parar cada rodillo
     * @param {integer=} options.yBounceOffset=30 Offset de desplazamiento del rodillo en el rebote
     * @param {integer=} options.msForReelStartSpin Time offset for reel start.
     * @param {integer=} options.offsetReelSpinSpeed Speed offset for reel rotation speed. Makes all reels rotate at a different pase.
     * @param {integer=} options.reelSpinSpeed The speed at which the reels rotate.
     * @param {object=} options.symbolWeights The different weight values and collections for the symbols that participate in the dynamic reel.
     * @param {collection=} options.prizeHype Collection of symbols that trigger prize hype effect on the reels.
     * @param {integer=} options.prizeHypeDelayMultiplier The duration of the prize hype effect when applied.
     *
     * @param {integer=} options.prizeHypeDelayToStopAnim  to make a delay to synchronize the animation when the real stops.
     */
    // eslint-disable-next-line
    this.setOptions = function( options ) {
        /* eslint-disable max-len */
        m_options.msStopReel = _.has( options, [ "msStopReel" ] ) && _.isInteger( options.msStopReel ) ? options.msStopReel : m_options.msStopReel;
        m_options.yBounceOffset = _.has( options, [ "yBounceOffset" ] ) && _.isInteger( options.msStopReel ) ? options.yBounceOffset : m_options.yBounceOffset;
        m_options.playSounds = _.has( options, [ "playSounds" ] ) && _.isBoolean( options.playSounds ) ? options.playSounds : m_options.playSounds;

        if( m_options.isDynamic ) {
            m_options.msForReelStartSpin = _.has( options, [ "msForReelStartSpin" ] ) && _.isInteger( options.msForReelStartSpin ) ? options.msForReelStartSpin : m_options.msForReelStartSpin;
            m_options.offsetReelSpinSpeed = _.has( options, [ "offsetReelSpinSpeed" ] ) && _.isInteger( options.offsetReelSpinSpeed ) ? options.offsetReelSpinSpeed : m_options.offsetReelSpinSpeed;
            m_options.reelSpinSpeed = _.has( options, [ "reelSpinSpeed" ] ) && _.isInteger( options.reelSpinSpeed ) ? options.reelSpinSpeed : m_options.reelSpinSpeed;

            m_options.symbolWeights = ( _.has( options, [ "symbolWeights" ] ) && options.symbolWeights !== null ) ? options.symbolWeights[ m_id ] : m_options.symbolWeights;
            m_options.prizeHype = ( _.has( options, [ "prizeHype" ] ) && options.prizeHype !== null ) ? options.prizeHype : m_options.prizeHype;
            m_options.prizeHypeDelayMultiplier = ( _.has( options, [ "prizeHypeDelayMultiplier" ] ) && options.prizeHypeDelayMultiplier !== null ) ? options.prizeHypeDelayMultiplier : m_options.prizeHypeDelayMultiplier;

            m_reelComponent.setSymbolWeights( m_options.symbolWeights );
        }

        // For prizeHypeAnim
        m_options.prizeHypeDelayToStopAnim = ( _.has( options, [ "prizeHypeDelayToStopAnim" ] ) && options.prizeHypeDelayToStopAnim !== null ) ? options.prizeHypeDelayToStopAnim : m_options.prizeHypeDelayToStopAnim;
        m_options.prizeHypeFadeAnim = ( _.has( options, [ "prizeHypeFadeAnim" ] ) && options.prizeHypeFadeAnim !== null ) ? options.prizeHypeFadeAnim : m_options.prizeHypeFadeAnim;

        /* eslint-enable max-len */
    };

    /**
     * Cambia los símbolos por los pasados en rfSymbolsArray. Útil para inicializar los rodillos con un valor.<br/>
     * <b>IMPORTANTE:</b> Si los rodillos están girando, los para de manera abrupta.
     * @param {Array} rfSymbolsArray Array con los RFSymbols a pintar.
     */
    this.setReelColumn = function( rfSymbolsArray ) {
        this.forceStopReel();
        updateRFGraphicSymbolsArray( rfSymbolsArray );
    };

    /**
     * Realiza el giro de rodillos
     */
    this.startSpin = function() {

        playSound( m_sounds.start );

        if( m_options.isDynamic ) {
            m_reelComponent.options.id = m_id;
            m_reelComponent.options.startSpinOffset = m_options.msForReelStartSpin;
            m_reelComponent.options.spinSpeedOffset = m_options.offsetReelSpinSpeed;
            m_reelComponent.options.spinSpeed = m_options.reelSpinSpeed;

            m_reelComponent.options.playSounds = m_options.playSounds;
            m_reelComponent.options.stopSound = m_sounds.stop;
        }

        m_reelComponent.spinStart();

        m_isSpinning = true;
        m_isStopping = false;
    };

    /**
     * Realiza una parada de todos los rodillos y presenta la escena final
     * @param {object=} rfSymbolsArray Array con los RFSymbols a pintar en la parada de los rodillos. Si no viene, deja los últimos iconos pintados
     * @param {function=} fOnStop Función que se llama al terminar la parada de rodillos
     */
    this.stopSpin = function( rfSymbolsArray, fOnStop ) {

        m_isSpinning = false;
        m_isStopping = true;

        // stop spin for the component
        m_reelComponent.options.bounceSumY = m_options.yBounceOffset;
        m_reelComponent.options.bounceTime = m_options.msStopReel;

        if( !_.isEmpty( rfSymbolsArray ) ) {
            updateRFGraphicSymbolsArray( rfSymbolsArray );
        }

        if( !m_options.isDynamic ) {
            playSound( m_sounds.stop );
        }

        stopReel( function() {
            m_isStopping = false;
            if( _.isFunction( fOnStop ) ) {
                fOnStop();
            }
        }, rfSymbolsArray );
    };

    /**
     * Realiza una parada abrupta (sin efecto rebote)
     */
    this.forceStopReel = function() {
        _forceStopReel();
    };

    /**
     * Muestra la animación de premio del símbolo indicado en el índice
     * @param {RFLine} rfLine RFLine que contiene la información de la línea a mostrar
     * @param {integer} idxWinPosition El índice con la posición dentro de la columna, que se ha de animar.
     * @param {function} fCallback Función que se llamará cuando se hayan terminado de animar todos los iconos
     */
    this.startWinPrizeAnimation = function( rfLine, idxWinPosition, fCallback ) {
        if( !_.isInteger( idxWinPosition ) ) { throw new Error( "RFGraphicReelColumn::startWinPrize aWinPosition is not an integer" ); }
        if( idxWinPosition >= m_numSymbols ) { throw new Error( "RFGraphicReelColumn::startWinPrize idxWinPosition has invalid length" ); }

        var payBox = m_payBoxes[ idxWinPosition ];
        payBox.setRFLine( rfLine );
        payBox.setVisible( true );

        if( m_oExtendedWild.hasExtendedWild ) {
            var payBoxExtendedWild = m_oExtendedWild.payboxes[ idxWinPosition ];
            payBoxExtendedWild.setRFLine( rfLine );
            payBoxExtendedWild.setVisible( true );
        }

        stopAllSounds();

        var rfGraphicSymbol = m_reelColumn[ idxWinPosition ];
        rfGraphicSymbol.startWinAnimation( false, function() {

            // Nota: En la versión 0.1.0 la visibilidad de los payboxes se controlaba aquí, pero pudiera
            // ser que hubiera animaciones asociadas a la línea ganadora con diferente duración
            // (por ejemplo en carga dinámica, hay tweens que tardan menos). Si al finalizar pusiéramos invisible el paybox,
            // se ocultaría mientras que los otros símbolos asociados por la línea ganadora, se mostraban.
            // Para evitar esto, se hace que "reels", pare todas las animaciones vía "stopAnimations", que oculta todos
            // los payboxes (además de finalizar las animaciones), al terminar las animaciones de todas los
            // símbolos asociados a la línea ganadora.

            if( _.isFunction( fCallback ) ) {
                fCallback();
            }
        } );
    };

    /**
     * Muestra la animación del wild extendido pasado
     * @param {RFExtendedWild} rfextendedWild RFExtendedWild que se animará
     * @param {function} fCallback Función que se llamará cuando se haya terminado de animar el wild extendido
     */
    this.startMultiplierAnimation = function( rfMultiplier, idxWinPosition, fCallback ) {
        /* eslint-disable max-len */
        if( !( rfMultiplier instanceof RFMultiplier ) ) { throw new Error( "RFGraphicReelColumn::startMultiplierAnimation rfMultiplier is not a RFMultiplier" ); }
        /* eslint-enable max-len */

        var multiplier = m_multipliers[ idxWinPosition ];
        multiplier.setRFMultiplier( rfMultiplier );
        multiplier.setVisible( true );
        multiplier.startAnimation( false, fCallback );
    };

    /**
     * Muestra la animación del wild extendido pasado
     * @param {RFExtendedWild} rfextendedWild RFExtendedWild que se animará
     * @param {function} fCallback Función que se llamará cuando se haya terminado de animar el wild extendido
     */
    this.startExtendedWildAnimation = function( rfExtendedWild, fCallback ) {
        /* eslint-disable max-len */
        if( !( rfExtendedWild instanceof RFExtendedWild ) ) { throw new Error( "RFGraphicReelColumn::startExtendedWildAnimation rfExtendedWild is not a RFExtendedWild" ); }
        /* eslint-enable max-len */

        if( m_oExtendedWild.hasExtendedWild ) {
            m_oExtendedWild.group.visible = true;
            m_oExtendedWild.graphicObject.setRFExtendedWild( rfExtendedWild );
            m_oExtendedWild.graphicObject.startAnimation( false, fCallback );
        }
    };

    /**
     * Muestra la animación de reclamo de los iconos indicados en el vector de posiciones
     * @param {RFLine} rfLine RFLine que contiene la información de la línea a mostrar
     * @param {integer} idxWinPosition El índice con la posición dentro de la columna, que se ha de animar.
     * @param {function} fCallback Función que se llamará cuando se hayan terminado de animar todos los iconos
     */
    this.startClaimAnimation = function( idxWinPosition, fCallback ) {
        /* eslint-disable max-len */
        if( !_.isInteger( idxWinPosition ) ) { throw new Error( "RFGraphicReelColumn::startClaimAnimation idxWinPosition is not an integer" ); }
        if( idxWinPosition >= m_numSymbols ) { throw new Error( "RFGraphicReelColumn::startClaimAnimation idxWinPosition has invalid length" ); }
        /* eslint-enable max-len */

        var rfGraphicsymbol = m_reelColumn[ idxWinPosition ];
        rfGraphicsymbol.startClaimAnimation( false, fCallback );
    };

    this.isPlayingAnimationbyIndexPosition = function( idxPosition ) {
        /* eslint-disable max-len */
        if( !_.isInteger( idxPosition ) ) { throw new Error( "RFGraphicReelColumn::isPlayingAnimationbyIndexPosition idxPosition is not an integer" ); }
        if( idxPosition >= m_numSymbols ) { throw new Error( "RFGraphicReelColumn::isPlayingAnimationbyIndexPosition idxPosition has invalid length" ); }
        /* eslint-enable max-len */

        var rfGraphicsymbol = m_reelColumn[ idxPosition ];
        rfGraphicsymbol.isPlayingAnimation();
    };

    /**
     * Para las animaciones tanto de reclamo como de premio de todo el rodillo. Además para la animación del wild extendido y los multiplicadores
     */
    this.stopAnimations = function() {

        _.forEach( m_reelColumn, function( o ) { o.stopAnimations(); } );
        _.forEach( m_payBoxes, function( o ) { o.setVisible( false ); } );
        _.forEach( m_multipliers, function( o ) { o.stopAnimation(); o.setVisible( false ); } );

        if( m_oExtendedWild.hasExtendedWild ) {
            _.forEach( m_oExtendedWild.payboxes, function( o ) { o.setVisible( false ); } );
            m_oExtendedWild.graphicObject.stopAnimation();
            m_oExtendedWild.group.visible = false;
        }
    };

    /**
     * Para las animaciones de premio de todo el rodillo.
     */
    this.stopWinAnimations = function() {
        _.forEach( m_reelColumn, function( o ) { o.stopWinAnimation(); } );
        _.forEach( m_payBoxes, function( o ) { o.setVisible( false ); } );
        _.forEach( m_multipliers, function( o ) { o.stopAnimation(); o.setVisible( false ); } );

        if( m_oExtendedWild.hasExtendedWild ) {
            _.forEach( m_oExtendedWild.payboxes, function( o ) { o.setVisible( false ); } );
        }
    };

    // Idle Anim
    this.stopClaimAnimations = function() {
        _.forEach( m_reelColumn, function( o ) { o.stopClaimAnimation(); } );
    };

    /**
     * Inicio de la animación de estela de emoción de los rodillos
     */
    this.startPrizeHypeAnimation = function(  ) {
        if( m_prizeHypeAnim ) {
            m_prizeHypeAnim.apha = 0;
            m_prizeHypeAnim.visible = true;
            game.add.tween( m_prizeHypeAnim ).to( { alpha: 1 }, m_options.prizeHypeFadeAnim,
                Phaser.Easing.Linear.None, true );
            m_prizeHypeAnim.animations.play( m_prizeHypeAnim.animations.currentAnim.name );
        }
    };

    /**
    *  Parar la animación de estela de emoción de los rodillos / hace invisible la animación
    */
    this.stopPrizeHypeAnimation = function() {
        if( m_prizeHypeAnim ) {
            setTimeout( function() {
                var tw = game.add.tween( m_prizeHypeAnim ).to( { alpha: 0 }, m_options.prizeHypeFadeAnim,
                    Phaser.Easing.Linear.None, true );
                // if( tw.totalDuration > 0 ) {
                tw.onComplete.addOnce( function() {
                    m_prizeHypeAnim.animations.stop();
                    m_prizeHypeAnim.visible = false;
                } );
                // }
                // else {
                //     m_prizeHypeAnim.animations.stop();
                //     m_prizeHypeAnim.visible = false;
                // }
            }, m_options.prizeHypeDelayToStopAnim );
        }
    };

    /**
     * Devuelve la posición X, Y (centrada) del símbolo indicado por el índice
     * @param {integer} idxSymbol El índice con las posiciones dentro de la columna, que se han de animar.
     */
    this.getSymbolPosition = function( idxSymbolPosition ) {
        if( !_.isInteger( idxSymbolPosition ) ) { throw new Error( "RFGraphicReelColumn::getSymbolRectangle aWinPosition is not an integer" ); }
        if( idxSymbolPosition >= m_numSymbols ) { throw new Error( "RFGraphicReelColumn::getSymbolRectangle idxSymbol has invalid length" ); }
        return m_reelColumn[ idxSymbolPosition ].getPosition();
    };

    /**
     * Get the current reel component.
     * @returns The columns reel component.
     */
    this.getReelComponent = function() {
        return m_reelComponent;
    };

    /**
     * Return if prizeHype for this reel is enable
     * @returns The config. variable prizeHypeEnable
     */
    this.isPrizeHypeEnable = function() {
        return m_options.prizeHypeEnable;
    };

    /**
     * Indica si todos los rodillos están girando
     */
    this.isSpinning = function() {
        return m_isSpinning;
    };

    /**
     * Indica está en proceso de parada
     */
    this.isStopping = function() {
        return m_isStopping;
    };

    /**
     * Indica si los rodillos están parados
     */
    this.isStopped = function() {
        return ( !m_isSpinning && !m_isStopping );
    };


    /**
    * Devuelve el número de símbolos que definen la columna
    */
    this.getNumSymbols = function() {
        return m_numSymbols;
    };

    /**
    * Devuelve si tiene o no extended Wild
    */
    this.hasExtendedWild = function() {
        return m_oExtendedWild.hasExtendedWild;
    };

    /**
     * De uso interno. Solo para debug
     * @private
     */
    this._getReelColumn = function() {
        return m_reelColumn;
    };

    /**
     * Fuerza una parada abrupta de un rodillo (sin efecto de rebote)
     */
    var _forceStopReel = function() {
        if( !self.isStopped() ) {

            m_reelComponent.bounceSumY = 0;
            m_reelComponent.bounceTime = 0;
            stopReel();

            m_isStopping = false;
            m_isSpinning = false;
        }
    };

    /**
     * Para el rodillo
     * @param {function} fOnStop función que se llama al finalizar
     */
    var stopReel = function( fOnStop, rfSymbolsArray ) {
        m_reelComponent.spinStop( fOnStop, rfSymbolsArray );
    };

    /**
     * Dado un array de RFSymbolos a pintar, actualiza todos los objetos gráficos de la matriz para pintar
     * @param {array} rfSymbolsArray Array con los RFSymbols a pintar en la parada de los rodillos.
     */
    var updateRFGraphicSymbolsArray = function( rfSymbolsArray ) {

        /* eslint-disable max-len */
        if( !_.isArray( rfSymbolsArray ) ) { throw new Error( "RFGraphicReelColumn::updateRFGraphicSymbols invalid rfSymbolsArray" ); }
        if( rfSymbolsArray.length !== m_numSymbols ) { throw new Error( "RFGraphicReelColumn::updateRFGraphicSymbols invalid num of reels" ); }
        /* eslint-enable max-len */

        for( var i = 0; i < m_reelColumn.length; ++i ) {
            m_reelColumn[ i ].setRFSymbol( rfSymbolsArray[ i ] );

            if( m_options.isDynamic ) {
                if( !m_initialized ) {
                    m_reelComponent.initBlurSymbolbyIndex( i, rfSymbolsArray[ i ] );
                }
            }
        }
        m_initialized = true;
    };

    var playSound = function( rfAudioSound ) {
        if( m_options.playSounds ) {
            rfAudioSound.play();
        }
    };

    var stopAllSounds = function() {
        _.forEach( m_sounds, function( rfAudioSound ) {
            stopSound( rfAudioSound );
        } );
    };

    var stopSound = function( rfAudioSound ) {
        rfAudioSound.stop();
    };

    // eslint-disable-next-line
    var getId = function() {
        return m_id;
    };

    var constructor = function() {
        if( !isValidGame( m_game ) ) {
            throw new Error( "RFGraphicSymbol: 'game' is not an object" );
        }
    };

    var m_id = id;
    var m_game = game;
    var m_reelColumn = [];
    var m_payBoxes = [];
    var m_multipliers = [];
    var m_reelComponent = null;
    var m_prizeHypeAnim = null;
    var m_oExtendedWild = {
        group         : null,
        payboxes      : [],
        graphicObject : null,
        hasExtendedWild : false
    };

    var m_initialized = false;
    var m_numSymbols = -1; // Número de símbolos que aparecen visualmente por rodillo
    var m_isSpinning = false;
    var m_isStopping = false;

    var m_options = {
        isDynamic: false,
        msStopReel : 1000,
        yBounceOffset : 30,
        playSounds: true,

        msForReelStartSpin: 150,
        offsetReelSpinSpeed: 100,
        reelSpinSpeed: 300,
        prizeHypeEnable: false,
        prizeHypeDelayToStopAnim: 400,
        prizeHypeFadeAnim: 500,
        symbolWeights: null

    };

    var m_sounds = {
        start : new RFAudioSound( m_game ),
        stop: new RFAudioSound( m_game ),
        prizeHype: new RFAudioSound( m_game )
    };

    var DEFAULTS = {
        PRIZEHYPE_RECT_X: 1,
        PRIZEHYPE_RECT_Y: 1,
        PRIZEHYPE_SCALE_X: 1,
        PRIZEHYPE_SCALE_Y: 1,
        PRIZEHYPE_NAME: "no-name-for-prizehype",
        PRIZEHYPE_ATLAS: "no-key-atlas-for-prizehype",
        PRIZEHYPE_FRAMES: null,
        PRIZEHYPE_FPS: 24,
        PRIZEHYPE_LOOP: true,
        PRIZEHYPE_VISIBLE: false
    };

    var self = this;

    constructor();
}

//------------------------------------------------------
// Source: src/engine/gamegraphic/rf.gamegraphic.reels.js
//------------------------------------------------------

/* eslint-disable max-lines */
/**
 * @requires rf.gamegraphic.symbol
 * @requires rf.gamegraphic.mask
 * @requires rf.comp.reel
 * @version 0.4.0
 * CAMBIOS:
 * 0.1.5 - Publica los métodos stopAllSounds, showExtendedWildPrizes y showWinPrize para permitir su llamada.
 * 0.1.6 - Corrección de un bug al parar los sonidos cuando hay Wilds extendidos.
 * 0.1.7 - Optimizados los accesos por lodash
 * 0.2.0 - Se añade soporte para visualizar un multiplicador. Éste se reproduce una vez se haya realizar la animación del símbolo.
 * 0.2.1 - Se controla correctamente la última parada de rodillos en 'stopReels' para invocar al callback.
 * 0.2.2 - Se añade identificador numérico en la generación de los reelcolumn.
 * 0.3.0 - Se añade funciones "initStarClaimAnimation",  "getReelAndSymboltoPlayClaim" y "stopClaimanimations"
 *         Para controlar y manejar las animaciones de tipo claim
 *       - Se añaden nuevas opciones para las animaciones claim
 *       - Se ha modificado el método "initialize"  para enlazar la señal desde el basegame del starSpin para parar
 *         las animaciones claim.
 * 0.3.1 - Se corrige las Claim anim. Se añade nueva variable de control para saber que todas las animaciones han acabado
 *       - y nuevo método para controlar mejor las animaciones 'launchClaimAnimations'
 * 0.3.2 - Corrección del claim animation y quitar señal del onSpin
 * 0.3.3 - Se han añadido dos variables de control para los timers (Start and offset) para las 'claim Animation'
 * 0.4.0 - Se ha mejorado la estela de la emoción para que ahora tenga en cuenta cuantos simbolos del prizeHype como máximo
 *         hay para que la estela de la emoción no continue cuando esta cantidad haya aparecido en un spin.
 */
// eslint-disable-next-line
function RFGraphicReels( game ) {
    "use strict";

    /**
     * @param {object} initData
     * @param {object} initData.reelsDefinition Definición de rodillos, que viene en el GameDefinition
     * @param {object=} initData.payBoxHeight Alto del paybox
     * @param {object=} initData.payBoxWidth Ancho del paybox
     *
     * @todo Se necesita payBoxHeight y payBoxWidth, puesto que a la hora de calcular la máscara global para las línea de premios,
     * dado que PHASER no permite realizar máscaras invertidas, necesitamos calcular todo el espacio experior alrededor de las payboxes
     * pero las payboxes no tienen tamaño en el Sprite hasta que no se inserta una imagen (que es justo el momento de dar el premio).
     * A futuro, podría ser interesante calcular esa máscara dinámicamente
     *
     */

    // TODO: Fix this
    // eslint-disable-next-line complexity
    this.initialize = function( initData ) {

        /* eslint-disable max-len */
        if( !_.isObject( initData ) ) { throw new Error( "RFGraphicReels::initialize missing initData" ); }
        if( !_.has( initData, [ "reelsDefinition" ] ) ) { throw new Error( "RFGraphicReels::initialize initData.reelsDefinition missing" ); }
        if( !_.has( initData.reelsDefinition, [ "reelColumns" ] ) ) { throw new Error( "RFGraphicReels::initialize missing initData.reelsDefinition.reelColumns" ); }
        /* eslint-enable max-len */

        m_usePayBoxMask = _.get( initData.reelsDefinition, [ "usePayBoxMask" ], m_usePayBoxMask );

        if( m_usePayBoxMask ) {
            assertAllKeys( initData, [ "payBoxHeight", "payBoxWidth" ] );
            /* eslint-disable max-len */
            if( !( _.isInteger( initData.payBoxHeight ) && initData.payBoxHeight > 0 ) ) { throw new Error( "RFGraphicReels::initialize invalid initData.payBoxHeight" ); }
            if( !( _.isInteger( initData.payBoxWidth ) && initData.payBoxWidth > 0 ) ) { throw new Error( "RFGraphicReels::initialize invalid initData.payBoxWidth" ); }
            /* eslint-enable max-len */

            // Read @todo
            m_payBoxHeight = initData.payBoxHeight;
            m_payBoxWidth = initData.payBoxWidth;

        }
        else {
            // win line
            m_line = new RFGraphicLine( m_game );
        }

        // Create the reels
        var nSymbolsPerReel = -1;
        var reelIdTemp = 0;
        _.forEach( _.keysOrdered( initData.reelsDefinition.reelColumns ), function( reelColumnKey ) {

            // add payBoxesOverSymbols in initData to each column
            var initReelData = initData.reelsDefinition.reelColumns[ reelColumnKey ];
            if( !_.has( initReelData, [ "payBoxesOverSymbols" ] ) ) {
                initReelData.payBoxesOverSymbols = _.has( initData.reelsDefinition, [ "payBoxesOverSymbols" ] )
                    ? initData.reelsDefinition.payBoxesOverSymbols
                    : false;
            }

            var rfGraphicReelColumn = new RFGraphicReelColumn( reelIdTemp, m_game );
            rfGraphicReelColumn.initialize( initData.reelsDefinition.reelColumns[ reelColumnKey ] );
            m_reels.push( rfGraphicReelColumn );

            nSymbolsPerReel = ( nSymbolsPerReel < 0 ? rfGraphicReelColumn.getNumSymbols() : nSymbolsPerReel );

            reelIdTemp++;

        } );

        m_numSymbolsPerReel = nSymbolsPerReel;
        m_numReels = m_reels.length;

        if( m_numReels <= 0 ) { throw new Error( "RFGraphicReels::initialize number of reels are <= 0" ); }
        if( m_numSymbolsPerReel <= 0 ) { throw new Error( "RFGraphicReels::initialize number of symbols per reel are <= 0" ); }

        if( m_usePayBoxMask ) {

            // win line
            m_line = new RFGraphicLine( m_game );

            m_lineMask = new RFMask( m_game );
            initializeRectanglesMask();

            // Precalculated array positions to build the line mask
            m_arrayPositions = [];
            for( var i = 1; i <= m_numReels; ++i ) {
                for( var j = 1; j <= m_numSymbolsPerReel; ++j ) {
                    m_arrayPositions.push( [ i, j ] );
                }
            }
        }

        // Sounds
        if( _.has( initData.reelsDefinition, [ "sounds", "reelsStart" ] ) ) {
            m_sounds.start.setSounds( initData.reelsDefinition.sounds.reelsStart );
        }
        else {
            console.info( "RFGraphicReelColumn::initialize no sounds.reelsStart" );
        }
        if( _.has( initData.reelsDefinition, [ "sounds", "reelsStop" ] ) ) {
            m_sounds.stop.setSounds( initData.reelsDefinition.sounds.reelsStop );
        }
        else {
            console.info( "RFGraphicReelColumn::initialize no sounds.reelsStop" );
        }
        if( _.has( initData.reelsDefinition, [ "sounds", "prizeHype" ] ) ) {
            m_sounds.prizeHype.setSounds( initData.reelsDefinition.sounds.prizeHype );
        }
        else {
            console.info( "%c RFGraphicReels::initialize no sounds.reelsHype. ", RFDebugUtils.getConsoleColors().REELS );
        }

    };

    /**
     * Modifica las opciones de giro de rodillos
     * @param {object} options Objeto con las opciones a modificar
     * @param {integer=} options.msStopReel=1000 Milisegundos que tardará la en parar cada rodillo
     * @param {integer=} options.msBetweenStops=100 Milisegundos que se esperarán antes de lanzar la siguiente parada de rodillo
     * @param {integer=} options.yBounceOffset=30 Offset de desplazamiento del rodillo en el rebote
     * @param {integer=} options.offsetReelSpinSpeed Speed offset for reel rotation speed. Makes all reels rotate at a different pase.
     * @param {integer=} options.reelSpinSpeed The speed at which the reels rotate.
     * @param {object=} options.symbolWeights The different weight values and collections for the symbols that participate in the dynamic reel.
     * @param {collection=} options.prizeHype Collection of symbols that trigger prize hype effect on the reels.
     * @param {integer=} options.prizeHypeDelayMultiplier The duration of the prize hype effect when applied.
     */
    // eslint-disable-next-line
    this.setOptions = function( options ) {
        /* eslint-disable max-len */
        m_options.msStopReel = _.has( options, [ "msStopReel" ] ) && _.isInteger( options.msStopReel ) ? options.msStopReel : m_options.msStopReel;
        m_options.yBounceOffset = _.has( options, [ "yBounceOffset" ] ) && _.isInteger( options.msStopReel ) ? options.yBounceOffset : m_options.yBounceOffset;
        m_options.msBetweenStops = _.has( options, [ "msBetweenStops" ] ) && _.isInteger( options.msBetweenStops ) ? options.msBetweenStops : m_options.msBetweenStops;

        m_options.msForReelStartSpin = _.has( options, [ "msForReelStartSpin" ] ) && _.isInteger( options.msForReelStartSpin ) ? options.msForReelStartSpin : m_options.msForReelStartSpin;
        m_options.offsetReelSpinSpeed = _.has( options, [ "offsetReelSpinSpeed" ] ) && _.isInteger( options.offsetReelSpinSpeed ) ? options.offsetReelSpinSpeed : m_options.offsetReelSpinSpeed;
        m_options.reelSpinSpeed = _.has( options, [ "reelSpinSpeed" ] ) && _.isInteger( options.reelSpinSpeed ) ? options.reelSpinSpeed : m_options.reelSpinSpeed;

        m_options.symbolWeights = _.has( options, [ "symbolWeights" ] ) ? options.symbolWeights : null;
        m_options.prizeHype = ( _.has( options, [ "prizeHype" ] ) ) ? options.prizeHype : m_options.prizeHype;

        console.log( "%c prizeHype received by reel: ", RFDebugUtils.getConsoleColors().REELS, m_options.prizeHype );

        m_options.prizeHypeDelay = ( _.has( options, [ "prizeHypeDelay" ] ) && _.isInteger( options.prizeHypeDelay ) ) ? options.prizeHypeDelay : m_options.prizeHypeDelay;
        m_options.prizeHypeDelayMultiplier = ( _.has( options, [ "prizeHypeDelayMultiplier" ] ) && _.isInteger( options.prizeHypeDelayMultiplier ) ) ? options.prizeHypeDelayMultiplier : m_options.prizeHypeDelayMultiplier;
        m_options.prizeHypeSoundOnlyOne = _.has( options, [ "prizeHypeSoundOnlyOne" ] ) && _.isBoolean( options.prizeHypeSoundOnlyOne ) ? options.prizeHypeSoundOnlyOne : m_options.prizeHypeSoundOnlyOne;

        m_options.playSounds = _.has( options, [ "playSounds" ] ) && _.isBoolean( options.playSounds ) ? options.playSounds : m_options.playSounds;
        m_options.startExtendedWildsAtSameTime = _.has( options, [ "startExtendedWildsAtSameTime" ] ) && _.isBoolean( options.startExtendedWildsAtSameTime ) ? options.startExtendedWildsAtSameTime : m_options.startExtendedWildsAtSameTime;

        // optimos for idle-claim animation
        m_options.msBtwnClaimAnimation = _.has( options, [ "msBtwnClaimAnimation" ] ) && _.isInteger( options.msBtwnClaimAnimation ) ? options.msBtwnClaimAnimation : m_options.msBtwnClaimAnimation;
        m_options.msStartClaimAnimation = _.has( options, [ "msStartClaimAnimation" ] ) && _.isInteger( options.msStartClaimAnimation ) ? options.msStartClaimAnimation : m_options.msStartClaimAnimation;
        m_options.modeClaimAnimationPlay = _.has( options, [ "modeClaimAnimationPlay" ] ) && _.isString( options.modeClaimAnimationPlay ) ? options.modeClaimAnimationPlay : m_options.modeClaimAnimationPlay;
        m_options.numberSymbolsPlayingClaim = _.has( options, [ "numberSymbolsPlayingClaim" ] ) && _.isInteger( options.numberSymbolsPlayingClaim ) ? options.numberSymbolsPlayingClaim : m_options.numberSymbolsPlayingClaim;


        /* eslint-enable max-len */

        _.forEach( m_reels, function( reelColumn ) {
            reelColumn.setOptions( options );
        } );
    };

    /**
     * Cambia los símbolos por los pasados en rfSymbolsMatrix. Útil para inicializar los rodillos con un valor.<br/>
     * <b>IMPORTANTE:</b> Si los rodillos están girando, los para de manera abrupta.
     * @param {matrix} rfSymbolsMatrix Matriz con los RFSymbols a pintar.
     */
    this.setReels = function( rfSymbolsMatrix ) {

        if( !_.isArray( rfSymbolsMatrix ) ) { throw new Error( "RFGraphicReels::setReels rfSymbolsMatrix is not an Array" ); }
        if( rfSymbolsMatrix.length !== m_numReels ) { throw new Error( "RFGraphicReels::setReels rfSymbolsMatrix has invalid length" ); }

        for( var i = 0; i < m_numReels; ++i ) {
            m_reels[ i ].setReelColumn( rfSymbolsMatrix[ i ] );
        }
    };

    /**
     * Realiza el giro de rodillos
     * @param {array|number} indexReelsToSpinArray Array con los índices (empezando en 0) de los rodillos que se quieren girar.
     * El giro sigue el orden indicado en el array. Si en lugar de un array se pasa un número, se puede indicar también el índice
     * de un solo rodillo a girar. Si no se pasa nada, realiza un giro de todos los rodillos.
     *
     * NOTA: Si se realiza un starSpin, mientras que se está relizando la parada de rodillos, es posible que se produzcan resultados
     * inesperados, como paradas de rodillos mientras se está produciendo el nuevo spin. No se ha realizado este control específico
     * puesto que puede ser el cliente quien decida hacerlo de ese modo, aunque posiblemente lo que haya olvidado es añadir el callback
     * de terminación de parada, antes de iniciar el siguiente spin.
     * A futuro se podría decidir si es conveniente que se realice una parada de rodillos abrupta (cancelando todos los timers de espera
     * de para parar los rodillos en el stop).
     */
    this.startSpin = function( indexReelsToSpinArray ) {

        if( m_claimTimeEventStart ) {
            m_game.time.events.remove( m_claimTimeEventStart );
        }
        if( m_claimTimeEventsOffset ) {
            m_game.time.events.remove( m_claimTimeEventsOffset );
        }

        stopAnimations();
        this.stopClaimAnimations();
        this.m_canPlayClaimAnim = false;

        this.stopWinPrizesCarrousel();
        this.forceStopReels();

        stopSound( m_sounds.start );
        playSound( m_sounds.start );

        // builds a correct idx indexReelsToSpinArray
        if( _.isNumber( indexReelsToSpinArray ) ) {
            indexReelsToSpinArray = [ indexReelsToSpinArray ];
        }
        else if( _.isArray( indexReelsToSpinArray ) ) {
            _.forEach( indexReelsToSpinArray, function( idx ) {
                assertIdxReel( idx );
            } );
        }
        else {
            indexReelsToSpinArray = _.range( m_numReels );
        }

        _.forEach( indexReelsToSpinArray, function( idxReel ) {
            var reel = m_reels[ idxReel ];
            reel.startSpin();
        } );

        m_spinningStartTime = m_game.time.now;
    };

    /**
     * Realiza una parada de TODOS los rodillos y presenta la escena final
     * @param {matrix} rfSymbolsMatrix Matriz con los RFSymbols a pintar en la parada de los rodillos.
     * @param {function} fOnStop Callback que se llamará cuando termine la parada de rodillos
     */
    this.stopSpin = function( rfSymbolsMatrix, prizeHypeObj, fOnStop ) {

        if( !_.isArray( rfSymbolsMatrix ) ) { throw new Error( "RFGraphicReels::stopSpin rfSymbolsMatrix is not array" ); }
        if( rfSymbolsMatrix.length !== m_numReels ) { throw new Error( "RFGraphicReels::stopSpin rfSymbolsMatrix has invalid length" ); }

        // build the reel array to stop
        var stopReelsArray = [];
        for( var idxReel = 0; idxReel < rfSymbolsMatrix.length; ++idxReel ) {
            stopReelsArray.push( {
                idxReel: idxReel,
                rfSymbolsArray: rfSymbolsMatrix[ idxReel ]
            } );
        }

        this.stopReels( stopReelsArray, prizeHypeObj, fOnStop );
    };

    /**
     * Realiza una parada solo de rodillos indicados en el array de objetos ( y en el orden indicado )
     * @param {array} stopReelsArray Array de parada de rodillos, con el índice de rodillos y con los RFSymbols a pintar.
     * Cada objeto del array tiene que seguir esta estructura:
     *   {
     *      idxReel: <integer>
     *      rfSymbolsArray: <Array de RFSymbols>
     *   }
     * @param {function} fOnStop Callback que se llamará cuando termine la parada de rodillos
     */
    // eslint-disable-next-line complexity
    this.stopReels = function( stopReelsArray, prizeHypeObj, fOnStop ) {

        if( !_.isArray( stopReelsArray ) ) { throw new Error( "RFGraphicReels::stopReels stopReelsArray is not array" ); }
        if( stopReelsArray.length > m_numReels ) { throw new Error( "RFGraphicReels::stopSpin stopReelsArray has invalid length" ); }

        function _stopSpin( reelColum, rfSymbolsArray, delayReelStop ) {
            if( reelColum.isPrizeHypeEnable() ) {
                reelColum.stopPrizeHypeAnimation();
                /* eslint-disable */
                if( !m_options.prizeHypeSoundOnlyOne ) {
                    _fadePHSoundQuitOthers();
                }

                if( m_options.prizeHypeSoundOnlyOne && !delayReelStop ) {
                    _fadePHSoundQuitOthers();
                }

                if( m_options.prizeHypeSoundOnlyOne && reelsToStop.length === ( nReelsStopped + 1 ) ) {
                    _fadePHSoundQuitOthers();
                }
            }

            /* eslint-enable */
            reelColum.stopSpin( rfSymbolsArray, function() {
                nReelsStopped++;
                var isFinalReel = reelsToStop.length === nReelsStopped;
                if( isFinalReel && _.isFunction( fOnStop ) ) {
                    fOnStop();
                }
            } );
        }

        function _fadePHSoundQuitOthers() {
            var listAudio = [];
            var timeToFade = 300;
            listAudio.push( m_sounds.prizeHype );
            fadeSound( m_sounds, timeToFade, 0, true );
            fadeSound( listAudio, timeToFade, 1, true );
        }

        function _prizeHypeEffect( reelColum ) {
            // m_sounds.prizeHype.isPlaying()
            if( !m_options.prizeHypeSoundOnlyOne ||
                ( m_options.prizeHypeSoundOnlyOne && !m_sounds.prizeHype.isPlaying() ) ) {
                var soundsToPlay = [];
                soundsToPlay.push( m_sounds.prizeHype );
                // playSound( m_sounds.prizeHype );
                /* eslint-disable */
                fadeIn( soundsToPlay, 200, 1 );
                var listAudio = [];
                listAudio.push( m_sounds.start );
                listAudio.push( m_sounds.stop );
                fadeSound( listAudio, 300, 0, false );
                /* eslint-enable */
            }
            reelColum.startPrizeHypeAnimation();
        }

        // caching the reels that needs to stop and setting the new rfsymbols to the reels that are not spinning
        var reelsToStop = [];
        var nReelsStopped = 0;
        var i = 0;
        for( i = 0; i < stopReelsArray.length; ++i ) {

            var stopReel = stopReelsArray[ i ];
            var idxReel = _.get( stopReel, [ "idxReel" ] );
            var rfSymbolsArray = _.get( stopReel, [ "rfSymbolsArray" ] );

            assertIdxReel( idxReel );
            if( !_.isArray( rfSymbolsArray ) ) { throw new Error( "RFGraphicReels::stopReels invalid rfSymbolsArray" ); }

            var reel = m_reels[ idxReel ];
            if( reel.isSpinning() ) {
                reelsToStop.push( {
                    reel: reel,
                    rfSymbolsArray: rfSymbolsArray
                } );
            }
            else {
                reel.setReelColumn( rfSymbolsArray );
            }
        }

        var previousSymbols = [];
        var scatterSymbolCount = 0;
        var delayReelStop = false;
        var previousStopTime = 0;

        // stops the reels

        for( i = 0; i < reelsToStop.length; ++i ) {
            var reelToStop = reelsToStop[ i ];
            if( reelToStop.reel.isPrizeHypeEnable() ) {
                if( previousSymbols !== null ) {
                    /* eslint-disable */
                    _.forEach( m_options.prizeHype, function( no ) {
                        var indexPrizeHypeArray = _.findIndex( prizeHypeObj, [ "strIco", no ] );
                        var maxPrizeHypeCount = indexPrizeHypeArray > -1 ? prizeHypeObj[ indexPrizeHypeArray ].times : 3;
                        _.forEach( previousSymbols, function( o ) {
                            _.forEach( o, function( po ) {
                                if( po.id === no ) {
                                    scatterSymbolCount++;
                                    if( ( scatterSymbolCount >= 2 ) && ( scatterSymbolCount < maxPrizeHypeCount ) ) {
                                        delayReelStop = true;
                                    } else {
                                        delayReelStop = false;
                                    }
                                }
                            } );
                        } );
                        scatterSymbolCount = 0;
                    } );
                    /* eslint-enable */
                }
            }

            // eslint-disable-next-line
            var stopTime = previousStopTime + m_options.msBetweenStops * ( ( delayReelStop ) ? m_options.prizeHypeDelayMultiplier : 1 );
            stopTime = ( ( reelToStop.reel.getReelComponent().options.id === 0 ) ? 0 : stopTime );
            m_game.time.events.add( stopTime, _stopSpin, this, reelToStop.reel, reelToStop.rfSymbolsArray, delayReelStop );

            if( delayReelStop ) {
                var prizeHypeTime = previousStopTime + m_options.msBetweenStops + m_options.prizeHypeDelay;
                m_game.time.events.add( prizeHypeTime, _prizeHypeEffect, this, reelToStop.reel );
            }

            previousSymbols.push( reelToStop.rfSymbolsArray );
            previousStopTime = stopTime;
            delayReelStop = false;
        }
    };

    /**
     * Fuerza una parada de rodillos de forma abrupta (sin efecto de rebote)
     */
    this.forceStopReels = function() {
        if( !self.isStopped() ) {
            _.forEach( m_reels, function( rfReelColumn ) {
                rfReelColumn.forceStopReel();
            } );
        }
    };

    /**
     * Inicia la animación de los premios.
     *
     * @param {object} oWinPrizes Objecto que contiene tanto el array de líneas de premios como los wilds extendendidos
     *
     * @param {array} oWinPrizes.aWinPrizes Array de ojetos con líneas ganadoras. CADA ELEMENTO, del aWinPrizes contendrá un objeto descrito a continuación
     * @param {array} oWinPrizes.aWinPrizes.aWinPosition Array con las posiciones dentro de la matriz de rodillos que se han de animar.
     * Las posiciones de rodillo + icono empiezan en la 1 porque el servidor así lo manda y por comodidad de uso.
     * @param {RFLine} oWinPrizes.aWinPrizes.rfLine Definición de la RFLine asociada.
     * @param {RFSymbol} oWinPrizes.aWinPrizes.rfSymbol Definición del RFSymbol asociado al premio.
     *
     * @param {array} oWinPrizes.extendedWildArray Array de ojetos con Wilds Extendidos. CADA ELEMENTO, del extendedWildArray contendrá un objeto
     *                descrito a continuación
     * @param {RFExtendedWild} oWinPrizes.extendedWildArray.rfExtendedWild Definición del Wild Extendido.
     * @param {integer} oWinPrizes.extendedWildArray.numReel Número del rodillo al que aplica el Wild Extendido.
     * @param {function} fCallback Función que se llamará cuando se hayan realizado todas las animaciones de premios
     */
    this.startWinPrizesCarrousel = function( oWinPrizes, fCallback ) {
        if( !self.isStopped() ) { throw new Error( "RFGraphicReels::startWinPrizesCarrousel Can't start carrousel if the reels are not stopped" ); }
        assertAllKeys( oWinPrizes, [ "prizesArray", "extendedWildArray" ] );

        var aWinPrizes = oWinPrizes.prizesArray;
        if( !_.isArray( aWinPrizes ) ) { throw new Error( "RFGraphicReels::startWinPrizesCarrousel oWinPrizes.aWinPrizes is not an array" ); }

        var aExtendedWild = oWinPrizes.extendedWildArray;
        if( !_.isArray( aWinPrizes ) ) { throw new Error( "RFGraphicReels::startWinPrizesCarrousel oWinPrizes.extendedWildArray is not an array" ); }

        var result = false;
        if( !m_isShowingCarrousel ) {
            if( aWinPrizes.length > 0 ) {
                m_isShowingCarrousel = true;
                _stopAllSounds();
                _showExtendedWildPrizes( aExtendedWild, function() { // 1.- Show Extended Wild
                    showWinPrizes( aWinPrizes, fCallback );         // 2.- Show Line prizes
                } );

            } // Only ExtendedWild
            else if( aExtendedWild.length > 0 ) {
                m_isShowingCarrousel = true;
                _stopAllSounds();
                _showExtendedWildPrizes( aExtendedWild, fCallback );
            }
            else if( _.isFunction( fCallback ) ) {
                fCallback();
            }

            result = true;
        }

        return result;
    };

    /**
     * Visualiza los wilds extendidos.
     * @param {array} aExtendedWildPrizes array con los wilds extendidos
     * @param {function} fCallback función que se invoca cuando termina de visualizarse la animación
     */
    this.showExtendedWildPrizes = function( aExtendedWildPrizes, fCallback ) {
        return _showExtendedWildPrizes( aExtendedWildPrizes, fCallback );
    };

    /**
     * Muestra la animación de los iconos indicados en el vector de posiciones
     * @param {object} winPrize
     * @param {RFLine} winPrize.rfLine La definición de la línea que se ha de mostrar
     * @param {array} winPrize.aWinPosition El Array con las posiciones que se han de animar
     * @param {RWSymbol} winPrize.rfSymbol El RFSymbol del premio
     * @param {boolean} bPlaySound Indica si tiene o no que reproducir el sonido del símbolo
     * @param {function} fCallback Función que se llamará cuando se hayan terminado de animar todos los iconos
     */
    this.showWinPrize = function( winPrize, bPlaySound, fCallback ) {
        return _showWinPrize( winPrize, bPlaySound, fCallback );
    };


    /**
     * Realiza la parada del carrusel de premios
     */
    this.stopWinPrizesCarrousel = function() {
        m_line.setVisible( false );
        m_line.deleteMask();

        stopAnimations();

        m_isShowingCarrousel = false;
    };

    /**
     * Devuelve el número de mílisegundos transcurridos en el spin
     */
    this.getSpinningTime = function() {
        var elapsed = 0;
        if( this.isSpinning() && m_spinningStartTime !== 0 ) {
            elapsed = m_game.time.now - m_spinningStartTime;
        }
        return elapsed;
    };

    /**
     * Indica si algún rodillo está girando
     */
    this.isSpinning = function() {
        var isSpinning = false;
        _.forEach( m_reels, function( reel ) {
            isSpinning = isSpinning || reel.isSpinning();
        } );
        return isSpinning;
    };

    /**
     * Indica está en proceso de parada
     */
    this.isStopping = function() {
        var isStopping = false;
        _.forEach( m_reels, function( reel ) {
            isStopping = isStopping || reel.isStopping();
        } );
        return isStopping;
    };

    /**
     * Para la reproducción de todos los sonidos asociados al los rodillos
     */
    this.stopAllSounds = function() {
        return _stopAllSounds();
    };

    /**
     * Indica si los rodillos están parados
     */
    this.isStopped = function() {
        return ( !this.isSpinning() && !this.isStopping() );
    };

    /**
     * Devuelve el número de rodillos
     */
    this.getNumberOfReels = function() {
        return m_numReels;
    };

    /**
     * Devuelve el número de símbolos por rodillos
     */
    this.getNumberOfSymbolsPerReels = function() {
        return m_numSymbolsPerReel;
    };

    /**
     * De uso interno. Solo para debug
     * @private
     */
    this._getReels = function() {
        return m_reels;
    };

    // 0.3.0 Manage Claim Symbol Animation
    this.initStartClaimAnimation = function() {
        if( m_options.modeClaimAnimationPlay !== ENUM_CLAIMMODE.NO_PLAY ) {
            m_canPlayClaimAnim = true;
            m_numSymbolsCompletedClaim = 0;
            m_claimTimeEventStart = m_game.time.events.add( m_options.msStartClaimAnimation, function() {
                if( m_canPlayClaimAnim ) {
                    self.launchClaimAnimations();
                }
            } );
            m_claimTimeEventStart.autoDestroy = true;
        }
        else {
            console.info( "%c Reels:: initStartClaimAnimation > CLAIM ANIM MODE SET TO 'NO_PLAY'  ",
                RFDebugUtils.getConsoleColors().INFO_GAMECONFIG );
        }

    };

    this.launchClaimAnimations = function() {
        if( m_canPlayClaimAnim ) {
            if( m_numSymbolsCompletedClaim <= 0 ) {
                var offsetPlayAnim = _.random( 0, m_options.msBtwnClaimAnimation );
                m_claimTimeEventsOffset = m_game.time.events.add( offsetPlayAnim, function() {
                    if( m_canPlayClaimAnim ) {
                        var indexObject = self.getReelAndSymboltoPlayClaim();
                        _.forEach( indexObject, function( objectIndex ) {
                            self.playClaimAnimation( m_reels[ objectIndex.indexReel ], objectIndex.indexSymbol );
                        } );
                    }
                } );
                m_claimTimeEventsOffset.autoDestroy = true;
            }
        }
    };

    this.playClaimAnimation = function( oReel, indexSymbol ) {
        if( m_canPlayClaimAnim ) {
            if( !oReel.isPlayingAnimationbyIndexPosition( indexSymbol ) ) {
                oReel.startClaimAnimation( indexSymbol, function() {
                    m_numSymbolsCompletedClaim--;
                    self.launchClaimAnimations();
                } );
            }
            else {
                m_numSymbolsCompletedClaim--;
                self.launchClaimAnimations();
            }
        }
    };

    this.getReelAndSymboltoPlayClaim = function() {
        var nsymbolsPerReel = self.getNumberOfSymbolsPerReels();
        var totalIndex = self.getNumberOfSymbolsPerReels() * self.getNumberOfReels();
        var indexArray = [];
        var numberSymbolsToDo = m_options.numberSymbolsPlayingClaim;
        // Init de la variable de control para saber cuantos tienen que parar
        m_numSymbolsCompletedClaim = m_options.numberSymbolsPlayingClaim;
        if( ( numberSymbolsToDo > totalIndex ) || ( numberSymbolsToDo < 0 ) ) { // si se configura mal, solo 1 un simbolo
            numberSymbolsToDo = 1;
            m_numSymbolsCompletedClaim = 1;
        }

        var arrayNumbers = Array.apply( null, { length: totalIndex } ).map( Function.call, Number );

        for( var i = 0; i < numberSymbolsToDo; i++ ) {
            var randomIndex = _.random( 0, arrayNumbers.length - 1 );
            var inArrayNumbers = _.pullAt( arrayNumbers, randomIndex )[ 0 ];

            var indexReel = _.floor( inArrayNumbers / nsymbolsPerReel, 0 );
            var indexSymbolOnReel = inArrayNumbers % nsymbolsPerReel;

            var indexObject = {
                indexReel: indexReel,
                indexSymbol: indexSymbolOnReel
            };
            indexArray.push( indexObject );
        }
        return indexArray;
    };


    // 0.3.0 Claim - Claim animations
    this.stopClaimAnimations = function() {
        _.forEach( m_reels, function( reelColum ) {
            reelColum.stopClaimAnimations();
        } );
    };

    /**
     * Visualiza los wilds extendidos.
     * @param {array} aExtendedWildPrizes array con los wilds extendidos
     * @param {function} fCallback función que se invoca cuando termina de visualizarse la animación
     */
    var _showExtendedWildPrizes = function( aExtendedWildPrizes, fCallback ) {

        /**
         * Visualiza los wilds extendidos comenzando a la vez
         * @param {array} _aExtendedWildPrizes
         * @param {function} _fCallback
         */
        function showExtendedWildPrizesAtSameTime( _aExtendedWildPrizes, _fCallback ) {

            function _fExtendedWildPrizeEndCallback() {
                numEndAnimations--;
                if( numEndAnimations <= 0 ) {
                    if( _.isFunction( _fCallback ) ) {
                        _fCallback();
                    }
                }
            }

            var numEndAnimations = _aExtendedWildPrizes.length;
            for( var i = 0; i < _aExtendedWildPrizes.length; ++i ) {

                // plays only the first extended wild sound to prevent plays multiples sounds at the same time
                var playSound = ( m_options.playSounds && i === 0 );

                showExtendedWildPrize( _aExtendedWildPrizes[ i ], playSound, _fExtendedWildPrizeEndCallback );
            }
        }

        /**
         * Visualiza los wilds extendidos comenzando de forma consecutiva y en el orden del array.
         * @param {array} _aExtendedWildPrizes
         * @param {function} _fCallback
         */
        function showExtendedWildPrizesInOrder( _aExtendedWildPrizes, _fCallback ) {

            function showExtendedWildPrizesInOrderInternal( __aExtendedWildPrizes, idx, __fCallback ) {
                if( idx < __aExtendedWildPrizes.length ) {
                    var playSound = m_options.playSounds;
                    showExtendedWildPrize( __aExtendedWildPrizes[ idx ], playSound, function() {
                        showExtendedWildPrizesInOrderInternal( __aExtendedWildPrizes, idx + 1, __fCallback );
                    } );
                }
                else if( _.isFunction( __fCallback ) ) {
                    __fCallback();
                }
            }

            showExtendedWildPrizesInOrderInternal( _aExtendedWildPrizes, 0, _fCallback );
        }

        if( !_.isArray( aExtendedWildPrizes ) ) { throw new Error( "showExtendedWildPrizes: aExtendedWildPrizes is not an array" ); }

        if( aExtendedWildPrizes.length > 0 ) {
            if( m_options.startExtendedWildsAtSameTime ) {
                showExtendedWildPrizesAtSameTime( aExtendedWildPrizes, fCallback );
            }
            else {
                showExtendedWildPrizesInOrder( aExtendedWildPrizes, fCallback );
            }
        }
        else if( _.isFunction( fCallback ) ) {
            fCallback();
        }

    };

    var showWinPrizes = function( aWinPrizes, fOnEndLoopCallback ) {

        // First loop
        var loop = false;
        var playSound = true;
        showWinPrizeInternal( aWinPrizes, 0, loop, playSound, function() {

            // When finish the first loop, we call the callback function
            if( _.isFunction( fOnEndLoopCallback ) ) {
                fOnEndLoopCallback();
            }

            // Nexts loops
            loop = true;
            playSound = false;
            showWinPrizeInternal( aWinPrizes, 0, loop, playSound );
        } );
    };

    // TODO: Fix this
    // eslint-disable-next-line max-params
    var showWinPrizeInternal = function( aWinPrizes, currentIdx, bLoop, playSound, fOnEndLoopCallback ) {

        if( m_isShowingCarrousel ) {

            if( currentIdx >= aWinPrizes.length ) { // base case
                if( _.isFunction( fOnEndLoopCallback ) ) {
                    fOnEndLoopCallback();
                }
                if( bLoop ) {
                    currentIdx = 0;  // loop again
                }
                else {
                    return; // end loop
                }
            }

            var currentPrize = aWinPrizes[ currentIdx ];
            _showWinPrize( currentPrize, playSound, function() {
                showWinPrizeInternal( aWinPrizes, currentIdx + 1, bLoop, playSound, fOnEndLoopCallback );  // recursive call
            } );
        }
    };

    /**
     * Muestra la animación de los iconos indicados en el vector de posiciones
     * @param {object} extendedWildPrize
     * @param {RFExtendedWild} extendedWildPrize.rfExtendedWild La definición del wild extendido
     * @param {integer} extendedWildPrize.numReel Número del rodillo (compienza en 1) donde se presentará la animación
     * @param {boolean} bPlaySound Indica si tiene o no que reproducir el sonido del símbolo
     * @param {function} fCallback Función que se llamará cuando se hayan terminado de animar todos los iconos
     */
    var showExtendedWildPrize = function( extendedWildPrize, bPlaySound, fCallback ) {
        assertAllKeys( extendedWildPrize, [ "rfExtendedWild", "numReel" ] );

        var idxReel = extendedWildPrize.numReel - 1;
        var rfExtendedWild = extendedWildPrize.rfExtendedWild;

        if( !( rfExtendedWild instanceof RFExtendedWild ) ) {
            console.warn( "RFExtendedWild is not valid", rfExtendedWild );
        }
        else {
            assertIdxReel( idxReel );

            if( bPlaySound ) {
                playSound( rfExtendedWild.soundKey );
            }

            m_reels[ idxReel ].startExtendedWildAnimation( rfExtendedWild, fCallback );
        }
    };

    /**
     * Muestra la animación de los iconos indicados en el vector de posiciones
     * @param {object} winPrize
     * @param {RFLine} winPrize.rfLine La definición de la línea que se ha de mostrar
     * @param {array} winPrize.aWinPosition El Array con las posiciones que se han de animar
     * @param {RWSymbol} winPrize.rfSymbol El RFSymbol del premio
     * @param {boolean} bPlaySound Indica si tiene o no que reproducir el sonido del símbolo
     * @param {function} fCallback Función que se llamará cuando se hayan terminado de animar todos los iconos
     */
    var _showWinPrize = function( winPrize, bPlaySound, fCallback ) {

        assertAllKeys( winPrize, [ "rfLine", "aWinPosition" ] );

        var rfLine = winPrize.rfLine;
        var aWinPosition = winPrize.aWinPosition;
        var rfSymbol = ( _.get( winPrize, [ "rfSymbol" ], null ) );

        if( !_.isArray( aWinPosition ) ) { throw new Error( "RFGraphicReels::showWinPrize aWinPosition is not an array" ); }
        if( aWinPosition.length > m_numReels ) { throw new Error( "RFGraphicReels::showWinPrize aWinPosition has invalid length" ); }

        if( bPlaySound ) {
            if( !( rfSymbol instanceof RFSymbol ) ) {
                console.warn( "prizeSoundSymbol is not valid to play sound", rfSymbol );
            }
            else {
                playSound( _.get( rfSymbol, [ "animations", "win", "soundKey" ] ) );
            }
        }

        function _fWinAnimationEndedCallback() {
            numEndAnimations--;
            if( numEndAnimations <= 0 ) {

                showMultipliers( winPrize, bPlaySound, function() {
                    stopWinAnimations();

                    if( _.isFunction( fCallback ) ) {
                        fCallback();
                    }
                } );
            }
        }

        var numEndAnimations = aWinPosition.length;
        for( var i = 0; i < aWinPosition.length; ++i ) {

            var aPosition = aWinPosition[ i ];

            assertPosition( aPosition ); // throws exception if possition is invalid

            var idxReel = aPosition[ 0 ] - 1;
            var idxSymbol = aPosition[ 1 ] - 1;

            m_reels[ idxReel ].startWinPrizeAnimation( rfLine, idxSymbol, _fWinAnimationEndedCallback );
        }

        m_line.setRFLine( rfLine );
        m_line.setVisible( true );
        updateMaskLine( rfLine, aWinPosition );
    };

    /**
     * Muestra la animación de los multiplicadores indicados en el vector de posiciones
     * @param {object} winPrize
     * @param {RFLine} winPrize.rfLine La definición de la línea que se ha de mostrar
     * @param {array} winPrize.aWinPosition El Array con las posiciones que se han de animar
     * @param {RWSymbol} winPrize.rfSymbol El RFSymbol del premio
     * @param {RWSymbol} winPrize.rfMultiplier El RFMultiplier con el multiplicador
     * @param {boolean} bPlaySound Indica si tiene o no que reproducir el sonido del símbolo
     * @param {function} fCallback Función que se llamará cuando se hayan terminado de animar todos los iconos
     */
    var showMultipliers = function( winPrize, bPlaySound, fCallback ) {

        function callToCallback() {
            if( _.isFunction( fCallback ) ) {
                fCallback();
            }
        }

        function _fWinAnimationEndedCallback() {
            numEndAnimations--;
            if( numEndAnimations <= 0 ) {
                stopWinAnimations();
                callToCallback();
            }
        }

        var aWinPosition = winPrize.aWinPosition;
        var numEndAnimations = aWinPosition.length;
        var rfMultiplier = _.get( winPrize, [ "rfMultiplier" ] );

        if( !rfMultiplier ) {
            callToCallback();
        }
        else {

            if( !_.isArray( aWinPosition ) ) { throw new Error( "RFGraphicReels::showMultiplier aWinPosition is not an array" ); }
            if( aWinPosition.length > m_numReels ) { throw new Error( "RFGraphicReels::showMultiplier aWinPosition has invalid length" ); }

            if( bPlaySound ) {
                if( !( rfMultiplier instanceof RFMultiplier ) ) {
                    console.warn( "RFMultiplier is not valid to play sound", rfMultiplier );
                }
                else {
                    playSound( _.get( rfMultiplier, [ "soundKey" ] ) );
                }
            }


            for( var i = 0; i < aWinPosition.length; ++i ) {

                var aPosition = aWinPosition[ i ];

                assertPosition( aPosition ); // throws exception if possition is invalid

                var idxReel = aPosition[ 0 ] - 1;
                var idxSymbol = aPosition[ 1 ] - 1;

                m_reels[ idxReel ].startMultiplierAnimation( rfMultiplier, idxSymbol, _fWinAnimationEndedCallback );
            }

        }
    };


    /**
     * Modifica la máscara de la línea en función del vector de posiciones de los premios
     * @param {RFLine} rfLine La definición de la línea que se ha de mostrar
     * @param {array} aWinPosition El Array con las posiciones premiadas.
     *                             Ya deben de estar chequeadas y ser válidas (se evita chequear por optimizar el cálculo).
     */
    var updateMaskLine = function( rfLine, aWinPosition ) {

        if( m_usePayBoxMask ) {
            m_lineMask.clear();
            m_lineMask.addRectangles( m_rectanglesMask );
            m_line.deleteMask();

            var noPrizePositionsArray = _.differenceWith( m_arrayPositions, aWinPosition, _.isEqual );

            // add the no prize positions array to the line mask
            _.forEach( noPrizePositionsArray, function( position ) {
                var idxReel = position[ 0 ] - 1;
                var idxSymbolInReel = position[ 1 ] - 1;
                var symbolPosition = m_reels[ idxReel ].getSymbolPosition( idxSymbolInReel );

                m_lineMask.addRectangle( {
                    x: symbolPosition.x - ( m_payBoxWidth / 2 ),
                    y: symbolPosition.y - ( m_payBoxHeight / 2 ),
                    width: m_payBoxWidth,
                    height: m_payBoxHeight
                } );
            } );

            // test: comment to see the mask
            m_line.setMask( m_lineMask.getMask() );
        }
    };

    /**
     * Para un rodillo
     * @param {integer} idxReel Índice del rodillo
     */
    // eslint-disable-next-line
    var stopReel = function( idxReel, fOnStop ) {
        assertIdxReel( idxReel );
        m_reels[ idxReel ].stopReel( fOnStop );
    };

    /**
     * Para las animaciones de todos los rodillos
     * @param {integer} idxReel Índice del rodillo
     */
    var stopAnimations = function() {
        _.forEach( m_reels, function( reelColum ) {
            reelColum.stopAnimations();
        } );
    };

    /**
     * Para las animaciones de premio todos los rodillos
     * @param {integer} idxReel Índice del rodillo
     */
    var stopWinAnimations = function() {
        _.forEach( m_reels, function( reelColum ) {
            reelColum.stopWinAnimations();
        } );
    };

    var initializeRectanglesMask = function() {

        m_rectanglesMask = [];

        var i;
        var rectangle = { x: 0, y: 0, width: 0, height: 0 };
        var symbolPosition = null;
        var symbolPositionNext = null;

        // --- VERTICAL RECTANGLES ------------

        // first rectangle
        symbolPosition = m_reels[ 0 ].getSymbolPosition( 0 );
        rectangle.width = symbolPosition.x - ( m_payBoxWidth / 2 );
        rectangle.height = m_game.height;
        m_rectanglesMask.push( _.clone( rectangle ) );

        for( i = 0; i < m_numReels - 1; ++i ) {
            symbolPosition = m_reels[ i ].getSymbolPosition( 0 );
            symbolPositionNext = m_reels[ i + 1 ].getSymbolPosition( 0 );
            rectangle.x = symbolPosition.x + ( m_payBoxWidth / 2 );
            rectangle.width = ( symbolPositionNext.x - ( m_payBoxWidth / 2 ) ) - rectangle.x;
            m_rectanglesMask.push( _.clone( rectangle ) );
        }

        // last rectangle
        symbolPosition = m_reels[ m_numReels - 1 ].getSymbolPosition( 0 );
        rectangle.x = symbolPosition.x + ( m_payBoxWidth / 2 );
        rectangle.width = m_game.width - rectangle.x;
        m_rectanglesMask.push( _.clone( rectangle ) );

        // --- HORIZONTAL RECTANGLES ------------

        rectangle = { x: 0, y: 0, width: 0, height: 0 };

        // first rectangle
        symbolPosition = m_reels[ 0 ].getSymbolPosition( 0 );
        rectangle.width = m_game.width;
        rectangle.height = symbolPosition.y - ( m_payBoxHeight / 2 );
        m_rectanglesMask.push( _.clone( rectangle ) );

        for( i = 0; i < m_numSymbolsPerReel - 1; ++i ) {
            symbolPosition = m_reels[ 0 ].getSymbolPosition( i );
            symbolPositionNext = m_reels[ 0 ].getSymbolPosition( i + 1 );
            rectangle.y = symbolPosition.y + ( m_payBoxHeight / 2 );
            rectangle.height = ( symbolPositionNext.y - ( m_payBoxHeight / 2 ) ) - rectangle.y;
            m_rectanglesMask.push( _.clone( rectangle ) );
        }

        // last rectangle
        symbolPosition = m_reels[ 0 ].getSymbolPosition( m_numSymbolsPerReel - 1 );
        rectangle.y = symbolPosition.y + ( m_payBoxHeight / 2 );
        rectangle.height = m_game.height - rectangle.y;
        m_rectanglesMask.push( _.clone( rectangle ) );

    };

    var playSound = function( rfAudioSoundOrSoundKeys ) {
        if( m_options.playSounds ) {
            if( rfAudioSoundOrSoundKeys instanceof RFAudioSound ) {
                rfAudioSoundOrSoundKeys.play();
            }
            else {
                var rfAudioSound = new RFAudioSound( m_game );
                rfAudioSound.setSounds( rfAudioSoundOrSoundKeys );
                playSound( rfAudioSound );
            }
        }
    };

    var _stopAllSounds = function() {
        _.forEach( m_sounds, function( rfAudioSound ) {
            rfAudioSound.stop();
        } );
    };

    var stopSound = function( rfAudioSound ) {
        rfAudioSound.stop();
    };


    var fadeIn = function( rfAudioSounds, timeToFadeout, volume ) {
        _.forEach( rfAudioSounds, function( oAudio ) {
            var tween = game.add.tween( oAudio ).to( { volume: volume }, timeToFadeout );
            tween.start();
            playSound( oAudio );
        } );
    };


    /**
     * @param {RFAudio} rfAudioSounds - audio a tratar
     * @param {Integer} timeToFadeout - duración del fade
     * @param {Float} volume - el volumen al que poner el audio
     * @param {bool} stopWhenVolumeZero - parar si el volumen es 0
     */
    var fadeSound = function( rfAudioSounds, timeToFadeout, volume, stopWhenVolumeZero ) {
        _.forEach( rfAudioSounds, function( oAudio ) {
            var tween = game.add.tween( oAudio ).to( { volume: volume }, timeToFadeout );
            tween.start();
            if( volume === 0 && stopWhenVolumeZero ) {
                tween.onComplete.addOnce( function() {
                    oAudio.stop();
                } );
            }
        } );
    };

    /**
     * Chequea si una posición es válida
     * @param {array} aPosition Array con 2 posiciones
     */
    var assertPosition = function( aPosition ) {
        if( !_.isArray( aPosition ) ) { throw new Error( "RFGraphicReels::assertPosition aPosition is not an array" ); }
        if( aPosition.length !== 2 ) { throw new Error( "RFGraphicReels::assertPosition aPosition has invalid values. Needs 2 values" ); }
        if( !_.isInteger( aPosition[ 0 ] ) ) { throw new Error( "RFGraphicReels::assertPosition aPosition has invalid values. It is not an integer" ); }
        if( !_.isInteger( aPosition[ 1 ] ) ) { throw new Error( "RFGraphicReels::assertPosition aPosition has invalid values. It is not an integer" ); }
        if( aPosition[ 0 ] <= 0 || aPosition[ 0 ] > m_numReels ) { throw new Error( "RFGraphicReels::assertPosition aPosition[0] has invalid value." ); }
        if( aPosition[ 1 ] <= 0 || aPosition[ 1 ] > m_numSymbolsPerReel ) {
            throw new Error( "RFGraphicReels::assertPosition aPosition[0] has invalid value." );
        }
    };

    /**
     * Chequea si el índice pasado corresponde a un idxReel válido
     * @param {integer} idxReel índice del reel.
     */
    var assertIdxReel = function( idxReel ) {
        if( idxReel >= m_numReels || idxReel < 0 ) { throw new Error( "Invalid idxReel (" + idxReel + "). Must be >= 0 and <=" + m_numReels ); }
    };

    var constructor = function() {
        if( !isValidGame( m_game ) ) {
            throw new Error( "RFGraphicSymbol: 'game' is not an object" );
        }
    };

    var m_game = game;
    var m_reels = [];
    var m_line = null;
    var m_lineMask = null;
    var m_rectanglesMask = [];
    var m_numSymbolsPerReel = -1; // Número de símbolos por rodillo
    var m_numReels = -1; // Número de rodillos
    var m_isShowingCarrousel = false;
    var m_payBoxHeight = 0;
    var m_payBoxWidth = 0;
    var m_usePayBoxMask = false;
    var m_spinningStartTime = 0; // en milisegundos
    var m_numSymbolsCompletedClaim = 0; // contador de simbolos para cuando acabe la animacion Claim
    // claim anim
    var m_canPlayClaimAnim = true;
    var m_claimTimeEventStart = null;
    var m_claimTimeEventsOffset = null;
    var m_sounds = {
        start: new RFAudioSound( m_game ),
        stop: new RFAudioSound( m_game ),
        prizeHype: new RFAudioSound( m_game )
    };

    var ENUM_CLAIMMODE = {
        NO_PLAY: "NO_PLAY",
        PLAY_ALL: "PLAY_ALL"
    };
    Object.freeze( ENUM_CLAIMMODE );

    var m_options = {
        msStopReel: 500,
        yBounceOffset: 30,
        msBetweenStops: 100,

        msForReelStartSpin: 150,
        offsetReelSpinSpeed: 100,
        reelSpinSpeed: 300,

        symbolWeights: null,
        prizeHype: null,

        prizeHypeDelay: 500,
        prizeHypeDelayMultiplier: 3,

        playSounds: true,
        startExtendedWildsAtSameTime: true,

        modeClaimAnimationPlay: ENUM_CLAIMMODE.NO_PLAY,
        msBtwnClaimAnimation: 2000,
        msStartClaimAnimation: 5000,
        numberSymbolsPlayingClaim: 1,

        prizeHypeSoundOnlyOne: false
    };
    var self = this;
    var m_arrayPositions = []; // Precalculated positions of the matrix. It's a fake matrix. I.e: for 5x3 reels = [ [1,1], [1,2] .... [5,3] ];

    constructor();
}

//------------------------------------------------------
// Source: src/engine/gamegraphic/rf.gamegraphic.symbol.js
//------------------------------------------------------

/**
 * @requires rf.comp.sybolanim
 * @version 0.5.0
 * CAMBIOS:
 * 0.1.3 - Optimizados los accesos por lodash
 * 0.2.0 - Se añaden los frames para la animación y se refactoriza el paso de parámetros.
 * 0.3.0 - Se pasa la propiedad restoreOriginalSymbolAtEnd al componente.
 * 0.4.0 - Se añade nueva propiedad "claimAnim" para animaciones Claim en los sprite behaviours
 *       - Se añade nuevo parametro de tiempo de animacion para los Claim
 * 0.5.0 - Se añade el paso del parametro "useAnimation" para controlar la ejecución de las animaciones.
 */
// eslint-disable-next-line
function RFGraphicSymbol( game ) {
    "use strict";

    this.setRFSymbol = function( rfSymbol ) {
        if( !( rfSymbol instanceof RFSymbol ) ) {
            throw new Error( "Invalid rfSymbol" );
        }
        if( _.isEmpty( rfSymbol.image.key ) ) {
            throw new Error( "RFSymbol.image.key is not defined" );
        }

        stopAnims();

        m_rfSymbol = rfSymbol;

        // Symbol image
        if( _.isEmpty( m_rfSymbol.image.atlas ) ) {
            m_SpriteSymbol.loadTexture( m_rfSymbol.image.key );
        }
        else {
            m_SpriteSymbol.loadTexture( m_rfSymbol.image.atlas, m_rfSymbol.image.key );
        }

        // Animations
        m_claimAnimComp.animTexture = _.isEmpty( m_rfSymbol.animations.claim.key ) ? null : m_rfSymbol.animations.claim.key;
        m_winAnimComp.animTexture = _.isEmpty( m_rfSymbol.animations.win.key ) ? null : m_rfSymbol.animations.win.key;

    };

    this.getRFSymbol = function() {
        return m_rfSymbol;
    };

    this.setVisible = function( bVisible ) {
        m_SpriteSymbol.visible = bVisible;
    };

    this.setX = function( x ) {
        m_SpriteSymbol.x = x;
    };

    this.setY = function( y ) {
        m_SpriteSymbol.y = y;
    };

    this.setXY = function( x, y ) {
        m_SpriteSymbol.x = x;
        m_SpriteSymbol.y = y;
    };

    this.setAnchor = function( anchorX, anchorY ) {
        m_SpriteSymbol.anchor.x = anchorX;
        m_SpriteSymbol.anchor.y = anchorY;
    };

    this.getSprite = function() {
        return m_SpriteSymbol;
    };

    this.getPosition = function() {
        return {
            x: m_SpriteSymbol.x,
            y: m_SpriteSymbol.y
        };
    };

    this.startClaimAnimation = function( bLoop, fCallback ) {
        stopAnims();
        startAnim( {
            animComp: m_claimAnimComp,
            frames: m_rfSymbol.animations.claim.frames,
            fps: m_rfSymbol.animations.claim.fps,
            restoreOriginalSymbolAtEnd: m_rfSymbol.animations.claim.restoreOriginalSymbolAtEnd,
            useAnimation: m_rfSymbol.animations.claim.useAnimation,
            loop: bLoop,
            fCallback: fCallback
        } );
    };

    this.isPlayingAnimation = function() {
        m_claimAnimComp.isPlayingAnimation();
    };

    this.stopClaimAnimation = function() {
        stopAnim( m_claimAnimComp );
    };

    this.startWinAnimation = function( bLoop, fCallback ) {
        stopAnims();
        startAnim( {
            animComp: m_winAnimComp,
            frames: m_rfSymbol.animations.win.frames,
            fps: m_rfSymbol.animations.win.fps,
            restoreOriginalSymbolAtEnd: m_rfSymbol.animations.win.restoreOriginalSymbolAtEnd,
            useAnimation: m_rfSymbol.animations.win.useAnimation,
            loop: bLoop,
            fCallback: fCallback
        } );
    };

    this.stopAnimations = function() {
        stopAnims();
    };

    this.stopWinAnimation = function() {
        stopAnim( m_winAnimComp );
    };

    var startAnim = function( oAnimParams /* animComp, frames, fps, bLoop, fCallback */ ) {
        var animComp = oAnimParams.animComp;
        animComp.repeat = oAnimParams.loop;
        animComp.func = oAnimParams.fCallback;
        animComp.animFPS = oAnimParams.fps;
        animComp.animFrames = oAnimParams.frames;
        animComp.restoreOriginalSymbolAtEnd = oAnimParams.restoreOriginalSymbolAtEnd;
        animComp.useAnimation = oAnimParams.useAnimation;
        animComp.start();
    };

    var stopAnim = function( animComp ) {
        animComp.stop();
    };

    var stopAnims = function() {
        stopAnim( m_claimAnimComp );
        stopAnim( m_winAnimComp );
    };

    var constructor = function( gameObj ) {

        if( !isValidGame( gameObj ) ) {
            throw new Error( "RFGraphicSymbol: 'game' is not a Phaser.Game" );
        }

        if( !_.has( gameObj, [ "behaviorPlugin" ] ) ) {
            throw new Error( "RFGraphicSymbol: We need the Phaser 'game' behavior plugin loaded in 'game.behaviourPlugin' property. " +
                "Ie.game.behaviorPlugin = game.plugins.add( Phaser.Plugin.Behavior );" );
        }
        m_game = gameObj;
        m_SpriteSymbol = m_game.add.sprite();

        // Enable the RFSymbolAnim component
        m_game.behaviorPlugin.enable( m_SpriteSymbol );

        // Claim animation
        m_SpriteSymbol.behaviors.set( _COMPONENT_ID.CLAIM_ANIM, RFSymbolAnimComp, {
            game       : m_game,
            autoStart  : false,
            repeat     : false,
            claimAnim  : true,
            autoCenter: true,
            msAnimTimeWithNoTexture: 500
        } );
        m_claimAnimComp = m_SpriteSymbol.behaviors.get( _COMPONENT_ID.CLAIM_ANIM ).options;

        // Win animation
        m_SpriteSymbol.behaviors.set( _COMPONENT_ID.WIN_ANIM, RFSymbolAnimComp, {
            game       : m_game,
            autoStart  : false,
            repeat     : false,
            claimAnim  : false,
            autoCenter : true
        } );
        m_winAnimComp = m_SpriteSymbol.behaviors.get( _COMPONENT_ID.WIN_ANIM ).options;
    };

    var m_game          = null;
    var m_rfSymbol      = null;
    var m_SpriteSymbol  = null;
    var m_claimAnimComp = null; // component reference
    var m_winAnimComp   = null; // component reference

    var _COMPONENT_ID = {
        CLAIM_ANIM : "claimAnimComp",
        WIN_ANIM   : "winAnimComp"
    };
    Object.freeze( _COMPONENT_ID );

    constructor( game );
}

//------------------------------------------------------
// Source: src/engine/gamedefinitions/rf.gamedefinition.animation.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFAnimation() {
    "use strict";

    this.name = ""; // The unique (within this Sprite) name for the animation, i.e. "run", "fire", "walk".
    this.atlas = ""; // The Spritesheet of the animation
    this.frames = null; // An array of numbers/strings that correspond to the frames to add to this
    // animation and in which order.e.g. [1, 2, 3 ] or [ 'run0', 'run1', run2 ]). If null then all frames will be used.
    this.fps = 30; // The speed at which the animation should play. The speed is given in frames per second.
    this.loop = false; // Whether or not the animation is looped or just plays once.

    /**
     * Inicializa el RFElement con la definición del GameDefinition
     * @param {object} oGameDefinition Definición del objeto
     * @param {string} id Id del RFElement
     */
    this.initialize = function( oGameDefinition, id ) {
        this.id = ( _.isString( id ) ? id : this.id );
        this.name = _.get( oGameDefinition, [ "name" ], this.name );
        this.atlas = _.get( oGameDefinition, [ "atlas" ], this.atlas );
        this.frames = _.get( oGameDefinition, [ "frames" ], this.frames );
        this.fps = _.get( oGameDefinition, [ "fps" ], this.fps );
        this.loop = _.get( oGameDefinition, [ "loop" ], this.loop );
    };
}


//------------------------------------------------------
// Source: src/engine/gamedefinitions/rf.gamedefinition.auto.js
//------------------------------------------------------

/**
 *
 */
// eslint-disable-next-line
function RFAuto(  ) {
    "use strict";

    var m_rfButtonsAuto = null;
    var m_rfLabelsAuto  = null;
    var m_rfImagesAuto  = null;
    var m_rfAuto = null;

    this.initialize = function( objAuto ) {

        m_rfAuto = {};
        m_rfButtonsAuto = new RFElementMap();
        m_rfLabelsAuto  = new RFElementMap();
        m_rfImagesAuto  = new RFElementMap();

        m_rfButtonsAuto.initialize( eRFElementType.BUTTON, objAuto.buttons );
        m_rfLabelsAuto.initialize( eRFElementType.LABEL, objAuto.labels );
        m_rfImagesAuto.initialize( eRFElementType.IMAGE, objAuto.images );

        var buttons = m_rfButtonsAuto.get();
        var labels = m_rfLabelsAuto.get();
        var images = m_rfImagesAuto.get();

        m_rfAuto.buttons = buttons;
        m_rfAuto.labels  = labels;
        m_rfAuto.images  = images;

    };

    this.getRFAutoMap = function() {
        return m_rfAuto;
    };
    this.getRFButtonsAutoMap = function() {
        return m_rfAuto.buttons;
    };
    this.getRFLabelsAutoMap = function() {
        return m_rfAuto.labels;
    };
    this.getRFImagesAutoMap = function() {
        return m_rfAuto.images;
    };
}

//------------------------------------------------------
// Source: src/engine/gamedefinitions/rf.gamedefinition.background.js
//------------------------------------------------------

/**
 *
 */
// eslint-disable-next-line
function RFBackground() {

    "use strict";

    /**
     * Identificador del fondo (es el código que se recibe desde el servidor).
     * @type {string}
     */
    this.id = "";

    /**
     * Imagen del fondo. Contiene su clave en Phaser y el atlas donde se encuentra.
     * @type {object}
     * @property {string} key Clave de Phaser
     * @property {string} atlas Atlas que contiene la imagen
     */
    this.image = {
        key   : "",
        atlas : ""
    };

    /**
     * Número de orden para permitir la ordenación de backgrounds
     */
    this.order = 0;

    /**
     * Inicializa el RFElement con la definición del GameDefinition
     * @param {object} oGameDefinition Definición del objeto
     * @param {string} id Id del RFElement
     */
    this.initialize = function( oGameDefinition, id ) {
        this.id = ( _.isString( id ) ? id : this.id );
        this.order = oGameDefinition.order;
        this.image.key = oGameDefinition.key;
        this.image.atlas = oGameDefinition.atlas;
    };

}


//------------------------------------------------------
// Source: src/engine/gamedefinitions/rf.gamedefinition.button.js
//------------------------------------------------------

/**
 *
 */
// eslint-disable-next-line
function RFButton() {
    "use strict";

    /**
     * Identificador de la boton (es el código que se recibe desde el servidor).
     * @type {string}
     */
    this.id = "";

    this.over = "";
    this.down = "";
    this.normal = "";
    this.disable = "";
    this.atlas = "";

    /**
     * Imagen del boton. Contiene su clave en Phaser y el atlas donde se encuentra.
     * @type {object}
     * @property {string} key Clave de Phaser
     * @property {string} atlas Atlas que contiene la imagen
     * @todo a futuro podria ser una lista de acciones
     */
    this.action = {
        name: "",
        parameters: {}
    };

    this.sounds = {
        over: "",
        down: ""
    };

    /**
     * Inicializa el RFElement con la definición del GameDefinition
     * @param {object} oGameDefinition Definición del objeto
     * @param {string} id Id del RFElement
     */
    this.initialize = function( oGameDefinition, id ) {
        this.id = ( _.isString( id ) ? id : this.id );
        this.over = oGameDefinition.over;
        this.down = oGameDefinition.down;
        this.normal = oGameDefinition.normal;
        this.disable = oGameDefinition.disable;
        this.atlas = oGameDefinition.atlas;
        this.action.name = _.get( oGameDefinition, [ "action", "name" ], this.action.name );
        this.action.parameters = _.get( oGameDefinition, [ "action", "parameters" ], this.action.parameters );
        this.sounds.over = _.get( oGameDefinition, [ "sounds", "over" ], this.sounds.over );
        this.sounds.down = _.get( oGameDefinition, [ "sounds", "down" ], this.sounds.down );
    };
}


//------------------------------------------------------
// Source: src/engine/gamedefinitions/rf.gamedefinition.elementfactory.js
//------------------------------------------------------

/**
 * @version 0.1.1
 * CAMBIOS
 * 0.1.1 - Se añade el multiplicador
 */

/**
 * Define la interfaz para los elementos del GameDefinition
 */
// eslint-disable-next-line
var iRFElement = new Interface( "iRFElement", [ "initialize" ] );

/**
 * Definición de los tipos de elementos
 */
// eslint-disable-next-line
var eRFElementType =  {
    BUTTON: "BUTTON",
    IMAGE: "IMAGE",
    LABEL: "LABEL",
    LINE: "LINE",
    PUNCH: "PUNCH",
    SOUND: "SOUND",
    SYMBOL: "SYMBOL",
    BACKGROUND: "BACKGROUND",
    EXTENDED_WILD: "EXTENDED_WILD",
    MULTIPLIER: "MULTIPLIER",
    ANIMATION: "ANIMATION"
};

/**
 * Factoría de creación de elementos
 */
// eslint-disable-next-line
var RFElementFactory = {

    // eslint-disable-next-line complexity
    createRFElement: function( rfElementType ) {
        "use strict";
        var rfElement;
        switch( rfElementType ) {
            case eRFElementType.BUTTON: rfElement = new RFButton(); break;
            case eRFElementType.IMAGE: rfElement = new RFImage(); break;
            case eRFElementType.LABEL: rfElement = new RFLabel(); break;
            case eRFElementType.LINE: rfElement = new RFLine(); break;
            case eRFElementType.PUNCH: rfElement = new RFPunch(); break;
            case eRFElementType.SOUND: rfElement = new RFSound(); break;
            case eRFElementType.SYMBOL: rfElement = new RFSymbol(); break;
            case eRFElementType.BACKGROUND: rfElement = new RFBackground(); break;
            case eRFElementType.EXTENDED_WILD: rfElement = new RFExtendedWild(); break;
            case eRFElementType.MULTIPLIER: rfElement = new RFMultiplier(); break;
            case eRFElementType.ANIMATION: rfElement = new RFAnimation(); break;
            default:
                throw new Error( "Invalid rfElementType '" + rfElementType + "'" );
        }

        Interface.ensureImplements( rfElement, iRFElement );
        return rfElement;
    }
};

//------------------------------------------------------
// Source: src/engine/gamedefinitions/rf.gamedefinition.elementmap.js
//------------------------------------------------------

/**
 *
 */
// eslint-disable-next-line
function RFElementMap() {
    "use strict";

    var m_rfElementMap = {};

    this.get = function( id ) {
        if( id !== undefined ) {
            return m_rfElementMap[ id ];
        }
        return m_rfElementMap;
    };

    this.initialize = function( elementType, gameDefinitionObjectMap ) {

        if( !_.isEmpty( m_rfElementMap ) ) {
            console.warn( "Reset the map with a new one", m_rfElementMap );
        }
        m_rfElementMap = {};

        // Build the map
        _.forEach( gameDefinitionObjectMap, function( oGameDefinition, key ) {

            // Create the element
            var rfElement = RFElementFactory.createRFElement( elementType );

            // Initialize and identifies it
            rfElement.initialize( oGameDefinition, key );

            // add to a map
            if( _.has( m_rfElementMap, key ) ) {
                console.warn( "Duplicate element in the map", m_rfElementMap, "with key", key,
                    "We override this", m_rfElementMap[ key ], "with this new one", rfElement );
            }

            m_rfElementMap[ key ] = rfElement;
        } );
    };
}

//------------------------------------------------------
// Source: src/engine/gamedefinitions/rf.gamedefinition.extendedwild.js
//------------------------------------------------------

/**
 * Definición de un wild extendido
 * @version 0.0.1
 * @constructor
 */
// eslint-disable-next-line
function RFExtendedWild() {
    "use strict";

    /**
     * Identificador del wild extendido (es el código que se recibe desde el servidor).
     * @type {string}
     */
    this.id = "";

    /**
     * Clave de Phaser asociado al Spritesheet.
     * @type {string}
     */
    this.key = "";

    /**
     * An array of numbers/strings that correspond to the frames to add to this animation and in which order.
     * e.g. [1, 2, 3] or ['run0', 'run1', run2]). If null then all frames will be used.
     * @type {array}
     */
    this.frames = null;

    /**
     * Número de fotogramas por segundo de la animación
     * @type {integer}
     */
    this.fps = 0;

    /**
     * Clave Phaser del sonido que se reproducirá en la animación
     * @type {string}
     */
    this.soundKey = "";

    /**
     * Inicializa el RFElement con la definición del GameDefinition
     * @param {object} oGameDefinition Definición del objeto
     * @param {string} id Id del RFElement
     */
    this.initialize = function( oGameDefinition, id ) {
        this.id = ( _.isString( id ) ? id : this.id );
        this.key = oGameDefinition.key;
        this.frames = oGameDefinition.frames;
        this.fps = oGameDefinition.fps;
        this.soundKey = oGameDefinition.soundKey;
    };
}


//------------------------------------------------------
// Source: src/engine/gamedefinitions/rf.gamedefinition.gamedefinition.js
//------------------------------------------------------

/**
 * @version 0.0.2
 */
// eslint-disable-next-line
var GameDefinition = null;

/**
 * Realiza la carga síncrona del fichero gamedefinition. En caso de que se esté intentando
 * cargar la versión móvil (con .mobile.json) y no se encuentre, se realiza un intento para
 * cargar el gamedefinition por defecto.
 */
// eslint-disable-next-line
function loadGameDefinitionFile( urlToGameDefinitionJSON, _isSecondTry ) {
    "use strict";
    var url = urlToGameDefinitionJSON; // "js/gamedefinitions/rf.gamedefinition" + ( isMobile() ? ".mobile" : "" ) + ".json";
    $.ajax( {
        url: url,
        async: false,
        dataType: "json",
        success: function( response ) {
            GameDefinition = response;
        },
        error: function( error ) {
            console.error( "Error loading GameDefinition:", urlToGameDefinitionJSON, error );

            var defaultGameDefinitionURL = _.replace( urlToGameDefinitionJSON, ".mobile.", "." );
            if( defaultGameDefinitionURL !== urlToGameDefinitionJSON ) {
                if( !_isSecondTry ) {
                    console.warn( "#####################################################" );
                    console.warn( "### WARN: Trying to get the default gamedefinition: ", defaultGameDefinitionURL );
                    console.warn( "#####################################################" );
                    loadGameDefinitionFile( defaultGameDefinitionURL, true );
                }
                else {
                    throw new Error( "Rare Error. Error loading GameDefinition: " + urlToGameDefinitionJSON );
                }
            }
            else {
                throw new Error( "Error loading GameDefinition: " + urlToGameDefinitionJSON );
            }
        }
    } );
}

//------------------------------------------------------
// Source: src/engine/gamedefinitions/rf.gamedefinition.image.js
//------------------------------------------------------

/**
 *
 */
// eslint-disable-next-line
function RFImage() {
    "use strict";

    /**
     * @type {string}
     */
    this.id = "";

    /**
     * Imagen de la imagen. Contiene su clave en Phaser y el atlas donde se encuentra.
     * @type {object}
     * @property {string} key Clave de Phaser
     * @property {string} atlas Atlas que contiene la imagen
     */
    this.image = {
        key   : "",
        atlas : ""
    };

    /**
     * Inicializa el RFElement con la definición del GameDefinition
     * @param {object} oGameDefinition Definición del objeto
     * @param {string} id Id del RFElement
     */
    this.initialize = function( oGameDefinition, id ) {
        this.id = ( _.isString( id ) ? id : this.id );
        this.image.key = oGameDefinition.key;
        this.image.atlas = oGameDefinition.atlas;
    };

}


//------------------------------------------------------
// Source: src/engine/gamedefinitions/rf.gamedefinition.label.js
//------------------------------------------------------

/**
 *
 */
// eslint-disable-next-line
function RFLabel() {
    "use strict";

    /**
     * Identificador del label (es el código que se recibe desde el servidor).
     * @type {string}
     */
    this.id = "";

    this.type = "";

    this.parameters = {  // xxx hay que quitar esto
        keySymbol :"",
        numberMatches : ""
    };

    this.rect = {
        x: "",
        y: "",
        height: "",
        width: ""
    };

    /**
     * Inicializa el RFElement con la definición del GameDefinition
     * @param {object} oGameDefinition Definición del objeto
     * @param {string} id Id del RFElement
     */
    this.initialize = function( oGameDefinition, id ) {
        this.id = ( _.isString( id ) ? id : this.id );
        this.type = oGameDefinition.type;
        this.parameters = oGameDefinition.parameters;
        this.rect = oGameDefinition.rect;
    };
}


//------------------------------------------------------
// Source: src/engine/gamedefinitions/rf.gamedefinition.line.js
//------------------------------------------------------

/**
 * Definición de una línea
 * @version 0.0.4
 * @constructor
 * CAMBIOS
 * 0.0.4 - Optimizados los accesos por lodash
 */
// eslint-disable-next-line
function RFLine() {

    "use strict";

    /**
     * Identificador de la línea (es el código que se recibe desde el servidor).
     * @type {string}
     */
    this.id = "";

    /**
     * Imagen de la linea. Contiene su clave en Phaser y el atlas donde se encuentra.
     * @type {object}
     * @property {string} key Clave de Phaser
     * @property {string} atlas Atlas que contiene la imagen
     * @property {object} rect Rectángulo de la línea
     */

    this.line = {
        key   : "",
        atlas : "",
        rect: {
            x: 0,
            y: 0,
            height: 0,
            width: 0
        }
    };

    /**
     * Imagen de la skittel. Contiene su clave en Phaser y el atlas donde se encuentra.
     * @type {object}
     * @property {string} key Clave de Phaser
     * @property {string} atlas Atlas que contiene la imagen
     * @property {object} rect Rectángulo del skittel
     */

    this.skittel = {
        key   : "",
        atlas : ""
    };

    /**
     * Imagen de la paybox. Contiene su clave en Phaser y el atlas donde se encuentra.
     * @type {object}
     * @property {string} key Clave de Phaser
     * @property {string} atlas Atlas que contiene la imagen
     */

    this.payBox = {
        key  : "",
        atlas: ""
    };

    /**
     * Inicializa el RFElement con la definición del GameDefinition
     * @param {object} oGameDefinition Definición del objeto
     * @param {string} id Id del RFElement
     */
    this.initialize = function( oGameDefinition, id ) {
        this.id = ( _.isString( id ) ? id : this.id );
        if( _.has( oGameDefinition, [ "line" ] ) ) {
            this.line.key = oGameDefinition.line.key;
            this.line.atlas = oGameDefinition.line.atlas;
            this.line.rect.x = oGameDefinition.line.rect.x;
            this.line.rect.y = oGameDefinition.line.rect.y;
            this.line.rect.width = oGameDefinition.line.rect.width;
            this.line.rect.height = oGameDefinition.line.rect.height;
        }
        if( _.has( oGameDefinition, [ "skittel" ] ) ) {
            this.skittel.key = oGameDefinition.skittel.key;
            this.skittel.atlas = oGameDefinition.skittel.atlas;
        }
        if( _.has( oGameDefinition, [ "payBox" ] ) ) {
            this.payBox.key = oGameDefinition.payBox.key;
            this.payBox.atlas = oGameDefinition.payBox.atlas;
        }
    };
}


//------------------------------------------------------
// Source: src/engine/gamedefinitions/rf.gamedefinition.multiplier.js
//------------------------------------------------------

/**
 * Definición de un multiplicador
 * @version 0.0.1
 */
// eslint-disable-next-line
var RFMultiplier = RFExtendedWild;

//------------------------------------------------------
// Source: src/engine/gamedefinitions/rf.gamedefinition.punch.js
//------------------------------------------------------

/**
 *
 */
// eslint-disable-next-line
function RFPunch() {
    "use strict";

    /**
     * Identificador del punch
     * @type {string}
     */
    this.id = "";

    /**
     * URL que contiene el JS con el punch a ejecutar
     */
    this.url = "";

    /**
     * Inicializa el RFElement con la definición del GameDefinition
     * @param {object} oGameDefinition Definición del objeto
     * @param {string} id Id del RFElement
     */
    this.initialize = function( oGameDefinition, id ) {
        this.id = ( _.isString( id ) ? id : this.id );
        this.url = oGameDefinition.url;
    };
}


//------------------------------------------------------
// Source: src/engine/gamedefinitions/rf.gamedefinition.sound.js
//------------------------------------------------------

/**
 *
 */
// eslint-disable-next-line
function RFSound() {
    "use strict";

    this.id = "";
    this.type = "";
    this.soundKeys = [];
    this.parameters = {};

    /**
     * Inicializa el RFElement con la definición del GameDefinition
     * @param {object} oGameDefinition Definición del objeto
     * @param {string} id Id del RFElement
     */
    this.initialize = function( oGameDefinition, id ) {
        this.id = ( _.isString( id ) ? id : this.id );
        this.type = oGameDefinition.type;
        this.soundKeys = oGameDefinition.soundKeys;
        this.parameters = _.get( oGameDefinition, [ "parameters" ], this.parameters );
    };

}


//------------------------------------------------------
// Source: src/engine/gamedefinitions/rf.gamedefinition.symbol.js
//------------------------------------------------------

/**
 * Definición de un símbolo
 * @version 0.2.0
 * @constructor
 * CAMBIOS
 * 0.0.2 - Optimizados los accesos por lodash
 * 0.1.0 - Se añade el atributo restoreOriginalSymbolAtEnd para las animaciones de cada símbolo. Refactoring de la creación de las animaciones.
 * 0.2.0 - Se modifica el objeto this.animations para incluir la variable "useAnimation" que controla la ejecución de las animaciones.
 */
// eslint-disable-next-line
function RFSymbol() {
    "use strict";

    var DEFAULT_ANIMATION_FPS = 30;

    /**
     * Identificador del símbolo (es el código que se recibe desde el servidor).
     * @type {string}
     */
    this.id = "";

    /**
     * Imagen del símbolo. Contiene su clave en Phaser y el atlas donde se encuentra.
     * @type {object}
     * @property {string} key Clave de Phaser
     * @property {string} atlas Atlas que contiene la imagen
     */
    this.image = {
        key: "",
        atlas: ""
    };

    /**
     * Animaciones del símbolo
     * @type {object}
     * @property {object} claim Animación de reclamo.
     * @property {string} claim.key Clave de Phaser asociado al Spritesheet.
     * @property {integer} claim.frames Número de frames de la animación.
     * @property {integer} claim.fps Número de fotogramas por segundo de la animación.
     * @property {string} claim.soundKey Clave Phaser del sonido que se reproducirá en la animación de reclamo (normalmente no hay ninguna)
     * @property {boolean} claim.restoreOriginalSymbolAtEnd Indica si tiene que restaurar el símbolo original al finalizar la animación
     * @property {boolean} claim.useAnimation Indica si se quiere usar la animación o no
     * @property {string} win Animación del icono cuando gana.
     * @property {string} win.key Clave de Phaser asociado al Spritesheet.
     * @property {integer} win.frames Número de frames de la animación.
     * @property {integer} win.fps Número de fotogramas por segundo de la animación.
     * @property {string} win.soundKey Clave Phaser del sonido que se reproducirá en la animación de premio
     * @property {boolean} win.restoreOriginalSymbolAtEnd Indica si tiene que restaurar el símbolo original al finalizar la animación
     * @property {boolean} win.useAnimation  Indica si tiene que restaurar el símbolo original al finalizar la animación
     */
    this.animations = {
        claim : {
            key      : "",
            frames   : 0,
            fps      : 0,
            soundKey : "",
            restoreOriginalSymbolAtEnd: true,
            useAnimation: false
        },
        win : {
            key      : "",
            frames   : 0,
            fps      : 0,
            soundKey : "",
            restoreOriginalSymbolAtEnd: true,
            useAnimation: true

        }
    };

    /**
     * Inicializa el RFElement con la definición del GameDefinition
     * @param {object} oGameDefinition Definición del objeto
     * @param {string} id Id del RFElement
     */
    this.initialize = function( oGameDefinition, id ) {
        this.id = ( _.isString( id ) ? id : this.id );

        this.image.key = oGameDefinition.key;
        this.image.atlas = oGameDefinition.atlas;

        initializeAnimation( this.animations.win, _.get( oGameDefinition, [ "animations", "win" ] ) );
        initializeAnimation( this.animations.claim, _.get( oGameDefinition, [ "animations", "claim" ] ) );

        // Si no hay ninguna referencia a la animación "claim" que el símbolo no haga nada
        if( _.has( oGameDefinition, [ "animations", "claim" ] ) ) {
            this.animations.claim.useAnimation = true;
        }

    };

    var initializeAnimation = function( oAnimation, gdAnimation ) {
        if( oAnimation && gdAnimation ) {
            oAnimation.key = gdAnimation.key;
            oAnimation.frames = gdAnimation.frames;
            oAnimation.fps = ( _.isNumber( gdAnimation.fps ) && gdAnimation.fps >= 0 ? gdAnimation.fps : DEFAULT_ANIMATION_FPS );
            if( _.has( gdAnimation, [ "soundKey" ] ) ) {
                oAnimation.soundKey = gdAnimation.soundKey;
            }
            oAnimation.restoreOriginalSymbolAtEnd = Boolean( _.get( gdAnimation, [ "restoreOriginalSymbolAtEnd" ], true ) );
        }
    };
}


//------------------------------------------------------
// Source: src/engine/gameloader/rf.loader.loadassets.js
//------------------------------------------------------

/**
 * @version 0.1.7
 * CAMBIOS
 * 0.1.4 - Se añade un parámetro nuevo 'loadAsset' (booleano) en los assets del GameDefinition para que no cargue un asset. Por defecto a true.
 * 0.1.5 - Se añade soporte para vídeos
 * 0.1.6 - Se refactoriza el código para la carga de mapa de assets
 * 0.1.7 - Optimizados los accesos por lodash
 */
// eslint-disable-next-line
function RFLoadAssets( game ) {
    "use strict";

    var self = this; // eslint-disable-line
    var m_game = null;
    var m_asyncLoader = null;

    var STR_LOAD_ASSET = "loadAsset";

    // Options: default values
    var m_options = {
        debug: false
    };
    this.setOptions = function( oOptions ) {
        m_options.debug = _.get( oOptions, [ "debug" ], m_options.debug );
        m_asyncLoader.setOptions( { debug: m_options.debug } );

        if( _.has( oOptions, [ "msToWaitNextAsyncLoad" ] ) ) {
            m_asyncLoader.setOptions( { msToWaitNextLoad: oOptions.msToWaitNextAsyncLoad } );
        }
    };

    /**
     * Realiza la carga de los assets pasados ( images, bitmapFonts, spriteSheets, webFonts )
     * @param {object} oAssets
     * @param {string=} strAssetsDesc
     */
    this.loadAssets = function( oAssets, strAssetsDesc ) {

        var numAssetsToLoad = 0;
        if( _.isObject( oAssets ) ) {

            if( m_options.debug ) { console.groupCollapsed( "LOAD ASSETS: " + ( _.isEmpty( strAssetsDesc ) ? "" : strAssetsDesc ) ); }

            numAssetsToLoad += loadImagesMap( oAssets.images );
            numAssetsToLoad += loadAtlasXMLsMap( oAssets.atlasXmls );
            numAssetsToLoad += loadSpriteSheetsMap( oAssets.spriteSheets );
            numAssetsToLoad += loadAudiosMap( oAssets.audios );
            numAssetsToLoad += loadBitmapFontsMap( oAssets.bitmapFonts );
            numAssetsToLoad += loadWebFontsMap( oAssets.webFonts );
            numAssetsToLoad += loadVideosMap( oAssets.videos );

            if( m_options.debug ) {
                console.log( "# Assets to load", numAssetsToLoad );
                console.groupEnd();
            }
        }

        return numAssetsToLoad;
    };


    /**
     * Función que carga un mapa de recursos genérico.
     * @param {object} oResourceMap
     * @param {function} fnLoadSingleResource
     * @param {string} strResource
     * @returns {integer} Número de assets cargados.
     */
    var loadResourceMap = function( oResourceMap, fnLoadSingleResource, strResource ) {
        if( !_.isFunction( fnLoadSingleResource ) ) { throw new Error( "fnLoadSingleResource is not a function" ); }

        var numAssetsToLoad = 0;
        strResource = strResource || "SINGLE RESOURCE";
        if( m_options.debug ) { console.group( strResource ); }
        _.forEach( _.keysOrdered( oResourceMap ), function( key ) {
            var oSingleResource = oResourceMap[ key ];
            if( fnLoadSingleResource( oSingleResource ) ) {
                numAssetsToLoad++;
            }
        } );
        if( m_options.debug ) { console.groupEnd(); }
        return numAssetsToLoad;
    };

    // Aliases
    var loadImagesMap       = function( oResourceMap ) { return loadResourceMap( oResourceMap, loadSingleImage,       "IMAGES" ); };
    var loadAtlasXMLsMap    = function( oResourceMap ) { return loadResourceMap( oResourceMap, loadSingleAtlasXML,    "ATLASXMLS" ); };
    var loadSpriteSheetsMap = function( oResourceMap ) { return loadResourceMap( oResourceMap, loadSingleSpriteSheet, "SPRITESHEETS" ); };
    var loadAudiosMap       = function( oResourceMap ) { return loadResourceMap( oResourceMap, loadSingleAudio,       "AUDIOS" ); };
    var loadBitmapFontsMap  = function( oResourceMap ) { return loadResourceMap( oResourceMap, loadSingleBitmapFont,  "BITMAPFONTS" ); };
    var loadWebFontsMap     = function( oResourceMap ) { return loadResourceMap( oResourceMap, loadSingleWebFont,     "WEBFONTS" ); };
    var loadVideosMap       = function( oResourceMap ) { return loadResourceMap( oResourceMap, loadSingleVideo,       "VIDEOS" ); };

    /**
     * Realiza la carga del atlasXML pasado
     * @param {object} oAtlasXML
     * @param {string}} oAtlasXML.key
     * @param {string}} oAtlasXML.textureURL
     * @param {string}} oAtlasXML.atlasURL
     */
    var loadSingleAtlasXML = function( oAtlasXML ) {
        assertAllKeys( oAtlasXML, [ "key", "textureURL", "atlasURL" ] );

        if( !_.get( oAtlasXML, [ STR_LOAD_ASSET ], true ) ) {
            if( m_options.debug ) { console.info( "atlasXML don't load (loadAsset = false):", oAtlasXML.key, oAtlasXML.textureURL, oAtlasXML.atlasURL ); }
            return false;
        }

        if( m_game.cache.checkImageKey( oAtlasXML.key ) ) {
            if( m_options.debug ) { console.log( "atlasXML (already in cache. We don't load it):", oAtlasXML.key, oAtlasXML.textureURL, oAtlasXML.atlasURL ); }
            return false;
        }

        m_game.load.atlasXML( oAtlasXML.key, oAtlasXML.textureURL, oAtlasXML.atlasURL );
        if( m_options.debug ) { console.log( "atlasXML:", oAtlasXML.key, oAtlasXML.textureURL, oAtlasXML.atlasURL ); }
        return true;
    };

    /**
     * Realiza la carga de todos los spritesshet incluídos en el objeto oSpriteSheetMap
     * @param {object} oSpriteSheetMap
     */
    var loadSpriteSheetsMapAsync = function( oSpriteSheetMap ) {
        if( m_options.debug ) { console.group( "SPRITESHEET ASYNC" ); }

        _.forEach( _.keysOrdered( oSpriteSheetMap ), function( key ) {
            var oSpriteSheet = oSpriteSheetMap[ key ];
            loadSingleSpriteSheetAsync( oSpriteSheet );
        } );
        if( m_options.debug ) { console.groupEnd(); }
    };

    /**
     * Realiza la carga del SpriteSheet pasado
     * @param {object} oSpriteSheet
     * @param {string} oSpriteSheet.key
     * @param {string} oSpriteSheet.url
     * @param {integer} oSpriteSheet.frameWidth
     * @param {integer} oSpriteSheet.frameHeight
     */
    var loadSingleSpriteSheet = function( oSpriteSheet ) {
        assertAllKeys( oSpriteSheet, [ "key", "url", "frameWidth", "frameHeight" ] );

        var loadAsset = false;
        var totalFrames = _.get( oSpriteSheet, [ "totalFrames" ], undefined );
        var asyncLoad = _.get( oSpriteSheet, [ "asyncLoad" ], false );

        if( !asyncLoad ) {

            if( !_.get( oSpriteSheet, [ STR_LOAD_ASSET ], true ) ) {
                if( m_options.debug ) {
                    console.info( "spriteSheet don't load (loadAsset = false):",
                        oSpriteSheet.key, oSpriteSheet.url, oSpriteSheet.frameWidth, oSpriteSheet.frameHeight, oSpriteSheet.totalFrames );
                }
            }
            else if( m_game.cache.checkImageKey( oSpriteSheet.key ) ) {
                if( m_options.debug ) {
                    console.log( "spriteSheet (already in cache. We don't load it):",
                        oSpriteSheet.key, oSpriteSheet.url, oSpriteSheet.frameWidth, oSpriteSheet.frameHeight, totalFrames );
                }
            }
            else {
                if( Number( totalFrames ) === 0 ) {
                    if( m_options.debug ) {
                        console.warn( "spriteSheet with totalFrames=0. WARNING: Phaser don't load it:",
                            oSpriteSheet.key, oSpriteSheet.url, oSpriteSheet.frameWidth, oSpriteSheet.frameHeight, totalFrames );
                    }
                }
                m_game.load.spritesheet( oSpriteSheet.key, oSpriteSheet.url, oSpriteSheet.frameWidth, oSpriteSheet.frameHeight, totalFrames );
                if( m_options.debug ) {
                    console.log( "spriteSheet:",
                        oSpriteSheet.key, oSpriteSheet.url, oSpriteSheet.frameWidth, oSpriteSheet.frameHeight, totalFrames );
                }
                loadAsset = true;
            }
        }

        return loadAsset;
    };

    /**
     * Realiza la carga del vídeo pasado
     * @param {object} oVideo
     * @param {string} oVideo.key
     * @param {string} oVideo.urls
     */
    var loadSingleVideo = function( oVideo ) {
        assertAllKeys( oVideo, [ "key", "url" ] );

        if( !_.get( oVideo, [ STR_LOAD_ASSET ], true ) ) {
            if( m_options.debug ) { console.info( "video don't load (loadAsset = false):", oVideo.key, oVideo.urls ); }
            return false;
        }

        var asyncLoad = _.get( oVideo, [ "asyncLoad" ], false );
        if( asyncLoad ) {
            if( m_options.debug ) { console.info( "Async load not implemented for videos. Load in standar way." ); }
        }

        if( m_game.cache.checkVideoKey( oVideo.key ) ) {
            if( m_options.debug ) { console.log( "video (already in cache. We don't load it):", oVideo.key, oVideo.urls ); }
            return false;
        }

        if( m_options.debug ) { console.log( "video:", oVideo.key, oVideo.url ); }
        m_game.load.video( oVideo.key, oVideo.url );
        return true;
    };

    /**
     * Realiza la carga del audio pasado
     * @param {object} oAudio
     * @param {string} oAudio.key
     * @param {string} oAudio.urls
     */
    var loadSingleAudio = function( oAudio ) {
        assertAllKeys( oAudio, [ "key", "urls" ] );

        if( !_.get( oAudio, [ STR_LOAD_ASSET ], true ) ) {
            if( m_options.debug ) { console.info( "audio don't load (loadAsset = false):", oAudio.key, oAudio.urls ); }
            return false;
        }

        var asyncLoad = _.get( oAudio, [ "asyncLoad" ], false );
        if( asyncLoad ) {
            if( m_options.debug ) { console.info( "Async load not implemented for audios. Load in standar way." ); }
        }

        if( m_game.cache.checkSoundKey( oAudio.key ) ) {
            if( m_options.debug ) { console.log( "audio (already in cache. We don't load it):", oAudio.key, oAudio.urls ); }
            return false;
        }

        if( m_options.debug ) { console.log( "audio:", oAudio.key, oAudio.urls ); }
        m_game.load.audio( oAudio.key, oAudio.urls );
        return true;
    };

    /**
     * Realiza la carga de la imagen pasada
     * @param {object} oImage
     * @param {string} oImage.key
     * @param {string} oImage.url
     */
    var loadSingleImage = function( oImage ) {
        assertAllKeys( oImage, [ "key", "url" ] );

        if( !_.get( oImage, [ STR_LOAD_ASSET ], true ) ) {
            if( m_options.debug ) { console.info( "image don't load (loadAsset = false):", oImage.key, oImage.url ); }
            return false;
        }

        var asyncLoad = _.get( oImage, [ "asyncLoad" ], false );
        if( asyncLoad ) {
            if( m_options.debug ) { console.info( "Async load not implemented for images. Load in standar way." ); }
        }

        // Este chequeo se realiza debido a un problema con PHASER y su carga asíncrona y carga por lotes.
        // La carga de la barra y background desde el GameDefinition, dado que están en assets y
        // se han de cargar en el boot y posteriormente se cargarán como assets en el preload, hará que
        // Phaser "borre" la textura base (la imagen en sí) en la segunda carga, pero sí que la tiene
        // en caché (a nivel lógico) y al ser asíncrona la nueva carga, al intentar renderizar antes
        // de que se vuelva a cargar, el motor cascará o mostrará un warning dependiendo de si es
        // WebGL o CANVAS. En caso de que le de tiempo a cargarlo, no cascaría.
        // El problema es algo más complejo que esto, pero se deja como nota aclaratoria de este chequeo
        // Y no, a pesar de que se puede pasar "override" (que ya de por sí, se pasa a false), no funciona, ya
        // que el problema es que Phaser no chequea su caché, sino la lista de batch de ficheros que va a cargar
        // (que es con el que mira el override)
        if( m_game.cache.checkImageKey( oImage.key ) ) {
            if( m_options.debug ) { console.log( "image (already in cache. We don't load it):", oImage.key, oImage.url ); }
            return false;
        }

        m_game.load.image( oImage.key, oImage.url );
        if( m_options.debug ) { console.log( "image:", oImage.key, oImage.url ); }
        return true;
    };

    /**
     * Realiza la carga del bitmapFont
     * @param {object} oBitmapFont
     * @param {string} oBitmapFont.key
     * @param {string} oBitmapFont.textureURL
     * @param {string} oBitmapFont.xmlURL
     */
    var loadSingleBitmapFont = function( oBitmapFont ) {
        assertAllKeys( oBitmapFont, [ "key", "textureURL", "xmlURL" ] );

        if( !_.get( oBitmapFont, [ STR_LOAD_ASSET ], true ) ) {
            if( m_options.debug ) { console.info( "bitmapFont don't load (loadAsset = false):", oBitmapFont.key, oBitmapFont.textureURL, oBitmapFont.xmlURL ); }
            return false;
        }

        var asyncLoad = _.get( oBitmapFont, [ "asyncLoad" ], false );
        if( asyncLoad ) {
            if( m_options.debug ) { console.info( "Async load not implemented for bitmapfont. Load in standar way." ); }
        }

        if( m_game.cache.checkBitmapFontKey( oBitmapFont.key ) ) {
            if( m_options.debug ) {
                console.log( "bitmapFont (already in cache. We don't load it):",
                    oBitmapFont.key, oBitmapFont.textureURL, oBitmapFont.xmlURL );
            }
            return false;
        }

        m_game.load.bitmapFont( oBitmapFont.key, oBitmapFont.textureURL, oBitmapFont.xmlURL );
        if( m_options.debug ) { console.log( "bitmapFont:", oBitmapFont.key, oBitmapFont.textureURL, oBitmapFont.xmlURL ); }
        return true;
    };

    /**
     * Realiza la carga del webFont
     * @param {object} oBitmapFont
     * @param {string} oBitmapFont.key
     * @param {string} oBitmapFont.textureURL
     * @param {string} oBitmapFont.xmlURL
     * @todo Al ser una implementación propia, de momento no se ha implementado una forma para
     * saber si el webFont está cargado en caché, por lo que siempre devuelve false, para
     * hacer que no se tenga en cuenta en el recuento de assets a cargar.
     */
    var loadSingleWebFont = function( oWebFont ) {
        assertAllKeys( oWebFont, [ "key", "url" ] );

        if( !_.get( oWebFont, [ STR_LOAD_ASSET ], true ) ) {
            if( m_options.debug ) { console.info( "oWebFont don't load (loadAsset = false):", oWebFont.key, oWebFont.url ); }
            return false;
        }

        var asyncLoad = _.get( oWebFont, [ "asyncLoad" ], false );
        if( asyncLoad ) {
            if( m_options.debug ) { console.info( "Async load not implemented for webFont. Load in standar way." ); }
        }

        m_game.load.webfont( oWebFont.key, oWebFont.url );
        if( m_options.debug ) { console.log( "webFont:", oWebFont.key, oWebFont.url ); }
        return true;
    };


    // ----------------------------------------------------------------- BEGIN ASYNCHRONOUS LOAD ---

    /**
     * Realiza la carga asíncrona de los assets pasados ( únicamente spriteSheets )
     * @param {object} oAssets
     * @param {string=} strAssetsDesc
     */
    this.loadAssetsAsync = function( oAssets, strAssetsDesc ) {
        if( _.isObject( oAssets ) ) {

            if( m_options.debug ) { console.groupCollapsed( "LOAD ASSETS ASYNC: " + ( _.isEmpty( strAssetsDesc ) ? "" : strAssetsDesc ) ); }

            loadSpriteSheetsMapAsync( oAssets.spriteSheets );

            if( m_options.debug ) { console.groupEnd(); }

            m_asyncLoader.startLoad( strAssetsDesc );

        }
    };

    /**
     * Realiza una parada de la carga de los assets ( útil si solo queremos cargar los
     * assets de manera asíncrona solo en la escena actual )
     */
    this.stopAsyncLoad = function() {
        m_asyncLoader.stopLoad();
    };

    /**
     * Realiza una pausa de la carga de los assets (se usa en cambio de escenas)
     */
    this.pauseAsyncLoad = function() {
        m_asyncLoader.pauseLoad();
    };

    /**
     * Continúa la descarga de assets que haya estado pausada a través de pauseAsyncLoad
     */
    this.continueAsyncLoad = function() {
        m_asyncLoader.continueLoad();
    };

    /**
     * Inicia la carga de assets en caso de haber sido parada. Si se para con "stop", al cancelar el temporizador del m_asyncLoader
     * Se perdería la descarga actual. Por eso se recomienda "pauseAsyncLoad" + "continueAsyncLoad"
     */
    this.startAsyncLoad = function() {
        m_asyncLoader.startLoad();
    };

    /**
     * Realiza la carga del SpriteSheet pasado
     * @param {object} oSpriteSheet
     * @param {string} oSpriteSheet.key
     * @param {string} oSpriteSheet.url
     * @param {integer} oSpriteSheet.frameWidth
     * @param {integer} oSpriteSheet.frameHeight
     */
    var loadSingleSpriteSheetAsync = function( oSpriteSheet ) {
        assertAllKeys( oSpriteSheet, [ "key", "url", "frameWidth", "frameHeight" ] );

        var asyncLoad = _.get( oSpriteSheet, [ "asyncLoad" ], false );
        if( asyncLoad ) {

            if( !_.get( oSpriteSheet, [ STR_LOAD_ASSET ], true ) ) {
                if( m_options.debug ) {
                    console.info( "spriteSheet (async) don't load (loadAsset = false):",
                        oSpriteSheet.key, oSpriteSheet.url, oSpriteSheet.frameWidth, oSpriteSheet.frameHeight, oSpriteSheet.totalFrames );
                }
            }

            var asyncObj = new RFAsyncObject( oSpriteSheet );
            asyncObj.totalFrames = _.get( oSpriteSheet, [ "totalFrames" ], -1 );
            asyncObj.forMobile = _.get( oSpriteSheet, [ "forMobile" ], true );

            if( Number( asyncObj.totalFrames ) === 0 ) {
                if( m_options.debug ) {
                    console.warn( "spriteSheet with totalFrames=0. WARNING: Phaser don't load it:",
                        oSpriteSheet.key, oSpriteSheet.url, oSpriteSheet.frameWidth, oSpriteSheet.frameHeight, asyncObj.totalFrames );
                }
            }

            if( m_options.debug ) {
                console.log( "spriteSheet:",
                    oSpriteSheet.key, oSpriteSheet.url, oSpriteSheet.frameWidth, oSpriteSheet.frameHeight, asyncObj.totalFrames );
            }

            m_asyncLoader.addSpriteSheet( asyncObj );
        }
    };

    // ----------------------------------------------------------------- END ASYNCHRONOUS LOAD ---

    var constructor = function( gameObj ) {
        if( !isValidGame( gameObj ) ) {
            throw new Error( "RFLoadAssets: 'game' is not an object" );
        }

        m_game = gameObj;
        m_asyncLoader = new RFAsyncLoader( m_game );
        m_asyncLoader.setOptions( { debug: m_options.debug } );
    };

    constructor( game );
}

//------------------------------------------------------
// Source: src/engine/gameloader/rf.loader.loadscene.js
//------------------------------------------------------

/* eslint-disable max-lines */
/**
 * @version 0.8.0
 * @todo
 * - Recibir game como objeto donde está el juego. Ahora se accede a la varible global "game", que pudiera tener otro nombre.
 * CAMBIOS:
 * 0.2.16 - Se incluyen templates para las labels
 * 0.2.17 - Se añade soporte para definir "anchor" (images, animations, buttons, backgrounds) y "pivot" (groups)
 * 0.2.18 - Se permite definir paytables para los labels
 * 0.2.19 - Soporte avanzado de templates.
 * 0.2.20 - Evita el tipado a TEMPLATE por herencia de objetos que en realidad no son templates.
 * 0.3.0  - Refactorización completa.
 * 0.3.1  - Soporte para pintado de canvas
 * 0.3.2  - Cambio del nodo "canvas" por "canvasContainer"
 * 0.3.3  - Evita lanzar excepciones en la construcción de textos. Si hay bitmapFont sin key, se crea una webFont con un estilo por defecto.
 * 0.3.4  - Añade la función setFormatedString que permite formatos dentro del texto mediante
 *          tags [color="..."], [strokeColor="..."], [fontStyle="..."] y [fontWeight= "..."]
 *          Se añaden además, funciones para resetar el formato de los textos (clearColors, clearFontStyles, clearFontWeights, clearStrokeColors, clearFormats )
 * 0.3.5  - Se añade soporte para vídeos
 * 0.3.6  - Soluciona el problema de formateo con saltos de línea para las labels
 * 0.3.7  - Añade el método setFormattedString y deja setFormatedString como deprecated
 * 0.3.8  - Elimina el método setFormatedString.
 * 0.3.9  - Se añaden nuevos atributos para el manejo de las fuentes de tipo webFont
 * 0.3.10 - Solo aplica el Fix para Phaser en el setFormattedString cuando NO usa 'wordWrap' (que en ese caso sí funciona correctamente)
 * 0.3.11 - Optimizados los accesos por lodash
 * 0.3.12 - Elimina las extensiones de funcionalidad para los labels
 * 0.4.0  - Eliminada dependencia externa
 * 0.4.1  - Se vuelve a mantener la dependencia externa hasta la próxima actualización.
 * 0.4.2  - Se añade la propiedad 'autoPlay' para las animaciones.
 * 0.4.3  - Se añade la información del punch cargado incorrectamente.
 * 0.5.0  - Se Añaden templates locales a la escena y globales. Soluciona el merge para obtener la definición de un objeto con templates.
 * 0.5.1  - Se permite la carga de punches de forma síncrona.
 * 0.5.2. - Se añade la posibilidad de posicionar grupos.
 * 0.6.0  - Se añade la opción de añadir sonido a las labels, para los valores de contador.
 * 0.7.0  - Se añade la creación de las tooltips
 * 0.8.0  - Se añade el método isEnabled para los botones.
 */

// eslint-disable-next-line
function RFLoadScenes( gameDefinition ) {
    "use strict";

    // ---------------------------------------------------
    // #region ENUMS AND CONSTANTS
    // ---------------------------------------------------

    var STR_TEMPLATE = "TEMPLATE";
    var DEFAULT_WEBFONT_STYLE = { fill: "black", backgroundColor: "yellow" };
    var DEFAULT_FONT_SIZE = 20;
    var DEFAULT_RECT_WIDTH = 100;
    var DEFAULT_RECT_HEIGHT = 100;

    // GameDefinition nodes
    var STR_TEMPLATES = "templates";
    var STR_BUTTONS = "buttons";
    var STR_VIDEOS = "videos";
    var STR_BACKGROUNDS = "backgrounds";
    var STR_LINES = "lines";
    var STR_LABELS = "labels";
    var STR_SYMBOLS = "symbols";
    var STR_IMAGES = "images";
    var STR_SOUNDS = "sounds";
    var STR_PUNCHES = "punches";
    var STR_EXTENDEDWILDS = "extendedWilds";
    var STR_MULTIPLIERS = "multipliers";
    var STR_ANIMATIONS = "animations";
    var STR_REELS = "reels";
    var STR_AUTO = "auto";
    var STR_AUTO_IMAGES = STR_AUTO + "." + STR_IMAGES;
    var STR_AUTO_LABELS = STR_AUTO + "." + STR_LABELS;
    var STR_AUTO_BUTTONS = STR_AUTO + "." + STR_BUTTONS;
    var STR_GROUPS = "groups";
    var STR_CANVAS = "canvasContainer";

    var eGameDefinitionType = {
        TEXT: "TEXT",
        VIDEO: "VIDEO",
        BACKGROUND: "BACKGROUND",
        IMAGE: "IMAGE",
        ANIMATION: "ANIMATION",
        BUTTON: "BUTTON",
        LINE: "LINE",
        SKITTLE: "SKITTLE",
        GROUP: "GROUP"
    };
    Object.freeze( eGameDefinitionType );

    // ---------------------------------------------------
    // #endregion ENUMS AND CONSTANTS
    // ---------------------------------------------------

    // ---------------------------------------------------
    // #region PROPERTIES
    // ---------------------------------------------------

    // RF Maps container objects
    var m_rfLines        = null;
    var m_rfSymbols      = null;
    var m_rfGraphicReels = null;
    var m_rfSounds       = null;
    var m_rfExtendWilds  = null;
    var m_rfMultipliers  = null;

    var m_scene = "";

    // Phaser built objects
    var m_phaserButtons     = {};
    var m_phaserVideos      = {};
    var m_phaserSkittels    = {};
    var m_phaserLines       = {};
    var m_phaserBackgrounds = {};
    var m_phaserButtonsAuto = {};
    var m_phaserLabelsAuto  = {};
    var m_phaserImagesAuto  = {};
    var m_phaserImages      = {};
    var m_phaserLabels      = {};
    var m_phaserAnimations  = {};
    var m_phaserGroups      = {};
    var m_phaserRootGroup   = null;

    // GameDefinition references
    var m_gdGlobalTemplates = {};
    var m_gdScene           = {};
    var m_gdSceneTemplates  = {};
    var m_gdButtons         = {};
    var m_gdVideos          = {};
    var m_gdAuto            = {}; // eslint-disable-line
    var m_gdImagesAuto      = {};
    var m_gdLabelsAuto      = {};
    var m_gdButtonsAuto     = {};
    var m_gdBackgrounds     = {};
    var m_gdImages          = {};
    var m_gdLines           = {};
    var m_gdLabels          = {};
    var m_gdReels           = {};
    var m_gdPunches         = {};
    var m_gdSymbols         = {};
    var m_gdSounds          = {};
    var m_gdExtendWilds     = {};
    var m_gdMultipliers     = {};
    var m_gdAnimations      = {};
    var m_gdGroups          = {};
    var m_gdCanvas          = []; // eslint-disable-line

    var m_gameDefinition = gameDefinition;
    if( m_gameDefinition === undefined ) {
        if( typeof GameDefinition !== "undefined" ) {
            console.warn( "loadScene: Using GameDefinition global object. You should provide the gameDefinitionFile" );
            // TODO: Fix this. Habrá que cambiar los juegos para que pasen un gameDefinition como parámetro el la siguiente versión.
            // eslint-disable-next-line
            m_gameDefinition = GameDefinition;
        }
        else {
            console.error( "loadScene. No GameDefinition at this point (via function parameter or GameDefinition global var)" );
            m_gameDefinition = {};
        }
    }
    // ---------------------------------------------------
    // #endregion PROPERTIES
    // ---------------------------------------------------

    // ---------------------------------------------------
    // #region CLEAR and INIT
    // ---------------------------------------------------
    var clear = function() {
        m_scene = "";

        m_rfLines        = new RFElementMap();
        m_rfSymbols      = new RFElementMap();
        m_rfSounds       = new RFElementMap();
        m_rfExtendWilds  = new RFElementMap();
        m_rfMultipliers  = new RFElementMap();

        m_rfGraphicReels = new RFGraphicReels( game );

        m_phaserButtons     = {};
        m_phaserVideos      = {};
        m_phaserSkittels    = {};
        m_phaserLines       = {};
        m_phaserBackgrounds = {};
        m_phaserButtonsAuto = {};
        m_phaserLabelsAuto  = {};
        m_phaserImagesAuto  = {};
        m_phaserImages      = {};
        m_phaserLabels      = {};
        m_phaserAnimations  = {};
        m_phaserGroups      = {};
        m_phaserRootGroup   = null;

        m_gdGlobalTemplates = {};
        m_gdScene           = {};
        m_gdSceneTemplates  = {};
        m_gdButtons         = {};
        m_gdAuto            = {};
        m_gdVideos          = {};
        m_gdImagesAuto      = {};
        m_gdLabelsAuto      = {};
        m_gdButtonsAuto     = {};
        m_gdBackgrounds     = {};
        m_gdImages          = {};
        m_gdLines           = {};
        m_gdLabels          = {};
        m_gdReels           = {};
        m_gdPunches         = {};
        m_gdSymbols         = {};
        m_gdSounds          = {};
        m_gdExtendWilds     = {};
        m_gdMultipliers     = {};
        m_gdAnimations      = {};
        m_gdGroups          = {};
        m_gdCanvas          = [];
    };

    var init = function( scene ) {

        clear();

        var result = false;
        if( _.has( m_gameDefinition, [ "scenes", scene ] ) ) {

            m_scene = scene;

            m_gdGlobalTemplates = _.get( m_gameDefinition, STR_TEMPLATES );

            m_gdScene = m_gameDefinition.scenes[ m_scene ];

            m_gdSceneTemplates = _.get( m_gdScene, STR_TEMPLATES, {} );
            m_gdButtons        = _.get( m_gdScene, STR_BUTTONS, {} );
            m_gdVideos         = _.get( m_gdScene, STR_VIDEOS, {} );
            m_gdBackgrounds    = _.get( m_gdScene, STR_BACKGROUNDS, {} );
            m_gdLines          = _.get( m_gdScene, STR_LINES, {} );
            m_gdLabels         = _.get( m_gdScene, STR_LABELS, {} );
            m_gdSymbols        = _.get( m_gdScene, STR_SYMBOLS, {} );
            m_gdImages         = _.get( m_gdScene, STR_IMAGES, {} );
            m_gdSounds         = _.get( m_gdScene, STR_SOUNDS, {} );
            m_gdPunches        = _.get( m_gdScene, STR_PUNCHES, {} );
            m_gdExtendWilds    = _.get( m_gdScene, STR_EXTENDEDWILDS, {} );
            m_gdMultipliers    = _.get( m_gdScene, STR_MULTIPLIERS, {} );
            m_gdAnimations     = _.get( m_gdScene, STR_ANIMATIONS, {} );
            m_gdReels          = _.get( m_gdScene, STR_REELS, {} );
            m_gdAuto           = _.get( m_gdScene, STR_AUTO, {} );
            m_gdImagesAuto     = _.get( m_gdScene, STR_AUTO_IMAGES, {} );
            m_gdLabelsAuto     = _.get( m_gdScene, STR_AUTO_LABELS, {} );
            m_gdButtonsAuto    = _.get( m_gdScene, STR_AUTO_BUTTONS, {} );
            m_gdGroups         = _.get( m_gdScene, STR_GROUPS, {} );
            m_gdCanvas         = _.get( m_gdScene, STR_CANVAS, [] );

            m_rfSymbols.initialize( eRFElementType.SYMBOL, m_gdSymbols );
            m_rfLines.initialize( eRFElementType.LINE, m_gdLines );
            m_rfSounds.initialize( eRFElementType.SOUND, m_gdSounds );
            m_rfExtendWilds.initialize( eRFElementType.EXTENDED_WILD, m_gdExtendWilds );
            m_rfMultipliers.initialize( eRFElementType.MULTIPLIER, m_gdMultipliers );

            initializeTemplates( m_gdGlobalTemplates );
            initializeTemplates( m_gdSceneTemplates );

            result = true;
        }
        else {
            console.warn( "Can't initalize the scene", scene, "Not exists in GameDefinition" );
        }

        return result;
    };

    var initializeTemplates = function( templatesMap ) {
        _.forEach( templatesMap, function( template ) {
            _.set( template, "isTemplate", true );
        } );
    };

    // ---------------------------------------------------
    // #endregion CLEAR and INIT
    // ---------------------------------------------------

    // --------------------------------------------------------------
    // #region GROUPS
    // --------------------------------------------------------------
    var loadGroups = function() {
        m_phaserRootGroup = null;
        if( !_.isEmpty( m_gdGroups ) ) {
            m_phaserRootGroup = createPhaserGroup( getRect(), "ROOT GROUP (" + m_scene + ")" );
            var createdObjects = createGroups();

            addCreatedObjectsToGroup( createdObjects, m_phaserRootGroup );
        }
    };

    var reassignChildGroups = function() {
        _.forEach( _.keysOrdered( m_phaserGroups ), function( key ) {
            var keyGroupThatBelongsTo = _.get( m_gdGroups, [ key, "group" ] );
            if( keyGroupThatBelongsTo ) {
                var phaserGroup = m_phaserGroups[ keyGroupThatBelongsTo ];
                if( phaserGroup ) {
                    phaserGroup.add( m_phaserGroups[ key ] );
                }
                else {
                    console.warn( "The group [", keyGroupThatBelongsTo, "] is not found to reassing nested groups" );
                }
            }
        } );
    };

    var addToGroup = function( phaserObj, gdElement ) {
        var groupId = _.get( gdElement, "group" );
        if( groupId ) {
            if( m_phaserGroups[ groupId ] ) {
                m_phaserGroups[ groupId ].add( phaserObj );
            }
            else {
                console.warn( "The groupId [", groupId, "] is not definned for", phaserObj );
            }
        }
    };
    // --------------------------------------------------------------
    // #endregion GROUPS
    // --------------------------------------------------------------

    // --------------------------------------------------------------
    // #region CREATE FUNCTIONS
    // --------------------------------------------------------------

    /**
     * Función genérica para crear objetos
     * @param {object} phaserMap Mapa donde se dejarán creados los objetos creados
     * @param {object} gdMap Mapa donde se encuentran ls definición de los objetos a crear
     * @param {function} createFunction Función de creación donde se le pasa la definición y el id asociado
     */
    var createObjects = function( phaserMap, gdMap, createFunction ) {
        if( !_.isObject( phaserMap ) || !_.isObject( gdMap ) || !_.isFunction( createFunction ) ) { return undefined; }

        var createdObjectsArray = [];
        _.forEach( _.keysOrdered( gdMap ), function( id ) {
            var gdObj = getFinalDefinition( gdMap[ id ], gdMap );
            if( !isTemplate( gdObj ) ) {
                createdObjectsArray = _.concat( createdObjectsArray, createFunction( gdObj, id ) );
            }
        } );

        addCreatedObjectsToPhaserMap( createdObjectsArray, phaserMap );

        return createdObjectsArray;
    };

    var createButtons = function( phaserMap, gdMap ) {
        return createObjects( ( phaserMap || m_phaserButtons ), ( gdMap || m_gdButtons ), createButton );
    };

    var createLines = function( phaserMap, gdMap ) {
        return createObjects( ( phaserMap || m_phaserLines ), ( gdMap || m_gdLines ), createLine );
    };

    var createSkittles = function( phaserMap, gdMap ) {
        return createObjects( ( phaserMap || m_phaserSkittels ), ( gdMap || m_gdLines ), createSkittle );
    };

    var createBackgrounds = function( phaserMap, gdMap ) {
        return createObjects( ( phaserMap || m_phaserBackgrounds ), ( gdMap || m_gdBackgrounds ), createBackground );
    };

    var createImages = function( phaserMap, gdMap ) {
        return createObjects( ( phaserMap || m_phaserImages ), ( gdMap || m_gdImages ), createImage );
    };

    var createVideos = function( phaserMap, gdMap ) {
        return createObjects( ( phaserMap || m_phaserVideos ), ( gdMap || m_gdVideos ), createVideo );
    };

    var createAnimations = function( phaserMap, gdMap ) {
        return createObjects( ( phaserMap || m_phaserAnimations ), ( gdMap || m_gdAnimations ), createAnimation );
    };

    var createLabels = function( phaserMap, gdMap ) {
        return createObjects( ( phaserMap || m_phaserLabels ), ( gdMap || m_gdLabels ), createLabel );
    };

    var createAdvancedLabels = function( phaserMap, gdMap ) {
        return createObjects( ( phaserMap || m_phaserLabels ), ( gdMap || m_gdLabels ), createAdvancedLabel );
    };

    var createGroups = function( phaserMap, gdMap ) {
        return createObjects( ( phaserMap || m_phaserGroups ), ( gdMap || m_gdGroups ), createGroup );
    };


    var createAuto = function() {
        return _.concat(
            createImages( m_phaserImagesAuto, m_gdImagesAuto, createImage ),
            createButtons( m_phaserButtonsAuto, m_gdButtonsAuto, createButton ),
            createLabels( m_phaserLabelsAuto, m_gdLabelsAuto, createLabel )
        );
    };

    var createReels = function() {
        if( _.size( m_gdReels ) > 0 ) {
            var payBoxSize = getPayBoxSize( m_gdScene );
            m_rfGraphicReels.initialize( {
                reelsDefinition: m_gdReels,
                payBoxHeight: payBoxSize.height,
                payBoxWidth: payBoxSize.width
            } );
        }
    };

    var createLine = function( gdObj, id ) {

        var phaserObj;
        if( _.has( gdObj, [ "line" ] ) ) {
            if( !_.isEmpty( _.get( gdObj, [ "line", "key" ] ) ) ) {
                phaserObj = createPhaserSprite( getRect( gdObj.line ), gdObj.line.atlas, gdObj.line.key );
            }
            else {
                phaserObj = createPhaserSprite( getRect() );
            }
            addCommonProperties( phaserObj, gdObj.line, id, eGameDefinitionType.LINE );

            addToGroup( phaserObj, gdObj.line );
        }

        return makeCreatedObject( phaserObj, id );
    };

    var createSkittle = function( gdObj, id ) {

        var phaserObj;
        if( _.has( gdObj, [ "skittel" ] ) ) {
            phaserObj = createPhaserButton( getRect( gdObj.skittel ), gdObj.skittel.atlas, undefined, gdObj.skittel.key );

            addCommonProperties( phaserObj, gdObj.skittel, id, eGameDefinitionType.SKITTLE );

            addToGroup( phaserObj, gdObj.skittel );
        }
        return makeCreatedObject( phaserObj, id );
    };

    /**
     * Crea imagen en phaser (o un tilesprite si está definido) y devuelve el objeto creado
     * @param {object} gdObj La definición del objeto
     * @param {string} id El identificativo de la definición del objeto
     * @return {object} El objeto de Phaser creado
     */
    var createBackground = function( gdObj, id ) {

        var createdObj = createImage( gdObj, id );
        var phaserObj = _.get( createdObj, [ "phaserObj" ] );
        if( phaserObj ) {

            // Override the gameDefinitioType
            _.set( phaserObj, "data.gameDefinitionType", eGameDefinitionType.BACKGROUND );

            // custom properties
            phaserObj.order = gdObj.order;

        }

        return createdObj;
    };

    /**
     * Crea imagen en phaser (o un tilesprite si está definido) y devuelve el objeto creado
     * @param {object} gdObj La definición del objeto
     * @param {string} id El identificativo de la definición del objeto
     * @return {object} El objeto de Phaser creado
     */
    var createImage = function( gdObj, id ) {

        var phaserObj = createPhaserSpriteOrTileSprite( gdObj, id );
        if( phaserObj ) {
            addCommonProperties( phaserObj, gdObj, id, eGameDefinitionType.IMAGE );

            // custom properties
            phaserObj.image = gdObj.key; // xxx this is for backward compatibility. Some scenes use it (???). Should be deprecated.

            // add to group
            addToGroup( phaserObj, gdObj );
        }

        return makeCreatedObject( phaserObj, id );
    };

    /**
     * Crea vídeo en phaser y devuelve el objeto creado
     * @param {object} gdObj La definición del objeto
     * @param {string} id El identificativo de la definición del objeto
     * @return {object} El objeto de Phaser creado
     */
    var createVideo = function( gdObj, id ) {

        var phaserObj = createPhaserVideo( gdObj, id );
        if( phaserObj ) {
            addCommonProperties( phaserObj, gdObj, id, eGameDefinitionType.VIDEO );

            // add custom properties
            var video = phaserObj.video;
            video.loop = _.get( gdObj, [ "loop" ], video.loop );
            video.playbackRate = _.get( gdObj, [ "playbackRate" ], video.playbackRate );

            // autoplay behaviour
            if( _.get( gdObj, [ "autoPlay" ], false ) ) {
                video.play( video.loop, video.playbackRate );
            }

            // add to group
            addToGroup( phaserObj, gdObj );
        }
        return makeCreatedObject( phaserObj, id );
    };

    /**
     * Crea un botón en phaser y devuelve el objeto creado
     * @param {object} gdObj La definición del objeto
     * @param {string} id El identificativo de la definición del objeto
     * @return {object} El objeto de Phaser creado
     */
    var createButton = function( gdObj, id ) {

        var rect = getRect( gdObj );
        var phaserObj = createPhaserButton( rect, gdObj.atlas, gdObj.over, gdObj.normal, gdObj.down, gdObj.up );
        if( phaserObj ) {
            addCommonProperties( phaserObj, gdObj, id, eGameDefinitionType.BUTTON );

            // custom properties
            phaserObj.forceOut = true;
            phaserObj.action = _.get( gdObj, [ "action", "name" ] );
            _.merge( phaserObj.parameters, _.get( gdObj, [ "action", "parameters" ], {} ) ); // This is for backward compatibility.

            // create the labels
            var createdLabels = [];
            var labelsInButton = _.get( gdObj, [ "labels" ] );
            _.forEach( labelsInButton, function( labelDefinition ) {
                labelDefinition = getFinalDefinition( labelDefinition );
                var createdLabel = createLabel( labelDefinition, id + "_text_" + createdLabels.length );
                if( createdLabel ) {
                    createdLabels.push( createdLabel );
                    phaserObj.addChild( createdLabel.phaserObj );
                }
                else {
                    console.warn( "There was an error creating a label for this button", gdObj );
                }
            } );

            // Add this labels to global m_phaserLabels
            addCreatedObjectsToPhaserMap( createdLabels, m_phaserLabels );

            // add sounds to button
            addSoundsToButton( phaserObj, gdObj );

            // add "disable" and "enable" functionality
            addRFButtonComponent( phaserObj, { disabledTexture: _.get( gdObj, [ "disable" ], null ) } );

            // add "toolTip" component functionality
            var toolTipInButtonGD = _.get( gdObj, [ "toolTip" ] );
            if( toolTipInButtonGD ) {
                var rectButton = { x: 0, y: 0 };
                var phaserObjToolTip = createToolTip( toolTipInButtonGD, id + "_tooltip", rectButton );
                if( _.has( phaserObjToolTip, "phaserObj" ) && phaserObjToolTip.phaserObj !== null ) {
                    var initData = {
                        objToolTip: phaserObjToolTip.phaserObj,
                        timeToShow: _.get( toolTipInButtonGD, "timeToShow", "" ),
                        timeToHide: _.get( toolTipInButtonGD, "timeToHide", "" )
                    };
                    phaserObj.addChild( phaserObjToolTip.phaserObj );
                    addRFToolTipComp( phaserObj, initData );
                }
            }

            // add to group
            addToGroup( phaserObj, gdObj );
        }

        return makeCreatedObject( phaserObj, id );
    };

    /**
     * Crea un texto (bitmapFont o webFont) phaser y devuelve el objeto creado
     * @param {object} gdObj La definición del objeto
     * @param {string} id El identificativo de la definición del objeto
     * @return {object} El objeto de Phaser creado
     */
    var createLabel = function( gdObj, id ) {

        var phaserObj = createPhaserText( gdObj, id );
        if( phaserObj ) {
            addCommonProperties( phaserObj, gdObj, id, eGameDefinitionType.TEXT );

            // This is for backward compatibility
            if( !_.has( gdObj, [ "anchor" ] ) ) {
                phaserObj.anchor.setTo( 0.5 );
            }
            if( _.get( gdObj, [ "autoCenter" ], false ) ) {
                var rect = getRect( gdObj );
                phaserObj.x += rect.width / 2;
                phaserObj.y += rect.height / 2;
            }

            // create gradient
            phaserObj._createGradient();

            // add component functionality
            addRFCounterComponent( phaserObj );

            // add counter sound
            var soundKey = _.get( gdObj, [ "soundKey" ] );
            if( soundKey ) {
                var rfAudioSound = new RFAudioSound( game );
                rfAudioSound.setSounds( soundKey );
                phaserObj.setRFAudioSound( rfAudioSound );
            }

            // add to group
            addToGroup( phaserObj, gdObj );
        }

        return makeCreatedObject( phaserObj, id );
    };

    /**
     * Crea un texto(s) (bitmapFont o webFont) phaser y devuelven el/los objeto(s) creado(s) (si son varios)
     * @param {object} gdObj La definición del objeto
     * @param {string} id El identificativo de la definición del objeto
     * @return {object} El objeto de Phaser creado
     */
    // TODO: Fix this
    // eslint-disable-next-line complexity
    var createAdvancedLabel = function( gdObj, id ) {

        var createdObjectsArray = [];
        var column = _.get( gdObj.parameters, [ "column" ], {
            "number": 1,
            "increment": {
                "x": 0,
                "y": 0
            },
            "keySymbolList": []
        } );

        assertAllKeys( column, [ "number", "increment.x", "increment.y", "keySymbolList" ] );

        var payTableColumn = _.get( gdObj, [ "parameters", "payTableColumn" ] );
        if( payTableColumn ) {
            assertAllKeys( payTableColumn, [ "keySymbol", "numberMatchesList", "increment.x", "increment.y" ] );
        }

        var idOriginal = id;
        for( var i = 0; i < column.number; i++ ) {

            var numIterations = payTableColumn ? payTableColumn.numberMatchesList.length : 1;
            for( var j = 0; j < numIterations; ++j ) {

                id = idOriginal;
                if( column.keySymbolList.length > i ) {
                    id += column.keySymbolList[ i ];
                }
                id += payTableColumn ? "_" + payTableColumn.keySymbol + "_" + payTableColumn.numberMatchesList[ j ] : "";


                var createdObj = createLabel( gdObj, id );
                var phaserObj = _.get( createdObj, [ "phaserObj" ] );

                if( phaserObj ) {

                    // Se incrementan los valores de x e y cuando es una columna
                    phaserObj.x += ( ( i ) * column.increment.x ) + ( payTableColumn ? payTableColumn.increment.x * j : 0 );
                    phaserObj.y += ( ( i ) * column.increment.y ) + ( payTableColumn ? payTableColumn.increment.y * j : 0 );

                    // Lista de keySymbols
                    if( !_.isEmpty( column.keySymbolList[ i ] ) ) {
                        phaserObj.parameters = _.clone( phaserObj.parameters, true );
                        phaserObj.parameters.keySymbol = column.keySymbolList[ i ];
                    }

                    if( payTableColumn ) {
                        phaserObj.parameters = _.clone( phaserObj.parameters, true );
                        phaserObj.parameters.keySymbol = payTableColumn.keySymbol;
                        phaserObj.parameters.numberMatches = payTableColumn.numberMatchesList[ j ];
                    }

                    createdObjectsArray.push( makeCreatedObject( phaserObj, id ) );
                }
            }
        }

        return createdObjectsArray;
    };

    var createAnimation = function( gdObj, id ) {
        var phaserObj;
        if( !_.isEmpty( gdObj.atlas ) ) {
            phaserObj = createPhaserSprite( getRect( gdObj ), gdObj.atlas );
            if( phaserObj ) {
                addCommonProperties( phaserObj, gdObj, id, eGameDefinitionType.ANIMATION );
                phaserObj.animations.add( gdObj.name, gdObj.frames, gdObj.fps, gdObj.loop );

                // autoplay behaviour
                if( _.get( gdObj, [ "autoPlay" ], false ) ) {
                    phaserObj.animations.play( gdObj.name );
                }

                // Kill on complete
                var killOnComplete = Boolean( _.get( gdObj, [ "killOnComplete" ], false ) );
                phaserObj.animations.currentAnim.killOnComplete = killOnComplete;

                addToGroup( phaserObj, gdObj );
            }
        }
        else {
            console.warn( "Animation with empty atlas. GameDefinition object:", gdObj );
        }

        return makeCreatedObject( phaserObj, id );
    };
    var createGroup = function( gdObj, id ) {

        var phaserObj = createPhaserGroup( getRect( gdObj ), id );
        if( phaserObj ) {
            addCommonProperties( phaserObj, gdObj, id, eGameDefinitionType.GROUP );

            setProperty( phaserObj, gdObj, "pivot.x" );
            setProperty( phaserObj, gdObj, "pivot.y" );

            // add to group
            addToGroup( phaserObj, gdObj );
        }

        return makeCreatedObject( phaserObj, id );
    };


    /**
     * The tooltip object has 2 elements
     * - background (image)
     * - lables (is an array)
     */
    var createToolTip = function( gdObj, id, rectParent ) {
        // create the image
        var phaserObjToolTip = game.add.sprite( _.get( rectParent, [ "x" ], 0 ), _.get( rectParent, [ "y" ], 0 ) );
        var BACKGROUND_NAME = "background";
        var background = null;
        if( _.has( gdObj, BACKGROUND_NAME ) ) {
            background = createImage( _.get( gdObj, BACKGROUND_NAME ), id + BACKGROUND_NAME + "_tooltip" );
        }
        else {
            background = makeCreatedObject( createPhaserSprite( rectParent, null, id + "_tooltipObj" ),
                id + BACKGROUND_NAME + "_EmptyTooltip" );
        }

        background.phaserObj.visible = false;
        phaserObjToolTip.addChild( background.phaserObj );

        // Add this image to global m_phaserImages
        addCreatedObjectsToPhaserMap( background.phaserObj, m_phaserImages );


        // create the labels
        var createdLabels = [];
        var labelsInButton = _.get( gdObj, [ "labels" ] );
        _.forEach( labelsInButton, function( labelDefinition ) {
            labelDefinition = getFinalDefinition( labelDefinition );
            var createdLabel = createLabel( labelDefinition, id + "_txtToolTip_" + createdLabels.length );
            if( createdLabel ) {
                createdLabels.push( createdLabel );
                if( phaserObjToolTip ) {
                    createdLabel.phaserObj.visible = false;
                    phaserObjToolTip.addChild( createdLabel.phaserObj );
                }
            }
            else {
                console.warn( "There was an error creating a label for this toolTip", gdObj );
            }
        } );

        // Add these labels to global m_phaserLabels
        addCreatedObjectsToPhaserMap( createdLabels, m_phaserLabels );

        return makeCreatedObject( phaserObjToolTip, id );
    };

    // --------------------------------------------------------------
    // #endregion CREATE FUNCTIONS
    // --------------------------------------------------------------

    // --------------------------------------------------------------
    // #region CREATE HELPERS
    // --------------------------------------------------------------

    /**
     *
     * @param {*} createdObjectsArray Array de objetos de este estilo { id: "lbl_1", phaserObj: { ... } }
     * @param {*} phaserMap Mapa donde se almacenarán
     */
    var addCreatedObjectsToPhaserMap = function( createdObjectsArray, phaserMap ) {
        if( !_.isObject( phaserMap ) ) { return; }

        if( _.isArray( createdObjectsArray ) ) {
            _.forEach( createdObjectsArray, function( obj ) {
                addCreatedObjectToPhaserMap( obj, phaserMap );
            } );
        }
        else if( _.isObject( createdObjectsArray ) ) {
            addCreatedObjectToPhaserMap( createdObjectsArray, phaserMap );
        }
    };

    var addCreatedObjectToPhaserMap = function( createdObject, phaserMap ) {
        if( !_.isObject( phaserMap ) ) { return; }

        var id = _.get( createdObject, "id" );
        var phaserObj = _.get( createdObject, "phaserObj" );

        if( _.isString( id ) && _.isObject( phaserObj ) ) {
            if( _.has( phaserObj, [ id ] ) ) {
                console.warn( "Duplicate id", id, "for this phaserMap", phaserMap,
                    "Override this element", phaserMap[ id ], "with this other element", phaserObj );
            }
            phaserMap[ id ] = phaserObj;
        }
        else {
            console.info( "Object not created", createdObject, "to add in", phaserMap );
        }
    };

    // --------------------------------------------------------------
    // #endregion CREATE HELPERS
    // --------------------------------------------------------------

    // --------------------------------------------------------------
    // #region CREATE PHASER FUNCTIONS
    // --------------------------------------------------------------

    var createPhaserSpriteOrTileSprite = function( gdObj ) {
        var phaserObj;

        var rect = getRect( gdObj );
        if( _.isObject( gdObj.tileSprite ) ) {

            phaserObj = createPhaserTileSprite( rect, gdObj.atlas, gdObj.key );
            if( gdObj.tileSprite.autoStart === true ) {
                var speed = {
                    x: _.get( gdObj.tileSprite, [ "speed", "x" ], 0 ),
                    y: _.get( gdObj.tileSprite, [ "speed", "y" ], 0 )
                };
                phaserObj.autoScroll( speed.x, speed.y );
            }
            phaserObj.tileSprite = gdObj.tileSprite;

            phaserObj.tileScale.x = _.get( gdObj.tileSprite, [ "tileScale", "x" ], 1 );
            phaserObj.tileScale.y = _.get( gdObj.tileSprite, [ "tileScale", "y" ], 1 );

        }
        else {
            phaserObj = createPhaserSprite( rect, gdObj.atlas, gdObj.key );
        }

        return phaserObj;
    };

    var createPhaserTileSprite = function( rect, atlas, key ) {
        return game.add.tileSprite(
            _.get( rect, [ "x" ] ),
            _.get( rect, [ "y" ] ),
            _.get( rect, [ "width" ] ),
            _.get( rect, [ "height" ] ),
            !_.isEmpty( atlas ) ? atlas : key,
            !_.isEmpty( atlas ) ? key : undefined
        );
    };

    var createPhaserSprite = function( rect, atlas, key ) {
        return game.add.sprite(
            _.get( rect, [ "x" ] ),
            _.get( rect, [ "y" ] ),
            !_.isEmpty( atlas ) ? atlas : key,
            !_.isEmpty( atlas ) ? key : undefined
        );
    };

    // TODO: Fix this
    // eslint-disable-next-line max-params
    var createPhaserButton = function( rect, atlas, overFrame, normalFrame, downFrame, upFrame ) {
        var phaserObj = game.add.button(
            _.get( rect, [ "x" ] ),
            _.get( rect, [ "y" ] ),
            ( atlas === undefined ? null : atlas ),
            null,
            null,
            ( overFrame === undefined ? null : overFrame ),
            ( normalFrame === undefined ? null : normalFrame ),
            ( downFrame === undefined ? null : downFrame ),
            ( upFrame === undefined ? null : upFrame )
        );

        // invisible button with width and height
        if( atlas === undefined ) {
            phaserObj.width = rect.width;
            phaserObj.height = rect.height;
        }

        return phaserObj;
    };


    var createPhaserText = function( gdObj, id ) {
        var isBitmapFont = !_.isEmpty( _.get( gdObj, [ "bitmapFont" ], null ) );
        var isWebFont = !_.isEmpty( _.get( gdObj, [ "webFont" ], null ) );
        if( isBitmapFont && isWebFont ) { throw new Error( "Multiple definition in '" + id + "' label ( is BitmapFont and WebFont )." ); }

        var phaserObj;
        if( isBitmapFont ) {
            phaserObj = createPhaserBitmapText( gdObj, id );
        }
        else if( isWebFont ) {
            phaserObj = createPhaserWebFontText( gdObj, id );
        }
        else {
            console.warn( "Label '" + id + "' without bitmapFont or webfont defined. We put a default webFont" );
            gdObj.webFont = {};
            phaserObj = createPhaserWebFontText( gdObj, id );
        }

        return phaserObj;
    };

    var createPhaserBitmapText = function( gdObj, id ) {

        var phaserObj;

        if( _.isEmpty( _.get( gdObj, [ "bitmapFont", "key" ] ) ) ) {
            console.warn( "Missing font key in '" + id + "' label for bitmapFont. We will create a default webFont instead" );
            gdObj.webFont = {};
            phaserObj = createPhaserWebFontText( gdObj, id );
        }
        else {
            var text = _.get( gdObj, [ "text" ], "" );
            var size = _.get( gdObj, [ "size" ], DEFAULT_FONT_SIZE );
            var rect = getRect( gdObj );
            var fontKey = _.get( gdObj, [ "bitmapFont", "key" ] );

            phaserObj = game.add.bitmapText( rect.x, rect.y, fontKey, text, size );

            var forceSet = true;
            setProperty( phaserObj, gdObj.bitmapFont, "smoothed", forceSet );

        }

        return phaserObj;
    };

    var createPhaserWebFontText = function( gdObj ) {

        var phaserObj;
        var text = _.get( gdObj, [ "text" ], "" );
        var size = _.get( gdObj, [ "size" ], DEFAULT_FONT_SIZE );
        var rect = getRect( gdObj );
        var fontKey = _.get( gdObj, [ "webFont", "key" ], "Arial" );
        var style = _.get( gdObj, [ "webFont", "style" ], DEFAULT_WEBFONT_STYLE );
        style.font = fontKey;
        style.fontSize = size;

        phaserObj = game.add.text( rect.x, rect.y, text, style );

        if( _.has( gdObj, [ "webFont" ] ) ) {

            var forceSet = true;
            /* eslint-disable max-len */
            // See ref: https://phaser.io/docs/2.6.2/Phaser.Text.html
            setProperty( phaserObj, gdObj.webFont, "lineSpacing", forceSet );        // number - Additional spacing (in pixels) between each line of text if multi-line.
            setProperty( phaserObj, gdObj.webFont, "smoothed", forceSet );           // boolean - Enable or disable texture smoothing for this Game Object. It only takes effect if the Game Object is using an image based texture.
            setProperty( phaserObj, gdObj.webFont, "shadowBlur", forceSet );         // number - The shadowBlur value. Make the shadow softer by applying a Gaussian blur to it. A number from 0 (no blur) up to approx. 10 (depending on scene).
            setProperty( phaserObj, gdObj.webFont, "shadowColor", forceSet );        // string - he color of the shadow, as given in CSS rgba format. Set the alpha component to 0 to disable the shadow.
            setProperty( phaserObj, gdObj.webFont, "shadowFill", forceSet );         // boolean - Sets if the drop shadow is applied to the Text fill.
            setProperty( phaserObj, gdObj.webFont, "shadowOffsetX", forceSet );      // number - The shadowOffsetX value in pixels. This is how far offset horizontally the shadow effect will be.
            setProperty( phaserObj, gdObj.webFont, "shadowOffsetY", forceSet );      // number - The shadowOffsetY value in pixels. This is how far offset vertically the shadow effect will be.
            setProperty( phaserObj, gdObj.webFont, "shadowStroke", forceSet );       // boolean - Sets if the drop shadow is applied to the Text stroke.
            setProperty( phaserObj, gdObj.webFont, "useAdvancedWrap", forceSet );    // boolean - https://phaser.io/docs/2.6.2/Phaser.Text.html#useAdvancedWrap
            /* eslint-enable */

            // textBounds
            var textBounds = _.get( gdObj.webFont, [ "textBounds" ] );
            if( _.isObject( textBounds ) ) {
                phaserObj.setTextBounds( textBounds.x, textBounds.y, textBounds.width, textBounds.height );
            }

        }

        return phaserObj;
    };

    var createPhaserVideo = function( gdObj ) {

        var phaserVideo = game.add.video( _.get( gdObj, [ "key" ] ) );

        // create a image via 'addToWorld'
        var rect = getRect( gdObj );
        var phaserObj = phaserVideo.addToWorld( rect.x, rect.y );

        // add a video property to the image
        phaserObj.video = phaserVideo;

        return phaserObj;
    };

    var createPhaserGroup = function( rect, id, phaserParent ) {
        var phaserGroup = game.add.group( phaserParent, id );
        phaserGroup.x = _.get( rect, "x", 0 );
        phaserGroup.y = _.get( rect, "y", 0 );
        return phaserGroup;
    };

    // --------------------------------------------------------------
    // #region ADD COMPONENTS SECTION
    // --------------------------------------------------------------
    var addRFButtonComponent = function( phaserObj, initData ) {
        var STR_RF_BUTTON_COMPONENT = "RFButtonComp";

        var initDataDefaults = {
            disabledTexture: null
        };
        initData = _.merge( initDataDefaults, initData );

        game.behaviorPlugin.enable( phaserObj );
        phaserObj.behaviors.set( STR_RF_BUTTON_COMPONENT, RFButtonComp, {
            disabledTexture: initData.disabledTexture
        } );

        phaserObj.enable = function( bEnable ) {
            bEnable = _.isBoolean( bEnable ) ? bEnable : true;
            if( bEnable ) {
                this.behaviors.get( STR_RF_BUTTON_COMPONENT ).options.enable();
            }
            else {
                this.behaviors.get( STR_RF_BUTTON_COMPONENT ).options.disable();
            }
        };
        phaserObj.disable = function() {
            phaserObj.enable( false );
        };
        phaserObj.isEnabled = function() {
            return this.behaviors.get( STR_RF_BUTTON_COMPONENT ).options.enabled;
        };
    };

    var addRFToolTipComp = function( phaserObj, initData ) {
        var STR_RF_TOOLTIP_COMPONENT = "RFToolTipComp";

        game.behaviorPlugin.enable( phaserObj );
        phaserObj.behaviors.set( STR_RF_TOOLTIP_COMPONENT, RFToolTipComp, {
            objToolTip: ( initData.objToolTip !==  undefined ) ? initData.objToolTip : null,
            timeToShow: ( initData.timeToShow !== "" ) ? initData.timeToShow : RFToolTipComp.options.timeToShow,
            timeToHide: ( initData.timeToHide !== "" ) ? initData.timeToHide : RFToolTipComp.options.timeToHide
        } );

        phaserObj.over = function() {
            this.behaviors.get( STR_RF_TOOLTIP_COMPONENT ).options.over();
        };
        phaserObj.out = function() {
            this.behaviors.get( STR_RF_TOOLTIP_COMPONENT ).options.out();
        };
        phaserObj.enableOver = function( enableTip ) {
            if( enableTip ) {
                this.behaviors.get( STR_RF_TOOLTIP_COMPONENT ).options.enableTip();
            }
            else {
                this.behaviors.get( STR_RF_TOOLTIP_COMPONENT ).options.disableTip();
            }
        };
    };

    var addRFCounterComponent = function( phaserObj, initData ) {

        var STR_RF_COUNTER_COMPONENT = "RFCounterComp";

        var initDataDefaults = {
            timeToFinish: 0
        };
        initData = _.merge( initDataDefaults, initData );

        // Enable the RFCounterComp component
        game.behaviorPlugin.enable( phaserObj );
        phaserObj.behaviors.set( STR_RF_COUNTER_COMPONENT, RFCounterComp, {
            game: game,
            timeToFinish: initData.timeToFinish,
            loopSound: initData.loop
        } );

        phaserObj._setFormattedString = phaserObj.setFormattedString;
        phaserObj.setFormattedString = function( str ) {
            this.behaviors.get( STR_RF_COUNTER_COMPONENT ).options.forceUpdate = true;
            this._setFormattedString( str );
            this._updateGradient();
        };

        phaserObj.setString = function( str ) {
            this.behaviors.get( STR_RF_COUNTER_COMPONENT ).options.forceUpdate = true;
            this.text = str;
            this._updateGradient();
        };
        phaserObj.setMoney = function( moneyValue, currencyISO, twTime, bPlayAudio ) {
            this.behaviors.get( STR_RF_COUNTER_COMPONENT ).options.moneyChange( moneyValue, currencyISO, twTime, bPlayAudio );
            this.behaviors.get( STR_RF_COUNTER_COMPONENT ).options.forceUpdate = false;
        };
        phaserObj.setValue = function( value, twTime, bPlayAudio ) {
            this.behaviors.get( STR_RF_COUNTER_COMPONENT ).options.valueChange( value, twTime, bPlayAudio );
            this.behaviors.get( STR_RF_COUNTER_COMPONENT ).options.forceUpdate = false;
        };
        phaserObj.setNumberOfDecimals = function( numberOfDecimals ) {
            this.behaviors.get( STR_RF_COUNTER_COMPONENT ).options.forceRFCurrencyDecimals = false;
            this.behaviors.get( STR_RF_COUNTER_COMPONENT ).options.numberOfDecimals = numberOfDecimals;
        };
        phaserObj.setRFAudioSound = function( rfAudioSound ) {
            this.behaviors.get( STR_RF_COUNTER_COMPONENT ).options.rfAudioSoundMeter = rfAudioSound;
        };
        phaserObj.setTimeToFinish = function( timeToFinish ) {
            this.behaviors.get( STR_RF_COUNTER_COMPONENT ).options.timeToFinish = timeToFinish;
        };
    };
    // --------------------------------------------------------------
    // #endregion ADD COMPONENTS SECTION END
    // --------------------------------------------------------------

    // --------------------------------------------------------------
    // #region HELPERS
    // --------------------------------------------------------------

    var makeCreatedObject = function( phaserObj, id ) {
        return {
            id        : id,
            phaserObj : phaserObj
        };
    };

    var isTemplate = function( gdElement ) {
        return ( _.get( gdElement, [ "type" ] ) === STR_TEMPLATE || _.get( gdElement, [ "isTemplate" ] ) );
    };

    /**
     * Devuelve una copia del objeto con los templates aplicados.
     * @param {object} gdElement Elemento del GameDefinition
     * @param {object} gdMap Mapa de objetos del GameDefinition donde se encuentran los templates
     * @returns {object} Un objeto copia con todas las propiedades heredadas en cascada
     */
    var getFinalDefinition = function( gdElement, gdMap ) {
        var finalDefinition = _.cloneDeep( gdElement );
        var initialDefinition = _.cloneDeep( gdElement );
        var wasTemplate = isTemplate( finalDefinition );
        if( _.has( finalDefinition, [ "templates" ] ) ) {
            var templatesArray = _.isString( finalDefinition.templates ) ? [ finalDefinition.templates ] : finalDefinition.templates;
            if( !_.isArray( templatesArray ) ) { throw new Error( "getFinalDefinition: 'templates' must be an string or an array" ); }

            // eslint-disable-next-line complexity
            _.forEach( templatesArray, function( strTemplate ) {
                if( !_.isString( strTemplate ) ) { throw new Error( "getFinalDefinition: The template must be an string" ); }

                // Gets the template:
                // 1) local template in the gdMap
                // 2) If not found in 1, look in scene templates
                // 3) If not found in 2, looh in global templates
                var templateObj = _.get( gdMap, [ strTemplate ], _.get( m_gdSceneTemplates, [ strTemplate ], _.get( m_gdGlobalTemplates, [ strTemplate ] ) ) );
                var templatesMap =
                    ( _.has( gdMap, [ strTemplate ] ) ? gdMap : null ) ||
                    ( _.has( m_gdSceneTemplates, [ strTemplate ] ) ? m_gdSceneTemplates : null ) ||
                    ( _.has( m_gdGlobalTemplates, [ strTemplate ] ) ? m_gdGlobalTemplates : null );

                if( !templateObj ) { throw new Error( "getFinalDefinition: The template " + strTemplate + " doesn't exist" ); }

                finalDefinition = _.merge( _.cloneDeep( finalDefinition ), getFinalDefinition( templateObj, templatesMap ) );

                // To prevent make definitions with type=TEMPLATE if the gdElement wasn't a TEMPLATE
                // if the gdElement wasn't a "TEMPLATE" and now is a "TEMPLATE" => drop the type, because really
                // it's a template and we need to draw it
                if( !wasTemplate && isTemplate( finalDefinition ) ) {
                    if( finalDefinition.type === STR_TEMPLATE ) {
                        delete finalDefinition.type;
                    }
                    delete finalDefinition.isTemplate;
                }
            } );
        }
        return _.merge( _.cloneDeep( finalDefinition ), initialDefinition );
    };

    var addCommonProperties = function( phaserObj, gdObj, id, gameDefinitionType ) {

        if( phaserObj ) {

            // Phaser simple properties
            var forceSet = true;
            setProperty( phaserObj, gdObj, "anchor.x" );
            setProperty( phaserObj, gdObj, "anchor.y" );
            setProperty( phaserObj, gdObj, "fixedToCamera" );
            setProperty( phaserObj, gdObj, "visible" );
            setProperty( phaserObj, gdObj, "alpha" );
            setProperty( phaserObj, gdObj, "tint" );
            setProperty( phaserObj, gdObj, "scale.x" );
            setProperty( phaserObj, gdObj, "scale.y" );
            setProperty( phaserObj, gdObj, "inputEnabled", forceSet );
            setProperty( phaserObj, gdObj, "angle", forceSet );

            // Phaser complex build properties
            setCropRectProperty( phaserObj, gdObj );
            setScaleMinMaxProperty( phaserObj, gdObj );
            setBlendMode( phaserObj, gdObj );

            // Custom properties ( xxx deberíamos de almacenarlo en data en un futuro )
            phaserObj.id = id;
            phaserObj.type = _.get( gdObj, [ "type" ] ); // xxx Este type sobreescrible el Type original de Phaser.
            phaserObj.parameters = _.get( gdObj, [ "parameters" ], {} );

            // stores id, type, parameters and the original gameDefinition object
            _.set( phaserObj, "data.id", id );
            _.set( phaserObj, "data.gameDefinitionType", gameDefinitionType );
            _.set( phaserObj, "data.type", _.get( gdObj, [ "type" ] ) );
            _.set( phaserObj, "data.parameters", _.get( gdObj, [ "parameters" ], {} ) );
            _.set( phaserObj, "data.gdObj", gdObj );
        }
    };

    var setCropRectProperty = function( phaserObj, gdObj ) {
        if( _.has( gdObj, [ "cropRect" ] ) && _.has( gdObj, [ "cropRect" ] ) ) {
            phaserObj.crop( new Phaser.Rectangle( _.get( gdObj, [ "cropRect", "x" ] ),
                _.get( gdObj, [ "cropRect", "y" ] ),
                _.get( gdObj, [ "cropRect", "width" ] ),
                _.get( gdObj, [ "cropRect", "height" ] ) ) );
        }
    };

    var setScaleMinMaxProperty = function( phaserObj, gdObj ) {
        if( _.has( gdObj, [ "scaleMax" ] ) ) {
            _.set( phaserObj, "scaleMax", new Phaser.Point( _.get( gdObj, [ "scaleMax", "x" ] ), _.get( gdObj, [ "scaleMax", "y" ] ) ) );
        }
        if( _.has( gdObj, [ "scaleMin" ] ) ) {
            _.set( phaserObj, "scaleMin", new Phaser.Point( _.get( gdObj, [ "scaleMin", "x" ] ), _.get( gdObj, [ "scaleMin", "y" ] ) ) );
        }
    };

    var setBlendMode = function( phaserObj, gdObj ) {
        if( _.has( gdObj, [ "blendMode" ] ) && _.has( phaserObj, [ "blendMode" ] ) ) {
            phaserObj.blendMode = getBlendMode( gdObj.blendMode );
        }
    };

    var getBlendMode = function( sBlendMode ) {
        return ( PIXI.blendModes[ sBlendMode ] !== undefined ? PIXI.blendModes[ sBlendMode ] : PIXI.blendModes.NORMAL );
    };

    var setProperty = function( phaserObj, gdObj, propertyPath, forceSet ) {
        forceSet = _.isBoolean( forceSet ) ? forceSet : false;
        if( _.has( gdObj, propertyPath ) ) {
            if( _.has( phaserObj, propertyPath ) || forceSet ) {
                _.set( phaserObj, propertyPath, _.get( gdObj, propertyPath ) );
            }
        }
    };

    var getRect = function( gdObj ) {
        return {
            x: _.get( gdObj, [ "rect", "x" ], 0 ),
            y: _.get( gdObj, [ "rect", "y" ], 0 ),
            width: _.get( gdObj, [ "rect", "width" ], DEFAULT_RECT_WIDTH ),
            height: _.get( gdObj, [ "rect", "height" ], DEFAULT_RECT_HEIGHT )
        };
    };

    var addSoundsToButton = function( phaserObj, gdObj ) {
        var soundsTypes = [ "over", "out", "down", "up" ];
        _.forEach( soundsTypes, function( soundType ) {
            addOnInputSound( phaserObj, soundType, _.get( gdObj, [ "sounds", soundType ] ) );
        } );
    };

    // sEventType: ( "over", "out", "down", "up" )
    var addOnInputSound = function( phaserObj, sEventType, sounds ) {
        if( phaserObj && !_.isEmpty( sounds ) ) {

            var STR_SEPARATOR = " ";
            var method = _.camelCase( "onInput" + STR_SEPARATOR + sEventType );
            if( _.has( phaserObj, [ method ] ) ) {

                // create the RFAudioSound
                _.set( phaserObj, [ "data", "_rfAudioSounds", sEventType ], new RFAudioSound( game ) );
                phaserObj.data._rfAudioSounds[ sEventType ].setSounds( sounds );

                // Asociate to onInputXXX
                phaserObj[ method ].add( function( obj ) {
                    obj.data._rfAudioSounds[ sEventType ].play();
                } );
            }
            else {
                console.warn( "The method", method, "don't exist in", phaserObj );
            }
        }
    };

    /**
     *
     * @param {Array|Object} createdObjects
     * @param {Phaser.Group} parentGroup
     */
    var addCreatedObjectsToGroup = function( createdObjects, parentGroup ) {
        if( _.isArray( createdObjects ) ) {
            _.forEach( createdObjects, function( createdObject ) {
                addCreatedObjectToGroup( createdObject, parentGroup );
            } );
        }
        else {
            addCreatedObjectToGroup( createdObjects, parentGroup );
        }
    };

    /**
     *
     * @param {Object} createdObject
     * @param {Phaser.Group} parentGroup
     */
    var addCreatedObjectToGroup = function( createdObject, parentGroup ) {
        if( parentGroup ) {
            if( _.has( createdObject, [ "phaserObj" ] ) ) {
                parentGroup.add( createdObject.phaserObj );
            }
        }
    };

    var loadPunches = function() {
        _.forEach( _.keysOrdered( m_gdPunches ), function( key ) {
            var oPunch = m_gdPunches[ key ];
            var url = oPunch.url;
            var asyncLoad = _.get( oPunch, "asyncLoad", true );
            $.ajax( {
                url: url,
                async: asyncLoad,
                dataType: "script",
                success: function() {
                    console.info( "Punch cargado correctamente:", oPunch.url );
                },
                error: function( error ) {
                    console.error( "Error cargando punch (" + oPunch.url + ") Exception:", error );
                }
            } );
        } );
    };

    var getPayBoxSize = function( gdScene ) {
        var payBoxSize = { width: 0, height: 0 };
        if( _.has( gdScene, [ "lines" ] ) ) {
            var oLine = _.find( gdScene.lines, function( o ) {
                return (
                    _.has( o, [ "payBox", "rect", "width" ] ) &&
                    _.has( o, [ "payBox", "rect", "height" ] ) &&
                    o.payBox.rect.width > 0 && o.payBox.rect.height > 0 );
            } );
            if( _.isObject( oLine ) ) {
                payBoxSize.width = oLine.payBox.rect.width;
                payBoxSize.height = oLine.payBox.rect.height;
            }
        }

        return payBoxSize;
    };

    var getDefaultCanvas = function() {
        return _.concat(
            STR_VIDEOS,
            STR_BACKGROUNDS,
            STR_REELS,
            STR_BUTTONS,
            STR_LINES,
            STR_IMAGES,
            STR_ANIMATIONS,
            STR_LABELS,
            STR_AUTO
        );
    };

    // --------------------------------------------------------------
    // #endregion HELPERS END
    // --------------------------------------------------------------

    // --------------------------------------------------------------
    // #region DRAW FUNCTIONS
    // --------------------------------------------------------------

    /**
     *
     * @param {string} strGdCanvasPathElement
     */
    var drawElement = function( strGdCanvasPathElement ) {

        var createdObjects;
        var pathToElement = _.split( strGdCanvasPathElement, "." );
        var numNodes = _.size( pathToElement );

        var gdMapPath;
        var gdMap;
        var mapType;
        var id;

        if( numNodes > 1 ) {
            gdMapPath = _.take( pathToElement, pathToElement.length - 1 );
            gdMap = _.get( m_gdScene, gdMapPath );
            mapType = _.join( gdMapPath, "." );
            id = _.last( pathToElement );

            createdObjects = drawSingleElement( gdMap, mapType, id );
        }
        else if( numNodes === 1 ) {
            mapType = _.head( pathToElement );
            gdMap = _.get( m_gdScene, mapType );

            createdObjects = drawElements( gdMap, mapType );

        }
        else {
            console.error( "Can't draw this canvas element", strGdCanvasPathElement );
        }

        return _.concat( createdObjects );
    };

    /**
     * Create a single object (or derivated as advanced label) for the mapType.
     * @param {object} gdMap
     * @param {string} mapType
     * @param {string} id
     */

    /* eslint-disable-next-line complexity */
    var drawSingleElement = function( gdMap, mapType, id ) {
        if( !( _.isObject( gdMap ) && _.isString( id ) && _.isString( mapType ) ) ) { return undefined; }

        var createdObjects;
        var gdObj = getFinalDefinition( gdMap[ id ], gdMap );
        if( !isTemplate( gdObj ) ) {
            switch( mapType ) {
                case STR_VIDEOS:
                    createdObjects = createVideo( gdObj, id );
                    addCreatedObjectsToPhaserMap( createdObjects, m_phaserVideos );
                    break;
                case STR_BACKGROUNDS:
                    createdObjects = createBackground( gdObj, id );
                    addCreatedObjectsToPhaserMap( createdObjects, m_phaserBackgrounds );
                    break;
                case STR_BUTTONS:
                    createdObjects = createButton( gdObj, id );
                    addCreatedObjectsToPhaserMap( createdObjects, m_phaserButtons );
                    break;
                case STR_IMAGES:
                    createdObjects = createImage( gdObj, id );
                    addCreatedObjectsToPhaserMap( createdObjects, m_phaserImages );
                    break;
                case STR_LABELS:
                    createdObjects = createAdvancedLabel( gdObj, id );
                    addCreatedObjectsToPhaserMap( createdObjects, m_phaserLabels );
                    break;
                case STR_ANIMATIONS:
                    createdObjects = createAnimation( gdObj, id );
                    addCreatedObjectsToPhaserMap( createdObjects, m_phaserAnimations );
                    break;
                case STR_GROUPS:
                    createdObjects = createGroup( gdObj, id );
                    addCreatedObjectsToPhaserMap( createdObjects, m_phaserGroups );
                    break;
                default:
                    break;
            }
        }

        return createdObjects;
    };
    /* eslint-enable-next-line complexity */

    /**
     * Create all objects for the mapType
     * @param {object} gdMap
     * @param {string} mapType
     */
    // eslint-disable-next-line complexity
    var drawElements = function( gdMap, mapType ) {
        if( !( _.isObject( gdMap ) && _.isString( mapType ) ) ) { return undefined; }

        var createdObjects;
        switch( mapType ) {
            case STR_VIDEOS:
                createdObjects = createVideos( m_phaserVideos, gdMap );
                break;
            case STR_BACKGROUNDS:
                createdObjects = createBackgrounds( m_phaserBackgrounds, gdMap );
                break;
            case STR_BUTTONS:
                createdObjects = createButtons( m_phaserButtons, gdMap );
                break;
            case STR_IMAGES:
                createdObjects = createImages( m_phaserImages, gdMap );
                break;
            case STR_LABELS:
                createdObjects = createAdvancedLabels( m_phaserLabels, gdMap );
                break;
            case STR_ANIMATIONS:
                createdObjects = createAnimations( m_phaserAnimations, gdMap );
                break;

            // xxx: debería de desaparecer
            case STR_AUTO:
                createdObjects = createAuto();
                break;

            // Casos especiales: Debieran devolver un grupo
            case STR_REELS:
                createdObjects = createReels();
                break;
            case STR_LINES:
                createdObjects = _.concat( createLines(), createSkittles() );
                break;

            default:
                break;
        }

        return createdObjects;
    };

    var drawGroup = function( gdCanvasElement, parentGroup ) {
        /* eslint-disable max-len */
        if( !_.isObject( gdCanvasElement ) ) { throw new Error( "Can't create a canvas group, because it's an object" ); }
        if( _.size( gdCanvasElement ) !== 1 ) { throw new Error( "Can't create a canvas group, because this object can only has one pair key-value (name of group and the element to draw)" ); }
        /* eslint-enable */

        var id = _.head( _.keys( gdCanvasElement ) );
        var gdMap = m_gdGroups;
        var gdObj = getFinalDefinition( gdMap[ id ], gdMap );

        var createdObject = createGroup( gdObj, id );
        addCreatedObjectsToPhaserMap( createdObject, m_phaserGroups );

        addCreatedObjectsToGroup( createdObject, parentGroup );

        drawScene( gdCanvasElement[ id ], _.get( createdObject, [ "phaserObj" ] ) );

        return createdObject;
    };

    var drawScene = function( gdCanvasElement, parentGroup ) {

        if( _.isString( gdCanvasElement ) ) {
            var createdObjects = drawElement( gdCanvasElement );
            addCreatedObjectsToGroup( createdObjects, parentGroup );
            if( parentGroup ) {
                _.forEach( createdObjects, function( createdObj ) {
                    parentGroup.add( createdObj.phaserObj );
                } );
            }
        }
        else if( _.isArray( gdCanvasElement ) ) {
            _.forEach( gdCanvasElement, function( element ) {
                drawScene( element, parentGroup );
            } );
        }
        else if( _.isObject( gdCanvasElement ) ) {
            drawGroup( gdCanvasElement, parentGroup );
        }
    };

    // --------------------------------------------------------------
    // #endregion DRAW FUNCTIONS
    // --------------------------------------------------------------

    // --------------------------------------------------------------
    // #region PUBLIC METHODS
    // --------------------------------------------------------------

    this.loadScene = function( scene ) {

        var result = false;

        if( init( scene ) ) {

            var canvas = _.get( m_gdScene, [ STR_CANVAS ] );
            var hasCanvas = Boolean( canvas );
            if( !hasCanvas ) {
                canvas = getDefaultCanvas();
                loadGroups();
            }

            drawScene( canvas );

            if( !hasCanvas ) {
                if( m_phaserRootGroup ) {

                    // This way, not include de "ROOT GROUP"
                    // _.forEach( m_phaserRootGroup.children, function( child ) {
                    //     game.world.add( child );
                    // } );
                    reassignChildGroups();
                    game.world.bringToTop( m_phaserRootGroup );
                }
            }

            loadPunches();

            result = true;
        }

        return result;
    };

    // Phaser Objects
    this.getPhaserButtons = function() { return m_phaserButtons; };
    this.getPhaserVideos = function() { return m_phaserVideos; };
    this.getPhaserBackgrounds = function() { return m_phaserBackgrounds; };
    this.getPhaserLabels = function() { return m_phaserLabels; };
    this.getPhaserSkittels = function() { return m_phaserSkittels; };
    this.getPhaserLines = function() { return m_phaserLines; };
    this.getPhaserButtonsAuto = function() { return m_phaserButtonsAuto; };
    this.getPhaserLabelsAuto = function() { return m_phaserLabelsAuto; };
    this.getPhaserImagesAuto = function() { return m_phaserImagesAuto; };
    this.getPhaserImages = function() { return m_phaserImages; };
    this.getPhaserAnimations = function() { return m_phaserAnimations; };
    this.getPhaserGroups = function() { return m_phaserGroups; };

    // RFElements
    this.getRFSymbols = function() { return m_rfSymbols.get(); };
    this.getRFSymbol = function( id ) { return m_rfSymbols.get( id ); };
    this.getRFLines = function() { return m_rfLines.get(); };
    this.getRFLine = function( id ) { return m_rfLines.get( id ); };
    this.getRFSounds = function() { return m_rfSounds.get(); };
    this.getRFSound = function( id ) { return m_rfSounds.get( id ); };
    this.getRFExtendedWilds = function() { return m_rfExtendWilds.get(); };
    this.getRFExtendedWild = function( id ) { return m_rfExtendWilds.get( id ); };
    this.getRFMultipliers = function() { return m_rfMultipliers.get(); };
    this.getRFMultiplier = function( id ) { return m_rfMultipliers.get( id ); };
    this.getRFGraphicReels = function() { return m_rfGraphicReels; };

    // --------------------------------------------------------------
    // #endregion PUBLIC METHODS
    // --------------------------------------------------------------
}
/* eslint-enable max-lines */

//------------------------------------------------------
// Source: src/engine/webservice/rf.gameserver.wrapper.js
//------------------------------------------------------

/**
 * @requires xmltojson.js
 * @requires rf.webservice.*
 * @version 0.5.0
 * CAMBIOS:
 * 0.1.13 - Corrige llamada incorrecta a consola.
 * 0.1.14 - Se añaden DICES y COCKTAIL, y se corrigen los requests de BONUS_SELECTION y PAIRS.
 * 0.1.15 - Devuelve el mensaje de error a la función de callback de error al volver del envío del mensaje
 * 0.1.16 - Optimizados los accesos por lodash
 * 0.1.17 - Se añade el cash al player en las genericResponse.
 * 0.2.0  - Se añade como opción si se realiza un chequeo de las respuestas
 * 0.2.1  - Se eliminan dependecias externas para la personalización de los colores.
 * 0.3.0  - Añade la petición de sendGetBalance (método GET).
 * 0.4.0  - Se añade una cola de peticiones para irlas tratando de una en una y evitar errores de concurrencia.
 * 0.5.0  - Se añade soporte para autoSendPings.
 */

/**
 * GameServer Command Codes
 */
/* eslint-disable */
var GS_COMMAND_CODES = {
    PING                : { request: "ping",                  response: "pingResponse",             necesaryParamsKeysArray: [],               optionalParamsKeyObj: {},                        RFWSClass: RFWSPingResponse },
    SPIN                : { request: "spin",                  response: "spinResponse",             necesaryParamsKeysArray: [],               optionalParamsKeyObj: {},                        RFWSClass: RFWSSpinResponse },
    SET_BET_PER_LINE    : { request: "setBetPerLine",         response: "setBetPerLineResponse",    necesaryParamsKeysArray: [ "betPerLine" ], optionalParamsKeyObj: {},                        RFWSClass: RFWSSetBetPerLineResponse },
    DOUBLE_OR_NOTHING   : { request: "doubleOrNothing",       response: "doubleOrNothingResponse",  necesaryParamsKeysArray: [],               optionalParamsKeyObj: { "isPlayingGame" : "0" }, RFWSClass: RFWSDoubleOrNothingResponse },
    SET_LINES           : { request: "setLines",              response: "setLinesResponse",         necesaryParamsKeysArray: [ "lines" ],      optionalParamsKeyObj: {},                        RFWSClass: RFWSSetLinesResponse },
    SET_MAX_BET         : { request: "setMaxBet",             response: "setMaxBetResponse",        necesaryParamsKeysArray: [],               optionalParamsKeyObj: {},                        RFWSClass: RFWSSetMaxBetResponse },
    PICK_UNTIL_STOP     : { request: "pickUntilStop",         response: "pickUntilStopResponse",    necesaryParamsKeysArray: [],               optionalParamsKeyObj: { "isPlayingGame" : "0" }, RFWSClass: RFWSPickUntilStopResponse },
    START_SESSION       : { request: "startSession",          response: "startSessionResponse",     necesaryParamsKeysArray: [],               optionalParamsKeyObj: {},                        RFWSClass: RFWSStartSessionResponse },
    GET_GAME_STATE      : { request: "getGameState",          response: "getGameStateResponse",     necesaryParamsKeysArray: [],               optionalParamsKeyObj: {},                        RFWSClass: RFWSGetGameStateResponse },
    GET_PAY_TABLE       : { request: "getPayTable",           response: "getPayTableResponse",      necesaryParamsKeysArray: [],               optionalParamsKeyObj: {},                        RFWSClass: RFWSGetPayTableResponse },
    SET_DENOM           : { request: "setDenom",              response: "setDenomResponse",         necesaryParamsKeysArray: [ "denom" ],      optionalParamsKeyObj: {},                        RFWSClass: RFWSSetDenomResponse },
    GET_FORCED_GAMES    : { request: "getForcedGames",        response: "getForcedGamesResponse",   necesaryParamsKeysArray: [],               optionalParamsKeyObj: {},                        RFWSClass: RFWSGetForcedGamesResponse },
    DUNGEON_DOORS       : { request: "dungeonDoors",          response: "dungeonDoorsResponse",     necesaryParamsKeysArray: [],               optionalParamsKeyObj: {},                        RFWSClass: RFWSDungeonDoorsResponse },
    DUNGEON_STAIRS      : { request: "dungeonStairs",         response: "dungeonStairsResponse",    necesaryParamsKeysArray: [],               optionalParamsKeyObj: {},                        RFWSClass: RFWSDungeonStairsResponse },
    DUNGEON_KING        : { request: "dungeonKing",           response: "dungeonKingResponse",      necesaryParamsKeysArray: [],               optionalParamsKeyObj: {},                        RFWSClass: RFWSDungeonKingResponse },
    COMMAND_HISTORY     : { request: "commandHistoryRequest", response: "commandHistoryResponse",   necesaryParamsKeysArray: [],               optionalParamsKeyObj: {},                        RFWSClass: RFWSCommandHistoryResponse },
    CAVERNS             : { request: "caverns",               response: "cavernsResponse",          necesaryParamsKeysArray: [],               optionalParamsKeyObj: {},                        RFWSClass: RFWSCavernsResponse },
    BONUS_SELECTION     : { request: "selectionBonus",        response: "bonusSelectionResponse",   necesaryParamsKeysArray: [],               optionalParamsKeyObj: { "isPlayingGame": "0" },  RFWSClass: RFWSBonusSelectionResponse },
    PAIRS               : { request: "cardPairs",             response: "pairsResponse",            necesaryParamsKeysArray: [],               optionalParamsKeyObj: { "isPlayingGame": "0" },  RFWSClass: RFWSPairsResponse },
    DICES               : { request: "dices",                 response: "dicesResponse",            necesaryParamsKeysArray: [],               optionalParamsKeyObj: { "isPlayingGame": "0" },  RFWSClass: RFWSDicesResponse },
    COCKTAIL            : { request: "cocktail",              response: "cocktailResponse",         necesaryParamsKeysArray: [],               optionalParamsKeyObj: { "isPlayingGame": "0" },  RFWSClass: RFWSCocktailResponse },
    ANIMALS             : { request: "animals",               response: "animalsResponse",          necesaryParamsKeysArray: [],               optionalParamsKeyObj: { "isPlayingGame": "0" },  RFWSClass: RFWSAnimalsResponse },
    GET_ALL_GAME_STATES : { request: "getAllGameStates",      response: "getAllGameStatesResponse", necesaryParamsKeysArray: [],               optionalParamsKeyObj: {},                        RFWSClass: RFWSGetAllGameStatesResponse },
    CARD_SHARP          : { request: "cardSharp",             response: "cardSharpResponse",        necesaryParamsKeysArray: [],               optionalParamsKeyObj: { "isPlayingGame": "0" },  RFWSClass: RFWSCardSharpResponse },
    RAISE_OR_BONUS      : { request: "raiseOrBonus",          response: "raiseOrBonusResponse",     necesaryParamsKeysArray: [],               optionalParamsKeyObj: { "isPlayingGame": "0" },  RFWSClass: RFWSRaiseOrBonusResponse },
    NUDGE               : { request: "nudge",                 response: "nudgeResponse",            necesaryParamsKeysArray: [],               optionalParamsKeyObj: { "isPlayingGame": "0" },  RFWSClass: RFWSNudgeResponse },
    CHANGE_GAME         : { request: "changeGame",            response: "changeGameResponse",       necesaryParamsKeysArray: [],               optionalParamsKeyObj: {},                        RFWSClass: RFWSChangeGameResponse },
    GET_ALL_FORCED_GAMES: { request: "getAllForcedGames",     response: "getAllForcedGamesResponse",necesaryParamsKeysArray: [],               optionalParamsKeyObj: {},                        RFWSClass: RFWSGetAllForcedGamesResponse },
    STRAWBERRY          : { request: "strawberrys",           response: "strawberrysResponse",      necesaryParamsKeysArray: [],               optionalParamsKeyObj: { "isPlayingGame": "0"},   RFWSClass: RFWSStrawberrysResponse }, // "isPlayingGame": "1", "idReel":"2"
    THREE_OPTIONS       : { request: "threeOptions",          response: "threeOptionsResponse",     necesaryParamsKeysArray: [],               optionalParamsKeyObj: { "isPlayingGame": "0" },  RFWSClass: RFWSThreeOptionsResponse }, // "isPlayingGame": "1", "takeWin":"1"
    FINDING_PRIZE       : { request: "findingPrize",          response: "findingPrizeResponse",     necesaryParamsKeysArray: [],               optionalParamsKeyObj: { "isPlayingGame": "0" },  RFWSClass: RFWSFindingPrizeResponse }, // "isPlayingGame": "1", "takeWin":"1"
    TREASURE            : { request: "treasure",              response: "treasureResponse",         necesaryParamsKeysArray: [],               optionalParamsKeyObj: { "isPlayingGame": "0" },  RFWSClass: RFWSTreasureResponse },
    PISTOLS             : { request: "pistols",               response: "pistolsResponse",          necesaryParamsKeysArray: [],               optionalParamsKeyObj: {},                        RFWSClass: RFWSPistolsResponse },
};
Object.freeze( GS_COMMAND_CODES );
/* eslint-enable */

// eslint-disable-next-line
function RFGameServerWrapper() {

    /* jshint -W087 */ // Ignore debugger
    "use strict";

    // We divide the playerBalance of the GameServer by this number
    var PLAYER_BALANCE_RESOLUTION = 1000;

    var STR_COMMAND = "command";
    var STR_BALANCE = "balance";
    var PLATFORM_ID = "";

    var self = this;

    this.onNetworkMessageSent = new Phaser.Signal();
    this.onNetworkMessageReceived = new Phaser.Signal();
    this.onNetworkMessageError = new Phaser.Signal();

    /**
     * Build all the "sendXXXX" functions to the wrapper
     */
    _.forEach( GS_COMMAND_CODES, function( commandCode ) {
        var sendCommand = _.camelCase( "send " + commandCode.request );
        if( _.has( self, [ sendCommand ] ) ) {
            console.warn( "This command is already defined", sendCommand );
            debugger;
        }
        else {
            self[ sendCommand ] = function( params, fOK, fERROR ) {
                send( commandCode, params, fOK, fERROR );
            };
        }
    } );


    /**
     * Initialize RFGameServerWrapper module
     */
    self.initialize = function( initParams, platform ) {

        PLATFORM_ID = ePlatformIDs.ONLINE; // Asegurar la retrocompatibilidad
        if( platform !== undefined ) {
            assertAllKeys( platform, [ "id" ] );
            PLATFORM_ID = platform.id;
        }

        assertAllKeys( initParams, [ "playerAuthenticationToken", "gameUid", "gameId", "isForFun" ] );

        m_playerAuthenticationToken = initParams.playerAuthenticationToken;
        m_gameUid = initParams.gameUid;
        m_gameId = initParams.gameId;

        self.setIsForFun( _.get( initParams, "isForFun", false ) );

        // autoPing initialization
        self.setOptions( {
            enableAutoPings: _.get( initParams, [ "enableAutoPings" ], m_options.enableAutoPings ),
            msBetweenAutoPings: _.get( initParams, [ "msBetweenAutoPings" ], m_options.msBetweenAutoPings ),
            autoPingsMaxAttempts: _.get( initParams, [ "autoPingsMaxAttempts" ], m_options.autoPingsMaxAttempts )
        } );
        return m_rfWebService.initialize( initParams );
    };

    /**
     * Opciones del wrapper y del módulo del webservice
     */
    var m_options = {
        debug: true,
        receiveLabelColor: "font-weight:bold; color:blue;", // only if debug=true
        receiveMessageColor: "color:LightSkyBlue;", // only if debug=true
        logResultPayload: false, // Logs the ResultPayLoad response to see previous in console sessions when refresh the browser. Only if debug=true.
        checkWSResponses: false,
        enableAutoPings: true,
        msBetweenAutoPings: 1000, // If another send was sent, we reset the interval.
        autoPingsMaxAttempts: 5
    };
    self.setOptions = function( oOptions ) {
        m_options.debug = _.get( oOptions, [ "debug" ], m_options.debug );
        m_options.receiveLabelColor = _.get( oOptions, [ "receiveLabelColor" ], m_options.receiveLabelColor );
        m_options.receiveMessageColor = _.get( oOptions, [ "receiveMessageColor" ], m_options.receiveMessageColor );
        m_options.logResultPayload = _.get( oOptions, [ "logResultPayload" ], m_options.logResultPayload );
        m_options.checkWSResponses = _.get( oOptions, [ "checkWSResponses" ], m_options.checkWSResponses );
        m_rfWebService.setOptions( oOptions );

        // Auto Pings
        m_options.enableAutoPings = _.get( oOptions, [ "enableAutoPings" ], m_options.enableAutoPings );
        m_options.msBetweenAutoPings = _.get( oOptions, [ "msBetweenAutoPings" ], m_options.msBetweenAutoPings );
        m_options.autoPingsMaxAttempts = _.get( oOptions, [ "autoPingsMaxAttempts" ], m_options.autoPingsMaxAttempts );
        m_sendPingWatchDog.setOptions( {
            msToPool: m_options.msBetweenAutoPings,
            maxAttempts: m_options.autoPingsMaxAttempts
        } );
        restartSendPingWatchDog();
    };

    /**
     * Permite cambiar el modo isForFun
     * @param {boolean} bValue valor a establecer
     */
    self.setIsForFun = function( bValue ) {
        bValue = _.isBoolean( bValue ) ? bValue : true;
        m_headers.isForFun = bValue;
    };

    /**
     * Permite cambiar el modo isForFun
     */
    self.getIsForFun = function() {
        return m_headers.isForFun;
    };

    /**
     * Send genérico, por si se quiere usar algún envío usando directamente la librería de RFWebServiceModule
     */
    self.send = function( params, fOK, fERROR ) {
        var onSuccess = function( wsResponse ) {
            if( _.isFunction( fOK ) ) {
                fOK( wsResponse );
            }

            self.onNetworkMessageReceived.dispatch( wsResponse );
        };

        var onError = function( wsResponse ) {
            if( _.isFunction( fERROR ) ) {
                fERROR( wsResponse );
            }

            self.onNetworkMessageError.dispatch( wsResponse );
        };

        self.onNetworkMessageSent.dispatch( params );

        restartSendPingWatchDog();
        return m_rfWebService.send( params, onSuccess, onError );
    };

    /**
     * Send genérico, por si se quiere usar algún envío usando directamente la librería de RFWebServiceModule
     */
    self.sendURLEncoded = function( params, fOK, fERROR ) {

        var onSuccess = function( wsResponse ) {
            if( _.isFunction( fOK ) ) {
                fOK( wsResponse );
            }

            self.onNetworkMessageReceived.dispatch( wsResponse );
        };

        var onError = function( wsResponse ) {
            if( _.isFunction( fERROR ) ) {
                fERROR( wsResponse );
            }

            self.onNetworkMessageError.dispatch( wsResponse );
        };

        self.onNetworkMessageSent.dispatch( params );

        restartSendPingWatchDog();
        return m_rfWebService.sendURLEncoded( params, onSuccess, onError );
    };

    // --------------------------------------------------------------------------
    // override the automatic "sendXXX" functions to build custom actions
    // --------------------------------------------------------------------------
    self.sendStartSession = function( params, fOK, fERROR ) {
        if( !_.isEmpty( m_gameSessionToken ) || !_.isEmpty( m_gameSessionUid ) ) {
            m_gameSessionUid = "";
            console.warn( "RFGameServerWrapper:: Already has a session. We start a new one." );
        }
        send( GS_COMMAND_CODES.START_SESSION, params, fOK, fERROR );
    };

    self.sendGetPayTable = function( params, fOK, fERROR ) {
        self.sendGetGameState( params, fOK, fERROR ); console.warn( "sendGetPayTable is DEPRECATED. Use sendGetGameState instead" );
    };

    /**
     * Realiza una petición al GameServer para obtener el balance
     * @param {object} requestParams
     * @param {function} fOk
     * @param {function} fERROR
     */
    self.sendGetBalance = function( requestParams, fOk, fERROR ) {

        // make our custom header
        m_headers.Authorization = m_gameSessionToken;

        // Add RequestUid
        requestParams.requestUid = m_requestUid++;

        var params = {
            type: "GET",
            headers: m_headers,
            url: getCurrentBalanceURL(),
            requestParams: requestParams
        };

        restartSendPingWatchDog();
        m_rfWebService.sendURLEncoded(
            params,
            // OK
            function( strJsonResponse ) {
                var callTofERROR = false;

                if( _.isFunction( fOk ) ) {

                    var wsResponse;
                    var gsResponseJSON = JSON.parse( strJsonResponse );

                    logReceivedMessage( gsResponseJSON );

                    if( _.has( gsResponseJSON, [ "properties", "errorCode" ] ) ) {
                        callTofERROR = true;
                        console.error( "sendInternal: Error received " );
                        console.error( "  - ErrorCode = ", gsResponseJSON.properties.errorCode );
                        console.error( "  - ErrorDescription = ", _.get( gsResponseJSON, [ "properties", "errorDescription" ] ) );
                        debugger;
                    }
                    else {
                        wsResponse = new RFWSGetBalanceResponse( _.get( gsResponseJSON, [ "properties" ] ) );
                        updateGameServerData( wsResponse, gsResponseJSON );
                    }

                    if( !callTofERROR ) {
                        fOk( wsResponse );
                    }
                    else if( _.isFunction( fERROR ) ) {
                        fERROR( strJsonResponse );
                    }
                }

            },
            // ERROR
            function( strJsonResponse ) {
                debugger;
                console.error( strJsonResponse );
                if( _.isFunction( fERROR ) ) {
                    fERROR( strJsonResponse );
                }
            }
        );
    };

    /**
     * Send genérico.
     * @param {string} strRequestId String que identifica la petición que se manda al servidor (ejemplo: "spin")
     * @param {object} params Parámetros de la petición
     * @param {function} fOk Función de callback en caso de Ok. Retorna como parámetro una RFWSGenericResponse
     * @param {function} fERROR Función de callback en caso de Error
     */
    self.sendGenericRequest = function( strRequestId, params, fOk, fERROR ) {
        if( !_.isString( strRequestId ) ) { throw new Error( "sendGenericRequest: strRequestId must be a string" ); }

        var commandCode = {
            request: strRequestId,
            response: strRequestId + "Response",
            necesaryParamsKeysArray: [],
            optionalParamsKeyObj: {},
            RFWSClass: RFWSGenericResponse
        };

        send( commandCode, params, fOk, fERROR );
    };

    /* ----------------------------------------------------------------------------------- */

    var send = function( commandCode, params, fOK, fERROR ) {

        if( _.isEmpty( commandCode ) || !_.has( commandCode, [ "necesaryParamsKeysArray" ] ) || !_.has( commandCode, [ "optionalParamsKeyObj" ] ) ) {
            console.error( "Invalid command code", commandCode );
            debugger;
        }
        else {
            var sendObj = {
                commandCode: commandCode,
                params: params,
                fOK: fOK,
                fERROR: fERROR
            };
            pushSendQueueObj( sendObj );
            processSendQueue();
        }
    };

    var pushSendQueueObj = function( sendObj ) {
        if( m_sendQueue.length > 0 ) {
            console.error( "Multiple messages queued. You must to avoid this, to prevent concurrent access in GameServer. ",
                "Current send message:", m_sendQueue[ 0 ], ", your message:", sendObj );
        }
        sendObj.queueStartTime = Date.now();
        m_sendQueue.push( sendObj );
    };

    var shiftSendQueueObj = function() {
        var processedObj = m_sendQueue.shift();
        return processedObj;
    };

    var endProcessSentObj = function( wasAnErrorInLastSend ) {

        wasAnErrorInLastSend = _.isBoolean( wasAnErrorInLastSend ) ? wasAnErrorInLastSend : false;

        // extract the processed message
        var processedObj = shiftSendQueueObj();

        processedObj.sendEndTime = Date.now();
        var sentElapsedTime = processedObj.sendEndTime - processedObj.sendStartTime;
        processedObj.sentElapsedTime = sentElapsedTime;

        var totalElapsedTime = sentElapsedTime + processedObj.totalQueueElapsedTime;
        processedObj.totalElapsedTime = totalElapsedTime;

        processedObj.sendStatus = "SENT_" + ( wasAnErrorInLastSend ? "ERROR" : "OK" );

        var MS_TO_WARN_COMMUNICATION_PERFORMANCE = 1000;
        var MS_TO_WARN_TOTAL_PROCESSED_MESSAGE = 2000;
        if( sentElapsedTime > MS_TO_WARN_COMMUNICATION_PERFORMANCE ) {
            console.warn( "Communication performance warning.", sentElapsedTime, "ms for this message", processedObj );
        }
        if( totalElapsedTime > MS_TO_WARN_TOTAL_PROCESSED_MESSAGE ) {
            console.warn( "Total process performance warning (communication + queue time).", totalElapsedTime, "ms for this message", processedObj );
        }

        // update the stats
        m_stats.totalSent++;
        if( wasAnErrorInLastSend ) {
            m_stats.totalSentWithError++;
        }
        else {
            m_stats.totalSentOk++;
        }
        m_stats.totalTime += sentElapsedTime;
        m_stats.avgTime = m_stats.totalTime / m_stats.totalSent;

        if( m_options.debug ) {
            console.log( "COMM STATS: ",
                "sent:", m_stats.totalSent,
                ", sentOk:", m_stats.totalSentOk,
                ", sentError:", m_stats.totalSentWithError,
                ", avgTime:", m_stats.avgTime,
                ", totalTime:", m_stats.totalTime );
        }
    };

    var processNextSendObj = function() {
        processSendQueue();
    };

    var processSendQueue = function() {
        var sendObj = m_sendQueue[ 0 ];
        var canProcess = sendObj && sendObj.sendStatus !== "SENDING";
        if( canProcess ) {

            var params = sendObj.params;
            var commandCode = sendObj.commandCode;
            var fOK = sendObj.fOK;
            var fERROR = sendObj.fERROR;
            sendObj.sendStartTime = Date.now();
            sendObj.sendStatus = "SENDING";

            sendObj.queueEndTime = Date.now();
            sendObj.totalQueueElapsedTime = sendObj.queueEndTime - sendObj.queueStartTime;
            var elapsedTime = sendObj.totalQueueElapsedTime;

            var MS_TO_WARN_QUEUE_MESSAGE = 500;
            if( sendObj.totalQueueElapsedTime > MS_TO_WARN_QUEUE_MESSAGE ) {
                console.warn( "Performance warning. Message queued for", elapsedTime, "ms.", sendObj );
            }
            sendInternal( {
                methodParams: makeRequest( params ),
                commandCode: commandCode,
                necesaryParamsKeysArray: commandCode.necesaryParamsKeysArray,
                optionalParamsKeyObj: commandCode.optionalParamsKeyObj,
                fERROR: function( strJsonResponse ) {
                    var wasAnError = true;
                    endProcessSentObj( wasAnError );

                    if( _.isFunction( fERROR ) ) {
                        fERROR( strJsonResponse );
                    }

                    processNextSendObj();
                },
                fOK: function( wsResponseJSON, gsResponse ) {

                    endProcessSentObj();

                    if( _.isFunction( fOK ) ) {
                        var wsResponse;
                        if( !_.isFunction( commandCode.RFWSClass ) ) {
                            debugger;
                            console.warn( "RFWSClass needed to build the object. We use the JSON instead" );
                            wsResponse = wsResponseJSON;
                        }
                        else {
                            wsResponse = new commandCode.RFWSClass( wsResponseJSON );
                        }
                        updateGameServerData( wsResponse, gsResponse );
                        if( m_options.checkWSResponses ) {
                            checkWSResponse( wsResponse );
                        }
                        fOK( wsResponse );
                    }

                    processNextSendObj();
                }
            } );
        }
    };

    var getPrettyTimeStamp = function() {
        function pad2( value ) {
            return _.padStart( value, 2, 0 );
        }
        var date = new Date();
        return (
            date.getFullYear().toString() + "/" +
            pad2( date.getMonth() + 1 ) + "/" +
            pad2( date.getDate() ) + " " +
            pad2( date.getHours() ) + ":" +
            pad2( date.getMinutes() ) + ":" +
            pad2( date.getSeconds() ) + "." +
            pad2( date.getMilliseconds() ) );
    };

    var makeRequest = function( params ) {
        var newParams = {};
        var requestParameters = {
            gameId: m_gameId,
            requestParameters: params
        };
        if( _.isEmpty( m_gameSessionUid ) ) {  // This is only for start session
            newParams.gameUid = m_gameUid;
            newParams.playerAuthenticationToken = m_playerAuthenticationToken;
            newParams.sessionParameters = requestParameters;
            newParams.sessionParameters.timeStamp = getPrettyTimeStamp();
        }
        else {
            newParams.commandParameters = requestParameters;
            newParams.commandParameters.timeStamp = getPrettyTimeStamp();
        }
        newParams.requestUid = m_requestUid++;

        return newParams;
    };

    var getCurrentURL = function( restComand ) {
        var url = m_rfWebService.getURL();
        return !_.isEmpty( m_gameSessionUid ) ? url + "/" + m_gameSessionUid + "/" + restComand : url;
    };

    var getCurrentCommandURL = function() {
        return getCurrentURL( STR_COMMAND );
    };

    var getCurrentBalanceURL = function() {
        return getCurrentURL( STR_BALANCE );
    };

    /**
     * @param {object} oSendInternalParams
     * @param {object} oSendInternalParams.methodParams
     * @param {array} oSendInternalParams.necesaryParamsKeysArray for oSendInternalParams.methodParams.commandParameters.
     * @param {object} oSendInternalParams.optionalParamsKeyObj ( key : default value )
     * @param {function} oSendInternalParams.fOK ( key : default value )
     * @param {function} oSendInternalParams.fERROR
     */
    var sendInternal = function( oSendInternalParams ) {

        assertAllKeys( oSendInternalParams, [ "commandCode", "methodParams" ] );

        // get the requestParameters or the sessionParameters (if it's the first send)
        var requestParameters = _.get(
            oSendInternalParams.methodParams,
            [ "commandParameters", "requestParameters" ],
            _.get( oSendInternalParams.methodParams, [ "sessionParameters", "requestParameters" ], null )
        );
        assertAllKeys( requestParameters, oSendInternalParams.necesaryParamsKeysArray );

        // create optional params for the request
        _.forEach( oSendInternalParams.optionalParamsKeyObj, function( defaultValue, key ) {
            if( !_.has( requestParameters, [ key ] ) ) {
                requestParameters[key] = defaultValue;
            }
        } );

        if( _.has( oSendInternalParams.methodParams, [ "sessionParameters" ] ) ) {
            oSendInternalParams.methodParams.sessionParameters = JSON.stringify( oSendInternalParams.methodParams.sessionParameters );
        }
        else {
            oSendInternalParams.methodParams.commandParameters = JSON.stringify( oSendInternalParams.methodParams.commandParameters );
        }

        // make our custom header
        m_headers.Authorization = m_gameSessionToken;

        var params = {
            headers: m_headers,
            url : getCurrentCommandURL(),
            requestParams : oSendInternalParams.methodParams
        };
        params.requestParams.commandCode = oSendInternalParams.commandCode.request;

        restartSendPingWatchDog( params.requestParams.commandCode );
        m_rfWebService.sendURLEncoded(
            params,
            // TODO: Fix this
            // eslint-disable-next-line complexity
            function( strJsonResponse ) {
                var callTofERROR = false;

                if( _.isFunction( oSendInternalParams.fOK ) ) {

                    var gsResponseJSON = JSON.parse( strJsonResponse );

                    logReceivedMessage( gsResponseJSON );

                    m_gameSessionToken = _.get( gsResponseJSON, [ "properties", "gameSessionToken" ], m_gameSessionToken );
                    m_gameSessionUid = _.get( gsResponseJSON, [ "properties", "gameSessionUid" ], m_gameSessionUid );

                    if( _.has( gsResponseJSON, [ "properties", "errorCode" ] ) ) {
                        callTofERROR = true;
                        console.error( "sendInternal: Error received " );
                        console.error( "  - ErrorCode = ", gsResponseJSON.properties.errorCode );
                        console.error( "  - ErrorDescription = ", _.get( gsResponseJSON, [ "properties", "errorDescription" ] ) );
                        debugger;
                    }
                    else {
                        var stringJSONResponse = _.get( gsResponseJSON, [ "properties", "configuration" ], "" ); // this is for start session (first request)
                        stringJSONResponse = _.get( gsResponseJSON, [ "properties", "resultPayload" ], stringJSONResponse ); // other requests
                        if( _.isEmpty( stringJSONResponse ) ) {
                            callTofERROR = true;
                            console.error( "sendInternal: Invalid Response (empty response)" );
                            debugger;
                        }

                        var wsResponseJSON;
                        try {
                            if( _.isString( stringJSONResponse ) ) {
                                wsResponseJSON = JSON.parse( stringJSONResponse );
                            }
                            else if( _.isObject( stringJSONResponse ) ) {
                                wsResponseJSON = stringJSONResponse;
                            }
                            else {
                                console.error( "sendInternal: Error. Invalid response. Must be and object or an string (" + wsResponseJSON + ")" );
                            }
                        }
                        catch( ex ) {
                            callTofERROR = true;
                            console.error( "sendInternal: JSON.parse error decoding (" + stringJSONResponse + ")", ex );
                            debugger;
                        }

                        if( wsResponseJSON ) {
                            if( !_.has( wsResponseJSON, [ oSendInternalParams.commandCode.response ] ) ) {
                                callTofERROR = true;
                                console.error( "sendInternal: Invalid Response (" + oSendInternalParams.commandCode.response + " response expected)" );
                                debugger;
                            }

                            if( !callTofERROR ) {
                                var wsResponse = wsResponseJSON[ oSendInternalParams.commandCode.response ];
                                oSendInternalParams.fOK( wsResponse, gsResponseJSON );
                                self.onNetworkMessageReceived.dispatch( wsResponse );
                            }
                        }
                    }
                }

                if( callTofERROR && _.isFunction( oSendInternalParams.fERROR ) ) {
                    oSendInternalParams.fERROR( strJsonResponse );
                    self.onNetworkMessageError.dispatch( strJsonResponse );
                }
            },
            function( strJsonResponse ) {
                logReceivedMessage( strJsonResponse );
                if( _.isFunction( oSendInternalParams.fERROR ) ) {
                    oSendInternalParams.fERROR( strJsonResponse );
                    self.onNetworkMessageError.dispatch( strJsonResponse );
                }
            }
        );
    };

    /**
     * Muestra por consola el mensaje recibido
     * @param {any} message
     */
    var logReceivedMessage = function( message ) {
        if( m_options.debug ) {
            if( m_options.logResultPayload ) {
                console.log( "%cRESPONSE << %c", m_options.receiveLabelColor, m_options.receiveMessageColor,
                    _.get( message, "properties.resultPayload" ), message );
            }
            else {
                console.log( "%cRESPONSE << %c", m_options.receiveLabelColor, m_options.receiveMessageColor, message );
            }
        }
    };

    /**
     * Actualiza el cash del player con lo que nos indique el gameServer en su respuesta
     * @param {object} wsResponse WebServiceReponse creado a partir de la respuesta del server
     * @param {object} gsResponse la respuesta del GameServer
     */
    var updateGameServerData = function( wsResponse, gsResponse ) {

        function getPlayerBalanceFromGSResponse( _gsResponse ) {
            var playerBalance = 0;
            if( _.has( _gsResponse, [ "properties", "playerBalance" ] ) ) {
                playerBalance = Number( _gsResponse.properties.playerBalance ) / PLAYER_BALANCE_RESOLUTION;
                if( PLATFORM_ID === ePlatformIDs.BARTOP ) { // Los creditos los tiene que coger de la bartop
                    // lottotechnology: override the cash
                    if( typeof MyGameState !== "undefined" ) {
                        playerBalance = _.get( MyGameState, "creditosNormales", playerBalance );
                    }
                }
            }
            else {
                console.warn( "Can't update the player cash, because we can't get the playerBalance", _gsResponse );
                debugger;
            }
            return playerBalance;
        }

        // wsResponse.response is for GenericRequests
        var player = _.get( wsResponse, [ "player" ] ) || _.get( wsResponse, [ "response", "player" ] );

        if( player ) {

            // override the cash value with playerBalance gameServer data
            player.cash = getPlayerBalanceFromGSResponse( gsResponse );

            if( player instanceof RFWSPlayer ) {
                console.info( "player.cash updated", wsResponse );
            }
            else if( _.has( player, [ "cash" ] ) ) {
                console.info( "player.cash updated", wsResponse );
            }
            else {
                console.error( "Bad player", wsResponse );
                debugger;
            }
        }
        else if( wsResponse instanceof RFWSGetBalanceResponse ) {
            wsResponse.playerBalance = getPlayerBalanceFromGSResponse( gsResponse );
            console.info( "playerBalance updated", wsResponse );
        }

        if(  _.has( wsResponse, [ "gameState" ] ) && wsResponse.gameState instanceof RFWSGameState ) {
            // update internal player responses
            updateGameServerData( wsResponse.gameState.play, gsResponse );
        }
    };

    var restartSendPingWatchDog = function( commandId ) {
        // We stop the watchcdog if we don't have a gameSessionUid
        if( m_gameSessionUid === "" ) {
            m_sendPingWatchDog.stop();
        }
        else if( commandId === GS_COMMAND_CODES.PING.request ) {
            // Don not restart
        }
        else if( m_options.enableAutoPings ) {
            m_sendPingWatchDog.stop();
            m_sendPingWatchDog.start();
        }
    };

    var m_rfWebService = new RFWebServiceModule();
    var m_requestUid = Date.now();
    var m_gameSessionUid = "";
    var m_gameSessionToken = "";
    var m_playerAuthenticationToken = "";
    var m_gameUid = "";
    var m_gameId = ""; // Game Identifier in each request inside of commandParameters or sessionParameters
    var m_headers = {};
    var m_sendQueue = [];
    var m_stats = {
        totalSentWithError: 0,
        totalSentOk: 0,
        totalSent: 0,
        avgTime: 0,
        totalTime: 0
    };
    var m_sendPingWatchDog = new RFWatchdog();
    m_sendPingWatchDog.setOptions( {
        watchFunction: self.sendPing,
        msToPool: m_options.msBetweenAutoPings,
        maxAttempts: m_options.autoPingsMaxAttempts
    } );
    restartSendPingWatchDog();
}



//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.defs.js
//------------------------------------------------------

/**
 * Definición de tipos de resultados devueltos por el webservice
 * @version 0.1.9
 * CAMBIOS:
 * 0.1.7 - Añade la escena ROULETTE
 * 0.1.8 - Se modifica PAIRS_GAME  por CARD_PAIRS_GAME
 * 0.1.9 - Añade la escena de FRUIT_SELECTION y MASK_GAME
 */
// eslint-disable-next-line
var eServerReturnCodes =  {
    OK                  : "0",
    INTERNAL_ERROR      : "1",
    INVALID_SESSION     : "2",
    INSUFFICIENT_FOUNDS : "3",
    INVALID_PLAY_GAME   : "5",
    LIMIT_EXCEEDED      : "6",
    OPERATION_CANCELLED : "7",
    FORBIDDEN_BETTING   : "8"
};
Object.freeze( eServerReturnCodes );

// eslint-disable-next-line
var eServerSceneId = {
    BASE_GAME         : "BASE_GAME",
    FREE_GAME         : "FREE_GAME",
    DOUBLE_OR_NOTHING : "DOUBLE_OR_NOTHING",
    PICK_UNTIL_STOP   : "PICK_UNTIL_STOP",
    DUNGEON_DOORS     : "DUNGEON_DOORS",
    DUNGEON_STAIRS    : "DUNGEON_STAIRS",
    DUNGEON_KING      : "DUNGEON_KING",
    CAVERNS_GAME      : "CAVERNS_GAME",
    BONUS_SELECTION   : "BONUS_SELECTION",
    CARD_PAIRS_GAME   : "CARD_PAIRS_GAME",
    COCKTAIL_GAME     : "COCKTAIL_GAME",
    DICES_GAME        : "DICES_GAME",
    ANIMALS_GAME      : "ANIMALS_GAME",
    CARD_SHARP        : "CARD_SHARP",
    RAISE_OR_BONUS    : "RAISE_OR_BONUS",
    UPPER_GAME        : "UPPER_GAME",
    STRAWBERRYS       : "STRAWBERRYS",
    TREASURE          : "TREASURE",
    THREE_OPTIONS     : "THREE_OPTIONS",
    FINDING_PRIZE     : "FINDING_PRIZE",
    PISTOLS_GAME      : "PISTOLS_GAME",
    ROULETTE          : "ROULETTE",
    FRUIT_SELECTION   : "FRUIT_SELECTION",
    MASK_GAME         : "MASK_GAME"
};
Object.freeze( eServerSceneId );

// eslint-disable-next-line
var eConnectionTypes = {
    UNKNOWN      : 0,
    CONNECTED    : 1,
    DISCONNECTED : 2
};
Object.freeze( eConnectionTypes );

var eLobbyReturnOption = {
    NONE: "NONE",
    URL: "URL",
    HISTORY_BACK: "HISTORY_BACK",
    CLOSE_WINDOW: "CLOSE_WINDOW",
    BARTOP: "BARTOP"
};
Object.freeze( eLobbyReturnOption );

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.animals.js
//------------------------------------------------------

/**
 * @version 0.0.2
 */
// eslint-disable-next-line
function RFWSAnimalsResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.animalsState.fromJson( json.animalsState );
            this.payTable.fromJson( json.payTable );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.animalsState = new RFWSAnimalsState();
    this.payTable = new RFWSPayTable();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.bonusselection.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSBonusSelectionResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.bonusSelectionState.fromJson( json.bonusSelectionState );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.bonusSelectionState = new RFWSBonusSelectionState();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.cardsharp.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSCardSharpResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.cardSharpState.fromJson( json.cardSharpState );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.player = new RFWSPlayer();
    this.cardSharpState = new RFWSCardSharpState();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.caverns.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSCavernsResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.cavernsState.fromJson( json.cavernsState );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.cavernsState = new RFWSCavernsState();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.changegame.js
//------------------------------------------------------

/**
 * Same format that RFWSGetAllGameStatesResponse
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSChangeGameResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // basic types / custom
            this.payTables = _.wsGetParsedJSON( json, "payTablesJSON", this.payTables );

            // complex types
            this.gameStates.fromJson( json.gameStates );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    // this.payTables = undefined;  // Each game has its own payTable structure. The game needs to parse it
    this.gameStates = new RFWSGameStates();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.cocktail.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSCocktailResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.cocktailState.fromJson( json.cocktailState );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.cocktailState = new RFWSCocktailState();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.commandhistory.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSCommandHistoryResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        this.historyList =  _.wsGetParsedJSON( json, "historyListJSON", this.historyList );
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.dices.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSDicesResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.dicesState.fromJson( json.dicesState );
            this.payTable.fromJson( json.payTable );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.dicesState = new RFWSDicesState();
    this.payTable = new RFWSPayTable();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.doubleornothing.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSDoubleOrNothingResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.player.fromJson( json.player );
            this.doubleOrNothingState.fromJson( json.doubleOrNothingState );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.player = new RFWSPlayer();
    this.doubleOrNothingState = new RFWSDoubleOrNothingState();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.dungeondoors.js
//------------------------------------------------------

/**
 * @version 0.0.2
 */
// eslint-disable-next-line
function RFWSDungeonDoorsResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.player.fromJson( json.player );
            this.dungeonDoorsState.fromJson( json.dungeonDoorsState );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.player = new RFWSPlayer();
    this.dungeonDoorsState = new RFWSDungeonDoorsState();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.dungeonking.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSDungeonKingResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.player.fromJson( json.player );
            this.dungeonKingState.fromJson( json.dungeonKingState );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.player = new RFWSPlayer();
    this.dungeonKingState = new RFWSDungeonKingState();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.dungeonstairs.js
//------------------------------------------------------

/**
 * @version 0.0.2
 */
// eslint-disable-next-line
function RFWSDungeonStairsResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.player.fromJson( json.player );
            this.dungeonStairsState.fromJson( json.dungeonStairsState );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.player = new RFWSPlayer();
    this.dungeonStairsState = new RFWSDungeonStairsState();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.findingprize.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSFindingPrizeResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.findingPrizeState.fromJson( json.findingPrizeState );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.findingPrizeState = new RFWSFindingPrizeState();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}




//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.generic.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSGenericResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {
            // basic types
            this.response = getJSON( json ) || json;
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.getallforcedgames.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSGetAllForcedGamesResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            this.forcedGames = _.wsGetParsedJSON( json, "forcedGames", this.forcedGames );

            // complex types
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---
    // this.forcedGamesArray = [];
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.getallgamestates.js
//------------------------------------------------------

/**
 * @version 0.0.2
 * CAMBIOS:
 * 0.0.2 - Se añade la opción de salida al lobby
 */
// eslint-disable-next-line
function RFWSGetAllGameStatesResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // basic types / custom
            this.payTables = _.wsGetParsedJSON( json, "payTablesJSON", this.payTables );

            // complex types
            this.gameStates.fromJson( json.gameStates );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );

            // This is the game configuration in the Server
            this.gameConfig = _.wsGetParsedJSON( json, "gameConfigJSON", this.gameConfig );

            this.lobbyReturnOptionMobile = getSanitizedReturnOption( _.get( this.gameConfig, "lobbyReturnOptionMobile", this.lobbyReturnOptionMobile ) );
            this.lobbyReturnOptionDesktop = getSanitizedReturnOption( _.get( this.gameConfig, "lobbyReturnOptionDesktop", this.lobbyReturnOptionDesktop ) );
            this.lobbyUrlDesktop = _.get( this.gameConfig, "lobbyUrlDesktop", this.lobbyUrlDesktop );
            this.lobbyUrlMobile = _.get( this.gameConfig, "lobbyUrlMobile", this.lobbyUrlMobile );

        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    // this.payTables = undefined;  // Each game has its own payTable structure. The game needs to parse it
    this.gameStates = new RFWSGameStates();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    var getSanitizedReturnOption = function( strLobbyReturnOption ) {
        return _.get( eLobbyReturnOption, strLobbyReturnOption, eLobbyReturnOption.NONE );
    };

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.getbalance.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSGetBalanceResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            this.playerBalance = Number( _.wsGet( json, "playerBalance", this.playerBalance ) );

        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---
    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.getforcedgames.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSGetForcedGamesResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            this.forcedGamesArray = _.wsGetParsedJSON( json, "forcedGamesArrayJSON", this.forcedGamesArray );

            // complex types
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---
    // this.forcedGamesArray = [];
    this.result = new RFWSResult();

    /* Server JSON description for forcedGamesArray
        [
            {
                id: "FREE_GAME",
                description: "Force free game",
                serverSceneId: "BASE_GAME"
            },
            ...
        ]
     */

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.getgamestate.js
//------------------------------------------------------

/**
 * @version 0.0.2
 */
// eslint-disable-next-line
function RFWSGetGameStateResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.payTable.fromJson( json.payTable );
            this.payTableFG.fromJson( json.payTableFG );
            this.player.fromJson( json.player );
            this.gameState.fromJson( json.gameState );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.payTable = new RFWSPayTable();
    this.payTableFG = new RFWSPayTable();
    this.player = new RFWSPlayer();
    this.gameState = new RFWSGameState();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.getpaytable.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSGetPayTableResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.payTable.fromJson( json.payTable );
            this.payTableFG.fromJson( json.payTableFG );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.payTable = new RFWSPayTable();
    this.payTableFG = new RFWSPayTable();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.nudge.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSNudgeResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.nudgeState.fromJson( json.nudgeState );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.player = new RFWSPlayer();
    this.nudgeState = new RFWSSpinState();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.pairs.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSPairsResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.pairsState.fromJson( json.pairsState );
            this.payTable.fromJson( json.payTable );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.pairsState = new RFWSPairsState();
    this.payTable = new RFWSPayTable();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.pickuntilstop.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSPickUntilStopResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.pickUntilStopState.fromJson( json.pickUntilStopState );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.pickUntilStopState = new RFWSPickUntilStopState();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.ping.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSPingResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.pistols.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSPistolsResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.pistolsState.fromJson( json.pistolsState );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.pistolsState = new RFWSPistolsState();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.raiseorbonus.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSRaiseOrBonusResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.raiseOrBonusState.fromJson( json.raiseOrBonusState );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.player = new RFWSPlayer();
    this.raiseOrBonusState = new RFWSRaiseOrBonusState();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.setbetperline.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSSetBetPerLineResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.player.fromJson( json.player );
            this.payTable.fromJson( json.payTable );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.player = new RFWSPlayer();
    this.payTable = new RFWSPayTable();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}



//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.setdenom.js
//------------------------------------------------------

/**
 * @version 0.0.2
 */
// eslint-disable-next-line
function RFWSSetDenomResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // basic types / custom
            this.payTables = _.wsGetParsedJSON( json, "payTablesJSON", this.payTables );

            // complex types
            this.payTable.fromJson( json.payTable ); // TODO: en el futuro sería interesante eliminar esto y que los juegos lean los "paytables"
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.payTable = new RFWSPayTable();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}



//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.setlines.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSSetLinesResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.payTable.fromJson( json.payTable );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.payTable = new RFWSPayTable();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}



//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.setmaxbet.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSSetMaxBetResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.payTable.fromJson( json.payTable );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.payTable = new RFWSPayTable();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}



//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.spin.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSSpinResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.spinState.fromJson( json.spinState );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.spinState = new RFWSSpinState();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.startsession.js
//------------------------------------------------------

/**
 * @version 0.0.7
 * CAMBIOS:
 * 0.0.5 - Optimizados los accesos por lodash
 * 0.0.6 - Se extrae la información de configuración para el cliente.
 * 0.0.7 - Se añade la opción de salida al lobby
 */
// eslint-disable-next-line
function RFWSStartSessionResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // basic types
            this.sessionId = _.wsGet( json, "sessionId", this.sessionId );
            this.version = _.wsGet( json, "version", this.version );

            // complex types
            this.payTable.fromJson( json.payTable );
            this.player.fromJson( json.player );

            if( _.has( json, [ "lastGameState" ] ) ) {
                this.lastGameState = new RFWSGameState();
                this.lastGameState.fromJson( json.lastGameState );
            }
            if( _.has( json, [ "gameStates" ] ) ) {
                this.gameStates = new RFWSGameStates();
                this.gameStates.fromJson( json.gameStates );
            }
            if( _.has( json, [ "payTablesJSON" ] ) ) {
                this.payTables = _.wsGetParsedJSON( json, "payTablesJSON", this.payTables );
            }

            // This is to overwrite the RFGameConfig with a config file.
            this.clientConfig = _.wsGetParsedJSON( json, "clientConfigJSON", this.clientConfig );

            // This is the game configuration in the Server
            this.gameConfig = _.wsGetParsedJSON( json, "gameConfigJSON", this.gameConfig );

            this.lobbyReturnOptionMobile = getSanitizedReturnOption( _.get( this.gameConfig, "lobbyReturnOptionMobile", this.lobbyReturnOptionMobile ) );
            this.lobbyReturnOptionDesktop = getSanitizedReturnOption( _.get( this.gameConfig, "lobbyReturnOptionDesktop", this.lobbyReturnOptionDesktop ) );
            this.lobbyUrlDesktop = _.get( this.gameConfig, "lobbyUrlDesktop", this.lobbyUrlDesktop );
            this.lobbyUrlMobile = _.get( this.gameConfig, "lobbyUrlMobile", this.lobbyUrlMobile );

            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    // this.sessionId = "";
    // this.version = "";
    // this.lastGameState = new RFWSGameState();  OPTIONAL
    // this.gameStates = new RFWSGameStates();  OPTIONAL
    this.payTable = new RFWSPayTable();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();


    var getSanitizedReturnOption = function( strLobbyReturnOption ) {
        return _.get( eLobbyReturnOption, strLobbyReturnOption, eLobbyReturnOption.NONE );
    };

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.strawberry.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSStrawberrysResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.strawberrysState.fromJson( json.strawberrysState );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.strawberrysState = new RFWSStrawberrysState();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.threeoptions.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSThreeOptionsResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.threeOptionsState.fromJson( json.threeOptionsState );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.threeOptionsState = new RFWSThreeOptionsState();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}




//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.response.treasure.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSTreasureResponse( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {

            // complex types
            this.treasureState.fromJson( json.treasureState );
            this.player.fromJson( json.player );
            this.result.fromJson( json.result );
        }
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    this.treasureState = new RFWSTreasureState();
    this.player = new RFWSPlayer();
    this.result = new RFWSResult();

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.animalsstate.js
//------------------------------------------------------

/**
 * @version 0.0.2
 */
// eslint-disable-next-line
function RFWSAnimalsState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic types
        this.currentAnimal = _.wsGetParsedJSON( json, "currentAnimalJSON", this.currentAnimal );
        this.selectedAnimalsArray = _.wsGetParsedJSON( json, "selectedAnimalsArrayJSON", this.selectedAnimalsArray );
        this.remainingSelections = Number( _.wsGet( json, "remainingSelections", this.remainingSelections ) );
        this.finished = _.wsGet( json, "finished", this.finished );
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    // this.selectedItems = {};
    // this.multiplier = 0;
    // this.remainingSelections = 0;
    // this.bonus = 0;

    /* server example:
      "animalsState":{
         "remainingSelections": 2,
         "selectedAnimalsArrayJSON": [
            {
                "idAnimal":"MONO",
                "prize":20.0
            },
            {
                "idAnimal":"GORILA",
                "prize":30.0
            }
         ],
         "currentAnimalJSON": {
            "idAnimal":"MONO",
            "prize":20.0
         } ,
         "finished": false
      },
    */

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.bonusselectionstate.js
//------------------------------------------------------

/**
 * @version 0.0.2
 */
// eslint-disable-next-line
function RFWSBonusSelectionState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic types
        this.idGameSelected = _.wsGet( json, "idGameSelected", this.idGameSelected );
        this.availableBonusGames = _.wsGet( json, "availableBonusGames", this.availableBonusGames );
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    /* server example
        "bonusSelectionState": {
            "IdGameSelected": "",
            "availableBonusGames": [
                "PAIRS",
                "COCKTAIL",
                "DICES"
            ]
        }
    */
    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.cardsharpstate.js
//------------------------------------------------------

/**
 * @version 0.0.2
 */
// eslint-disable-next-line
function RFWSCardSharpState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic types
        this.prize = Number( _.wsGet( json, "prize", this.prize ) );
        this.prizeWinArray = _.wsGetParsedJSON( json, "prizeWinArrayJSON", this.prizeWinArray );
        this.prizesArray = _.wsGetParsedJSON( json, "prizesArrayJSON", this.prizesArray );
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }

    /* Server example
        {
            "prize": "0",
            "prizeWinArrayJSON": {
                "p1": "0.0", // La elegida
                "p2": "0.0",
                "p3": "0.0"
            },
            "optPrizeListJSON": {
                "p1": "22.0",
                "p2": "31.200000000000003",
                "p3": "29.400000000000002"
            }
        }
    */

}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.cavernsstate.js
//------------------------------------------------------

/**
 * @version 0.0.2
 */
// eslint-disable-next-line
function RFWSCavernsState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic type
        this.acumulattedPrize = Number( _.wsGet( json, "acumulattedPrize", this.acumulattedPrize ) );
        this.currentSelectionCavernLevel = Number( _.wsGet( json, "currentSelectionCavernLevel", this.currentSelectionCavernLevel ) );
        this.arrayNextCavernPosibleValues = _.wsGetParsedJSON( json, "arrayNextCavernPosibleValuesJSON", this.arrayNextCavernPosibleValuesJSON );
        this.selectedCavern = _.wsGetParsedJSON( json, "selectedCavernJSON", this.selectedCavernJSON );
        this.selectedItems = _.wsGetParsedJSON( json, "selectedItemsJSON", this.selectedItemsJSON );
        this.remainingSelections = Number( _.wsGet( json, "remainingSelections", this.remainingSelections ) );
        this.finished = Number( _.wsGet( json, "finished", this.finished ) );
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    // prizeType values: MULTIPLIER || NUMBER || STOP_GAME

    /* example of server:

        "cavernsState": {
            "prize": "5.0",
            "acumulattedPrize": "5.0",
            "currentSelectionCavernLevel": 1,
            "arrayNextCavernPosibleValuesJSON": ["STOP_GAME","24","X2","20"],
            "selectedCavernJSON": {
                "selectedId": "PICK_1",
                "actualLevelCavern": "1.0",
                "prizeType": "NUMBER",
                "cavernPrizes": ["12.0","18.0","8.0"],
                "prize": "5.0"
            },
            "selectedItemsJSON": [
                {
                    "selectedId": "PICK_1",
                    "actualLevelCavern": "1.0",
                    "prizeType": "NUMBER",
                    "cavernPrizes": ["12.0","18.0","8.0"],
                    "prize": "5.0"
                }
            ],
            "remainingSelections": "4",
            "finished": "0"
        }
    */

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.cocktailstate.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSCocktailState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic types
        this.selectedCocktails = _.wsGetParsedJSON( json, "selectedCocktailsJSON", this.selectedCocktails );
        this.acumulattedPrize = Number( _.wsGet( json, "acumulattedPrize", this.acumulattedPrize ) );
        this.remainingSelections = Number( _.wsGet( json, "remainingSelections", this.remainingSelections ) );
        this.finished = Number( _.wsGet( json, "finished", this.finished ) );

    };

    // -------------------------------------------------- CONSTRUCTOR ---

    // this.selectedCocktails = {};
    // this.multiplier = 0;
    // this.remainingSelections = 0;
    // this.bonus = 0;

    /* server example:
        "cocktailState":{
         "selectedCocktailsJSON":[],
         "acumulattedPrize":0.0 ,
         "remainingSelections": 3 ,
         "finished": false
      },
    */

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.dicesstate.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSDicesState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic types
        this.selectedDices = _.wsGetParsedJSON( json, "selectedDicesJSON", this.selectedDices );
        this.remainingSelections = Number( _.wsGet( json, "remainingSelections", this.remainingSelections ) );
        this.finished = Number( _.wsGet( json, "finished", this.finished ) );

    };

    // -------------------------------------------------- CONSTRUCTOR ---

    // this.selectedDices = {};
    // this.remainingSelections = 0;
    // this.finished = false;

    /* server example:
        "dicesState":{
         "selectedDices": {
            "alias":"OCHO",
            "prize":1.6
         } ,
         "remainingSelections": 2,
         "finished": false
      }
    */

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}


//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.doubleornothingstate.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSDoubleOrNothingState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic types
        this.initPrize = Number( _.wsGet( json, "initPrize", this.initPrize ) );
        this.prize = Number( _.wsGet( json, "prize", this.prize ) );
        this.nextPrize = Number( _.wsGet( json, "nextPrize", this.nextPrize ) );
        this.hasLost = Number( _.wsGet( json, "hasLost", this.hasLost ) );
        this.remainingGames = Number( _.wsGet( json, "remainingGames", this.remainingGames ) );
        this.numShowItem = Number( _.wsGet( json, "numShowItem", this.numShowItem ) );
        this.initPrize = Number( _.wsGet( json, "initPrize", this.initPrize ) );
        this.risk = Number( _.wsGet( json, "risk", this.risk ) );
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    // this.initPrize = 0;
    // this.prize = 0;
    // this.nextPrize = 0;
    // this.hasLost = 0;
    // this.remainingGames = 0;
    // this.numShowItem = 0;

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.dungeondoors.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSDungeonDoorsState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic types
        this.prize = Number( _.wsGet( json, "prize", this.prize ) );
        this.hasLost = Number( _.wsGet( json, "hasLost", this.hasLost ) );
        this.bonusWin = Number( _.wsGet( json, "bonusWin", this.bonusWin ) );
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}



//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.dungeonking.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSDungeonKingState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic types
        this.prize = Number( _.wsGet( json, "prize", this.prize ) );
        this.hasLost = Number( _.wsGet( json, "hasLost", this.hasLost ) );
        this.bonusWin = Number( _.wsGet( json, "bonusWin", this.bonusWin ) );
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}



//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.dungeonstairs.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSDungeonStairsState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic types
        this.prize = Number( _.wsGet( json, "prize", this.prize ) );
        this.hasLost = Number( _.wsGet( json, "hasLost", this.hasLost ) );
        this.bonusWin = Number( _.wsGet( json, "bonusWin", this.bonusWin ) );
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.findingprize.js
//------------------------------------------------------

/**
 * @version 0.0.3
 */
// eslint-disable-next-line
function RFWSFindingPrizeState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic types
        this.prizesArray = _.wsGet( json, "prizesArray", this.prizesArray );
        this.acumulatedPrize = Number( _.wsGet( json, "acumulatedPrize", this.acumulatedPrize ) );
        this.prize = Number( _.wsGet( json, "prize", this.prize ) );
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.gamestate.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSGameState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        this.play = _.wsGetParsedJSON( json, "playJSON", this.play );
        this.responseType = _.wsGet( json, "responseType", this.responseType );
        this.serverSceneId = _.wsGet( json, "serverSceneId", this.serverSceneId );
    };

    // -------------------------------------------------- CONSTRUCTOR ---
    // this.play = {};
    // this.responseType = "";
    // this.serverSceneId = "";

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.gamestates.js
//------------------------------------------------------

/**
 * This class contains as keys the eServerSceneId, and as value the state asociated
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSGameStates( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        var self = this;
        _.forEach( json, function( gameState, key ) {
            var gameStateObj = RFWebServiceUtil.createRFWSGameState( key, gameState );
            if( gameStateObj ) {
                self[ key ] = gameStateObj;
            }
        } );
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.pairsstate.js
//------------------------------------------------------

/**
 * @version 0.0.2
 */
// eslint-disable-next-line
function RFWSPairsState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic types
        this.selectedCards = _.wsGetParsedJSON( json, "selectedCardsJSON", this.selectedCards );
        this.accumulatedPrize = Number( _.wsGet( json, "accumulatedPrize", this.accumulatedPrize ) );
        this.remainingSelections = Number( _.wsGet( json, "remainingSelections", this.remainingSelections ) );
        this.finished = Number( _.wsGet( json, "finished", this.finished ) );
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    // this.selectedItems = {};
    // this.multiplier = 0;
    // this.remainingSelections = 0;
    // this.bonus = 0;

    /* server example:
        "pairsState": {
            "prize": "0.0",
            "selectedItemsJSON": [
                {
                    "isWinner": "0.0",
                    "selectedID": "PICK_3",
                    "cardAlias": "REY",
                    "prize": "3.0"
                }
            ],
            "remainingSelections": "7",
            "accumulatedPrize": "0.0",
            "finished": "0",
            "cardResult": {
                "isWinner": "0.0",
                "selectedID": "PICK_3",
                "cardAlias": "REY",
                "prize": "3.0"
            }
        }
    */

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.paytable.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSPayTable( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        this.prizeList =  _.wsGetParsedJSON( json, "prizeListJSON", this.prizeList );
    };

    // -------------------------------------------------- CONSTRUCTOR ---
    // this.prizeList = {};

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.pickuntilstopstate.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSPickUntilStopState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic types
        this.selectedItems = _.wsGetParsedJSON( json, "selectedItemsJSON", this.selectedItems );
        this.multiplier = Number( _.wsGet( json, "multiplier", this.multiplier ) );
        this.remainingSelections = Number( _.wsGet( json, "remainingSelections", this.remainingSelections ) );
        this.bonus = Number( _.wsGet( json, "bonus", this.bonus ) );
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    // this.selectedItems = {};
    // this.multiplier = 0;
    // this.remainingSelections = 0;
    // this.bonus = 0;

    /* example of server selectedItems:
       {
           "pick1" : {
                bonus: 2,
                multiplier: 5
            }
        }
    */

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.pistols.js
//------------------------------------------------------

// eslint-disable-next-line
function RFWSPistolsState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic type
        this.currentShot = _.wsGetParsedJSON( json, "currentShotJSON", this.currentShot );
        this.selectedShots = _.wsGetParsedJSON( json, "selectedShotsJSON", this.selectedShots );
        this.accumulatedPrize = _.wsGet( json, "accumulatedPrize", this.accumulatedPrize );
        this.remainingSelections = Number( _.wsGet( json, "remainingSelections", this.remainingSelections ) );
        this.finished = Number( _.wsGet( json, "finished", this.finished ) );
    };

    // -------------------------------------------------- CONSTRUCTOR ---


    /* example of server:

        "pistolsState":{
         "remainingSelections": 2,
         "currentShotJSON": {
            "selectedShotId":"ITEM_2",
            "numPrizes":2,
            "lShotPrizes":[8.0,
            12.0]
         },
         "selectedShotsJSON": [{
            "selectedShotId":"ITEM_2",
            "numPrizes":2,
            "lShotPrizes":[8.0,
            12.0]
         }],
         "accumulatedPrize": 4.0 ,
         "finished": false
      }
    */

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.player.js
//------------------------------------------------------

/**
 * @version 0.1.0
 * CAMBIOS
 * 0.1.0 - Se añade la información del round
 */
// eslint-disable-next-line
function RFWSPlayer( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        this.cash = Number( _.wsGet( json, "cash", this.cash ) );
        this.denom = Number( _.wsGet( json, "denom", this.denom ) );
        this.bonus = Number( _.wsGet( json, "bonus", this.bonus ) );
        this.multiplier = Number( _.wsGet( json, "multiplier", this.multiplier ) );
        this.betPerLine = Number( _.wsGet( json, "betPerLine", this.betPerLine ) );
        this.currentServerSceneId = _.wsGet( json, "currentServerSceneId", this.currentServerSceneId );
        this.totalBet = Number( _.wsGet( json, "totalBet", this.totalBet ) );
        this.numLines = Number( _.wsGet( json, "numLines", this.numLines ) );
        this.currencyISO = _.wsGet( json, "currencyISO", this.currencyISO );
        this.planInf = Number( _.wsGet( json, "planInf", this.planInf ) );
        this.planSup = Number( _.wsGet( json, "planSup", this.planSup ) );
        this.internalBet = Number( _.wsGet( json, "internalBet", this.internalBet ) );
        this.round = _.wsGet( json, "round", this.round );
    };

    // -------------------------------------------------- CONSTRUCTOR ---
    // this.cash = 0;
    // this.denom = 0.0;
    // this.bonus = 0;
    // this.multiplier = 0;
    // this.betPerLine = 0;
    // this.currentServerSceneId = "";
    // this.numLines = 0;
    // this.totalBet = 0;
    // this.currencyISO = "";
    // this.planInf = 0;
    // this.planSup = 0;

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.raiseorbonusstate.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSRaiseOrBonusState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic types
        this.currentPrize = Number( _.wsGet( json, "currentPrize", this.currenPrize ) );
        this.nextPrize = Number( _.wsGet( json, "nextPrize", this.nextPrize ) );
        this.lastPrize = Number( _.wsGet( json, "lastPrize", this.lastPrize ) );
        this.bonus = Number( _.wsGet( json, "bonus", this.bonus ) );
        this.currentLevel = Number( _.wsGet( json, "currentLevel", this.currentLevel ) );
        this.subGameState = _.wsGet( json, "subGameState", this.subGameState );
        this.finished = _.wsGet( json, "finished", this.finished );
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }

}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.reels.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSReels( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        if( _.isObject( json ) ) {
            var reelMatrix = [];
            var numReels = _.filter( _.keys( json ), function( o ) { return _.startsWith( o, "reel" ); } ).length;
            for( var i = 0; i < numReels; ++i ) {
                reelMatrix.push( makeReelArray( json[ "reel" + ( i + 1 ) ] ) );
            }

            if( isReelMatrixValid( reelMatrix ) ) {
                this.reelMatrix = reelMatrix;
            }
        }
    };

    var makeReelArray = function( jsonResponseReel ) {
        var reelArray = [];
        if( _.isObject( jsonResponseReel ) ) {
            var numSymbols = _.filter( _.keys( jsonResponseReel ), function( o ) { return _.startsWith( o, "f" ); } ).length;
            for( var i = 0; i < numSymbols; ++i ) {
                reelArray.push( _.wsGet( jsonResponseReel, "f" + ( i + 1 ) ) );
            }
        }
        return reelArray;
    };

    var isReelMatrixValid = function( reelMatrix ) {
        var isValid = true;
        var length = -1;
        if( _.isArray( reelMatrix ) ) {
            for( var i = 0; i < reelMatrix.length && isValid; ++i ) {
                length = ( length < 0 ? reelMatrix[ i ].length : length );
                isValid = ( reelMatrix[ i ].length === length );
            }
        }
        else {
            isValid = false;
        }
        return isValid;
    };

    // -------------------------------------------------- CONSTRUCTOR ---
    this.reelMatrix = [];

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.result.js
//------------------------------------------------------

/**
 * @version 0.0.1
 */
// eslint-disable-next-line
function RFWSResult( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {
        this.returnCode = _.wsGet( json, "returnCode", this.returnCode );
        this.returnMessage = _.wsGet( json, "returnMessage", this.returnMessage );
        this.transactionId = _.wsGet( json, "transactionId", this.transactionId );
    };

    // -------------------------------------------------- CONSTRUCTOR ---
    // this.returnCode = "";
    // this.returnMessage = "";
    // this.transactionId = "";

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.spinstate.js
//------------------------------------------------------

/**
 * @version 0.0.5
 */
// eslint-disable-next-line
function RFWSSpinState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic types
        this.prize = Number( _.wsGet( json, "prize", this.prize ) );
        this.bonus = Number( _.wsGet( json, "bonus", this.bonus ) );
        this.acumulatedPrize = Number( _.wsGet( json, "acumulatedPrize", this.acumulatedPrize ) );
        this.winPrizesArray = _.wsGetParsedJSON( json, "winPrizesArrayJSON", this.winPrizesArray );
        this.extendedWildArray = _.wsGetParsedJSON( json, "extendedWildArrayJSON", this.extendedWildArray );
        this.nudge = Number( _.wsGet( json, "nudge", this.nudge ) );
        this.hold = Number( _.wsGet( json, "hold", this.hold ) );
        this.subGameState = _.wsGet( json, "subGameState", this.subGameState );
        this.multiply = Number( _.wsGet( json, "multiply", this.multiply ) );
        this.bigPrize = Boolean( _.wsGet( json, "bigPrize", this.bigPrize ) );
        this.nextReelIdsToSpinArray = _.wsGetParsedJSON( json, "spinReelIds", this.nextReelIdsToSpinArray );

        // complex types
        this.reels.fromJson( json );
    };

    /*
     * -------------------------------------------------- CONSTRUCTOR ---
     * this.prize = 0;
     * this.bonus = 0;
     * this.acumulatedPrize = 0;
     * this.nudge = 0
     * this.hold = 0
     * this.subGameState  => Estado de la máquina en este juego.
     *                       P.e en la Gnomos:  "NORMAL","AVANCES","RETENCION","GIRO_EXTRA","PREMIO_AVANCE" ]
     */
    this.reels = new RFWSReels();

    /*
     * this.winPrizesArray = [];      // See definition bellow
     * this.extendedWildArray = [];   // See definition bellow
     */

    /*
     * winPrizesArray server example:
     * [
     * {
     * "line": [ [ 1, 3 ], [ 2, 2 ], [ 3, 1 ], [ 4, 2 ], [ 5, 3 ] ],
     * "strIco": "BB", // can be an array of icos: ["BB", "CC"]
     * "times": 2,
     * "value": 2,
     * "numLineWinner": 4
     * },
     * ...
     * ]
     *
     * extendedWildArray server example:
     * [
     * {
     * "numReel": 1,
     * "idExtendedWild": "EW"
     * },
     * ...
     * ]
     *
     */

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.strawberrystate.js
//------------------------------------------------------

/**
 * @version 0.0.2
 */
// eslint-disable-next-line
function RFWSStrawberrysState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic types
        this.prize = Number( _.wsGet( json, "prize", this.prize ) );
        this.prizesArray = _.wsGet( json, "prizesArray", this.prizesArray );
        this.selectedArray = _.wsGet( json, "selectedArray", this.selectedArray );
        this.remainingOptions = Number( _.wsGet( json, "remainingOptions", this.remainingOptions ) );
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.threeoptions.js
//------------------------------------------------------

/**
 * @version 0.0.2
 */
// eslint-disable-next-line
function RFWSThreeOptionsState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic types
        this.prize = Number( _.wsGet( json, "prize", this.prize ) );
        this.prizesArray = _.wsGet( json, "prizesArray", this.prizesArray );
        this.remainingOptions = Number( _.wsGet( json, "remainingOptions", this.remainingOptions ) );
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.type.treasurestate.js
//------------------------------------------------------

/**
 * @version 0.0.2
 */
// eslint-disable-next-line
function RFWSTreasureState( jsonResponse ) {
    "use strict";

    this.fromJson = function( json ) {

        // basic types
        this.prize = Number( _.wsGet( json, "prize", this.prize ) );
        this.prizesArray = _.wsGet( json, "prizesArray", this.prizesArray );
        this.selectedIdxPrizesArray = _.wsGet( json, "selectedIdxPrizesArray", this.selectedIdxPrizesArray );
        this.selectedPrizesArray = _.wsGet( json, "selectedPrizesArray", this.selectedPrizesArray );
    };

    // -------------------------------------------------- CONSTRUCTOR ---

    if( _.isObject( jsonResponse ) ) { this.fromJson( jsonResponse ); }
}

//------------------------------------------------------
// Source: src/engine/webservice/rf.webservice.watchdog.js
//------------------------------------------------------

/* jshint -W087 */ // Ignore debugger
/**
 * Módulo que realizará pings usando el wrapper de conexión indicado en la inicialización y según las opciones
 * establecidas. Se pueden especificar callbacks cuando se produzcan conexiones y desconexiones para poder
 * actuar en consecuencia.
 *
 * @requires rf.webservice.*
 * @version 0.0.2
 * CAMBIOS
 * 0.0.2 - Optimizados los accesos por lodash
 */
// eslint-disable-next-line
function RFWSWatchdog() {
    "use strict";

    /**
     * Initialize RFWatchdog module
     */
    this.initialize = function( initParams ) {
        if( !m_initialized ) {
            m_initialized = true;
            m_wrapper = _.get( initParams, [ "webServiceWrapper" ], m_wrapper );

            m_watchdog.setOptions( {
                watchFunction   : m_wrapper.sendPing,
                onOkCallback    : onOkCallback,
                onErrorCallback : onErrorCallback
            } );
        }
    };

    /**
     * Options to this module
     */
    var m_options = {
        msToPool             : 1000,
        maxAttempts          : 5,
        onDisconnectCallback : null,
        onConnectCallback    : null
    };
    this.setOptions = function( oOptions ) {
        m_options.maxAttempts          = _.get( oOptions, [ "maxAttempts" ],          m_options.maxAttempts );
        m_options.msToPool             = _.get( oOptions, [ "msToPool" ],             m_options.msToPool );
        m_options.onDisconnectCallback = _.get( oOptions, [ "onDisconnectCallback" ], m_options.onDisconnectCallback );
        m_options.onConnectCallback    = _.get( oOptions, [ "onConnectCallback" ],    m_options.onConnectCallback );

        // fordward the options to the watchdog
        m_watchdog.setOptions( {
            maxAttempts : m_options.maxAttempts,
            msToPool    : m_options.msToPool
        } );
    };

    /**
     * Start pooling with "ping" method for the setted wrapper
     */
    this.start = function() {
        if( m_initialized ) {
            m_watchdog.start();
        }
    };

    /**
     * Stop wathdog pooling
     */
    this.stop = function() {
        if( m_initialized ) {
            m_watchdog.stop();
            m_connectionState = eConnectionTypes.UNKNOWN;
        }
    };

    /**
     * is pooling?
     */
    this.isPooling = function() {
        return m_watchdog.isPooling();
    };

    /**
     * Returns the last state of connection
     */
    this.isConnected = function() {
        return m_connectionState === eConnectionTypes.CONNECTED;
    };

    /**
     * Indicates what is the connection state of the module
     */
    this.getConnectionState = function() {
        return m_connectionState;
    };

    var onOkCallback = function() {
        console.info( "RFWSWatchdog: Connected" );
        m_connectionState = eConnectionTypes.CONNECTED;
        if( _.isFunction( m_options.onConnectCallback ) ) {
            m_options.onConnectCallback();
        }
    };

    var onErrorCallback = function() {
        console.warn( "RFWSWatchdog: Disconnected" );
        m_connectionState = eConnectionTypes.DISCONNECTED;
        if( _.isFunction( m_options.onDisconnectCallback ) ) {
            m_options.onDisconnectCallback();
        }
    };

    var m_initialized = false;
    var m_connectionState = eConnectionTypes.UNKNOWN;
    var m_wrapper = null;
    var m_watchdog = new RFWatchdog();
}


//------------------------------------------------------
// Source: src/engine/webservice/utils/rf.webservice.utils.js
//------------------------------------------------------

/**
 * @version 0.0.8
 * CAMBIOS:
 * 0.0.7: Se permite la construción de un estado, aunque no esté definido, devolviendo el mismo valor del JSON
 * 0.0.8: Siempre realiza el chequeo de las respuestas en checkWSResponse, si se solicita.
 */

/**
 * Para poder acceder a los nodos parseados por xmlToJson (valor "#text") o directamente desde un json
 * @param {object} json Json parseado desde xmlToJson ( accede al valor "#text" ) o un json normal.
 * @param {string|array} properties Propiedad o propiedades a los que acceder al json
 * @defaultValue {any} Valor por defecto, que se devolverá en caso de que no se encuentre la ruta en el json
 */
_.wsGet = function( json, properties, defaultValue ) {
    "use strict";
    // if "#text" doesn't exist, but "@attributes" exists, return the default value, because the node is empty
    return _.get( json, _.concat( properties, "#text" ), _.has( json, "@attributes" ) ? defaultValue : _.get( json, _.concat( properties ), defaultValue ) );
};

/**
 * Devuelve el JSON parseado. Si no es un JSON stringificado y es un objeto, lo devuelve directamente.
 * @param {object} json Json parseado desde xmlToJson ( accede al valor "#text" ) o un json normal.
 * @param {string|array} properties Propiedad o propiedades a los que acceder al json
 * @defaultValue {any} Valor por defecto, que se devolverá en caso de que no se encuentre la ruta en el json
 */
_.wsGetParsedJSON = function( json, properties, defaultValue ) {
    "use strict";
    var getValue = _.wsGet( json, properties, null );
    if( _.isObject( getValue ) ) {
        return getValue;
    }
    else if( _.isString( getValue ) ) {
        return JSON.parse( getValue );
    }

    return defaultValue;
};

/**
 * Comprueba si existen valores numéricos inválidos (NaN) o no definidos en el objeto pasado.
 * @param {object} obj Objeto a chequear
 */
// eslint-disable-next-line
checkWSResponse = function( object ) {
    "use strict";
    function checkWSResponseInternal ( obj, originalObj, strObject ) {
        _.forEach( obj, function( value, key )  {
            if( value === undefined || _.isNaN( value ) ) {
                console.warn( ( _.isEmpty( strObject ) ? key
                    : ( strObject + "." + key ) ) + " : Invalid value or not setted. Checking response ", originalObj );
            }
            else if( _.isObject( value ) ) {
                checkWSResponseInternal( value, originalObj, _.isEmpty( strObject ) ? key : ( strObject + "." + key ) );
            }
        } );
    }

    if( object ) {
        checkWSResponseInternal( object, object, "" );
    }
};

// eslint-disable-next-line
var RFWebServiceUtil = {

    /**
     * Returns a new object with the correct response
     * @param {RFWSGameState} rfwsGameState
     */
    createRFWSResponse : function( rfwsGameState ) {
        "use strict";

        var wsResponse;
        if( rfwsGameState instanceof RFWSGameState ) {

            var commandCode = _.head( _.filter( GS_COMMAND_CODES, function( o ) { return o.response === rfwsGameState.responseType; } ) );

            if( commandCode ) {
                wsResponse = new commandCode.RFWSClass( rfwsGameState.play );
            }
            else {
                console.warn( "No rfwsGameState.responseType defined", rfwsGameState.responseType );
            }

        }
        else {
            console.warn( "createRFWSResponse: rfwsGameState is not a RFWSGameState", rfwsGameState );
        }
        return wsResponse;
    },

    /**
     * Returns the class of the state
     * @param {string} gameStateName
     * @param {object} gameStateJSON
     */
    /* eslint-disable-next-line complexity */
    createRFWSGameState: function( gameStateName, gameStateJSON ) {
        "use strict";

        var wsGameState;
        if( _.isString( gameStateName ) && _.isObject( gameStateJSON ) ) {
            switch( gameStateName ) {
                case eServerSceneId.BASE_GAME:
                case eServerSceneId.UPPER_GAME:
                    wsGameState = new RFWSSpinState( gameStateJSON );
                    break;
                case eServerSceneId.CARD_SHARP:
                    wsGameState = new RFWSCardSharpState( gameStateJSON );
                    break;
                case eServerSceneId.RAISE_OR_BONUS:
                    wsGameState = new RFWSRaiseOrBonusState( gameStateJSON );
                    break;
                case eServerSceneId.STRAWBERRYS:
                    wsGameState = new RFWSStrawberrysState( gameStateJSON );
                    break;
                case eServerSceneId.THREE_OPTIONS:
                    wsGameState = new RFWSThreeOptionsState( gameStateJSON );
                    break;
                case eServerSceneId.FINDING_PRIZE:
                    wsGameState = new RFWSFindingPrizeState( gameStateJSON );
                    break;
                case eServerSceneId.TREASURE:
                    wsGameState = new RFWSTreasureState( gameStateJSON );
                    break;
                default:
                    console.warn( "getRFWSGameStateClass: gameStateName not controlled '" + gameStateName + "'" );
                    wsGameState = gameStateJSON;
                    break;
            }
        }
        else {
            console.warn( "getRFWSGameStateClass: invalid arguments (gameStateName : string, gameStateJSON : object)", gameStateName, gameStateJSON );
        }

        return wsGameState;
    }
    /* eslint-enable-next-line complexity */

};


//------------------------------------------------------
// Source: src/engine/websocket/rf.websocket.wrapper.js
//------------------------------------------------------

/**
 * Wrapper de comunicación por websocket
 *
 * @version 0.1.0
 *
 * @requires rf.websocket.js
 *
 * CAMBIOS:
 * 0.1.0 - Versión inicial.
 */


// eslint-disable-next-line
var RFWebSocketWrapper = function() {

    var m_options = {
        debug: true,
        defaultSendDestination: "",
        defaultSendHeaders: {}
    };
    this.setOptions = function( oOptions ) {
        _.forEach( oOptions, function( value, keyOption ) {
            self.setOption( keyOption, value );
        } );
        return this;
    };
    this.setOption = function( keyOption, value ) {
        if( _.has( m_options, keyOption ) ) {
            _.set( m_options, keyOption, value );
        }
        propagueOptions();
        return this;
    };
    this.getOptions = function() {
        return _.clone( m_options );
    };
    this.getOption = function( keyOption ) {
        return _.get( m_options, keyOption );
    };
    var propagueOptions = function() {
        m_webSocketModule.setOptions( {
            debug: m_options.debug,
            verbose: false
        } );
    };


    this.connect = function( serverURL, headers, connectCallback, errorCallback ) {
        return m_webSocketModule.connect( serverURL, headers, connectCallback, errorCallback );
    };
    this.disconnect = function( disconnectCallback ) {
        return m_webSocketModule.disconnect( disconnectCallback );
    };

    // subscribe( callback, headers ) => subscribe to defaultDestination
    // subscribe( destination, callback, headers )
    this.subscribe = function() {
        if( arguments.length >= 1 ) {
            var callback, destination, headers;
            switch( arguments.length ) {
                case 1:
                case 2:
                    destination = m_options.defaultSendDestination;
                    callback = arguments[ 0 ];
                    headers = arguments[ 1 ];
                    break;
                case 3: // eslint-disable-line no-magic-numbers
                default:
                    destination = arguments[ 0 ];
                    callback = arguments[ 1 ];
                    headers = arguments[ 2 ];
                    break;
            }
            return m_webSocketModule.subscribe( destination, callback, headers );
        }

        console.error( "WSWrapper: Invalid number of arguments" );
        return undefined;
    };
    this.unsubscribe = function( subscription ) {
        return m_webSocketModule.unsubscribe( subscription );
    };
    this.unsubscribeAll = function( subscriptions ) {
        return m_webSocketModule.unsubscribeAll( subscriptions );
    };
    // send( ); => send empty data to default destination with default headers
    // send( data );
    // send( headers, data );
    // send( destination, headers, data );
    this.send = function() {

        var data, destination, headers;
        switch( arguments.length ) {
            case 0:
                destination = m_options.defaultSendDestination;
                headers = _.clone( m_options.defaultSendHeaders );
                break;
            case 1:
                destination = m_options.defaultSendDestination;
                headers = _.clone( m_options.defaultSendHeaders );
                data = arguments[ 0 ];
                break;
            case 2:
                destination = _.clone( m_options.defaultSendDestination );
                headers = arguments[ 0 ];
                data = arguments[ 1 ];
                break;
            case 3: // eslint-disable-line no-magic-numbers
            default:
                destination = arguments[ 0 ];
                headers = arguments[ 1 ];
                data = arguments[ 2 ];
                break;
        }

        return m_webSocketModule.send( destination, headers, data );
    };
    this.isConnected = function() {
        return m_webSocketModule.isConnected();
    };

    var self = this;
    var m_webSocketModule = new RFWebSocketModule();
};


//------------------------------------------------------
// Source: src/engine/retailNotificationWrapper/rf.retailNotificationWrapper.js
//------------------------------------------------------

/**
 * Wrapper de notificaciones para retail.
 * Realiza envíos HTTP tanto a SAS como a homologación.
 * @version 1.0.1
 * 1.0.0 - Elimina la petición de inicio de rodillos porque no se usa.
 */
// eslint-disable-next-line no-unused-vars
function RFRetailNotificationWrapper() {
    "use strict";

    // Tabla de peticiones en las que se indica la URL a la que atacar y los parámetros necesarios con
    // sus valores por defecto que se enviarán de no pasarse en la petición
    var REQUESTS = {
        HOMOLOGATION: {
            GAME:    { URL: "/Homologation/Game/",   PARAMS: { status: undefined } }, // [ "open" | "close" ]
            ROUND:   { URL: "/Homologation/Round/",  PARAMS: { round: undefined, amount: 0 } },
            MAX_BET: { URL: "/Homologation/MaxBet/", PARAMS: { maxBetReach: false } }
        },
        SAS: {
            WIN: { URL: "/SlotAccountingSystem/Win/", PARAMS: { winAmount: 0, winUid: undefined } },
            BET: { URL: "/SlotAccountingSystem/Bet/", PARAMS: { betAmount: 0, betUid: undefined } }
        }
    };
    Object.freeze( REQUESTS );

    /**
     * Inicializa el módulo
     * @params {object} data Objeto con datos de inicialización para el módulo RFWebServiceModule.
     */
    this.initialize = function( data ) {

        m_http = new RFWebServiceModule();

        var DEFAULT_TIMEOUT = 10000;

        var params = _.clone( data );
        params.name = "RFRetailNotificationWrapper";
        params.type = "POST";
        params.contentType = "application/json";
        params.timeout = _.get( data, [ "timeout" ], DEFAULT_TIMEOUT );

        m_http.initialize( params );
    };

    /**
     * Devuelve el objeto de estadísticas de envíos que maneja el módulo.
     */
    this.getStats = function() {
        return m_stats;
    };

    this.setHomologationEnabled = function( bValue ) {
        m_homologationEnabled = _.isBoolean( bValue ) ? bValue : m_homologationEnabled;
    };

    this.getHomologationEnabled = function() {
        return m_homologationEnabled;
    };

    // #region HOMOLOGATION =====================================================

    /**
     * Registra el inicio del juego
     */
    this.sendStartGameRequest = function( params, onSuccess, onError ) {
        if( !m_homologationEnabled ) { return; }
        var requestParams = makeParamsFromRequest( REQUESTS.HOMOLOGATION.GAME, params );
        sendRequestInternal( requestParams, onSuccess, onError );
    };

    /**
     * Registra un final de round hacia HOMOLOGACIÓN.
     */
    this.sendHomologationRoundRequest = function( params, onSuccess, onError ) {
        if( !m_homologationEnabled ) { return; }
        var requestParams = makeParamsFromRequest( REQUESTS.HOMOLOGATION.ROUND, params );
        sendRequestInternal( requestParams, onSuccess, onError );
    };

    /**
     * Registra una apuesta máxima hacia HOMOLOGACIÓN.
     */
    this.sendMaxBetReachedRequest = function( params, onSuccess, onError ) {
        if( !m_homologationEnabled ) { return; }
        var requestParams = makeParamsFromRequest( REQUESTS.HOMOLOGATION.MAX_BET, params );
        sendRequestInternal( requestParams, onSuccess, onError );
    };

    // #endregion HOMOLOGATION =================================================

    // #region SAS =====================================================

    /**
     * Registra una petición de WIN hacia el SAS
     */
    this.sendSASWinRequest = function( args, onSuccess, onError ) {
        var params = makeParamsFromRequest( REQUESTS.SAS.WIN, args );
        var fnName = "sendSASWinRequest";

        var _onSuccess = function( response ) {
            console.log( fnName, "OK", args );
            m_stats.SAS.ok.win++;
            getAFunction( onSuccess )( response );
        };
        var _onError = function( response ) {
            console.error( fnName, "ERROR", args );
            m_stats.SAS.error.win++;
            getAFunction( onError )( response );
        };

        console.log( fnName, "SENT", params.requestParams );

        var retry = makeSASRetryRequest( params, _onSuccess, _onError );
        retry.execute();
        m_stats.SAS.sent.win++;
    };

    /**
     * Registra una petición de BET hacia el SAS
     */
    this.sendSASBetRequest = function( args, onSuccess, onError ) {
        var params = makeParamsFromRequest( REQUESTS.SAS.BET, args );
        var fnName = "sendSASBetRequest";

        var _onSuccess = function( response ) {
            console.log( fnName, "OK", args );
            m_stats.SAS.ok.bet++;
            getAFunction( onSuccess )( response );
        };
        var _onError = function( response ) {
            console.error( fnName, "ERROR", args );
            m_stats.SAS.error.bet++;
            getAFunction( onError )( response );
        };

        console.log( fnName, "SENT", params.requestParams );

        var retry = makeSASRetryRequest( params, _onSuccess, _onError );
        retry.execute();
        m_stats.SAS.sent.bet++;
    };

    /**
     * Función que, ante una respuesta del módulo RFWebServiceModule, indica si se devuelve un código 201 (created)
     */
    var isCreated = function() {
        var responseType = arguments[ 1 ];
        if( responseType === "error" ) {
            return false;
        }
        var CREATED = 201;
        var httpResponse = arguments[ 2 ];
        return _.get( httpResponse, [ "status" ] ) === CREATED;
    };

    /**
     * Función que, ante una respuesta del módulo RFWebServiceModule, indica si no se devuelve un código 201 (created)
     */
    var isNotCreated = function() {
        return !isCreated.apply( null, arguments );
    };

    var makeSASRetryRequest = function( params, onSuccess, onError ) {
        var retry = makeRetryRequest( params, onSuccess, onError );
        retry.hasToRetryFunction = isNotCreated;
        return retry;
    };

    // #endregion SAS =================================================

    /**
     * Crea una instancia de un reintentador de Jobs y configura un Job que realiza una petición http.
     * @returns {RFRetryJob} Una instancia de un reintentador de jobs
     */
    var makeRetryRequest = function( params, onSuccess, onError ) {

        // create the 'data' parameter to use with http module.
        params.data = JSON.stringify( _.get( params, [ "requestParams" ] ) );

        var rfJob = new RFSendRequestJob();
        rfJob.http = m_http;
        rfJob.request = params;

        var retry = new RFRetryJob( rfJob );
        retry.onSuccess = onSuccess;
        retry.onError = onError;
        return retry;
    };

    /**
     * @param {REQUESTS.HOMOLOGATION.* || REQUESTS.SAS.* } requestDefinition
     * @param {Object} params
     */
    var makeParamsFromRequest = function( requestDefinition, params ) {

        var url = m_http.getURL() + requestDefinition.URL;
        var requestParams = _.cloneDeep( requestDefinition.PARAMS );

        assertAllKeys( params, _.keys( requestParams ) );

        // update the requestParams
        _.forEach( requestParams, function( value, key ) {
            if( _.has( params, key ) ) {
                requestParams[ key ] = params[ key ];
            }
        } );

        return {
            url: url,
            requestParams: requestParams
        };
    };

    var sendRequestInternal = function( params, onSuccess, onError ) {
        var sendParams = {
            url: params.url,
            data: JSON.stringify( params.requestParams )
        };
        m_http.send( sendParams, onSuccess, onError );
    };

    // RFWebServiceModule internal module
    var m_http;
    var m_homologationEnabled = true;

    // Statistics
    var m_stats = {
        SAS: {
            sent: {
                bet: 0,
                win: 0,
                get total() { return this.bet + this.win; }
            },
            ok: {
                bet: 0,
                win: 0,
                get total() { return this.bet + this.win; }
            },
            error: {
                bet: 0,
                win: 0,
                get total() { return this.bet + this.win; }
            }
        }
    };
}

//------------------------------------------------------
// Source: src/engine/slo2engine/rf.slo2engine.js
//------------------------------------------------------

/**
 * Slo2engine Module
 */
// eslint-disable-next-line
var Slo2engine = Slo2engine || {};

Slo2engine.initialized = false;

Slo2engine.initialize = function( game ) {
    "use strict";
    if( !Slo2engine.initialized ) {
        if( isValidGame( game ) ) {
            if( !game.behaviorPlugin ) {
                game.plugins.add( Phaser.Plugin.Behavior );
            }
            Slo2engine.initialized = true;
        }
    }
};

//------------------------------------------------------
// Source: src/sbg/game/defs/rf.defs.js
//------------------------------------------------------

/**
 * @version 0.4.0
 * CAMBIOS:
 * 0.3.0: se añade la acción de salida al lobby
 * 0.4.0: se añade la acción de subir volumen
 * 0.5.0: añadido BET_MAX, BONUS y BIG_PRIZE como definiciones de sonido
 */
var eSceneNames = {
    BOOT: "boot",
    PRELOAD: "preload",
    RECONNECTION: "reconnection",
    BASEGAME: "basegame",
    FREEGAME: "freegame",
    HELP: "help",
    DOUBLE: "double",
    PICK: "pick",
    DUNGEON_DOORS: "dungeon_doors",
    DUNGEON_STAIRS: "dungeon_stairs",
    DUNGEON_KING: "dungeon_king",
    CUSTOM_: "CUSTOM_" // to build custom scenes
};
Object.freeze( eSceneNames );

// eslint-disable-next-line no-unused-vars
var eCustomSceneNames = {
    CUSTOM_GAME_1: "CUSTOM_GAME_1 (to be override)",
    CUSTOM_GAME_2: "CUSTOM_GAME_2 (to be override)",
    CUSTOM_GAME_3: "CUSTOM_GAME_3 (to be override)"
};

var eActionNames = {
    goHelp: "GO_HELP",
    goDouble: "GO_DOUBLE",
    lineUp: "LINE_UP",
    changeDenom: "CHANGE_DENOM",
    betMax: "BET_MAX",
    betPerLineUp: "BET_PER_LINE_UP",
    spin: "SPIN",
    autoGame: "AUTO_GAME",
    autoX: "AUTO_X",
    exitHelp: "EXIT_HELP",
    nextPayHelp: "NEXT_PAY_HELP",
    prevPayHelp: "PREV_PAY_HELP",
    selectcardBack: "CARD_BACK",
    takeWin: "TAKE_WIN",
    pickSelection: "PICK_SELECTION",
    exitToLobby: "EXIT_TO_LOBBY",
    volumeUp: "VOLUME_UP"
};
Object.freeze( eActionNames );

var eLabelsNames = {
    msgMain: "MAIN",
    msgSecundary: "SECUNDARY",
    msgWin: "WIN",
    msgCredits: "CREDITS",
    msgCreditsPrizeUp: "CREDITSPRIZEUP",
    msgBetPerLine: "BET_PER_LINE",
    msgTotalBet: "TOTAL_BET",
    msgLine: "LINE",
    symbolPrizeBaseGame: "SYMBOL_PRIZE_BASEGAME",
    symbolPrizeFreeGame: "SYMBOL_PRIZE_FREEGAME",
    msgCurrentPrize: "CURRENT_PRIZE",
    msgNextPrize: "NEXT_PRIZE",
    msgPrize: "PRIZE",
    msgBonds: "BONDS",
    msgMult: "MULT",
    msgWinFreegame: "WIN_FREE",
    msgDenom: "DENOM",
    version: "VERSION"
};
Object.freeze( eLabelsNames );

var eSoundTypes = {
    CANT_SPIN: "CANT_SPIN",
    INSUFFICIENT_FOUNDS: "INSUFFICIENT_FOUNDS",
    LIMIT_EXCEEDED: "LIMIT_EXCEEDED",
    LINE_UP: "LINE_UP",
    BET_UP: "BET_UP",
    BET_MAX: "BET_MAX",
    DENOM_UP: "DENOM_UP",
    COUNTER: "COUNTER",
    COUNTERWINUP: "COUNTERWINUP",
    BKG_BASEGAME: "BKG_BASEGAME",
    DOUBLE_WIN: "DOUBLE_WIN",
    DOUBLE_LOSE: "DOUBLE_LOSE",
    DOUBLE_NOTHING: "DOUBLE_NOTHING",
    HELP: "HELP",
    PICK: "PICK",
    END_PICK: "END_PICK",
    FREEGAME: "FREEGAME",
    CONGRAT: "CONGRAT",
    BONUS: "BONUS",
    BIG_PRIZE: "BIG_PRIZE"
};
Object.freeze( eSoundTypes );

var eEnvironments = {
    PRODUCTION: "PRODUCTION",
    TEST: "TEST",
    DEVELOPMENT: "DEVELOPMENT",
    DEMO: "DEMO",
    HOMOLOGATION: "HOMOLOGATION"
};
Object.freeze( eEnvironments );

var eUseLocalBets = {
    AUTO: "AUTO",
    TRUE: "TRUE",
    FALSE: "FALSE"
};
Object.freeze( eUseLocalBets );

var eScreenId = {
    BASE: "BASE",
    UPPER: "UPPER"
};
Object.freeze( eScreenId );

//------------------------------------------------------
// Source: src/sbg/game/appdata/rf.gameconfig.js
//------------------------------------------------------

/**
 * @version 0.15.0
 * CAMBIOS:
 * 0.5.2 - Se añade reconnection.enable y reconnection.sceneToLoadIfReconnectionIsDisable
 * 0.5.3 - Se añade la gestión de moneda/créditos en game.displayCredits (true|false)
 * 0.5.4 - Se añaden los keybindings asociados al juego
 * 0.5.5 - Se cambia el nombre de 'forceReels' a 'forcedGames' en los keybindings. Se añade 'allowForceReelsUtility'
 * 0.5.6 - Se añaden nuevos códigos de color
 * 0.5.7 - Muestra premios de línea por configuración 'game.displayPrizeLines'
 * 0.6.0 - Se añade 'game.waitUntilDisplayAllPrizeLines' para no permitir jugar hasta que no se muestren todos los premios.
 * 0.7.0 - Se añade msMinToExitFromFreeGamesWithPrize y msMinToExitFromFreeGamesWithNoPrize. Se elimina msMinToExitFromFreeGames.
 *         Modifica el valor por defecto de msMinToEnterInPick a 2000
 * 0.7.1 - Se añade el color del separador para las FSM
 * 0.8.0 - Modificación de los valores por defecto de parada de rodillos y tiempo entre parada de rodillos.
 * 0.9.0 - Se añade el valor mínimo de apuesta.
 *       - Se amplía el timeout de respuesta del GameServer a 20 segundos.
 * 0.10.0 - Se añade una propiedad de configuración para permitir las apuestas en local.
 * 0.11.0 - Se añade la configuración para las SBG
 * 0.12.0 - Se añade configuración para el volumen.
 * 0.13.0 - Se añaden los keyBindings de cashout, exit y autoGame.
 * 0.14.0 - Se añaden opciones para permitir habilitar/deshabiliar el SAS, poner en debug los webSckets y opciones de Big Prizes.
 * 0.15.0 - Se añade configuración para autoPing
 * 0.16.0 - Se añade screenId a la configuración de SBG. Cambio de literales a las referencias de los enumerados.
 */

/**
 * Console color templates for better debugging.
 * Copy the default scheme and make your own custom template, to use in RFGameConfig.debug.console.colorTemplate
 */

// eslint-disable-next-line no-implicit-globals
var RFConsoleColorTemplates = {
    default: {
        initializingGame: "color:white; background:red",
        enterScene: "background:yellow;",
        sendLabel: "font-weight:bold; color:green;",
        sendMessage: "color:lightgray;",
        receiveLabel: "font-weight:bold; color:blue;",
        receiveMessage: "color:LightSkyBlue;",
        fsmFromState: "color:blue;",
        fsmSeparator: "color:black;",
        fsmToState: "color:white; background:green; font-weight:bold;",
        fsmDescription: "color:orange;",
        serverVersion: "color:white; background:green"
    },
    blackTheme: {
        initializingGame: "color:white; background:red",
        enterScene: "background:yellow; color:black",
        sendLabel: "font-weight:bold; color:lightgreen;",
        sendMessage: "color:DimGrey;",
        receiveLabel: "font-weight:bold; color:cyan;",
        receiveMessage: "color:LightSkyBlue;",
        fsmFromState: "color:cyan;",
        fsmSeparator: "color:gray;",
        fsmToState: "color:white; background:green; font-weight:bold;",
        fsmDescription: "color:orange;",
        serverVersion: "color:white; background:green"
    }
};

// eslint-disable-next-line no-implicit-globals, no-unused-vars
var RFGameConfig = {

    gameId: "gameId: SLO2SBG (to be overwriten)",
    gameName: "gameName: SLO2SBG (to be overwriten)",
    version: "version: SLO2SBG (to be overwriten)",

    screen: {
        width: 0,
        height: 0
    },
    game: {
        assets: {
            desktop: {
                loadAllScenesAtPreload: true,
                scenesToLoadAtPreloadArray: [], // eSceneNames.HELP, eSceneNames.DOUBLE ...
                msToWaitNextAsyncLoad: 1000
            },
            default: {
                loadAllScenesAtPreload: false,
                scenesToLoadAtPreloadArray: [], // eSceneNames.HELP, eSceneNames.DOUBLE ...
                msToWaitNextAsyncLoad: 1000
            }
        },
        reports: {
            history: {
                url: "history.html"
            }
        },
        keyBindings: {
            // basegame
            forcedGames: Phaser.Keyboard.SPACEBAR, // also freegame
            spin: Phaser.Keyboard.ENTER, // also freegame
            maxBet: Phaser.Keyboard.M,
            goHelp: Phaser.Keyboard.ESC,
            betPerLinePlus1: [ Phaser.Keyboard.B, Phaser.Keyboard.UP ],
            betPerLineMinus1: Phaser.Keyboard.DOWN,
            linesPlus1: [ Phaser.Keyboard.L, Phaser.Keyboard.RIGHT ],
            linesMinus1: Phaser.Keyboard.LEFT,
            showHistory: Phaser.Keyboard.H,
            cashOut: Phaser.Keyboard.C,
            exit: Phaser.Keyboard.X,
            autoGame: Phaser.Keyboard.A,
            // help
            exitHelp: Phaser.Keyboard.ESC,
            previousPage: Phaser.Keyboard.LEFT,
            nextPage: Phaser.Keyboard.RIGHT
        },
        masterVolumeSteps: [ 1, 0.6, 0.3 ], // eslint-disable-line
        msMinSpinTime: 2000,
        msStopReel: 300,
        yBounceOffset: 20,
        msBetweenStops: 120,
        msMinToStartNewGameInAutoGameWithNoPrize: 1000,
        msMinToStartNewGameInAutoGameWithPrize: 1000,
        msMinToEnterInFreeGames: 2000,
        msMinToStartFirstSpinFreeGames: 2000,
        msMinToExitFromFreeGamesWithPrize: 4000,
        msMinToExitFromFreeGamesWithNoPrize: 1000,
        msMinToEnterInPick: 2000,
        msTweenCounterCredits: 500,
        msTweenCounterWin: 500,
        msTweenCounterPrizeLines: 500,
        canBeForced: true,
        startExtendedWildsAtSameTime: true,
        useTransitions: true,
        spinWhenTakingWinFromSpinButton: true,
        useKeyBindings: true,
        forceAllowHistoryKeyBinding: true,
        displayCredits: false,
        allowForceReelsUtility: true,
        displayPrizeLines: false,
        waitUntilDisplayAllPrizeLines: false,
        allowPressingSpinButtonToGoToAutoGame: false,
        msPressingSpinButtonToGoToAutoGame: 500,
        allowSkittleSetLines: true,
        minBet: 0,
        useLocalBets: eUseLocalBets.AUTO, // Possible values: TRUE, FALSE, AUTO (if server sends the information we will use local bets)
        autoPlayWithCash: true,
        showBigPrizesBeforeCarrousel: false,
        canBigPrizeBeSkipped: false,

        /**
         * true -> will maintain the shown symbols on FREE_GAME when transitioning from BASE_GAME and viceversa.
         * false -> will not maintain these symbols.
         */
        maintainReelSymbolsAfterTransition: false,

        upper: {
            transitScenes: {
                // "baseSceneKey" : "upperSceneKey"
            }
        }
    },
    reconnection: {
        enable: true,
        sceneToLoadIfReconnectionIsDisable: eSceneNames.BASE_GAME,
        maxAttemptsToReconnect: 3,
        msTotalToConsumeAllAttempts: 20000
    },
    debug: {
        logLoaddedAssets: true,
        checkWSResponses: false, // keep informed ( via warn in console ) about empty or invalid responses from the server
        console: {
            colorTemplate: RFConsoleColorTemplates.blackTheme
        }
    },
    gameServer: {
        url: "gameServer.url (to be overwriten)",
        timeout: 20000,
        supportCors: true,
        isForFun: false,
        playerAuthenticationToken: QueryParameters.token,
        gameUid: "gameServer.gameUid: SLO2SBG (to be overwriten)",
        gameId: "gameServer.gameId: SLO2SBG (to be overwriten)",
        enableAutoPings: true,
        msBetweenAutoPings: 480000, // 8 minutes
        autoPingsMaxAttempts: 5
    },
    sbg: {
        msToWaitForTheGameConfiguration: 3000,
        enable: true,
        rest: "sbg.rest: SLO2SBG (to be overwriten)",
        restTimeout: 20000,
        socket: {
            server: "sbg.socket.server (to be overwriten)",
            subscription: "sbg.socket.subscription (to be overwriten)",
            debug: false
        },
        sas: {
            enable: true
        },
        screenId: eScreenId.BASE
    },
    environment: {
        id: eEnvironments.PRODUCTION
    }
};

//------------------------------------------------------
// Source: src/sbg/game/appdata/rf.globaldata.js
//------------------------------------------------------

/**
 * @version 0.5.0
 * CAMBIOS:
 * 0.1.2 - Se añade el valor actual de la denominación para poder mostrar los créditos.
 * 0.2.0 - Añade si el dispositivo es móvil o no.
 * 0.3.0 - Se añade configuración para las SBG, volumen y mantenimiento de apuesta entre cambio de juego.
 * 0.4.0 - Se añade el multiplicador "base" para las apuestas locales.
 * 0.5.0 - Se añade el mute del audio
 */

/* eslint-disable no-magic-numbers */
// eslint-disable-next-line no-implicit-globals, no-unused-vars
var RFGlobalData = {

    locale: {
        texts: {},
        lang: "es",
        country: "ES",
        currencyISO: "EUR",
        numberOfDecimals: 2,
        decimalSeparator: ".",
        thousandSeparator: ","
    },

    net: {
        session: "",
        wrapper: null
    },

    reconnection: {
        attemptsToReconnectLeft: _.get( RFGameConfig, [ "reconnection", "maxAttemptsToReconnect" ], 5 ),
        lastReconnectionAttempt: null
    },

    game: {
        lastScene: "",
        currentDenom: -1,

        // used for local bets
        useLocalBets: false,
        localBets: {
            currentLinesIdx: 0,
            currentDenomIdx: 0,
            currentBetPerLineIdx: 0,
            currentBase: 1,
            isDirty: true // needs update with serverData
        },

        lobbyReturnOption: "NONE",
        lobbyUrl: "",

        currentMasterVolumeStepIdx: 0
    },

    // received at the start from the SBG machine
    sbg: {
        enableToSendGameConfigurationToSocket: false,
        gameConfiguration: {
            mute: false,
            volume: 1,
            totalBet: 0
        }
    },

    device: {
        isMobile: isMobile()
    },

    reels: {
        "basegame": undefined,
        "freegame": undefined
    }

};
/* eslint-enable no-magic-numbers */

//------------------------------------------------------
// Source: src/sbg/game/scenes/rf.scene.base.js
//------------------------------------------------------

/* eslint-disable max-lines */
/* jshint -W087 */ // Ignore debugger

/**
 * Escena base de la que heredarán todas las escenas
 * @version CUSTOM SBG 0.14.0c
 * CAMBIOS:
 * 0.14.0c - Se envía el amount en el mensaje de final de ronda para homologación.
 *         - Se manda el cambio de escena si la pantalla es la inferior.
 * 0.13.0c - Permite la generación de premios cuando no están definidas las líneas.
 * 0.12.0c - Si se recibe del socket un GAME_CONFIGURATION con un PUSH, se setea el valor de mute indicado.
 *         - Se crea un método "sendExitToLobby" que envía un mensaje por websocket de salida al Lobby.
 *         - Se añaden nuevas señales: onGamePaused, onGameResumed, onBetChanged, onMuteAudio
 * 0.11.0c - SceneUtils::getPrizesArray obtiene ahora el símbolo correcto para reproducir el sonido.
 *           Si viene prizeSoundSymbolId se usa ese RFSymbol, si no, se usa el de strIco.
 * 0.10.0c - Se añade la apuesta base como multiplicador para las apuestas locales.
 * 0.9.0c - Nueva función "getPrizeHypeObject" para conseguir un objeto con el símbolo y el número
 *          de veces máximas que un símbolo PrizeHype va a aparecer en la siguiente tirada.
 * 0.11.0c - Se añade la función makePayTable (proviene del basegame - se generaliza)
 * --------------------------------------------------------
 * 0.0.22 - Coloca el mensaje de error de conexión centrado.
 * 0.0.23 - Elimina el console.log del histórico.
 * 0.0.24 - Evita realizar reconexión si estuviera deshabilitada desde el archivo de configuración.
 * 0.0.25 - Se añade updateGlobalData como utilidad de la escena. Se añade además un setMoney para la gestión de créditos y moneda.
 * 0.0.26 - Se añaden funciones de inicialización para skittles, líneas y creditLabels.
 * 0.0.27 - Se añade función para añadir keybindings
 * 0.1.0  - Se añade parte protegida para slo2game. Se exporta SceneUtils
 * 0.1.1  - Añade el idioma hacia el histórico.
 *        - Posibilidad de inicializar las labels con textos del idioma, referenciando su clave. Ejemplo "Inicializando {MACHINE_ID}".
 * 0.2.0  - Añade prize y accumulated prized a al getPrizesArray
 * 0.2.1  - Se cambia la llamada a setFormattedString
 * 0.2.2  - HotFix para tratar los mensajes de error. Se añade un texto para mostrar en el mensaje de error
 * 0.2.3  - Se deja el alpha de la pantalla de error a 0.7
 * 0.2.4  - Añade el offset de la cámara para posicionar los textos de error.
 * 0.3.0  - En el update de los datos globales, se tiene en cuenta si es una respuesta genérica.
 * 0.3.1  - Añade una función de callback en el método hideCovers
 * 0.4.0  - Añade el multiplicador al método getPrizesArray
 * 0.4.1  - Añade el objeto GameDefinition a la carga de la escena.
 * 0.5.0  - Se exporta la función "formatMoneyOrCredits" para que slo2engine pueda formatear los textos del GameDefinition.
 * 0.5.1  - Se añade un método de SceneUtils para sobrescribir la configuración.
 * 0.5.2  - Se añaden métodos en el SceneUtils para mostrar la versión y crear textos.
 * 0.5.3  - Se añaden dos funciones que controlan el cierre de la ventana.
 * 0.6.0  - Añade una función de carga de la escena que inicializa los labels, skittels...
 * 0.7.0  - Gestión de apuestas locales y utilidades para su manejo. Gestión de websocket por escena.
 * 0.8.0  - Lectura de la configuración de la SBG y mantenimiento de volumen y apuesta entre cambio de juego.
 */

/* global GameDefinition */
// eslint-disable-next-line no-implicit-globals, max-statements, no-unused-vars
var BaseScene = function( game, strSceneName ) {
    "use strict";

    var _protected = {};
    var self = this;

    var DEFAULTS = {
        MS_TO_WAIT_NEXT_ASYNC_LOAD: 100,
        MS_TO_HIDE_COVERS: 1000,
        MS_TO_HIDE_POSTER: 2000,
        MS_FADEIN_POSTER: 200,
        MS_FADEOUT_POSTER: 500,
        BLACK_SCREEN_POSTER_ALPHA: 0.6,
        FONT_SIZE: 20,
        ERROR_TITLE_FONT_SIZE: 20,
        DESCRIPTION_FONT_SIZE: 15,
        MS_TOTAL_TO_CONSUME_ALL_ATTEMPTS: 5000,
        MAX_ATTEMPTS_TO_RECONNECT: 5
    };
    Object.freeze( DEFAULTS );

    var CONST = {
        UNDEFINED_SCENE: "__undefinedScene"
    };
    Object.freeze( CONST );

    this.myScene = _.isString( strSceneName ) ? strSceneName : CONST.UNDEFINED_SCENE;

    /**
     * Preload de la clase Base
     * @param {object} oParams Parámetros del preload.
     * @param {boolean} oParams.loadAssets Indica si queremos cargar los assets de la escena o no
     * @param {boolean} oParams.showLoadingBar Indica si queremos mostrar la barra de carga.
     */
    this.preloadBase = function( oParams ) {

        checkMySceneKey();

        var enterSceneColor = _.get( RFGameConfig, [ "debug", "console", "colorTemplate", "enterScene" ], "background:yellow;" );
        console.log( "%c----------------------------------------------- PRELOADING " + self.myScene + " ---", enterSceneColor );

        var numAssetsToLoad = 0;

        // Carga de assets de la escena
        if( _.get( oParams, [ "loadAssets" ], true ) ) {
            numAssetsToLoad = self.loadAssets( _.get( GameDefinition, [ "scenes", self.myScene, "assets" ], null ), self.myScene );
        }

        if( _.get( oParams, [ "showLoadingBar" ], true ) ) {
            if( numAssetsToLoad > 0 ) {
                self._drawLoadingBar();
            }
        }
    };

    /**
     * Create de la clase Base
     * @param {object} oParams Parámetros del create.
     * @param {boolean} oParams.loadAssets Indica si queremos cargar los assets de la escena o no
     */
    this.createBase = function( oParams ) {

        var enterSceneColor = "background:lightyellow;";
        console.log( "%c----------------------------------------------- CREATING " + self.myScene + " ---", enterSceneColor );

        // Carga asíncrona de assets ( ha de estar en el create )
        if( _.get( oParams, [ "loadAssets" ], true ) ) {
            self.loadAssetsAsync( _.get( GameDefinition, [ "scenes", self.myScene, "assets" ], null ), self.myScene );
        }

        if( RFGlobalData.net.wrapper ) {
            RFGlobalData.net.wrapper.onNetworkMessageReceived.add( SceneUtils.onNetworkMessageReceived );
        }

        if( RFGlobalData.net.wsWrapper ) {
            m_websocketHandler = RFGlobalData.net.wsWrapper.subscribe( socketReception );
        }
    };

    /**
     * Create de la clase Base
     */
    this.shutdownBase = function( oParams ) {
        if( _.get( oParams, [ "stopAsyncLoad" ], true ) ) {
            m_rfLoadAssets.stopAsyncLoad();
        }

        if( RFGlobalData.net.wsWrapper ) {
            RFGlobalData.net.wsWrapper.unsubscribe( m_websocketHandler );
        }

        if( RFGlobalData.net.wrapper ) {
            RFGlobalData.net.wrapper.onNetworkMessageReceived.remove( SceneUtils.onNetworkMessageReceived );
        }

        self.onShutdown.dispatch();

        // Remove all the registered signals
        self.onShutdown.removeAll();
    };

    /**
     * Realiza la carga de assets pasada en el objeto. Debe de seguir la estructura del GameDefinition,
     * esto es: "common" e idiomas, y dentro de cada uno de ellos, los assets ( images, atlas... )
     * @param {object} oAssets
     * @param {string} strDescription Descripción para loggear en la carga
     */
    this.loadAssets = function( oAssets, strDescription ) {

        m_rfLoadAssets = self.getRFLoadAssets();
        m_rfLoadAssets.setOptions( {
            debug: _.get( RFGameConfig, [ "debug", "logLoaddedAssets" ], false ),
            msToWaitNextAsyncLoad: _.get( RFGameConfig,
                [ "game", "assets", ( self.game.device.desktop ? "desktop" : "default" ), "msToWaitNextAsyncLoad" ], DEFAULTS.MS_TO_WAIT_NEXT_ASYNC_LOAD )
        } );

        var strDesc = _.isString( strDescription ) ? " - " + strDescription : "";

        var numAssetsToLoad = 0;
        var userlang = RFGlobalData.locale.lang;
        if( !_.isEmpty( userlang ) ) {
            if( _.has( oAssets, userlang ) ) {
                numAssetsToLoad += m_rfLoadAssets.loadAssets( oAssets[ userlang ], userlang + strDesc );
            }
        }

        if( _.has( oAssets, "common" ) ) {
            numAssetsToLoad += m_rfLoadAssets.loadAssets( oAssets.common, "common" + strDesc );
        }

        return numAssetsToLoad;
    };

    /**
     * Realiza la carga de assets de forma asíncrona pasada en el objeto. Debe de seguir la estructura
     * del GameDefinition, esto es: "common" e idiomas, y dentro de cada uno de ellos, los assets ( images, atlas... )
     *
     * IMPORTANTE. Desde la escena solo se puede llamar en el create.
     *
     * @param {object} oAssets
     * @param {string} strDescription Descripción para loggear en la carga
     */
    this.loadAssetsAsync = function( oAssets, strDescription ) {

        m_rfLoadAssets = self.getRFLoadAssets();

        var strDesc = _.isString( strDescription ) ? " - " + strDescription : "";

        var userlang = RFGlobalData.locale.lang;
        if( !_.isEmpty( userlang ) ) {
            if( _.has( oAssets, userlang ) ) {
                m_rfLoadAssets.loadAssetsAsync( oAssets[ userlang ], userlang + strDesc );
            }
        }

        if( _.has( oAssets, "common" ) ) {
            m_rfLoadAssets.loadAssetsAsync( oAssets.common, "common" + strDesc );
        }
    };

    /**
     * Devuelve la instancia para cargar escenas
     */
    this.getRFLoadScenes = function() {
        return ( m_rfLoadScenes = ( m_rfLoadScenes || new RFLoadScenes( GameDefinition ) ) );
    };

    /**
     * Devuelve la instancia para cargar assets
     */
    this.getRFLoadAssets = function() {
        return ( m_rfLoadAssets = ( m_rfLoadAssets || new RFLoadAssets( m_game ) ) );
    };

    /**
     * Realiza la carga de la barra de progreso
     * @todo: Ver si poner el fondo o no.
     */
    this._drawLoadingBar = function() {

        var loadScenes = self.getRFLoadScenes();
        loadScenes.loadScene( eSceneNames.PRELOAD );

        var imagesMap = loadScenes.getPhaserImages();
        if( _.has( imagesMap, [ "loadingBar" ] ) ) {
            self.game.load.setPreloadSprite( imagesMap.loadingBar );
        }
        else {
            console.info( "No loading bar found in GameDefinition" );
        }

        // var imagesMap = this.getRFLoadScenes().getPhaserImages();
        // if( _.has( imagesMap, [ "loadingBar" ] ) ) {
        //     m_self.game.load.setPreloadSprite( imagesMap.loadingBar );
        // } else {
        //     console.info( "No loading bar found in GameDefinition" );
        // }

    };

    // Signals que se exportan hacia fuera. Si los punchs necesitan destruir/construir algo, se han de registrar a estas señales.
    this.onShutdown = new Phaser.Signal();

    // #region websockets
    var socketReception = function( data ) {
        var message = JSON.parse( data.body );

        switch( message.type ) {
            case "GAME_CONFIGURATION":
                if( _.get( message, [ "data", "method" ] ) === "PUSH" ) {
                    var mute = Boolean( _.get( message, [ "data", "content", "mute" ] ) );
                    SceneUtils.setMuteAudio( mute );
                }
                break;
            case "GAME_STOP":
                console.log( "GAME_STOP received" );
                SceneUtils.pauseGame();
                sendToSocket( { type: "GAME_STOP_RECEIVED" } );
                break;
            case "GAME_RESUME":
                console.log( "GAME_RESUME received" );
                SceneUtils.resumeGame();
                sendToSocket( { type: "GAME_RESUME_RECEIVED" } );
                break;
            default:
                break;
        }
    };

    var sendToSocket = function( data ) {
        if( RFGlobalData.net.wsWrapper ) {
            RFGlobalData.net.wsWrapper.send( data );
        }
    };
    // #endregion websockets

    // ==============================================================
    //  UTILS AND HELPERS SECTION
    // ==============================================================

    var SceneUtils = {

        /**
         * Devuelve un array filtrado con todos aquellos objectos de phaser, que
         * tengan la clave indicada en parameterKey que sea igual al valor a chequear en valueToCheck.
         * En caso de que no tenga valueToCheck, devuelve todos los objectos que contengan ese parámetro definido
         * @param {object} phaserMap Objecto de phaser
         * @param {string} parameterKey La clave dentro de "parameters"
         * @param {Array|any} valueToCheck El valor (o valores si es un array) con el/los que comparar.
         */
        filterByParameters: function( phaserMap, parameterKey, valueToCheck ) {
            if( valueToCheck === undefined ) {
                return _.filter( phaserMap, function( o ) {
                    return _.has( o, [ "parameters", parameterKey ] );
                } );
            }
            return _.filter( phaserMap, function( o ) {
                var value = _.get( o, [ "parameters", parameterKey ] );
                if( value !== undefined ) {
                    valueToCheck = ( _.isArray( valueToCheck ) ? valueToCheck : [ valueToCheck ] );
                    return ( _.intersection( [ value ], valueToCheck ).length > 0 );
                }
                return false;

            } );

        },

        filterByAction: function( phaserButtons, strActionName ) {
            return _.filter( phaserButtons, function( o ) { return o.action === strActionName; } );
        },

        filterButtonsByAction: function( strActionName ) {
            return SceneUtils.filterByAction( self.getRFLoadScenes().getPhaserButtons(), strActionName );
        },

        getButtonByAction: function( strActionName ) {
            var filteredButtons = SceneUtils.filterButtonsByAction( strActionName );
            if( filteredButtons.length > 1 ) {
                console.warn( "Multiple Buttons with '" + strActionName + "' action in this scene" );
            }
            return _.head( filteredButtons );
        },

        filterByType: function( phaserMap, strOrArrayTypeName ) {
            function _filterByType( _phaserMap, strTypeName ) {
                return _.filter( _phaserMap, function( o ) { return o.type === strTypeName; } );
            }

            var filtered = [];
            if( _.isArray( strOrArrayTypeName ) ) {
                _.forEach( strOrArrayTypeName, function( strTypeName ) {
                    filtered = _.union( filtered, _filterByType( phaserMap, strTypeName ) );
                } );
            }
            else {
                filtered = _filterByType( phaserMap, strOrArrayTypeName );
            }

            return filtered;
        },

        setTextAll: function( phaserTextsMap, txt ) {
            if( _.isString( txt ) ) {
                _.forEach( phaserTextsMap, function( phaserText ) {
                    phaserText.setText( txt );
                } );
            }
        },

        setVisibleAll: function( phaserObjectMap, bVisible ) {
            if( _.isBoolean( bVisible ) ) {
                _.forEach( phaserObjectMap, function( oPhaser ) {
                    oPhaser.visible = bVisible;
                } );
            }
        },
        setVisibleAllByType: function( phaserObjectMap, bVisible, type ) {
            if( _.isBoolean( bVisible ) && _.isString( type ) ) {
                _.forEach( phaserObjectMap, function( oPhaser ) {
                    if( _.has( oPhaser, "type" ) ) {
                        if( oPhaser.type === type ) {
                            oPhaser.visible = bVisible;
                        }
                    }
                } );
            }
        },

        updateGlobalData: function( rfwsResponse ) {
            rfwsResponse = rfwsResponse instanceof RFWSGenericResponse ? rfwsResponse.response : rfwsResponse;
            RFGlobalData.locale.currencyISO = _.get( rfwsResponse, [ "player", "currencyISO" ], RFGlobalData.locale.currencyISO );
            RFGlobalData.game.currentDenom = _.get( rfwsResponse, [ "player", "denom" ], RFGlobalData.game.currentDenom );
        },

        setMoney: function( phaserLabel, value, currencyISO ) {
            if( _.get( RFGameConfig, "game.displayCredits" ) ) {
                var currentDenom = _.get( RFGlobalData, "game.currentDenom", -1 );
                if( currentDenom > 0 ) {
                    value /= currentDenom;
                }
            }
            phaserLabel.setMoney( value, currencyISO );
        },

        setButtonNormal: function( phaserButton ) {
            var frameNormal = _.get( phaserButton, "data.gdObj.normal" );
            if( _.isInteger( frameNormal ) ) {
                phaserButton.frame = frameNormal;
            }
            else if( _.isString( frameNormal ) ) {
                phaserButton.frameName = frameNormal;
            }
        },

        setButtonEnable: function( phaserButtonMap, bEnable ) {
            _.forEach( phaserButtonMap, function( oPhaserButton ) {
                oPhaserButton.enable( bEnable );
            } );
        },

        /* eslint-disable complexity */
        getPrizesArray: function( rfWSSpinState, rfLoader ) {
            rfLoader = rfLoader instanceof RFLoadScenes ? rfLoader : self.getRFLoadScenes();
            var prizesArray = [];
            if( rfWSSpinState instanceof RFWSSpinState ) {
                var winArray = rfWSSpinState.winPrizesArray;
                if( _.isArray( winArray ) ) {
                    for( var i = 0; i < winArray.length; ++i ) {
                        var oLine = winArray[ i ];
                        var rfLine = rfLoader.getRFLine( oLine.numLineWinner );
                        var strIco = _.isArray( oLine.strIco ) ? _.head( oLine.strIco ) : oLine.strIco;

                        // Get the correct rfSymbol
                        var prizeSoundSymbolId = _.isArray( oLine.prizeSoundSymbolId ) ? _.head( oLine.prizeSoundSymbolId ) : oLine.prizeSoundSymbolId;
                        prizeSoundSymbolId = prizeSoundSymbolId ? prizeSoundSymbolId : strIco;
                        var rfSymbol = rfLoader.getRFSymbol( prizeSoundSymbolId );

                        var aWinPosition = _.slice( oLine.line, 0, oLine.times );
                        var prize = _.get( oLine, "prize" );
                        var accumulatedPrize = _.get( oLine, "accumulatedPrize" );
                        var rfMultiplier = oLine.multiplier ? rfLoader.getRFMultiplier( oLine.multiplier ) : undefined;

                        var isValid = true;
                        if( !( rfLine instanceof RFLine ) ) {
                            rfLine = new RFLine();
                            rfLine.id = oLine.numLineWinner;
                            console.warn( "getPrizesArray: Invalid RFLine from this numLineWinner", oLine.numLineWinner,
                                "Create RFLine with", oLine.numLineWinner, "as id" );
                        }

                        if( !( rfSymbol instanceof RFSymbol ) ) {
                            isValid = false;
                            console.warn( "getPrizesArray: Invalid RFSymbol from this strIco", strIco, "['" + strIco + "']" );
                        }

                        if( isValid ) {
                            prizesArray.push( {
                                rfLine: rfLine,
                                rfSymbol: rfSymbol,
                                aWinPosition: aWinPosition,
                                prize: prize,
                                accumulatedPrize: accumulatedPrize,
                                rfMultiplier: rfMultiplier
                            } );
                        }
                    }
                }
            }
            else {
                console.warn( "getPrizesArray: Not a RFWSSpinResponse", rfWSSpinState );
            }
            return prizesArray;
        },
        /* eslint-enable complexity */
        getExtendedWildArray: function( rfWSSpinState ) {
            var prizesArray = [];
            if( rfWSSpinState instanceof RFWSSpinState ) {
                var rfLoader = self.getRFLoadScenes();
                var extendedWildArray = rfWSSpinState.extendedWildArray;
                if( _.isArray( extendedWildArray ) ) {
                    for( var i = 0; i < extendedWildArray.length; ++i ) {
                        var oExtendedWild = extendedWildArray[ i ];
                        var numReel = oExtendedWild.numReel;
                        var idExtendedWild = oExtendedWild.idExtendedWild;
                        var rfExtendedWild = rfLoader.getRFExtendedWild( idExtendedWild );

                        var isValid = true;
                        if( !( rfExtendedWild instanceof RFExtendedWild ) ) {
                            isValid = false;
                            console.warn( "getExtendedWildArray: Invalid RFExtendedWild from this idExtendedWild", idExtendedWild );
                        }

                        if( isValid ) {
                            prizesArray.push( {
                                rfExtendedWild: rfExtendedWild,
                                numReel: numReel
                            } );
                        }
                    }
                }
            }
            else {
                console.warn( "getExtendedWildArray: Not a RFWSSpinResponse", rfWSSpinState );
            }
            return prizesArray;
        },

        /**
         * A partir de la matriz de símbolos recibida desde el servidor, se crea la matriz de RFSymbols
         * @param {array} aServerSymbolMatrix Matriz de símbolos recibida desde el servidor
         * @param {RFLoadScenes|undefined} rfLoader RFLoader del que se extraerán los recursos.
         */
        getRFSymbolsMatrix: function( aServerSymbolMatrix, rfLoader ) {
            rfLoader = rfLoader instanceof RFLoadScenes ? rfLoader : self.getRFLoadScenes();
            var matrix = [];
            for( var i = 0; i < aServerSymbolMatrix.length; ++i ) {
                var reelRFSymbol = [];
                for( var j = 0; j < aServerSymbolMatrix[ i ].length; ++j ) {
                    var symbolKey = aServerSymbolMatrix[ i ][ j ];
                    var rfSymbol = rfLoader.getRFSymbol( symbolKey );
                    if( !_.isObject( rfSymbol ) ) {
                        throw new Error( "Received symbolKey from server not defined: '" + symbolKey + "'" );
                    }
                    reelRFSymbol.push( rfSymbol );
                }
                matrix.push( reelRFSymbol );
            }
            return matrix;
        },

        setLastRFSymbolsMatrix: function( rfSymbolsMatrix, sceneKey ) {
            if( sceneKey === undefined ) {
                sceneKey = game.state.getCurrentState().key;
            }

            RFGlobalData.reels[ sceneKey ] = rfSymbolsMatrix;
        },

        getLastRFSymbolsMatrix: function( sceneKey ) {
            if( sceneKey === undefined ) {
                sceneKey = game.state.getCurrentState().key;
            }

            return RFGlobalData.reels[ sceneKey ];
        },

        handleTransitionSymbolsForReels: function( reels ) {
            if( RFGameConfig.game.maintainReelSymbolsAfterTransition ) {
                if( game.state.getCurrentState().key === eSceneNames.BASEGAME ) {
                    SceneUtils.setLastRFSymbolsMatrix( SceneUtils.getLastRFSymbolsMatrix( eSceneNames.FREEGAME ), eSceneNames.BASEGAME );
                }
                else {
                    SceneUtils.setLastRFSymbolsMatrix( SceneUtils.getLastRFSymbolsMatrix( eSceneNames.BASEGAME ), eSceneNames.FREEGAME );
                }

                var initialReelMatrix = SceneUtils.getLastRFSymbolsMatrix();

                if( initialReelMatrix !== undefined ) {
                    reels.setReels( SceneUtils.getLastRFSymbolsMatrix() );
                }
            }
        },

        /**
         * función que obtiene del spinState del winPrizesArrayJSON los valores
         * "strIco" y "times" para crear un objeto para controlar la estela de la emoción
         * y saber si la tiene que hacer la estela de la emoción y a cuantos símbolos están
         */
        getPrizeHypeObject: function( rfWSSpinState ) {
            var prizeHypeArray = [];
            var prizeHypeSymbols = RFGameConfig.game.prizeHype;
            if( rfWSSpinState instanceof RFWSSpinState ) {
                var winPrizeArrayJSON = rfWSSpinState.winPrizesArray;
                if( _.isArray( winPrizeArrayJSON ) && _.isArray( prizeHypeSymbols ) ) {
                    for( var index = 0; index < winPrizeArrayJSON.length; index++ ) {
                        var winPrizeStrIco = winPrizeArrayJSON[ index ].strIco;
                        if( _.indexOf( prizeHypeSymbols, winPrizeStrIco ) > -1 ) {
                            var prizeHypeObj = {};
                            prizeHypeObj.strIco = winPrizeArrayJSON[ index ].strIco;
                            prizeHypeObj.times = winPrizeArrayJSON[ index ].times;
                            prizeHypeArray.push( prizeHypeObj );
                        }
                    }
                }
            }
            return prizeHypeArray;
        },

        // @param {number|function} [ twTime ]
        // @param {function}  [ fCallback ]
        hideCovers: function( twTime, fCallback ) {
            var rfLoader = self.getRFLoadScenes();
            fCallback = ( _.isFunction( twTime ) && fCallback === undefined ) ? twTime : fCallback;
            twTime = _.isNumber( twTime ) && twTime > 0 ? twTime : DEFAULTS.MS_TO_HIDE_COVERS;

            var phaserImages = rfLoader.getPhaserImages();
            var callbackRegistered = false;
            _.forEach( phaserImages, function( oPhaserImage ) {
                if( oPhaserImage.parameters.cover === true && oPhaserImage.alpha !== 0 ) {
                    var tw = game.add.tween( oPhaserImage ).to( { alpha: 0 }, twTime, Phaser.Easing.Linear.None, true );
                    if( !callbackRegistered && _.isFunction( fCallback ) ) {
                        tw.onComplete.addOnce( fCallback );
                        callbackRegistered = true;
                    }
                }
            } );

            if( !callbackRegistered && _.isFunction( fCallback ) ) {
                fCallback();
            }
        },

        hideAllPosters: function() {
            var phaserImages = self.getRFLoadScenes().getPhaserImages();
            var phaserLabels = self.getRFLoadScenes().getPhaserLabels();
            var phaserArray = _.union(
                SceneUtils.filterByParameters( phaserImages, "tag", [ STR_INTRO_POSTER, STR_OUTRO_POSTER ] ),
                SceneUtils.filterByParameters( phaserLabels, "tag", [ STR_INTRO_POSTER, STR_OUTRO_POSTER ] )
            );
            SceneUtils.setVisibleAll( phaserArray, false );
        },

        showIntroPoster: function( msToHidePoster ) {

            var phaserImages = self.getRFLoadScenes().getPhaserImages();
            var phaserLabels = self.getRFLoadScenes().getPhaserLabels();

            // Build the Intro Poster elements
            var phaserArray = _.union(
                SceneUtils.filterByParameters( phaserImages, "tag", STR_INTRO_POSTER ),
                SceneUtils.filterByParameters( phaserLabels, "tag", STR_INTRO_POSTER )
            );

            if( _.size( phaserArray ) > 0 ) {

                msToHidePoster = msToHidePoster > 0 ? msToHidePoster : DEFAULTS.MS_TO_HIDE_POSTER;

                // Show elements and add to poster group
                var group = game.add.group();
                _.forEach( phaserArray, function( oPhaser ) {
                    oPhaser.visible = true;
                    group.add( oPhaser );
                } );

                // Poster fade in
                SceneUtils.fadeIn( group, DEFAULTS.MS_FADEIN_POSTER, Phaser.Easing.Quadratic.In );

                // Poster fade out
                self.game.time.events.add( msToHidePoster, function() {
                    SceneUtils.fadeOut( group, DEFAULTS.MS_FADEOUT_POSTER, Phaser.Easing.Quadratic.Out );
                } );

            }
            else {
                console.warn( "No parameters.tag = '" + STR_INTRO_POSTER + "' in gameDefinition 'images' or 'labels', to show the final prize." );
            }

        },

        showOutroPoster: function() {

            var phaserImages = self.getRFLoadScenes().getPhaserImages();
            var phaserLabels = self.getRFLoadScenes().getPhaserLabels();

            // Build the Outro Poster elements
            var phaserArray = _.union(
                SceneUtils.filterByParameters( phaserImages, "tag", STR_OUTRO_POSTER ),
                SceneUtils.filterByParameters( phaserLabels, "tag", STR_OUTRO_POSTER )
            );

            if( _.size( phaserArray ) > 0 ) {

                var msToShowBlackScreen = 1000;

                // Black Screen sprite
                var fadeSprite = SceneUtils.createFadeSprite();

                // Show
                fadeSprite.alpha = 0;
                SceneUtils.fadeTo( fadeSprite, msToShowBlackScreen, DEFAULTS.BLACK_SCREEN_POSTER_ALPHA, Phaser.Easing.Quadratic.Out );

            }
            else {
                console.warn( "No parameters.tag = '" + STR_OUTRO_POSTER + "' in gameDefinition 'images' or 'labels', to show the final prize." );
            }

            // Show elements and add to poster group
            var group = game.add.group();
            _.forEach( phaserArray, function( oPhaser ) {
                oPhaser.visible = true;
                group.add( oPhaser );
            } );
        },

        hasValidSession: function( wsResponse ) {
            var result = true;
            if( !_.isEmpty( wsResponse ) ) {

                // Get the valid value for rfwsResult
                var rfwsResult;
                if( wsResponse.result instanceof RFWSResult ) {
                    rfwsResult = wsResponse.result;
                }
                else if( wsResponse instanceof RFWSGenericResponse ) {
                    rfwsResult = _.get( wsResponse.response, [ "result" ] );
                }

                if( rfwsResult ) {
                    if( rfwsResult.returnCode === eServerReturnCodes.INVALID_SESSION ) {
                        result = false;
                        console.error( "Invalid session. Try a reconnection (we will always try to reconnect if we lose the session).", wsResponse );
                        debugger;
                        game.state.start( eSceneNames.RECONNECTION );
                    }
                }
                else {
                    result = false;
                    console.error( "Invalid wsResponse. Needed a 'result.returnCode' value", wsResponse );
                    debugger;
                }

            }
            else {
                // no session ( ping responses )
            }
            return result;
        },

        isValidWSResult: function( wsResponse ) {
            var result = false;
            var wsResult = _.get( wsResponse, [ "result" ] );
            if( wsResult instanceof RFWSResult ) {
                result = (
                    wsResponse.result.returnCode === eServerReturnCodes.OK ||
                    wsResponse.result.returnCode === eServerReturnCodes.INSUFFICIENT_FOUNDS ||
                    wsResponse.result.returnCode === eServerReturnCodes.LIMIT_EXCEEDED );
            }
            return result;
        },

        createPhaserText: function( params ) {
            var smoothed = _.get( params, "smoothed", false );
            var useShadow = _.get( params, "useShadow", true );
            var text = _.get( params, "text", "" );
            var size = _.get( params, "size", DEFAULTS.FONT_SIZE );
            var color = _.get( params, "color", "#ffffff" );
            var align = _.get( params, "align", "center" );
            var x = _.get( params, "x", 0 );
            var y = _.get( params, "y", 0 );
            var anchorX = _.get( params, "anchorX", 0 );
            var anchorY = _.get( params, "anchorY", 0 );

            var phText;
            phText = self.game.add.text( 0, 0, text, { align: align, fontSize: size || DEFAULTS.FONT_SIZE, font: "Arial", fill: color || "#ffffff" } );

            if( align === "topLeft" ) {
                x = 0;
                y = self.game.camera.y + phText.height;
                anchorX = 0;
                anchorY = 0;
            }
            else if( align === "topCenter" ) {
                x = self.game.camera.width / 2;
                y = self.game.camera.y + phText.height;
                anchorX = 0.5;
                anchorY = 0;
            }
            else if( align === "topRight" ) {
                x = self.game.camera.width;
                y = self.game.camera.y + phText.height;
                anchorX = 1;
                anchorY = 0;
            }
            else if( align === "bottomLeft" ) {
                x = 0;
                y = self.game.camera.height + self.game.camera.y - phText.height;
                anchorX = 0;
                anchorY = 0;
            }
            else if( align === "bottomCenter" ) {
                x = self.game.camera.width / 2;
                y = self.game.camera.height + self.game.camera.y - phText.height;
                anchorX = 0.5;
                anchorY = 0;
            }
            else if( align === "bottomRight" ) {
                x = self.game.camera.width;
                y = self.game.camera.height + self.game.camera.y - phText.height;
                anchorX = 1;
                anchorY = 0;
            }

            phText.anchor.x = anchorX;
            phText.anchor.y = anchorY;
            phText.x = x;
            phText.y = y;
            phText.smoothed = smoothed;

            if( useShadow ) {
                phText.setShadow( 1, 2, "rgba(0, 0, 0, 0.5)", 1 );
            }
            return phText;
        },

        createBottomCenterText: function( text, size, color, useShadow ) {
            return SceneUtils.createPhaserText( {
                text: text,
                size: size,
                color: color,
                useShadow: useShadow,
                align: "bottomCenter"
            } );
        },

        drawError: function( strErrorTitle, strErrorDescription ) {

            strErrorTitle = _.isString( strErrorTitle ) ? strErrorTitle : "";
            strErrorDescription = _.isString( strErrorDescription ) ? strErrorDescription : "";

            // blackSCreen
            var errorSprite = SceneUtils.createFadeSprite();
            errorSprite.alpha = 0.7;
            errorSprite.visible = true;

            var centerX = self.game.camera.width / 2;
            var centerY = self.game.camera.height / 2;

            if( self.game.cache.checkImageKey( "error" ) ) {
                var x = centerX + self.game.camera.x;
                var y = centerY + self.game.camera.y;

                var errorText = self.game.add.sprite( x, y, "error" );
                errorText.anchor.setTo( 0.5, 0.5 );
            }
            else {
                // todo: Put a text or something
            }

            // Create the title text
            var phErrorTitle;
            if( strErrorTitle.length > 0 ) {
                phErrorTitle = SceneUtils.createBottomCenterText( strErrorTitle, DEFAULTS.ERROR_TITLE_FONT_SIZE );
            }

            // Create the description text
            var phErrorDesc;
            if( strErrorDescription.length > 0 ) {
                phErrorDesc = SceneUtils.createBottomCenterText( strErrorDescription, DEFAULTS.DESCRIPTION_FONT_SIZE );
            }

            // Moves the title and position
            if( phErrorTitle ) {
                phErrorTitle.y -= _.get( phErrorDesc, "height", 0 );
            }

            console.log( phErrorTitle, phErrorDesc );

            // remove all inputs
            self.game.input.enabled = false;
        },


        // TODO: Fix this
        // eslint-disable-next-line complexity
        getWSError: function( wsResponse ) {

            // todo: Hot Fix que habría que solucionar en el wrapper del engine.
            // TODO: Fix this
            // eslint-disable-next-line complexity
            function convertGSErrorToServerError( errorCode ) {

                /*
                                           // 0
                    "Fail"                 // 1
                    "GameSessionClosed"    // 2
                    "InvalidGameSession"   // 3
                                           // 4
                                           // 5
                    "InsufficientFunds"    // 6
                    "LimitsExceeded"       // 7
                    "StatusForbidsBetting" // 8
                    "WalletUnavailable"    // 9
                */
                switch( errorCode ) {
                    case "ServerError": return eServerReturnCodes.INTERNAL_ERROR;
                    case "Fail": return eServerReturnCodes.INTERNAL_ERROR;
                    case "BadRequest": return eServerReturnCodes.INTERNAL_ERROR;
                    case "GameSessionNotFound": return eServerReturnCodes.INVALID_SESSION;
                    case "GameSessionInactive": return eServerReturnCodes.INVALID_SESSION;
                    case "GameSessionClosed": return eServerReturnCodes.INVALID_SESSION;
                    case "InvalidGameSession": return eServerReturnCodes.INVALID_SESSION;
                    case "InvalidGameSessionToken": return eServerReturnCodes.INVALID_SESSION;
                    case "InsufficientFunds": return eServerReturnCodes.INSUFFICIENT_FOUNDS;
                    case "LimitsExceeded": return eServerReturnCodes.LIMIT_EXCEEDED;
                    case "WalletUnavailable": return eServerReturnCodes.OPERATION_CANCELLED;
                    case "StatusForbidsBetting": return eServerReturnCodes.FORBIDDEN_BETTING;
                    default: break;
                }
                return errorCode;
            }

            var texts = _.get( RFGlobalData, "locale.texts" );
            var returnCode;
            var returnMessage;
            var errorObj;
            if( !_.isEmpty( wsResponse ) ) {

                // Get the valid value for rfwsResult
                var response;
                var rfwsResult;
                if( wsResponse.result instanceof RFWSResult ) {
                    rfwsResult = wsResponse.result;
                }
                else if( wsResponse instanceof RFWSGenericResponse ) {
                    rfwsResult = _.get( wsResponse.response, [ "result" ] );
                }
                // todo: Hot Fix que habría que solucionar en el wrapper del engine.
                else {
                    response = getJSON( _.get( getJSON( wsResponse ), "properties.resultPayload" ) );
                    if( response ) {
                        console.warn( "Error from Server" );
                        rfwsResult = _.get( response, "errorResponse.result" );
                    }
                    else {
                        // Esta es una respuesta que viene directamente sin tratar por el servidor (por ejemplo un error 500)
                        console.warn( "Error from GS not threated in server" );
                        response = getJSON( wsResponse.responseText );
                        if( response ) {
                            rfwsResult = {
                                returnCode: convertGSErrorToServerError( _.get( response, "properties.errorCode", "NO ERROR CODE" ) ),
                                returnMessage: _.get( response, "properties.errorDescription", "" )
                            };
                        }
                        else {
                            // HTTP status errors
                            rfwsResult = {
                                returnCode: _.get( texts, "UNDEFINED_ERROR", "Error indefinido" ),
                                returnMessage: _.get( texts, "UNDEFINED_ERROR", "Error indefinido" )
                            };

                            var statusText = _.get( wsResponse, "statusText" );

                            // TODO: Fix this
                            // eslint-disable-next-line max-depth
                            switch( statusText ) {
                                case "error":
                                    returnCode = _.get( texts, "NETWORK_ERROR", "Error de conexión" );
                                    returnMessage = returnCode;
                                    break;
                                case "timeout":
                                    returnCode = _.get( texts, "REQUEST_TIMEOUT", "La petición ha tardado demasiado tiempo" );
                                    returnMessage = returnCode;
                                    break;
                                default:
                                    break;
                            }
                            rfwsResult.returnCode = returnCode;
                            rfwsResult.returnMessage = returnMessage;
                        }
                    }
                }

                if( rfwsResult ) {
                    returnCode = rfwsResult.returnCode;
                    returnMessage = rfwsResult.returnMessage;

                    // default error
                    errorObj = {
                        returnCode: returnCode,
                        strError: returnCode, // override in the switch if matched
                        strDescriptionError: returnMessage
                    };

                    switch( returnCode ) {
                        case eServerReturnCodes.INTERNAL_ERROR: errorObj.strError = _.get( texts, "INTERNAL_ERROR", errorObj.strError ); break;
                        case eServerReturnCodes.INVALID_SESSION: errorObj.strError = _.get( texts, "INVALID_SESSION", errorObj.strError ); break;
                        case eServerReturnCodes.INSUFFICIENT_FOUNDS: errorObj.strError = _.get( texts, "INSUFFICIENT_FOUNDS", errorObj.strError ); break;
                        case eServerReturnCodes.INVALID_PLAY_GAME: errorObj.strError = _.get( texts, "INVALID_PLAY_GAME", errorObj.strError ); break;
                        case eServerReturnCodes.LIMIT_EXCEEDED: errorObj.strError = _.get( texts, "LIMIT_EXCEEDED", errorObj.strError ); break;
                        case eServerReturnCodes.OPERATION_CANCELLED: errorObj.strError = _.get( texts, "OPERATION_CANCELLED", errorObj.strError ); break;
                        case eServerReturnCodes.FORBIDDEN_BETTING: errorObj.strError = _.get( texts, "FORBIDDEN_BETTING", errorObj.strError ); break;
                        default: break;
                    }
                }
            }
            return errorObj;
        },

        fERROR: function( result ) {

            // stop the sounds and log the error
            self.game.sound.stopAll();
            console.error( "ERROR: ", result );

            var errorObj = SceneUtils.getWSError( result );
            var strError = errorObj === undefined
                ? result
                : "Return code: '" + errorObj.returnCode + "', strError: '" + errorObj.strError +
                "', strDescriptionError: '" + errorObj.strDescriptionError + "'";
            console.error( "ERROR: ", strError ); /* RemoveLogging:skip*/

            var strErrorTitle = _.get( errorObj, "strError" );
            // var strDescriptionError = _.get( errorObj, "strDescriptionError" );

            var reconnectionEnabled = _.get( RFGameConfig, "reconnection.enable", true );
            if( !reconnectionEnabled ) {
                console.warn( "Reconnection scene disabled" );
                SceneUtils.drawError( strErrorTitle );
                SceneUtils.sendExitToLobby();
                return;
            }

            if( RFGlobalData.reconnection.attemptsToReconnectLeft >= 0 ) {
                // initialize for the first time
                var now = _.now();
                if( !RFGlobalData.reconnection.lastReconnectionAttempt ) {
                    RFGlobalData.reconnection.lastReconnectionAttempt = now;
                }

                // Checks if we need to reset the counter
                var msTotalToConsumeAllAttempts = _.get(
                    RFGameConfig,
                    [ "reconnection", "msTotalToConsumeAllAttempts" ],
                    DEFAULTS.MS_TOTAL_TO_CONSUME_ALL_ATTEMPTS
                );
                var msInAttemptReconnections = now - RFGlobalData.reconnection.lastReconnectionAttempt;
                if( msInAttemptReconnections <= msTotalToConsumeAllAttempts ) {
                    // one attempt less
                    RFGlobalData.reconnection.attemptsToReconnectLeft--;
                }
                else {
                    // reset Attempt counter
                    RFGlobalData.reconnection.attemptsToReconnectLeft = _.get(
                        RFGameConfig,
                        [ "reconnection", "maxAttemptsToReconnect" ],
                        DEFAULTS.MAX_ATTEMPTS_TO_RECONNECT
                    ) - 1;
                    RFGlobalData.reconnection.lastReconnectionAttempt = now;
                    msInAttemptReconnections = 0;
                }

                // change the scene if we have attempts
                if( RFGlobalData.reconnection.attemptsToReconnectLeft >= 0 ) {
                    console.warn(
                        "TRY RECONNECTION ( left:" + RFGlobalData.reconnection.attemptsToReconnectLeft +
                        ", ms between attempts: " + msInAttemptReconnections + " timeStamp: " +
                        RFGlobalData.reconnection.lastReconnectionAttempt + " )"
                    );
                    self.game.state.start( eSceneNames.RECONNECTION );
                }
                else {
                    console.error( "No more tries to reconnect." );
                    SceneUtils.drawError( strErrorTitle );
                    SceneUtils.sendExitToLobby();

                }
            }
            else {
                console.error( "No more tries to reconnect." );
                SceneUtils.drawError( strErrorTitle );
                SceneUtils.sendExitToLobby();
            }
        },

        loadScene: function( sceneName, rfLoader ) {
            rfLoader = rfLoader instanceof RFLoadScenes ? rfLoader : self.getRFLoadScenes();
            rfLoader.loadScene( sceneName );
            SceneUtils.initSkittels( rfLoader );
            SceneUtils.initLines( rfLoader );
            SceneUtils.initLabels( rfLoader );
            SceneUtils.initCreditLabels( rfLoader );
            SceneUtils.initExitToLobbyButtons( rfLoader );
        },

        initSound: function( rfAudioSound, soundType, rfLoader ) {
            rfLoader = rfLoader instanceof RFLoadScenes ? rfLoader : self.getRFLoadScenes();
            var rfSounds = rfLoader.getRFSounds();
            var filteredSounds = SceneUtils.filterByType( rfSounds, soundType );
            if( filteredSounds.length > 0 ) {
                rfAudioSound.setSounds( _.head( filteredSounds ).soundKeys );
                if( filteredSounds.length > 1 ) {
                    console.warn( "Multiple sounds this type:", soundType );
                }
            }
            else {
                console.warn( "No sounds found with this soundType", soundType );
            }
        },

        initLabels: function( rfLoader ) {
            rfLoader = rfLoader instanceof RFLoadScenes ? rfLoader : self.getRFLoadScenes();
            _.forEach( rfLoader.getPhaserLabels(), function( phaserLabel ) {
                SceneUtils.initLabel( phaserLabel );
            } );
        },

        initLabel: function( phaserLabel ) {

            // Export formatMoneyOrCredits function
            if( !_.get( phaserLabel, "formatMoneyOrCredits" ) ) {
                phaserLabel.formatMoneyOrCredits = SceneUtils.formatMoneyOrCredits;
            }

            // Export setMoneyOrCredits function
            if( _.get( phaserLabel, "setMoney" ) && !_.get( phaserLabel, "setMoneyOrCredits" ) ) {

                phaserLabel.setMoneyOrCredits = function( moneyValue, currencyISO, twTime, bPlayAudio ) {
                    var moneyOrCreditsObj = SceneUtils.getMoneyOrCredits( moneyValue, currencyISO );
                    phaserLabel.setMoney( moneyOrCreditsObj.moneyValue, moneyOrCreditsObj.currencyISO, twTime, bPlayAudio );
                };

                var str = _.get( phaserLabel, "data.gdObj.text" );

                var localeTextsMap = _.get( RFGlobalData, [ "locale", "texts" ] );
                var finalStr = RFLabelUtils.getReplacedText( str, localeTextsMap );
                phaserLabel.setFormattedString( finalStr );

            }
        },

        initSkittels: function( rfLoader ) {
            rfLoader = rfLoader instanceof RFLoadScenes ? rfLoader : self.getRFLoadScenes();
            _.forEach( rfLoader.getPhaserSkittels(), function( oPhaserSkittel, id ) {
                oPhaserSkittel.visible = Number( id ) > 0;
            } );
        },

        initLines: function( rfLoader ) {
            rfLoader = rfLoader instanceof RFLoadScenes ? rfLoader : self.getRFLoadScenes();
            SceneUtils.setVisibleAll( rfLoader.getPhaserLines(), false );
        },

        initCreditLabels: function( rfLoader ) {
            rfLoader = rfLoader instanceof RFLoadScenes ? rfLoader : self.getRFLoadScenes();
            var creditLabels = SceneUtils.filterByParameters( rfLoader.getPhaserImages(), "isCreditLabel", true );
            SceneUtils.setVisibleAll( creditLabels, _.get( RFGameConfig, "game.displayCredits", false ) );
        },

        getMoneyOrCredits: function( moneyValue, currencyISO ) {
            var resultObj = {
                moneyValue: moneyValue,
                currencyISO: currencyISO
            };
            if( _.get( RFGameConfig, "game.displayCredits" ) ) {
                var currentDenom = _.get( RFGlobalData, "game.currentDenom", -1 );
                if( currentDenom > 0 ) {
                    resultObj.moneyValue /= currentDenom;
                    resultObj.currencyISO = "XXX";
                }
            }
            return resultObj;
        },

        // TODO: Fix this
        // eslint-disable-next-line max-params
        formatMoneyOrCredits: function( moneyValue, currencyISO, valueSymbolSeparator, decimalSeparator, thousandSeparator ) {
            var moneyOrCreditsObj = SceneUtils.getMoneyOrCredits( moneyValue, currencyISO );
            return RFCurrencyUtils.format(
                moneyOrCreditsObj.moneyValue,
                moneyOrCreditsObj.currencyISO,
                valueSymbolSeparator,
                decimalSeparator,
                thousandSeparator
            );
        },

        createFadeSprite: function( color ) {
            var fadeSprite = null;
            var width = self.game.world.width;
            var height = self.game.world.height;
            var bmd = self.game.add.bitmapData( width, height );
            bmd.ctx.beginPath();
            bmd.ctx.rect( 0, 0, width, height );
            bmd.ctx.fillStyle = color || "#000000";
            bmd.ctx.fill();
            fadeSprite = self.game.add.sprite( self.game.world.centerX, self.game.world.centerY, bmd );
            fadeSprite.anchor.setTo( 0.5, 0.5 );
            fadeSprite.inputEnabled = true;
            fadeSprite.visible = false;
            return fadeSprite;
        },

        // TODO: Fix this
        // eslint-disable-next-line max-params
        fadeTo: function( phaserObj, fadingTime, alpha, easing, fCallbackEnd ) {
            if( _.hasAllKeys( phaserObj, [ "alpha", "visible" ] ) ) {
                alpha = ( ( alpha >= 0 && alpha <= 1 ) ? alpha : 1 );
                fadingTime = fadingTime > 0 ? fadingTime : Phaser.Timer.HALF;
                if( fadingTime === 0 ) {
                    phaserObj.visible = true;
                    phaserObj.alpha = alpha;
                }
                else {
                    phaserObj.visible = true;
                    easing = easing || Phaser.Easing.Linear.None;
                    var tw = self.game.add.tween( phaserObj ).to( { alpha: alpha }, fadingTime, easing );
                    if( _.isFunction( fCallbackEnd ) ) {
                        tw.onComplete.add( fCallbackEnd );
                    }
                    tw.start();
                }
            }
        },

        fadeIn: function( phaserObj, fadingTime, easing, fCallbackEnd ) {
            phaserObj.alpha = 0;
            SceneUtils.fadeTo( phaserObj, fadingTime, 1, easing, fCallbackEnd );
        },

        fadeOut: function( phaserObj, fadingTime, easing, fCallbackEnd ) {
            SceneUtils.fadeTo( phaserObj, fadingTime, 0, easing, function() {
                phaserObj.visible = false;
                if( _.isFunction( fCallbackEnd ) ) {
                    fCallbackEnd();
                }
            } );
        },

        showHistory: function( url, data ) {

            // append the line parameters to the url
            if( _.isString( url ) ) {

                localStorage.historyData = window.btoa( JSON.stringify( data ) );

                url += ( url.indexOf( "?" ) !== -1 ? "&" : "?" );
                url += "lang=" + _.get( RFGlobalData, "locale.lang", "es" );

                var $dialog = $( "<div></div>" )
                    .html( "<iframe style=\"border: 0px; \" src=\"" + url + "\" width=\"100%\" height=\"100%\"></iframe>" )
                    .dialog( {
                        autoOpen: false,
                        modal: true,
                        height: 500,
                        width: 600,
                        title: RFGameConfig.gameName
                    } );
                $dialog.dialog( "open" );

            }
            else {
                console.warn( "showHistory: Can't open the history window. Invalid URL", url );
            }
        },

        addKeyBinding: function( keyBinding, fCallback, context ) {
            keyBinding = _.isInteger( keyBinding ) ? [ keyBinding ] : keyBinding;
            if( _.isArray( keyBinding ) ) {
                _.forEach( keyBinding, function( keyb ) {
                    self.game.input.keyboard.addKey( keyb ).onDown.add( fCallback, context );
                } );
            }
        },

        overrideConfig: function( clientConfig ) {

            var overrideConfigInternal = function( configFile ) {
                if( !_.isString( configFile ) ) {
                    console.info( "Invalid config file", configFile );
                }
                else {
                    configFile = "./js/game/appdata/configs/" + configFile + "?date=" + Date.now();
                    var newRFGameConfigObj = loadJSONSync( configFile, function( /* error */ ) {
                        console.info( "Can't read " + configFile );
                    } );

                    if( _.isObject( newRFGameConfigObj ) ) {
                        _.merge( RFGameConfig, newRFGameConfigObj );
                        console.info( "Merged RFGameConfig with new configuration", configFile, newRFGameConfigObj );
                    }
                }
            };

            var configFiles = _.get( clientConfig, [ "configFiles" ] );
            if( configFiles === undefined ) {
                console.info( "There is no configFiles" );
            }
            else if( !_.isArray( configFiles ) ) {
                console.info( "configFiles is not an array", configFiles );
            }
            else {
                _.forEach( configFiles, function( configFile ) {
                    overrideConfigInternal( configFile );
                } );
            }
        },

        displayVersion: function() {
            var text = RFGameConfig.version + " (" + RFGameConfig.build + ")";
            var fontSize = 10;
            var phText = SceneUtils.createPhaserText( {
                text: text,
                size: fontSize,
                align: "topLeft"
            } );
            phText.alpha = 0.3;
            return phText;
        },

        exitToLobby: function() {
            switch( RFGlobalData.game.lobbyReturnOption ) {
                case eLobbyReturnOption.URL:
                    window.location = RFGlobalData.game.lobbyUrl;
                    break;
                case eLobbyReturnOption.HISTORY_BACK:
                    window.history.back();
                    break;
                case eLobbyReturnOption.CLOSE_WINDOW:
                    window.close();
                    break;
                case eLobbyReturnOption.BARTOP:
                    // TODO: Implementar esto cuando se haga el merge con el slo2game de la bartop
                    break;

                case eLobbyReturnOption.NONE:
                default:
                    break;
            }
        },

        initExitToLobbyButtons: function( rfLoader ) {
            rfLoader = rfLoader instanceof RFLoadScenes ? rfLoader : self.getRFLoadScenes();
            var buttons = SceneUtils.filterByAction( rfLoader.getPhaserButtons(), eActionNames.exitToLobby );

            // Only change the visibility to false if RFGlobalData.game.lobbyReturnOption is NONE.
            var invisible = RFGlobalData.game.lobbyReturnOption === eLobbyReturnOption.NONE;
            if( invisible ) {
                SceneUtils.setVisibleAll( buttons, false );
            }
        },

        /**
         * @description Checks the input parameter with the setup environment in env.json.
         * @example isCurrentEnvironmentEqualsTo( eEnvironments.PRODUCTION );
         * @param {ENVIRONMENT} environmentId The environment to compare with the current environment.
         * Is defaulted to eEnvironments.PRODUCTION if no environmentId is input.
         * @returns {boolean} returns true if environmentId is equal to the setup environment in env.json, else false.
         */
        isCurrentEnvironmentEqualsTo: function( environmentId ) {
            if( environmentId === undefined ) {
                environmentId = eEnvironments.PRODUCTION;
                console.warn(
                    "%c SceneUtils.isCurrentEnvironmentEqualsTo is being invoked without an environmentId to check and has been defaulted to %s." +
                    "You should include a parameter including an environment id.",
                    RFDebugUtils.getConsoleColors().WARNING,
                    eEnvironments.PRODUCTION
                );
            }

            if( this.getEnvironment() === environmentId ) {
                return true;
            }
            return false;
        },

        getEnvironment: function() {
            return RFGameConfig.environment.id;
        },

        hasRoundStarted: function() {
            return RFGlobalData.game.hasRoundStarted;
        },
        setHasRoundStarted: function( bValue ) {
            RFGlobalData.game.hasRoundStarted = Boolean( bValue );
        },

        /**
         * @param args Contains the arguments used to generate the requests handled by this function.
         * @description This function manages the values related to the round state and any request that must be made on round start.
         */
        handleRoundStart: function( args ) {
            if( args === undefined ) {
                console.warn( " handleRoundStart is being called with no args. " );
            }

            if( !SceneUtils.hasRoundStarted() ) {
                console.log( "%c Round Start " + ( new Date() ).toString(), RFDebugUtils.getConsoleColors().ROUND );
                SceneUtils.setHasRoundStarted( true );

                if( SceneUtils.isCurrentEnvironmentEqualsTo( eEnvironments.HOMOLOGATION ) ) {
                    if( RFGlobalData.net.retailNotificationWrapper ) {
                        RFGlobalData.net.retailNotificationWrapper.sendHomologationRoundRequest( { "round": "start", "amount": 0 } );
                    }
                }

                if( RFGlobalData.net.retailNotificationWrapper ) {
                    RFGlobalData.net.retailNotificationWrapper.sendSASBetRequest( {
                        "betAmount": _.get( args, [ "betAmount" ], null ),
                        "betUid": _.get( args, [ "betUid" ], null )
                    } );
                }
            }
        },

        /**
         * @param args Contains the arguments used to generate the requests handled by this function.
         * @description This function manages the values related to the round state and any request that must be made on round end.
         * @example args can be an object like { "winAmount": 100 }, using this value for the sendSASWinRequest.
         */
        handleRoundEnd: function( args, force ) {
            if( SceneUtils.hasRoundStarted() || force ) {
                console.log( "%c Round End " + ( new Date() ).toString(), RFDebugUtils.getConsoleColors().ROUND );
                SceneUtils.setHasRoundStarted( false );

                var winAmount;
                if( SceneUtils.isCurrentEnvironmentEqualsTo( eEnvironments.HOMOLOGATION ) ) {
                    if( RFGlobalData.net.retailNotificationWrapper ) {
                        winAmount = _.get( args, [ "winAmount" ], 0 );
                        RFGlobalData.net.retailNotificationWrapper.sendHomologationRoundRequest( {
                            "round": "end",
                            "amount": winAmount
                        } );
                    }
                }

                if( _.hasAllKeys( args, [ "winAmount", "winUid" ] ) ) {
                    winAmount = _.get( args, [ "winAmount" ] );
                    var winUid = _.get( args, [ "winUid" ] );
                    if( RFGlobalData.net.retailNotificationWrapper ) {
                        RFGlobalData.net.retailNotificationWrapper.sendSASWinRequest( {
                            "winAmount": winAmount,
                            "winUid": winUid
                        } );
                    }
                }
                else {
                    console.warn( "HandleRoundEnd is called without an argument winAmount or winUid.", args );
                }
            }
        },

        /**
         * @description Delegate function that is subscribed to a signal of network wrapper.
         */
        onNetworkMessageReceived: function( response ) {
            if( SceneUtils.hasRoundStarted() && _.has( response, [ "player", "round" ] ) ) {
                var roundInfo = _.get( response, [ "player", "round" ], "" );
                var isEndOfRound = SceneUtils.isEndOfRound( roundInfo );

                console.log( "%cIs end of round [%s]", "color:#ff00ff", isEndOfRound, response );

                if( isEndOfRound ) {
                    if( _.has( roundInfo, [ "acumulatedPrize" ] ) ) {
                        var parameters = {
                            "winAmount": _.get( roundInfo, [ "acumulatedPrize" ], 0 ),
                            "winUid": _.get( roundInfo, [ "roundUid" ], "" )
                        };
                        SceneUtils.handleRoundEnd( parameters );
                    }
                    else {
                        console.log( "Response %s", response );
                    }
                }
            }
        },

        isEndOfRound: function( roundInfo ) {
            if( _.has( roundInfo, [ "endRound" ] ) ) {
                var isEndOfRound = _.get( roundInfo, [ "endRound" ], false );
                return isEndOfRound;
            }

            console.warn( "A network message response did not contain player.round.endRound property, check if this is an intended behaviour." );
            return false;
        },

        // The gameData must have this structure:
        // gameData = {
        //     cash: 0,
        //     totalBet: 0,
        //     lines: 0,
        //     denom: 0,
        //     internalBet: 0,
        //     betPerLine: 0 // internalBet * denom
        // };
        updateLocalBet: function( oLocalBet, gameData ) {

            function hasOperator( str ) {
                return str[ 0 ] === "+" || str[ 0 ] === "-";
            }
            function getOperator( str ) {
                var result;
                if( hasOperator( str ) ) {
                    result = str[ 0 ];
                }
                return result;
            }
            function getValue( str ) {
                var value;
                if( hasOperator( str ) ) {
                    value = Number( str.substring( 1 ) );
                }
                else {
                    value = Number( str );
                }
                return value;
            }

            function getNewIdx( strValue, array, currentIdx ) {
                var value;
                var operator;
                var newCurrentIdx = currentIdx;

                if( strValue !== "" ) {
                    value = getValue( strValue );
                    if( hasOperator( strValue ) ) {
                        operator = getOperator( strValue );
                        value *= ( operator === "+" ? 1 : -1 ); // increment or decrement value
                        newCurrentIdx = ( currentIdx + value ) % array.length;

                        // reposition the idx
                        newCurrentIdx = newCurrentIdx < 0 ? ( newCurrentIdx + array.length ) : newCurrentIdx;
                    }
                    else {
                        newCurrentIdx = array.indexOf( value );
                        if( newCurrentIdx === -1 ) {
                            throw new Error( "Invalid index for this value:" + strValue + " and this array: " + JSON.stringify( array ) );
                        }
                    }
                }

                return newCurrentIdx;
            }

            function updateCurrentLocalBetData() {

                var betsPerLine = SceneUtils.getBetsPerLineArray();
                var lines = SceneUtils.getLinesArray();
                var denoms = SceneUtils.getDenomsArray();

                gameData.internalBet = _.get( betsPerLine, localBets.currentBetPerLineIdx, gameData.internalBet );
                gameData.lines = _.get( lines, localBets.currentLinesIdx, gameData.lines );
                gameData.denom = _.get( denoms, localBets.currentDenomIdx, gameData.denom );

                // Update the global current denom
                RFGlobalData.game.currentDenom = gameData.denom;

                gameData.betPerLine = gameData.internalBet * gameData.denom;
                gameData.totalBet = SceneUtils.getCurrentTotalBet( oLocalBet.serverSceneId );

            }

            function iterateOverBets() {

                var betsPerLine = SceneUtils.getBetsPerLineArray();
                var lines = SceneUtils.getLinesArray();
                var denoms = SceneUtils.getDenomsArray();

                var interations = 0;
                if( strBetPerLine !== "" ) {
                    interations = betsPerLine.length;
                }
                else if( strLines !== "" ) {
                    interations = lines.length;
                }
                else if( strDenom !== "" ) {
                    interations = denoms.length;
                }

                while( interations >= 0 ) {

                    localBets.currentBetPerLineIdx = getNewIdx( strBetPerLine, betsPerLine, localBets.currentBetPerLineIdx );
                    localBets.currentDenomIdx = getNewIdx( strDenom, denoms, localBets.currentDenomIdx );
                    localBets.currentLinesIdx = getNewIdx( strLines, lines, localBets.currentLinesIdx );

                    var totalBet = SceneUtils.getCurrentTotalBet( oLocalBet.serverSceneId );
                    if( !SceneUtils.hasEnoughMoney( gameData.cash, totalBet ) ) {
                        interations--;
                    }
                    else {
                        break;
                    }
                }

                if( interations <= 0 ) {
                    console.error( "Can't get a valid bet" );
                }
            }

            function setMaximumBetFromBetPerLineIdx() {
                var betsPerLine = SceneUtils.getBetsPerLineArray();
                var lines = SceneUtils.getLinesArray();
                var denoms = SceneUtils.getDenomsArray();

                localBets.currentBetPerLineIdx = getNewIdx( strBetPerLine, betsPerLine, localBets.currentBetPerLineIdx );
                localBets.currentDenomIdx = getNewIdx( strDenom, denoms, localBets.currentDenomIdx );
                localBets.currentLinesIdx = getNewIdx( strLines, lines, localBets.currentLinesIdx );

                SceneUtils.setMaximumBet( oLocalBet.serverSceneId, gameData.cash, localBets.currentBetPerLineIdx );
            }

            var localBets = RFGlobalData.game.localBets;

            var strBetPerLine = _.get( oLocalBet, "betPerLine", "" );
            var strLines = _.get( oLocalBet, "lines", "" );
            var strDenom = _.get( oLocalBet, "denom", "" );

            if( RFGlobalData.game.useLocalBets ) {

                var hasAnOperator = hasOperator( strBetPerLine ) || hasOperator( strLines ) || hasOperator( strDenom );

                if( hasAnOperator ) {
                    iterateOverBets();
                }
                else {
                    var totalBet = SceneUtils.getCurrentTotalBet( oLocalBet.serverSceneId );
                    if( !SceneUtils.hasEnoughMoney( gameData.cash, totalBet ) ) {
                        setMaximumBetFromBetPerLineIdx();
                    }
                }

                updateCurrentLocalBetData();

                SceneUtils.onBetChanged.dispatch( oLocalBet, gameData );
            }
        },

        getTotalBetMultiplierArray: function( eServerSceneId ) {
            var DEFAULT_TOTAL_BET_MULTIPLIER = [ "BET", "DENOM", "LINES" ];
            var multiplierArray = _.get( RFGlobalData, [ "game", "gameConfig", "totalBetMultipliers", eServerSceneId ] );

            if( !multiplierArray ) {
                console.warn( "Invalid totalBetMultiplier for this eServerSceneId:", eServerSceneId );
                multiplierArray = _.get( RFGlobalData, [ "gameConfig", "totalBetMultipliers", eServerSceneId ] );
            }

            if( !multiplierArray ) {
                multiplierArray = DEFAULT_TOTAL_BET_MULTIPLIER;
                console.warn( "Using a default totalBetMultiplier: ", DEFAULT_TOTAL_BET_MULTIPLIER );
            }

            return multiplierArray;
        },

        // The oBetIdx must have this structure:
        // oBetIdx = {
        //     linesIdx: 0,
        //     denomIdx: 0,
        //     betPerLineIdx: 0,
        //     serverSceneId: "BASE_GAME"
        // };
        getTotalBetFrom: function( oBetIdx ) {

            var betsPerLineArray = SceneUtils.getBetsPerLineArray();
            var linesArray = SceneUtils.getLinesArray();
            var denomsArray = SceneUtils.getDenomsArray();
            var base = SceneUtils.getCurrentBase();

            var internalBet = _.get( betsPerLineArray, oBetIdx.betPerLineIdx );
            var lines = _.get( linesArray, oBetIdx.linesIdx );
            var denom = _.get( denomsArray, oBetIdx.denomIdx );

            var totalBet = 0;
            if( internalBet && lines && denom ) {

                var multiplyArray = SceneUtils.getTotalBetMultiplierArray( _.get( oBetIdx, "serverSceneId" ) );
                totalBet = 1;
                totalBet *= ( _.hasValue( multiplyArray, "BET" ) ? internalBet : 1 );
                totalBet *= ( _.hasValue( multiplyArray, "LINES" ) ? lines : 1 );
                totalBet *= ( _.hasValue( multiplyArray, "DENOM" ) ? denom : 1 );
                totalBet *= ( _.hasValue( multiplyArray, "BASE" ) ? base : 1 );
            }
            else {
                throw new Error( "getTotalBetFrom: Invalid values" );
            }

            return totalBet;
        },

        getCurrentTotalBet: function( serverSceneId ) {
            var localBets = RFGlobalData.game.localBets;
            var oBet = {
                betPerLineIdx: localBets.currentBetPerLineIdx,
                linesIdx: localBets.currentLinesIdx,
                denomIdx: localBets.currentDenomIdx,
                serverSceneId: serverSceneId
            };
            var totalBet = SceneUtils.getTotalBetFrom( oBet );

            return totalBet;
        },

        // update paytable
        makePayTable: function( eServerSceneId ) {
            var internalBet = SceneUtils.getCurrentInternalBet();
            var lines = SceneUtils.getCurrentLines();
            var denom = SceneUtils.getCurrentDenom();
            var base = SceneUtils.getCurrentBase();

            if( internalBet === undefined || lines === undefined || denom === undefined || base === undefined ) {
                throw new Error( "Error: some value is undefined. internalBet: " + internalBet +
                    ", lines: " + lines + ", denom: " + denom + ", base: " + base );
            }

            function processPayTable( prizeList ) {
                _.forEach( prizeList, function( oPrizes ) {
                    var multiplyArray = _.get( oPrizes, [ "multiply" ], [] );
                    var multiplyFactor = 1;
                    multiplyFactor *= ( _.hasValue( multiplyArray, "BET" ) ? internalBet : 1 );
                    multiplyFactor *= ( _.hasValue( multiplyArray, "LINES" ) ? lines : 1 );
                    multiplyFactor *= ( _.hasValue( multiplyArray, "DENOM" ) ? denom : 1 );
                    multiplyFactor *= ( _.hasValue( multiplyArray, "BASE" ) ? base : 1 );

                    oPrizes.prizes = _.mapValues( oPrizes.prizes, function( prize ) { return prize * multiplyFactor; } );

                    delete oPrizes.multiply;
                } );
            }

            var prizeList = _.cloneDeep( _.get( RFGlobalData, [ "game", "gameConfig", "payTables", eServerSceneId ] ) );
            processPayTable( prizeList );

            return prizeList;
        },

        setMaximumBet: function( serverSceneId, cash, fromBetPerLineIdx ) {
            if( !_.isNumber( cash ) || _.isNaN( cash ) ) { throw new Error( "Invalid cash: " + cash ); }
            cash = precise( cash );

            var result = true;
            var localBets = RFGlobalData.game.localBets;

            var betsPerLine = SceneUtils.getBetsPerLineArray();
            var betPerLineIdx = fromBetPerLineIdx !== undefined ? fromBetPerLineIdx : betsPerLine.length - 1;

            // Iterate from the maximum bet to the minimum until we has enough money
            while( betPerLineIdx >= 0 ) {
                var totalBet = SceneUtils.getTotalBetFrom( {
                    linesIdx: localBets.currentLinesIdx,
                    denomIdx: localBets.currentDenomIdx,
                    betPerLineIdx: betPerLineIdx,
                    serverSceneId: serverSceneId
                } );
                if( SceneUtils.hasEnoughMoney( cash, totalBet ) ) {
                    break;
                }
                betPerLineIdx--;
            }

            if( betPerLineIdx < 0 ) {
                console.error( "Can't set the maximum bet" );
                betPerLineIdx = 0;
                result = false;
            }

            localBets.currentBetPerLineIdx = betPerLineIdx;

            return result;
        },

        hasEnoughMoney: function( cash, totalBet ) {

            if( !_.isNumber( cash ) || _.isNaN( cash ) ) { throw new Error( "Invalid cash: " + cash ); }
            if( !_.isNumber( totalBet ) || _.isNaN( totalBet ) ) { throw new Error( "Invalid totalBet: " + totalBet ); }

            cash = precise( cash );
            totalBet = precise( totalBet );

            return cash >= totalBet;
        },

        isMaximumBet: function( serverSceneId, cash ) {
            var isMax = false;
            var totalBet = SceneUtils.getCurrentTotalBet( serverSceneId );
            if( SceneUtils.hasEnoughMoney( cash, totalBet ) ) {

                var localBets = RFGlobalData.game.localBets;
                var betsPerLineArray = SceneUtils.getBetsPerLineArray();

                if( localBets.currentBetPerLineIdx === betsPerLineArray.length - 1 ) {
                    isMax = true;
                }
                else {
                    var nextBetPerLineIdx = localBets.currentBetPerLineIdx + 1;
                    var oBet = {
                        betPerLineIdx: nextBetPerLineIdx,
                        linesIdx: localBets.currentLinesIdx,
                        denomIdx: localBets.currentDenomIdx,
                        serverSceneId: serverSceneId
                    };

                    totalBet = SceneUtils.getTotalBetFrom( oBet );

                    isMax = !SceneUtils.hasEnoughMoney( cash, totalBet );
                }
            }

            return isMax;
        },

        getBetsPerLineArray: function() {
            return _.get( RFGlobalData, [ "game", "gameConfig", "betsPerLine" ], [] );
        },
        getLinesArray: function() {
            return _.get( RFGlobalData, [ "game", "gameConfig", "lines" ], [] );
        },
        getDenomsArray: function() {
            return _.get( RFGlobalData, [ "game", "gameConfig", "denoms" ], [] );
        },
        getCurrentInternalBet: function() {
            return _.get( SceneUtils.getBetsPerLineArray(), RFGlobalData.game.localBets.currentBetPerLineIdx );
        },
        getCurrentLines: function() {
            return _.get( SceneUtils.getLinesArray(), RFGlobalData.game.localBets.currentLinesIdx );
        },
        getCurrentDenom: function() {
            return _.get( SceneUtils.getDenomsArray(), RFGlobalData.game.localBets.currentDenomIdx );
        },
        getCurrentBase: function() {
            return _.get( RFGlobalData, [ "game", "gameConfig", "base" ], 1 );
        },
        setDirtyLocalBet: function( bValue ) {
            RFGlobalData.game.localBets.isDirty = _.isBoolean( bValue ) ? bValue : true;
        },
        isDirtyLocalBet: function() {
            return RFGlobalData.game.localBets.isDirty;
        },

        pauseGame: function() {
            m_game.paused = true;
            SceneUtils.onGamePaused.dispatch();
        },

        resumeGame: function() {
            m_game.paused = false;
            SceneUtils.onGameResumed.dispatch();
        },

        setMuteAudio: function( bMute ) {
            if( _.isBoolean( bMute ) ) {
                var oldValue = SceneUtils.getMuteAudio();
                var newValue = bMute;
                m_game.sound.mute = bMute;
                SceneUtils.onMuteAudio.dispatch( oldValue, newValue );
            }
        },

        getMuteAudio: function() {
            return m_game.sound.mute;
        },

        volumeUp: function() {
            RFGlobalData.game.currentMasterVolumeStepIdx = ( RFGlobalData.game.currentMasterVolumeStepIdx + 1 ) % RFGameConfig.game.masterVolumeSteps.length;
            SceneUtils.setSnappedMasterVolume( SceneUtils.getCurrentMasterVolume() );
        },

        volumeDown: function() {
            RFGlobalData.game.currentMasterVolumeStepIdx = ( RFGlobalData.game.currentMasterVolumeStepIdx - 1 ) % RFGameConfig.game.masterVolumeSteps.length;
            SceneUtils.setSnappedMasterVolume( SceneUtils.getCurrentMasterVolume() );
        },

        getCurrentMasterVolume: function() {
            return _.get( RFGameConfig.game.masterVolumeSteps, RFGlobalData.game.currentMasterVolumeStepIdx );
        },

        setSnappedMasterVolume: function( value ) {

            // Gets the closest value to the volume steps
            var lastDistance;
            _.forEach( RFGameConfig.game.masterVolumeSteps, function( step, index ) {
                var distance = Math.abs( value - step );
                if( lastDistance === undefined || distance < lastDistance ) {
                    lastDistance = distance;
                    RFGlobalData.game.currentMasterVolumeStepIdx = index;
                }
            } );

            SceneUtils.setMasterVolume( SceneUtils.getCurrentMasterVolume() );
        },

        setMasterVolume: function( value ) {
            var oldValue = m_game.sound.volume;
            m_game.sound.volume = value;
            SceneUtils.onMasterVolumeChanged.dispatch( oldValue, value );
        },

        sendSBGGameConfiguration: function() {
            if( RFGlobalData.sbg.enableToSendGameConfigurationToSocket ) {
                sendToSocket( {
                    type: "GAME_CONFIGURATION",
                    data: {
                        method: "PUT",
                        content: RFGlobalData.sbg.gameConfiguration
                    }
                } );
            }
        },

        sendExitToLobby: function() {
            if( RFGameConfig.sbg.enable ) {
                var toSocket = {
                    type: "EXIT",
                    data: null
                };
                sendToSocket( toSocket );
            }
        },

        onGamePaused: new Phaser.Signal(),
        onGameResumed: new Phaser.Signal(),
        onMasterVolumeChanged: new Phaser.Signal(),
        onBetChanged: new Phaser.Signal(),
        onMuteAudio: new Phaser.Signal()
    };

    // ==============================================================
    //  END UTILS AND HELPERS SECTION
    // ==============================================================

    var onMasterVolumeChanged = function( /* oldValue, newValue */ ) {
        RFGlobalData.sbg.gameConfiguration.volume = SceneUtils.getCurrentMasterVolume();
        SceneUtils.sendSBGGameConfiguration();
    };

    var onBetChanged = function( /* oLocalBet, gameData */ ) {
        RFGlobalData.sbg.gameConfiguration.totalBet = precise( SceneUtils.getCurrentTotalBet( eServerSceneId.BASE_GAME ) );
        SceneUtils.sendSBGGameConfiguration();
    };

    var onMuteAudio = function(  /* oldValue, newValue */ ) {
        RFGlobalData.sbg.gameConfiguration.mute = m_game.sound.mute;
        SceneUtils.sendSBGGameConfiguration();
    };

    var onBeforeUnloadWindow = function() {
        if( _.has( self, [ "game", "sound", "destroy" ] ) ) {
            self.game.sound.destroy();
        }
    };
    var onUnloadWindow = function() {
        if( _.has( self, [ "game", "sound", "destroy" ] ) ) {
            self.game.sound.destroy();
        }
    };

    var onStateChange = function( currentStateKey, previousStateKey ) {

        if( RFGameConfig.sbg.screenId !== eScreenId.BASE ) { return; }

        if( self.myScene === previousStateKey ) {
            console.log( "onStateChange: currentStateKey", currentStateKey, "previousStateKey", previousStateKey );
            sendToSocket( {
                type: "CHANGE_BASE_SCENE",
                data: {
                    currentStateKey: currentStateKey,
                    previousStateKey: previousStateKey
                }
            } );
        }
    };

    var checkMySceneKey = function() {
        if( self.myScene === undefined || self.myScene === CONST.UNDEFINED_SCENE ) {
            addSceneKeysToSceneInstances( self.game );
        }
    };

    // -----------------------------------------------------
    var m_game = game;
    var m_rfLoadScenes = null;
    var m_rfLoadAssets = null;
    var m_websocketHandler = null;

    var STR_INTRO_POSTER = "INTRO_POSTER";
    var STR_OUTRO_POSTER = "OUTRO_POSTER";

    if( !isValidGame( m_game ) ) { throw new Error( "BaseScene: game is not an Phaser game" ); }

    // binds ----------------------------------------------------
    SceneUtils.onMasterVolumeChanged.add( onMasterVolumeChanged );
    SceneUtils.onMuteAudio.add( onMuteAudio );
    SceneUtils.onBetChanged.add( onBetChanged );

    m_game.state.onStateChange.add( onStateChange );

    // -----------------------------------------------------
    // EVENTO PARA CAPTURAR LA DESCONEXIÓN DE RED
    // -----------------------------------------------------
    try {
        window.addEventListener( "offline", function( /* e */ ) {
            if( !( ( game.state.getCurrentState() instanceof ReconnectionScene ) ||
                ( game.state.getCurrentState() instanceof PreloadScene ) ||
                ( game.state.getCurrentState() instanceof BootScene )
            ) ) {
                game.state.start( eSceneNames.RECONNECTION );
            }
        } );
        window.addEventListener( "online", function( ex ) {
            console.log( ex );
        } );
    }
    catch( ex ) { /* */ }

    // Evento de cierre de ventanas
    try {
        window.onbeforeunload = onBeforeUnloadWindow;
        window.onunload = onUnloadWindow;
    }
    catch( ex ) { /* */ }


    _protected.SceneUtils = SceneUtils;

    if( self.constructor !== BaseScene ) { return _protected; }
    return undefined;
};



//------------------------------------------------------
// Source: src/sbg/game/scenes/rf.scene.basegame.js
//------------------------------------------------------

/* eslint-disable max-lines */

/* jshint -W087 */ // Ignore debugger

/* eslint-disable max-len */
// #region @version CambiosCUSTOM SBG 0.3.0c
/**
 * CAMBIOS:
 * 0.3.0c   - Se añade el envío del inicio del juego a homologación.
            - Se añade el sonido de apuesta máxima.
 * 0.2.0c   - Se añade control de visualización de premios con BigPrize y carrusel.
 * 0.1.144c - Si se está mostrando un premio, se evita que comience una partida de forma automática, por tiempo de inactividad.
 *          - Se libera el sprite del m_fadeSprite al salir de la escena.
 *          - Se manda el betUid y el winUid para el SAS.
 *          - Se registra a la señal de onGameResumed para actualizar el último valor de cash.
 *          - Eliminación del acceso del objeto global "game".
 *          - Refactorización de código:
 *             - Se llama drawCash en lugar de estar copiando el código repetidamente.
 *             - Se crea una función para actualizar el tiempo de pulsación al momento actual.
 *          - Se corrigen algunos errores ortográficos.
 * 0.1.143c - Se modifica la manera en la que se construye la tabla de pagos, ahora se tiene en cuenta si es por apuesta local o de manera tradicional
 * 0.1.142c - Se extrae el valor de keybinding sin usar valores por defecto. Se elimina la cancelación del cobro.
 * 0.1.141c - Se añade la apuesta base como multiplicador para las apuestas locales.
 * 0.1.140c - Se ha modificado el método stopSpin para que tenga en cuenta el máximo de prizeHype
 *           que pueden aparecer en este spin (para parar a tiempo la estela de la emoción)
 * 0.1.130c - Se restaura el nombre de las señales
 * 0.1.129c - Se resetea el tiempo de inactividad ante cualquier pulsación de botón.
 * 0.1.128c - Gestión de volumen.
 * 0.1.127c - Gestión de apuestas locales.
 * 0.1.126c - Se hace que el botón de apuesta máxima no realice el spin.
 * 0.1.125c - Se elimina el control de apuesta mínima para pasar a la lotería.
 * 0.1.124c - Al retornar de un spin, si la apuesta total ha cambiado, situación que ocurre en caso de que no tuviera créditos para realizar la apuesta actual
 *            y el servidor cambie la denominación, se solicita al servidor la paytable actualizada para informar al juego superior.
 * 0.1.123c - Controla que antes de iniciar una partida automática, no se encuentre con en modo auto activado.
 *          - Para evitar saturar el websocket, no se envía el estado de los botones cuando éstos no hayan cambiado el estado respecto a la actualización anterior.
 *          - Se eliminan los mensajes de recepción de los sockets, puesto que el wrapper ya los pinta.
 * 0.1.122c - En configuración se puede indicar con showBigPrizesBeforeCarrousel si se quiere que el bigprize se enseñe antes de que acabe el carrusel de premios
 * 0.1.121c - El botón de apuesta borra el mensaje del label principal.
 *          - Se hace visible o invisible el botón de juego automático si se pulsa sobre su contrario
 * 0.1.120c - Se añade setFormattedString para el mensaje de buena suerte.
 * 0.1.119c - Para crear la variable del localStorage donde se guarda el resultado del último giro se usa el gameName en vez de el gameId
 * 0.1.118c - Se cambia el scheduleNextSpin para que se siga el juego en automático cuando ha tocado un premio
  * 0.1.117c - Se manda el sendActiveButtons con el botón de juego automático solo si el jugador tiene saldo suficiente
 * 0.1.116c - Dentro de fAfterEndFirstLoop esperamos el mismo tiempo para acabar el tween del transvase de premios a créditos para poner la variable m_isShowingPrizes a false
 *          - Ahora se usa el identificador del juego para guardar los rodillos en el localStorage
 * 0.1.115c - Se realiza un removeAll a los signals en el create, para que no se añadan varias veces la misma función a los mismos si se cambia de escena
 * 0.1.114c - Cuando el jugador tiene 0.10 de saldo, ya no se entra en el Doble o nada a sugerencia de Enrique
 * 0.1.113c - Se comprueba que exista m_phaserImagesAuto.imgLabel antes de hacer nada con él
 * 0.1.112c - Se escucha por socket BALANCE_DECREASE
 * 0.1.111c - Envía el texto formateado en la señal de onStartPrizeLine.
 * 0.1.110c - El signal m_self.onStartSpin.dispatch(); se lanza en el startSpin
 * 0.1.109c - Se guarda en el localStorage los últimos rodillos para que no se enseñe el cover al iniciar el juego
 * 0.1.108c - Creamos una signal para los big prizes
 * 0.1.107c - Se quita m_audioSounds.limitExceeded.play(); de checkMoney, porque en ocasiones sonaba muchas veces
 *          - Dentro de updateData también se modifica el valor del playerData.cash.
 * 0.1.106c - Dentro de stopAutoGames: if( playerData > 0 ) { sendActiveButtons(); }
 * 0.1.105c - Cuando se inserta una moneda se envían los botones encendidos
 * 0.1.104c - En la función turnOffButtonsWithoutCash se envía como botón encendido únicamente el EXIT
 * 0.1.103c - Ahora se añade el incremento de saldo del jugador cuando los rodillos han parado
 * 0.1.102c -  playerData.balanceIncreased para sumar el dinero que mete el jugador mientras giran los rodillos
 * 0.1.101c - En el BALANCE_INCREASE se añade el value al data (que es lo que se espera recibir)
 * 0.1.100c - Se acepta tanto BALANCE_INCREASE como INCREASE_BALANCE para sumar monedas
 * 0.1.99c - Se muestran las líneas al ser pulsadas y realizan un fadeout cuando se dejan de pulsar.
 * 0.1.98c - Se recoge en el playerData los valores de numLines y betPerLine para que funcione el checkMoney si la respuesta nos llega desde el sendGetBalance
 * 0.1.97c - Cuando hay saldo suficiente para jugar, se quita el texto de Límite excedido
 * 0.1.96c - Se crea la función turnOffButtonsWithoutCash, que apaga las luces de la botonera si no hay saldo
 *         - Se cambia el checkMoney para que, si no tiene el wsResponse, se fíe de los datos recogidos en playerData
 * 0.1.95c - Se corrige un fallo del getLotteryUrl que comprueba si se está en producción o no.
 *         - Se apagan los botones si no hay saldo en Idle
 * 0.1.94c - Cuando se pulsa el botón de forzar partidas, se comprueba con isProductionServer si se está en producción. De ser así, no se hace nada.
 * 0.1.93c - Se crea un updateCashByGS, que recibe los datos haciendo un command al servidor. Útil cuando se llega del Doble o Nada
 *         - En el updateCashBySocket se comprueba que, si el saldo del jugador es igual al LOTTERY_MINIMUM_CASH, se lanza el Doble o Nada.
 * 0.1.92c - No se reproduce el sonido de error cuando se está en Doble o Nada
 *         - Cuando se vuelve del Doble o Nada se vuelve a actualizar los datos
 * 0.1.91c - Se coloca game.stage.disableVisibilityChange = false para hacer que el juego se quede en pausa cuando se pierde el foco (para el Doble o Nada, por ejemplo)
 * 0.1.90c - Corregido lolofallo idiota que devolvía la Bichos, en vez del Doble o Nada, cuando se jugaba en producción
 * 0.1.89c - Si se recibe BALANCE_INCREASE por el socket, el saldo del jugador se incrementa.
 *         - Se elimina el 'BALANCE', puesto que con lo anterior ya no es necesario
  * 0.1.88c - Se añade el sendGetBalance para recoger el dinero del jugador.
 *         - Se quita la comprobación del error de concurrencia, puesto que, con lo anterior, ya no se usa.
 * 0.1.86c - Se comprueba si el error que ha llegado es un 429. Si es ese error (llamadas concurrentes), el juego sigue funcionando.
 *         - Si el dinero introducido es igual que el valor mínimo para entrar al Doble o Nada, se entra en el Doble o Nada
 * 0.1.84c - La variable urlToLaunch para cargar el Doble o Nada incluye &local=true
 * 0.1.83c - Chapuzada en el getLotteryUrl para comprobar si estamos en demo o producción
 * 0.1.82c - Se cambia el getLotteryUrl para ver si el doble o nada está en demo o en producción
 *         - Se comprueba que el valor enviado por el balance es numérico
 *         - Para evitar llamadas concurrentes, se comenta el UPDATE_BALANCE
 * 0.1.81c - Leemos el UPDATE_BALANCE y el BALANCE
 * 0.1.80c - Por si no existe, se usa var m_msToExitWithNoCash = _.get( RFGameConfig, 'game.msToExitWithNoCash', 30000 );
 * 0.1.79c - Cuando se entra en doble o nada se apagan todas las luces de los botones
 * 0.1.78c - Cuando el jugador no tiene saldo, se lanza un sonido de error (m_audioSounds.limitExceeded.play();)
 * 0.1.77c - Se usa la tecla W para comprobar si el saldo se actualiza
 * 0.1.76c - Se vuelve a colocar 'BALANCE' porque se han de hacer cambios en el lobby
 * 0.1.75c - Se añade el 'UPDATE_BALANCE' y se quita el 'BALANCE' para recibir una actualización de saldo del jugador
 * 0.1.74c - Cambio en el onBtnSpin para que se pueda acceder al doble o nada si el jugador tiene 0.10€
 * ------------------------------------------------------
 * 0.1.10 - Se añade el forceOut a los botones dentro del addActionButtons
 * 0.1.11 - Se cambia el sendCommandHistory por sendCommandHistoryRequest
 * 0.1.12 - Se llama a updateGlobalData de SceneUtils y se inicializan las labels para la gestión de moneda y créditos (requiere versión >= 0.0.25 de BaseScene)
 * 0.1.14 - Se inicializa la visibilidad de las líneas y los skittles.
 * 0.1.15 - Se inicializan los keybindings vía config (requiere versión >= 0.0.27 de BaseScene y >= 0.5.4 de RFGameConfig)
 * 0.1.16c - Se hace desaparecer el cartel de denom y la pulsación de líneas cuando el juego es sbg
 * 0.1.17c - Se adapta a slo2game
 * 0.1.18c - Se eliminan botones y etiquetas innecesarias cuando se juega en sbg
 * 0.1.19c - Se añade updateCashOnSpin para que se descuente dinero nada más comenzar a jugar
 * 0.1.20c - Si se recibe la respuesta con dinero tras una tirada, y ésta no ha resultado premiada, se cambia el label del cash del jugador. Si no, dicho label se actualiza solo tras actualizar el premio
 * 0.1.21c - Se añade la función connectToSocket para conectarse al socket cuando es una sbg con doble monitor
 * 0.1.22c - Cuando el juego recibe un UPPER_GAME_CONNECTED vía socket, le manda la tabla de pagos que se guarda en playerData
 * 0.1.23c - El juego transita a la escena de bonos FRUIT_SELECTION
 * 0.1.24c - Se muestra el forzado en pantalla durante debug
 * 0.1.25c - Juego automático
 * 0.1.26c - Se cambia el tiempo del label cash a 0 para que no reproduzca ningún sonido cuando no hay premio
 * 0.1.27c - En el exitInit se manda por socket un "START" para que la pantalla superior sepa que se ha iniciado el juego
 * 0.1.28c - Añadida la función updateCashBySocket, para pintar el dinero que tiene el jugador enviado vía socket
 * 0.1.29c - Se corrige un error a la hora de recibir los datos del BALANCE vía socket, y se añade el dinero que se coje mediante esta vía al player.cash
 * 0.1.30c - Se cambia player.cash a playerData.cash, y se escucha también el UPPER_GAME_CONNECTED vía json
 * 0.1.31c - Llamada al servidor cuando se recibe una actualización de saldo vía socket
 * 0.1.32c - Pintamos lo obtenido de la actualización del dinero del jugador mediante cket
 * 0.1.33c - Dentro del updateData se añade al player el valor del cash si éste no viene
 * 0.1.34c - Permitimos colores con el setformatedString en la barra de mensajes secundarios
 * 0.1.35c - En changeStateByServerSceneId se crean tres CUSTOM_GAME
 * 0.1.36c - Añade la visualización de premio por línea
 * 0.1.37c - En el arranque comprueba la variable global residualPrize, por si se ha vuelto del juego de bonos y hay que sumar el dinero al contador del jugador
 * 0.1.38c - Se añade el pintado de créditos residuales
 * 0.1.39c - Se renombra el horroroso startStarting por startInit
 * 0.1.40c - Se crea el estado WAITING_RESIDUAL al que se transita si hay dinero que provenga del juego de bonos. Se puede cambiar por configuración en
 *           RFGameConfig.game.waitForResidual
 * 0.1.41c - Se crea un PRE_IDLE para evitar (o no, depende de la configuración) que se puedan pulsar los botones durante el carrusel de premios
 * 0.1.42c - Cuando acaba el carrusel de premios, se decrementa el valor de premios y se incrementa el de créditos. Está ñapeado de manera rápida.
 * 0.1.43c - Se recoge el número de juegos automáticos restantes de RFGlobalData.game.autoGameMode.numAutoGames
 * 0.1.44c - Cambios para adaptarse al wrapper del socket. Se quita los stringifaos porque el wrapper, que es muy bueno, lo hace solito
 *         - Botones de cobrar y salir.
 * 0.1.45c - Se modifica la llamada de setFormatedString a setFormattedString
 * 0.1.46c - Muestra el mensaje de inténtelo otra vez cuando no se obtiene premio
 * 0.1.47c - Se añaden (a pelo, fuera de la configuración), los botones C para hacer el cashout y X para el exit.
 * 0.1.48c - Se mejora la forma de colocar los keyBindings para cobrar y salir
 * 0.1.49c - Se cambia la manera en la que se controlan los tiempos de espera cuando se está en automático
 * 0.1.50c - Se crea una nueva función (onBtnToogleAutogame) que se podrá llamar cuando se pulse el botón A. Arranca o para el juego automático. Solo para SBG.
 * 0.1.51c - Se permite que el juego pueda volver del estado "CASH_OUT"
 * 0.1.52c - Se mandan por socket los botones activos para que se iluminen en la botonera
 * 0.1.53c - Si el jugador tiene dinero, comienza a jugar el juego automáticamente según los milisegundos definidos en RFGameConfig.game.msToStartReelsWithCash
 * 0.1.54c - Se añade el sonido de Límite excedido cuando se intenta jugar y no se disponen de créditos suficientes.
 *         - Se formatea el texto secundario con colorines cuando se enseña Límite excedido
 *         - Se cambia el checkMoney para que no elimine el texto secundario tras cambiar la apuesta y venir de un "límite excedido"
 * 0.1.55c - El botón 'STOP_AUTO' se convierte en 'AUTO_X' para el control de luces de la botonera
 *         - Se crea la función turnOffButtons para apagar todos los botones físicos
 *         - Se clonan los botones en sendActiveButtons para que no se sobreescriban los valores del automático
 * 0.1.56c - Si el jugador no tiene dinero y pasan msToExitWithNoCash milisegundos, se sale del juego
 * 0.1.57c - Se sobrescribe la función SceneUtils.fERROR para que envíe el juego directamente al lobby ante un error
 *         - Cuando se pulsa jugar y se tiene 0.10€, se abre el juego Doble o Nada (LOTTERY)
 *         - Se recibe el comando para salir del estado LOTTERY
 *         - Creación de varias regiones para que el código quede más bonito, y corrección de errores menores.
 * 0.1.58c - Se crea un signal (onStopSpin) que se llama cada vez que terminan de girar los rodillos. Útil para añadir funcionalidades mediante punchs
 * 0.1.59c - Con el botón de cancelar autoGames se debería apagar el botón físico
 * 0.1.60c - Se crea la señal onStartPrizeLine que se llama cuando se muestra una línea con premio
 * 0.1.61c - Se cambia el mensaje que se manda vía socket cuando el jugador tiene 0,10€ de LOTTERY a OPEN_LOTTERY
 * 0.1.62c - Se lanza el juego de Doble o Nada sin comprobar si tiene saldo suficiente para la partida o no
 * 0.1.63c - Si se está en el estado LOTTERY (Doble o nada) y se introduce una moneda, al socket se manda que cierre dicho juego.
 * 0.1.64c - Solo se lanza el doble o nada si el jugador tiene dinero.
 * 0.1.65c - Nuevas signals: onStartSpin y onSpinResponse
 * 0.1.66c - Se lanza el Doble o Nada transcurrido un tiempo.
 *         - Se actualiza el saldo tras volver del Doble o Nada
 * 0.1.67c - Se crea la función updateCash para actualizar el dinero que tiene el jugador en el game server. Se usa, tanto para la vuelta del Doble o Nada como para cuando alguien introduce monedas.
 * 0.1.68c - El updateCash se llama cuando se sale del estado Lottery (exitLottery)
 * 0.1.69c - Cuando se mete una moneda se resetea el contador para que el juego comience automáticamente
 *         - Si el jugador no tiene dinero, no se intenta comenzar jugar y se sale de la partida
 * 0.1.70c - La acción onBtnCashOut solo funciona si el cliente tiene dinero
 * 0.1.71c - Cuando la apuesta es mayor que el dinero que tiene el jugador, no se hace nada
 * 0.1.72c - Se resetea el tiempo cada vez que hay un onBtnSpin
 * 0.1.73c - Corrección de un problema menor (para forzar el que no tuviese saldo el jugador se había colocado a pelo)
 * 0.1.74c - Se refactoriza la función isProductionServer a SceneUtils.checkEnvironment( environmentId ) que retorna true si el entorno indicado es el actual, sino false.
 * 0.1.75c - Se hacen los ajustes para integrar los cambios en el sonido de la estela de la emoción del slo2engine 0.21.0
  */
/* eslint-enable max-len max-lines */
// #endregion

// eslint-disable-next-line no-implicit-globals, max-statements
var BaseGameScene = function( game ) {
    "use strict";

    // Call to base Scene Constructor
    var _protected = BaseScene.call( this, game );

    var m_super = this;
    var SceneUtils = _protected.SceneUtils;

    var self = this;
    var m_rfLoader = null;
    var m_rffsm = null;
    var m_phaserButtons = null;
    var m_phaserSkittels = null;
    var m_phaserLines = null;
    var m_phaserLabels = null;
    var m_phaserLabelsAuto = null;
    var m_phaserImagesAuto = null;
    var m_phaserButtonsAuto = null;
    var m_phaserButtonAuto = null;
    var m_phaserButtonsAutoX = []; // buttons with autoX action
    var m_phaserButtonsDenom = []; // buttons with change denom action
    var m_phaserButtonsDouble = []; // buttons double + takewin actions
    var m_phaserVolumeButtons = null;
    var m_fadeSprite = null;

    var m_texts = {};
    var m_waitingToNextAutoGameTimer = null;
    var m_autoGameMenuVisible = false;
    var m_autoGameMode = false;
    var m_numAutoGames = 0;
    var m_reels = null;
    var m_audioSounds = {
        bonus: new RFAudioSound( game ),
        bkgBasegame: new RFAudioSound( game ),
        lineUp: new RFAudioSound( game ),
        betUp: new RFAudioSound( game ),
        betMax: new RFAudioSound( game ),
        denomUp: new RFAudioSound( game ),
        insufficientFounds: new RFAudioSound( game ),
        limitExceeded: new RFAudioSound( game ),
        cantSpin: new RFAudioSound( game ),
        bigPrize: new RFAudioSound( game )
    };
    var m_canSpin = false;
    var m_takeWinFromSpinButton = false;
    var m_waitResidualPrizeTimer = null;

    var lbl_win = null;

    // Datos del jugador para poder restar el dinero de su cuenta antes de que llegue la respuesta del servidor.
    // También se guarda la última tabla de pagos para poder mandarla a la pantalla superior cuando ésta se conecte
    /* var LOTTERY_MINIMUM_CASH = 0.1; */
    var m_data = {
        isCashDirty: false,         // Indica si el valor de cash no es el definitivo y ha podido cambiar entre operaciones (por ejemplo al dar un Jackpot)
        cashValueWhenIsDirty: -1,   // Valor del dinero cuando se modificó entre operaciones (-1 indica que no tiene un valor válido)
        cash: 0,
        totalBet: 0,
        lines: 0,
        denom: 0,
        internalBet: 0,
        betPerLine: 0.2, // internalBet * denom
        payTable: null,
        msLastPressedButton: 0,
        balanceIncreased: 0,
        lastSpinResponse: null,
        startCarrouselTime: 0
    };

    var m_lastButtonListSent = null;

    var socket_handler = null;

    // Force reels
    var m_oForceReels = {
        forceNextGame: false, // next game is forced
        forcedGamesArray: [],
        currentIdx: -1 // current idx of the forcedGamesArray
    };

    var m_specialForceReels = [];

    // var m_lastRFSymbolsMatrix = null;

    var DEFAULTS = {
        MS_TWEEN_COUNTER_PRIZE_LINES: 500,
        MS_TWEEN_COUNTER_WIN: 500,
        MS_TWEEN_COUNTER_CREDITS: 500,
        MS_TWEEN_COUNTER_RESIDUAL_CREDITS: 1000,
        MS_TO_START_THE_TWEEN_COUNTER_RESIDUAL_CREDITS: 500,
        MS_MIN_SPIN_TIME: 3000,
        MS_STOP_REEL: 300,
        Y_BOUNCE_OFFSET: 20,
        MS_BETWEEN_STOPS: 100,
        MS_WAIT_RESIDUAL: 1000,
        MS_TO_EXIT_WITH_NO_CASH: 30000,
        MS_FADEOUT_LINE: 2000,
        MS_MIN_TO_START_NEW_GAME_IN_AUTO_GAME_WITH_PRIZE: 1000,
        MS_MIN_TO_START_NEW_GAME_IN_AUTO_GAME_WITH_NO_PRIZE: 500,
        MS_MIN_TO_ENTER_IN_FREEGAMES: 1000,
        BET_PER_LINE: 0.2,

        MS_FOR_REEL_START_SPIN: 150,
        OFFSET_REEL_SPIN_SPEED: 100,
        REEL_SPIN_SPEED: 300,

        MS_PRIZE_HYPE_DELAY: 500,
        MS_PRIZE_HYPE_DELAY_MULTIPLIER: 3
    };
    Object.freeze( DEFAULTS );

    var DISABLE_OPACITY = 0.4;
    var MS_FADEOUT_LINE = 2000;
    var MS_TWEEN_COUNTER_PRIZE_LINES = _.get( RFGameConfig, "game.msTweenCounterPrizeLines", DEFAULTS.MS_TWEEN_COUNTER_PRIZE_LINES );
    var MS_TWEEN_COUNTER_WIN = _.get( RFGameConfig, "game.msTweenCounterWin", DEFAULTS.MS_TWEEN_COUNTER_WIN );
    var MS_TWEEN_COUNTER_CREDITS = _.get( RFGameConfig, "game.msTweenCounterWin", DEFAULTS.MS_TWEEN_COUNTER_CREDITS );
    var MS_TWEEN_COUNTER_RESIDUAL_CREDITS = _.get( RFGameConfig, "game.msTweenCounterResidualCredits", DEFAULTS.MS_TWEEN_COUNTER_RESIDUAL_CREDITS );
    var MS_TO_START_THE_TWEEN_COUNTER_RESIDUAL_CREDITS = _.get(
        RFGameConfig, "game.msToWaitToStartTheTweenCounterResidualCredits", DEFAULTS.MS_TO_START_THE_TWEEN_COUNTER_RESIDUAL_CREDITS
    );
    var SBG_NUM_AUTOGAMES = 100;

    var m_isShowingPrizes = false;
    var m_isShowingFirstLoopCarrousel = false;

    // 0.1.75c - Mejoras sonido
    var MS_SOUND_FADE = _.get( RFGameConfig, "game.msSoundFade", DEFAULTS.MS_SOUND_FADE );
    var SOUND_VOLUME_MIN = _.get( RFGameConfig, "game.soundVolumeMin", DEFAULTS.SOUND_VOLUME_MIN );
    var SOUND_VOLUME_MAX = _.get( RFGameConfig, "game.soundVolumeMax", DEFAULTS.SOUND_VOLUME_MAX );

    var m_homologationStartGameSent = false;

    // #region signals
    this.onStopSpin = new Phaser.Signal();
    this.onStartPrizeLine = new Phaser.Signal();
    this.onSpinResponse = new Phaser.Signal();
    this.onStartSpin = new Phaser.Signal();
    this.onBigPrize = new Phaser.Signal();
    this.onCancelBigPrize = new Phaser.Signal();
    // #endregion

    this.nextReelIdsToSpinArray = [];

    /**
     * Exports an utility function to set force the reels
     *
     * @param {array} array Array of integers with the server reel position
     */
    this._specialForceReels = function( array ) {

        // Check errors
        if( !RFGameConfig.game.canBeForced ) {
            console.error( "FORCE REELS: Can't be forced" ); /* RemoveLogging:skip*/
            return false;
        }
        if( !_.isArray( array ) ) {
            console.error( "FORCE REELS: Needs an array of integers" ); /* RemoveLogging:skip*/
            return false;
        }
        if( array.length !== m_reels.getNumberOfReels() ) {
            console.error( "FORCE REELS: Invalid length of reels" ); /* RemoveLogging:skip*/
            return false;
        }

        // Reset
        m_specialForceReels = {};

        for( var i = 0; i < array.length; ++i ) {
            var value = array[ i ];
            if( !_.isInteger( value ) || value < 0 ) {
                console.error( "FORCE REELS: All values in the array needs to be integer and >= 0" ); /* RemoveLogging:skip*/
                m_specialForceReels = {};
                return false;
            }
            m_specialForceReels[ "reel" + ( i + 1 ) ] = value;

        }

        return true;
    };

    this.shutdown = function() {

        // call to parent
        m_super.shutdownBase();

        var destroyChildren = true;
        var destroyTexture = true;
        m_fadeSprite.destroy( destroyChildren, destroyTexture );

    };

    this.preload = function() {

        // call to parent
        m_super.preloadBase();

    };

    this.create = function() {
        // Se eliminan los signals
        this.onStopSpin.removeAll();
        this.onStartPrizeLine.removeAll();
        this.onSpinResponse.removeAll();
        this.onStartSpin.removeAll();
        this.onBigPrize.removeAll();

        this.nextReelIdsToSpinArray = [];

        // call to parent
        m_super.createBase();

        // creamos la máquina de estados
        m_rffsm = new RFFsm();

        // iniciamos la máquina de estados con los estados definidos
        m_rffsm.initialize( m_fsmInitData );

        // ponemos el estado inicial
        m_rffsm.changeState( eStateNames.INIT );

        // Para que el juego entre en pausa si se carga el Doble o Nada
        // game.stage.disableVisibilityChange = false;

        // Si el localStorage tiene ya unos rodillos, pintamos esos y no enseñamos el cover
        if( !RFGameConfig.game.maintainReelSymbolsAfterTransition && localStorage.getItem( "latestDataBase" + RFGameConfig.gameName ) ) {
            try {
                var data = atob( localStorage.getItem( "latestDataBase" + RFGameConfig.gameName ) );
                SceneUtils.setLastRFSymbolsMatrix( SceneUtils.getRFSymbolsMatrix( JSON.parse( data ) ) );
                m_reels.setReels( SceneUtils.getLastRFSymbolsMatrix() );
                SceneUtils.hideCovers( 1 );
            }
            catch( ex ) {
                console.warn( "Error in latestData stored in localStorage", ex );
            }
        }

        this.onSpinResponse.add( function( spinResponse ) {
            var parameters = {
                "betAmount": spinResponse.player.totalBet,
                "betUid": _.get( spinResponse.player.round, [ "betUid" ], "" )
            };
            SceneUtils.handleRoundStart( parameters );
        } );

        // El servicio de retail api necesita inicializar la comunicación con un mensaje de end.
        if( SceneUtils.isCurrentEnvironmentEqualsTo( eEnvironments.HOMOLOGATION ) ) {
            if( RFGlobalData.net.retailNotificationWrapper && !m_homologationStartGameSent ) {

                var _onError = function( response ) {
                    var FORBIDDEN = 403;
                    if( _.get( response, [ "status" ] ) === FORBIDDEN ) {
                        console.info( "HOMOLOGATION is FORBIDDEN" );
                        RFGlobalData.net.retailNotificationWrapper.setHomologationEnabled( false );
                    }
                };

                m_homologationStartGameSent = true;
                console.info( "%cSTART GAME (HOMOLOGATION)", RFDebugUtils.getConsoleColors().ROUND );
                RFGlobalData.net.retailNotificationWrapper.sendStartGameRequest( { status: "open" }, _.noop, _onError );
            }
        }
    };

    // #region Transitions
    var transitionOut = function() {
        if( self.game.transitions ) {
            self.game.transitions.animation( {
                animation: "trans_out",
                fps: 21,
                killOnComplete: true,
                scale: 1.7
            } );
        }
        else {
            SceneUtils.fadeOut( m_fadeSprite );
        }
    };
    var transitionIn = function( sceneName ) {
        RFGlobalData.net.wsWrapper.unsubscribe( socket_handler );
        if( self.game.transitions ) {
            self.game.transitions.animation( {
                animation: "trans_in",
                scale: 1.7,
                fps: 21,
                onComplete: function() {
                    self.game.state.start( sceneName, false );
                }
            } );
        }
        else {
            self.game.state.start( sceneName, false );
        }
    };
    // #endregion

    var startInit = function() {

        m_lastButtonListSent = null;

        // creacion de la escena
        loadScene();

        // black screen
        m_fadeSprite = SceneUtils.createFadeSprite();
        if( !self.game.transitions ) {
            SceneUtils.fadeIn( m_fadeSprite, 0 );
        }
        transitionOut();

        // recogemos los botones para añadirle su action
        addActionsButtons( m_phaserButtons );
        addActionsButtons( m_phaserButtonsAuto );

        // recogemos los skittels para añadirle sus actions
        addActionSkittels();

        // Keyboard bindings
        addKeyBindings();

        // inicialización aleatoria de rodillos
        m_reels.setOptions( {
            msStopReel: _.get( RFGameConfig.game, "msStopReel", DEFAULTS.MS_STOP_REEL ),
            yBounceOffset: _.get( RFGameConfig.game, "yBounceOffset", DEFAULTS.Y_BOUNCE_OFFSET ),
            msBetweenStops: _.get( RFGameConfig.game, "msBetweenStops", DEFAULTS.MS_BETWEEN_STOPS ),

            msForReelStartSpin: _.get( RFGameConfig.game, "msForReelStartSpin", DEFAULTS.MS_FOR_REEL_START_SPIN ),
            offsetReelSpinSpeed: _.get( RFGameConfig.game, "offsetReelSpinSpeed", DEFAULTS.OFFSET_REEL_SPIN_SPEED ),
            reelSpinSpeed: _.get( RFGameConfig.game, "reelSpinSpeed", DEFAULTS.REEL_SPIN_SPEED ),

            symbolWeights: _.get( _.get( RFGameConfig.game, "symbolWeights", null ), "BASE_GAME", null ),
            prizeHype: _.get( RFGameConfig.game, "prizeHype", null ),

            prizeHypeDelay: _.get( RFGameConfig.game, "prizeHypeSoundDelay", DEFAULTS.MS_PRIZE_HYPE_DELAY ),
            prizeHypeDelayMultiplier: _.get( RFGameConfig.game, "prizeHypeDelayMultiplier", DEFAULTS.MS_PRIZE_HYPE_DELAY_MULTIPLIER ),
            prizeHypeFadeAnim: _.get( RFGameConfig.game, "prizeHypeFadeAnim", DEFAULTS.PRIZEHYPEANIM_FADE ),
            prizeHypeDelayToStopAnim: _.get( RFGameConfig.game, "prizeHypeDelayToStopAnim", DEFAULTS.PRIZEHYPEANIM_DELAYTOSTOP ),
            prizeHypeSoundOnlyOne: _.get( RFGameConfig.game, "prizeHypeSoundOnlyOne", false ),

            startExtendedWildsAtSameTime: _.get( RFGameConfig.game, "startExtendedWildsAtSameTime", true ),
            playSounds: true
        } );
        SceneUtils.setLastRFSymbolsMatrix( SceneUtils.getLastRFSymbolsMatrix() || getRandomMatrixRFSymbols() );

        m_audioSounds.bkgBasegame.playAll();


        // deshabilitamos los botones
        SceneUtils.setButtonEnable( m_phaserButtons, false );
        SceneUtils.setButtonEnable( m_phaserButtonsAuto, false );

        // Paramos los juegos automáticos
        if( !_.get( RFGlobalData, "game.autoGameMode.enable", false ) ) { stopAutoGames(); }

        // "deshabilitamos" líneas y skittels
        lineVisible( 0 );
        // skittelVisible( 0 );

        if( RFGameConfig.game.canBeForced ) {
            getForcedGames( reconnection );
        }
        else {
            reconnection();
        }

        // Label Win
        lbl_win = SceneUtils.filterByType( m_phaserLabels, eLabelsNames.msgWin );


        m_isShowingPrizes = false;

        var wsWrapper = RFGlobalData.net.wsWrapper;
        socket_handler = wsWrapper.subscribe( socketReception );

    };

    // #region SOCKET COMMUNICATION
    var socketReception = function( data ) {

        var message = JSON.parse( data.body );

        switch( message.type ) {
            case "BALANCE_DECREASE":
                decreaseCash( message.data );
                break;
            case "BALANCE_INCREASE":
                updateCashBySocket( message.data );
                break;
            case "UPDATE_BALANCE":
                updateCash();
                break;
            case "CASH_OUT_RESPONSE":
                m_rffsm.changeState( eStateNames.IDLE, {}, "From CashOut" );
                updateCashBySocket( message.data );
                break;
            case "CANCEL_CASH_OUT":
                cashOutCancelled();
                break;
            case "UPPER_GAME_CONNECTED":
                sendCurrentPayTable();
                break;
            case "CLOSE_LOTTERY":
                lotteryClosed();
                break;
            case "START_SPIN":
                onBtnSpin();
                break;
            default:
                break;
        }
    };

    var sendToSocket = function( data ) {
        if( isSBG() ) { RFGlobalData.net.wsWrapper.send( data ); }
    };
    // #endregion

    var cashOutCancelled = function() {
        if( isStateCashOut() ) { stopAutoGames(); m_rffsm.changeState( eStateNames.IDLE, {}, "Cash Out Cancelled" ); }
    };

    var lotteryClosed = function() {
        if( isStateLottery() ) {
            stopAutoGames();
            m_rffsm.changeState( eStateNames.IDLE, {}, "Lottery Closed" );
            updateCashByGS();
        }
    };

    var startPreIdle = function() {
        // Comprobamos si el juego está en autogame
        m_autoGameMode = _.get( RFGlobalData, "game.autoGameMode.enable", false );
        m_numAutoGames = _.get( RFGlobalData, "game.autoGameMode.numAutoGames", 0 );

        if( !hasToWaitUntilDisplayAllPrizeLines() ) {
            m_rffsm.changeState( eStateNames.IDLE, {}, "From PreIdle" );
        }
        else {
            enablePlayableButtons( false );
        }

    };

    var updatePreIdle = function() {
        if( !m_isShowingPrizes ) { m_rffsm.changeState( eStateNames.IDLE, {}, "From Update PreIdle" ); }
    };

    var startWaitingResidual = function() {
        enablePlayableButtons( false );
    };

    var updateWaitingResidual = function() {
        var label_values_sum = 0;
        _.forEach( lbl_win, function( label ) {
            label_values_sum += label.value;
        } );
        if( label_values_sum === 0 ) {
            if( m_autoGameMode ) {
                scheduleNextSpin( DEFAULTS.MS_WAIT_RESIDUAL );
                m_rffsm.changeState( eStateNames.IDLE, {}, "Idle from residial" );
            }
            else {
                m_rffsm.changeState( eStateNames.PRE_IDLE, {}, "Idle from residial" );
            }
        }
    };

    var sendActiveButtons = function() {
        var btn_auto = _.find( m_phaserButtons, function( btn ) { return btn.action === "STOP_AUTO"; } );
        // Una ñapa para que el botón de autoJuego se desactive si no se está en juego automático
        if( !m_autoGameMode && !_.isUndefined( btn_auto ) ) {
            btn_auto.behaviors.get( "RFButtonComp" ).options.enabled = false;
        }
        var buttonList = _.filter( m_phaserButtons, function( btn ) {
            return ( ( btn.behaviors.has( "RFButtonComp" ) ) && ( btn.behaviors.get( "RFButtonComp" ).options.enabled === true ) );
        } );
        var buttonIdList = [];
        _.forEach( buttonList, function( button ) {
            var action = button.action;
            if( action === "STOP_AUTO" ) { action = "AUTO_X"; }
            buttonIdList.push( action );
        } );

        var hasDifferences = _.xor( m_lastButtonListSent, buttonIdList ).length > 0;
        if( hasDifferences ) {

            m_lastButtonListSent = buttonIdList;

            var toSocket = {
                type: "ENABLE_BUTTON_LIST",
                data: {
                    buttonIdList: buttonIdList
                }
            };

            sendToSocket( toSocket );
        }
    };

    var turnOffButtons = function() {
        var toSocket = {
            type: "ENABLE_BUTTON_LIST",
            data: {
                buttonIdList: []
            }
        };
        sendToSocket( toSocket );
    };

    var exitWaitingResidual = function() {
        enablePlayableButtons( true );
    };

    var drawResidualPrize = function( wsResponse ) {

        var residualPrize = RFGlobalData.game.residualPrize;
        if( residualPrize > 0 ) {

            var cash = Number( _.get( wsResponse, "player.cash" ) );
            var fakeCash = cash - residualPrize;
            if( fakeCash > 0 ) {
                drawCash( fakeCash, 0 );
                drawLabelWin( residualPrize, 0 );
                self.game.time.events.remove( m_waitResidualPrizeTimer );
                m_waitResidualPrizeTimer = self.game.time.events.add( MS_TO_START_THE_TWEEN_COUNTER_RESIDUAL_CREDITS, function() {
                    drawCash( cash, MS_TWEEN_COUNTER_RESIDUAL_CREDITS );
                    drawLabelWin( 0, MS_TWEEN_COUNTER_RESIDUAL_CREDITS );
                } );
                if( _.get( RFGameConfig, [ "game", "waitForResidual" ], false ) ) {
                    m_rffsm.changeState( eStateNames.WAITING_RESIDUAL, {}, "Start waiting residual" );
                }
            }
            else {
                console.warn( "Invalid fake cash. Cash", cash, "ResidualPrize", residualPrize, "FakeCash", fakeCash );
                drawLabelWin( 0, 0 );
                drawCash( cash, 0 );
            }

            // reset the residual credits
            RFGlobalData.game.residualPrize = 0;
        }
    };

    var getRandomMatrixRFSymbols = function() {
        var matrix = [];
        var numReels = m_reels.getNumberOfReels();
        var numSymbolsPerReels = m_reels.getNumberOfSymbolsPerReels();
        var mapSymbols = m_rfLoader.getRFSymbols();
        for( var i = 0; i < numReels; ++i ) {
            matrix.push( _.sampleSize( mapSymbols, numSymbolsPerReels ) );
        }
        return matrix;
    };

    var startIdle = function() {
        enablePlayableButtons( true );
        updateLastPressedButtonToNow();
        if( m_data.cash > 0 ) { sendActiveButtons(); }
        turnOffButtonsWithoutCash();
        if( m_data.balanceIncreased > 0 ) {
            m_data.balanceIncreased = 0;
            updateCash();
        }
    };

    var canAutoPlay = function() {
        return RFGameConfig.game.autoPlayWithCash;
    };

    var loopIdle = function() {
        if( m_isShowingPrizes ) {
            updateLastPressedButtonToNow();
        }
        if( canAutoPlay() && self.game.time.now > m_data.msLastPressedButton + RFGameConfig.game.msToStartReelsWithCash ) {
            if( isStateIdle() && ( m_data.cash > 0 ) && !m_autoGameMode ) { onBtnSpin(); }
        }
        var m_msToExitWithNoCash = _.get( RFGameConfig, "game.msToExitWithNoCash", DEFAULTS.MS_TO_EXIT_WITH_NO_CASH );
        if( self.game.time.now > m_data.msLastPressedButton + m_msToExitWithNoCash ) {
            if( ( isStateIdle() ) && ( m_data.cash === 0 ) ) { onBtnExit(); }
        }
    };

    var exitInit = function() {
        if( isSBG() ) {
            var toSocket = {
                type: "START"
            };
            sendToSocket( toSocket );
        }
    };

    var exitIdle = function() { /* Nothing */ };

    var startSpinning = function( oStateParams ) {
        // Ponemos a 0 el saldo incrementado
        m_data.balanceIncreased = 0;

        self.game.time.events.remove( m_waitResidualPrizeTimer );

        self.game.debug.reset();
        enablePlayableButtons( false );

        // Mandamos información al socket para saber que se ha hecho un spin
        if( isSBG() ) {
            var toSocket = {
                type: "SPINNING"
            };
            sendToSocket( toSocket );
        }

        // Hacemos que el botón de parar auto, si estamos en sbg, se quede siempre activo
        if( isSBG() ) {
            var m_phaserButtonsStop = SceneUtils.filterByAction( m_phaserButtons, "STOP_AUTO" );
            _.forEach( m_phaserButtonsStop, function( oPhaserButton ) {
                oPhaserButton.enable( true );
            } );
        }

        var canSpin = true;
        _.forEach( m_phaserLabels, function( oPhaserLabel ) {
            if( oPhaserLabel.type === eLabelsNames.msgMain ) {
                oPhaserLabel.setFormattedString( m_texts.GOODLUCK );
            }
        } );

        if( m_autoGameMode ) {
            console.log( "AUTOGAMES NUMBER", m_numAutoGames );
            if( m_numAutoGames > 0 ) {
                m_numAutoGames--;
                drawNumAutoGames( m_numAutoGames );
            }
            else {
                stopAutoGames();
                m_rffsm.changeState( eStateNames.PRE_IDLE, {}, "End Auto Games" );
                canSpin = false;
            }
        }

        if( canSpin ) {

            self.onStartSpin.dispatch( m_data );
            var audiosToChange = [];
            audiosToChange.push( m_audioSounds.bkgBasegame );
            changeAudiosVolumen( SOUND_VOLUME_MIN, audiosToChange );

            drawLabelWin( 0 );
            showAutoGameMenu( false );
            hideLines();

            m_reels.startSpin( self.nextReelIdsToSpinArray );

            // Mandamos los botones que estén activos al socket
            sendActiveButtons();

            var params = {
                sessionId: RFGlobalData.net.session,
                signature: "0",
                serverSceneId: eServerSceneId.BASE_GAME
            };
            if( isForced() ) {
                params.forcedGameId = m_oForceReels.forcedGamesArray[ m_oForceReels.currentIdx ].id;
                m_oForceReels.forceNextGame = false;
                m_oForceReels.currentIdx = -1;
            }
            if( isSpecialForced() ) {
                params.specialForce = m_specialForceReels;
                m_specialForceReels = {};
            }
            if( RFGlobalData.game.useLocalBets ) {
                params.gameStatus = {
                    lines: m_data.lines,
                    betPerLine: m_data.internalBet,
                    denom: m_data.denom
                };
                SceneUtils.setDirtyLocalBet( true );
            }
            _.extend( params, oStateParams ); // add custom params

            // Descontamos el dinero del jugador antes de recibir la respuesta del servidor
            updateCashOnSpin();

            RFGlobalData.net.wrapper.sendSpin( params, onSpinResponse, SceneUtils.fERROR );

        }
    };

    var exitSpinning = function() { /* Nothing */ };

    var updateCashOnSpin = function() {
        m_data.cash -= m_data.totalBet;
        var cash = m_data.cash > 0 ? m_data.cash : 0;
        drawCash( cash, 0 );
    };


    var startCashingOut = function() {
        enablePlayableButtons( false );
    };

    var startLottery = function() {
        updateCash();
        updateLastPressedButtonToNow();
        turnOffButtons();
        var toSocket = {
            type: "OPEN_LOTTERY",
            data: {
                "url": getLotteryUrl()
            }
        };
        sendToSocket( toSocket );
    };

    var exitLottery = function() {
        updateLastPressedButtonToNow();
        updateCash();
    };

    var startTakingWin = function() {
        var params = {
            sessionId: RFGlobalData.net.session,
            signature: "0",
            serverSceneId: eServerSceneId.DOUBLE_OR_NOTHING,
            takeWin: "1"
        };
        RFGlobalData.net.wrapper.sendDoubleOrNothing( params, onDoubleOrNothingResponse, SceneUtils.fERROR );
    };

    var startExit = function( oParams ) {


        if( _.has( oParams, "sceneName" ) ) {
            var timeToWait = 0;
            if( _.has( oParams, "msToWait" ) ) {
                timeToWait = oParams.msToWait;
            }
            if( timeToWait > 0 ) {
                self.game.time.events.add( timeToWait, function() {
                    stopAllSounds();
                    transitionIn( oParams.sceneName );
                } );
            }
            else {
                stopAllSounds();
                transitionIn( oParams.sceneName );
            }
        }
        else {
            console.warn( "We are on EXIT state without no scene to go next" );
        }
    };

    var updateLastPressedButtonAndDoAction = function( theFunction ) {
        return function() {
            updateLastPressedButtonToNow();
            theFunction.apply( null, arguments );
        };
    };

    // Función que retorna la funcion asociada al strFunctionName si existe
    // eslint-disable-next-line complexity
    var getOnClickFunction = function( strFunctionName ) {

        switch( strFunctionName ) {
            case eActionNames.goHelp: return updateLastPressedButtonAndDoAction( onBtnGoHelp );
            case eActionNames.lineUp: return updateLastPressedButtonAndDoAction( onBtnSetLines );
            case eActionNames.betMax: return updateLastPressedButtonAndDoAction( onBtnSetMaxBet );
            case eActionNames.betPerLineUp: return updateLastPressedButtonAndDoAction( onBtnSetBetPerLine );
            case eActionNames.spin: return updateLastPressedButtonAndDoAction( onBtnSpin );
            case eActionNames.autoGame: return updateLastPressedButtonAndDoAction( onBtnAutoGame );
            case eActionNames.autoX: return updateLastPressedButtonAndDoAction( onBtnAutoX );
            case eActionNames.goDouble: return updateLastPressedButtonAndDoAction( onBtnGoDouble );
            case eActionNames.takeWin: return updateLastPressedButtonAndDoAction( onBtnTakeWin );
            case eActionNames.changeDenom: return updateLastPressedButtonAndDoAction( onBtnSetDenom );
            case eActionNames.volumeUp: return updateLastPressedButtonAndDoAction( onBtnVolumeUp );
            case "STOP_AUTO": return updateLastPressedButtonAndDoAction( stopAutoSbg );
            case "CASH_OUT": return updateLastPressedButtonAndDoAction( onBtnCashOut );
            case "EXIT": return updateLastPressedButtonAndDoAction( onBtnExit );
            case "FORCE": return updateLastPressedButtonAndDoAction( setForceReel );
            default:
                console.warn( "Function ", strFunctionName, " not implemented in this scene" );
                return updateLastPressedButtonAndDoAction( onBtnNotDefined );
        }
    };

    var onSkittelOver = function( button ) {
        onSkittelDown( button );
    };
    var onSkittelOut = function( button ) {
        fadeOutLine( [ button.id ], MS_FADEOUT_LINE );
    };

    var onSkittelDown = function( button ) {
        if( isStateIdle() ) {
            var line = m_phaserLines[ button.id ];
            self.game.tweens.remove( line.data._fadeTween );
            line.data._fadeTween = null;
            line.visible = true;
            line.alpha = 1;
        }
    };

    var fadeOutLine = function( idLine, twToFade ) {
        twToFade = twToFade || DEFAULTS.MS_FADEOUT_LINE;
        var line = m_phaserLines[ idLine ];
        if( !line.data._fadeTween ) {
            line.data._fadeTween = self.game.add.tween( line ).to( { alpha: 0 }, twToFade, Phaser.Easing.Quartic.Out, true );
            line.data._fadeTween.onComplete.add( function() {
                line.data._fadeTween = null;
                line.visible = false;
            } );
        }
    };

    var onSkittelUp = function( button ) {
        if( isStateIdle() ) {
            if( !isSBG() ) {
                var numLines = button.id;
                if( Number( numLines ) > 0 ) {
                    if( !RFGlobalData.game.useLocalBets ) {
                        m_rffsm.changeState( eStateNames.CHANGING_BET, {}, "skittelUp" );
                        sendSetLines( String( numLines ), onSetLinesResponse, SceneUtils.fERROR );
                    }
                    else {
                        updateLocalBet( { lines: String( numLines ) } );
                        m_audioSounds.lineUp.play( Number( numLines ) );
                    }
                }
                else {
                    console.warn( "Invalid numLines in skittelUp:", numLines );
                }
            }
            else {
                fadeOutLine( [ button.id ], MS_FADEOUT_LINE );
            }
        }
    };

    var onBtnCashOut = function() {
        if( m_data.cash > 0 ) {
            turnOffButtons();

            if( isStateIdle() ) {
                var toSocket = {
                    type: "CASH_OUT",
                    data: null
                };
                sendToSocket( toSocket );
                m_rffsm.changeState( eStateNames.CASH_OUT, {}, "CASH OUT" );
            }
        }
        else {
            // Como el jugador no tiene saldo, se lanza el sonido de error
            m_audioSounds.limitExceeded.play();
        }
    };

    var onBtnExit = function() {
        if( isStateIdle() ) {

            turnOffButtons();

            var toSocket = {
                type: "EXIT",
                data: null
            };
            sendToSocket( toSocket );
        }
    };

    var onBtnSetDenom = function() {
        if( !isSBG() ) {
            stopAutoGames();
            if( isStateIdle() ) {
                if( !RFGlobalData.game.useLocalBets ) {
                    m_rffsm.changeState( eStateNames.CHANGING_BET, {}, "onBtnSetDenom" );
                    var params = {
                        sessionId: RFGlobalData.net.session,
                        signature: "0",
                        denom: "+1"
                    };
                    RFGlobalData.net.wrapper.sendSetDenom( params, onSetDenomResponse, SceneUtils.fERROR );
                }
                else {
                    updateLocalBet( { denom: "+1" } );
                    m_audioSounds.denomUp.play();
                }
            }
        }
    };

    var onBtnVolumeUp = function() {
        SceneUtils.volumeUp();
    };

    var refreshVolumeButton = function() {
        var currentMasterVolume = SceneUtils.getCurrentMasterVolume();
        _.forEach( m_phaserVolumeButtons, function( button ) {
            button.visible = _.get( button, [ "parameters", "volume" ] ) === currentMasterVolume;
        } );
    };

    var setForceReel = function() {
        if( RFGameConfig.game.canBeForced ) {
            var numForcedGames = _.size( m_oForceReels.forcedGamesArray );
            if( numForcedGames > 0 ) {
                m_oForceReels.currentIdx = ( m_oForceReels.currentIdx + 1 === numForcedGames ? -1 : ( m_oForceReels.currentIdx + 1 ) % numForcedGames );
                m_oForceReels.forceNextGame = m_oForceReels.currentIdx >= 0;
            }

            // eslint-disable-next-line max-len
            var text = "FORCE: " + ( m_oForceReels.forceNextGame ? m_oForceReels.forcedGamesArray[ m_oForceReels.currentIdx ].id + " (" + m_oForceReels.forcedGamesArray[ m_oForceReels.currentIdx ].description + ")" : "Normal play" );

            self.game.debug.reset();

            var X_DEBUG = 20;
            var Y_DEBUG = 20;
            self.game.debug.text( text, X_DEBUG, Y_DEBUG );
            console.log( text ); /* RemoveLogging:skip*/
        }
    };

    var getForcedGames = function( fEnd ) {
        var params = {
            sessionId: RFGlobalData.net.session,
            serverSceneId: eServerSceneId.BASE_GAME
        };
        RFGlobalData.net.wrapper.sendGetForcedGames( params, function( wsResponse ) {
            console.log( "GET_FORCED_GAMES RESPONSE:", wsResponse );
            if( SceneUtils.hasValidSession( wsResponse ) ) {
                m_oForceReels.forcedGamesArray = wsResponse.forcedGamesArray;

                if( _.isFunction( fEnd ) ) {
                    fEnd( wsResponse );
                }
            }
        }, fEnd );
    };

    // #region KEYBINDINGS

    var addKeyBindings = function() {
        if( _.get( RFGameConfig, [ "game", "useKeyBindings" ], true ) ) {
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.forcedGames" ), updateLastPressedButtonAndDoAction( setForceReel ) );
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.spin" ), updateLastPressedButtonAndDoAction( onBtnSpin ) );
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.maxBet" ), updateLastPressedButtonAndDoAction( onBtnSetMaxBet ) );
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.goHelp" ), updateLastPressedButtonAndDoAction( onBtnGoHelp ) );
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.betPerLinePlus1" ), updateLastPressedButtonAndDoAction( onKeybUp ) );
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.betPerLineMinus1" ), updateLastPressedButtonAndDoAction( onKeybDown ) );
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.linesPlus1" ), updateLastPressedButtonAndDoAction( onKeybRight ) );
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.linesMinus1" ), updateLastPressedButtonAndDoAction( onKeybLeft ) );
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.cashOut" ), updateLastPressedButtonAndDoAction( onBtnCashOut ) );
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.exit" ), updateLastPressedButtonAndDoAction( onBtnExit ) );
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.autoGame" ), updateLastPressedButtonAndDoAction( onBtnToogleAutogame ) );
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.showHistory" ), updateLastPressedButtonAndDoAction( onKeybCommandHistory ) );
            // Se usa la tecla W para probar si el saldo se actualiza
            SceneUtils.addKeyBinding( Phaser.Keyboard.W, function() {
                var toSocket = {
                    type: "BALANCE_INCREASE",
                    data: {
                        "value": 0.10
                    }
                };
                sendToSocket( toSocket );
                // updateCashBySocket( 10000 );
            } );
            SceneUtils.addKeyBinding( Phaser.Keyboard.T, function() {
                updateCash();
            } );

        }
        else if( _.get( RFGameConfig, [ "game", "forceAllowHistoryKeyBinding" ], true ) ) {
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.showHistory" ), onKeybCommandHistory );
        }
    };

    var onKeybUp = function() {
        stopAutoGames();
        if( isStateIdle() ) {
            drawLabelMain( "" );
            if( !RFGlobalData.game.useLocalBets ) {
                m_rffsm.changeState( eStateNames.CHANGING_BET, {}, "onKeybRight (onKeybUp +1)" );
                sendSetBetPerLine( "+1", onSetBetPerLineResponse, SceneUtils.fERROR );
            }
            else {
                updateLocalBet( { betPerLine: "+1" } );
                m_audioSounds.betUp.play();
            }
        }
    };

    var onKeybDown = function() {
        stopAutoGames();
        if( isStateIdle() ) {
            if( !RFGlobalData.game.useLocalBets ) {
                m_rffsm.changeState( eStateNames.CHANGING_BET, {}, "onKeybLeft (onKeybDown -1)" );
                sendSetBetPerLine( "-1", onSetBetPerLineResponse, SceneUtils.fERROR );
            }
            else {
                updateLocalBet( { betPerLine: "-1" } );
                m_audioSounds.betUp.play();
            }
        }
    };

    var onKeybLeft = function() {
        stopAutoGames();
        if( isStateIdle() ) {
            if( !RFGlobalData.game.useLocalBets ) {
                m_rffsm.changeState( eStateNames.CHANGING_BET, {}, "onKeybDown (onKeybLeft -1)" );
                sendSetLines( "-1", onSetLinesResponse, SceneUtils.fERROR );
            }
            else {
                updateLocalBet( { lines: "-1" } );
                m_audioSounds.lineUp.play( m_data.lines );
            }
        }
    };

    var onKeybRight = function() {
        stopAutoGames();
        if( isStateIdle() ) {
            if( !RFGlobalData.game.useLocalBets ) {
                m_rffsm.changeState( eStateNames.CHANGING_BET, {}, "onKeybUp (onKeybRight +1)" );
                sendSetLines( "+1", onSetLinesResponse, SceneUtils.fERROR );
            }
            else {
                updateLocalBet( { lines: "+1" } );
                m_audioSounds.lineUp.play( m_data.lines );
            }
        }
    };

    var onKeybCommandHistory = function() {
        if( isStateIdle() ) {
            RFGlobalData.net.wrapper.sendCommandHistoryRequest( {}, function( wsCommandHistoryResponse ) {
                if( wsCommandHistoryResponse instanceof RFWSCommandHistoryResponse ) {
                    console.log( "COMMAND HISTORY RESPONSE", wsCommandHistoryResponse );
                    var url = _.get( RFGameConfig, [ "game", "reports", "history", "url" ], "history.html" );
                    SceneUtils.showHistory( url, JSON.stringify( wsCommandHistoryResponse.historyList ) );
                }
            } );
        }
    };

    // #endregion

    // #region on button clicks

    var onBtnGoHelp = function() {
        stopAutoGames();
        if( isStateIdle() ) {
            if( !isSBG() ) {
                // Si no es SBG
                m_rffsm.changeState( eStateNames.EXIT, { sceneName: eSceneNames.HELP }, "Exit to Help" );
            }
            else {
                // Si es SBG
                var toSocket = {
                    type: "HELP",
                    data: null
                };
                sendToSocket( toSocket );
            }
        }
    };

    var onBtnSetLines = function() {
        stopAutoGames();
        if( isStateIdle() && !isSBG() ) {
            if( !RFGlobalData.game.useLocalBets ) {
                m_rffsm.changeState( eStateNames.CHANGING_BET, {}, "onBtnSetLines" );
                sendSetLines( "+1", onSetLinesResponse, SceneUtils.fERROR );
            }
            else {
                updateLocalBet( { lines: "+1" } );
                m_audioSounds.lineUp.play( m_data.lines );
            }
        }
    };

    var onBtnSetMaxBet = function() {
        stopAutoGames();
        if( isStateIdle() ) {
            if( !RFGlobalData.game.useLocalBets ) {
                m_rffsm.changeState( eStateNames.CHANGING_BET, {}, "onBtnSetMaxBet" );
                var params = {
                    sessionId: RFGlobalData.net.session,
                    signature: "0",
                    var: "0"
                };
                RFGlobalData.net.wrapper.sendSetMaxBet( params, onSetMaxBetResponse, SceneUtils.fERROR );
            }
            else if( SceneUtils.setMaximumBet( eServerSceneId.BASE_GAME, m_data.cash ) ) {
                updateLocalBet( {} );
                m_audioSounds.betMax.play();
            }
        }
    };

    var onBtnSetBetPerLine = function() {
        stopAutoGames();
        if( isStateIdle() ) {
            drawLabelMain( "" );
            if( !RFGlobalData.game.useLocalBets ) {
                m_rffsm.changeState( eStateNames.CHANGING_BET, {}, "onBtnSetBetPerLine" );
                sendSetBetPerLine( "+1", onSetBetPerLineResponse, SceneUtils.fERROR );
            }
            else {
                updateLocalBet( { betPerLine: "+1" } );
                m_audioSounds.betUp.play();
            }
        }
    };

    // TODO: Fix this
    // eslint-disable-next-line complexity
    var onBtnSpin = function() {
        stopAutoGames();

        if( isStateIdle() ) {
            if( isBetLowerThanCash( m_data ) ) {
                m_audioSounds.limitExceeded.play();
            }
            else if( canSpin() ) {
                m_rffsm.changeState( eStateNames.SPINNING );
            }
        }
        // SHOWING CARRUSEL
        else if( isStateDisplayingCarrousel() ) {
            var currentServerSceneId = _.get( m_data.lastSpinResponse, [ "player", "currentServerSceneId" ] );
            if( !hasToWaitUntilDisplayAllPrizeLines() ) {
                // Skip the Carrousel To start BIG PRIZE
                var hasBigPrize = _.get( m_data.lastSpinResponse, [ "spinState", "bigPrize" ], false );
                var hasToShowBigPrize = hasBigPrize && hasBigPrizeListeners() && !RFGameConfig.game.showBigPrizesBeforeCarrousel;
                if( hasToShowBigPrize ) {
                    displayBigPrize( m_data.lastSpinResponse, function() {
                        console.log( "Pressed BTN_SPIN while displaying carrousel, with Big Prize and Big Prized has been ended now. Chaging to serverSceneId", currentServerSceneId );
                        changeStateByServerSceneId( currentServerSceneId );
                    } );
                }
                // NO Big PRIZE or shown Before
                else {
                    endFirstLoopCarrousel( m_data.lastSpinResponse );
                    changeStateByServerSceneId( currentServerSceneId );
                }
            }
        }
        // SHOWING BIG PRIZE
        else if( isStateDisplayingBigPrize() ) {
            if( canBigPrizeBeSkipped() ) {
                self.onCancelBigPrize.dispatch();
            }
        }
    };

    var onBtnAutoGame = function() {
        if( isStateIdle() ) {
            showAutoGameMenu( !m_autoGameMenuVisible );
        }
    };

    var onBtnToogleAutogame = function() {
        // Ñapa rápida para que el botón físico de juego automático funcione
        if( isStateIdle() && isSBG() ) {
            startAutoGames( Number.MAX_SAFE_INTEGER );
            if( canSpin() ) {
                m_rffsm.changeState( eStateNames.SPINNING );
            }
            else {
                if( m_data.cash > 0 ) { sendActiveButtons(); }
                stopAutoGames();
            }
        }
        else if( isStateSpinning() ) { stopAutoGames(); }
    };

    var onBtnAutoX = function( button ) {
        if( _.has( button, "parameters.numAuto" ) && _.isInteger( button.parameters.numAuto ) ) {
            // Si es sbg le ponemos un número ridículo para que haya automático infinito (o casi). Pumu, a lo loco.
            if( isSBG() ) { startAutoGames( Number.MAX_SAFE_INTEGER ); }
            else if( m_numAutoGames === 0 ) { startAutoGames( button.parameters.numAuto ); }
            else { startAutoGames( m_numAutoGames ); }

            if( isStateIdle() ) {
                if( isBetLowerThanCash( m_data ) ) { m_audioSounds.limitExceeded.play(); }
                if( canSpin() ) {
                    m_rffsm.changeState( eStateNames.SPINNING );
                }
                else {
                    stopAutoGames();
                }
            }
            else {
                console.warn( "Invalid state to start auto games.", m_rffsm.getState() );
            }
        }
    };

    var onBtnGoDouble = function() {
        SceneUtils.setButtonEnable( m_phaserButtonsDouble, false );
        enablePlayableButtons( false );
        stopAutoGames();
        if( isStateDouble() ) {
            var params = {
                sessionId: RFGlobalData.net.session,
                signature: "0",
                serverSceneId: eServerSceneId.DOUBLE_OR_NOTHING,
                takeWin: "0"
            };
            RFGlobalData.net.wrapper.sendDoubleOrNothing( params, function( wsResponse ) {
                console.log( "ON GO DOUBLE RESPONSE", wsResponse );
                if( SceneUtils.hasValidSession( wsResponse ) ) {
                    m_rffsm.changeState( eStateNames.EXIT, { sceneName: eSceneNames.DOUBLE }, "Enter to Double" );
                }
            }, SceneUtils.fERROR );
        }
    };

    var onBtnTakeWin = function() {
        stopAutoGames();
        if( isStateDouble() ) {
            m_rffsm.changeState( eStateNames.TAKING_WIN );
        }
    };

    var onBtnNotDefined = function( button ) {
        console.warn( "Action not defined for this button: ", button );
    };

    var updatePayTable = function( fCallbackOk, fCallbackError ) {
        var params = {
            sessionId: RFGlobalData.net.session,
            serverSceneId: eServerSceneId.BASE_GAME
        };
        RFGlobalData.net.wrapper.sendGetGameState( params, function( wsResponse ) {
            console.log( "GAME STATE RESPONSE", wsResponse );
            updateData( wsResponse );
            if( _.isFunction( fCallbackOk ) ) {
                fCallbackOk( wsResponse );
            }
        }, fCallbackError );
    };

    // #endregion

    // #region SERVER RESPONSES

    var onGetGameStateResponse = function( wsResponse ) {
        console.log( "GAME STATE RESPONSE", wsResponse );
        if( SceneUtils.hasValidSession( wsResponse ) ) {
            updateData( wsResponse );
        }

        updateNextReelIdsToSpinArray( wsResponse );
    };

    var onSetLinesResponse = function( wsResponse ) {
        console.log( "SET LINES RESPONSE", wsResponse );
        if( SceneUtils.hasValidSession( wsResponse ) ) {
            var numLine = ( wsResponse instanceof RFWSSetLinesResponse ) ? wsResponse.player.numLines - 1 : undefined;
            m_audioSounds.lineUp.play( numLine );
            updateData( wsResponse );
            m_rffsm.changeState( eStateNames.IDLE );
        }
    };

    var onSetBetPerLineResponse = function( wsResponse ) {
        console.log( "BET PER LINE RESPONSE", wsResponse );
        if( SceneUtils.hasValidSession( wsResponse ) ) {
            m_audioSounds.betUp.play();
            updateData( wsResponse );
            m_rffsm.changeState( eStateNames.IDLE );
        }
    };

    var onSetMaxBetResponse = function( wsResponse ) {
        console.log( "SET MAX BET REPONSE", wsResponse );
        if( SceneUtils.hasValidSession( wsResponse ) ) {
            m_audioSounds.betMax.play();
            updateData( wsResponse );
            m_rffsm.changeState( eStateNames.IDLE );
        }
    };

    var onSetDenomResponse = function( wsResponse ) {
        console.log( "SET DENOM RESPONSE", wsResponse );
        if( SceneUtils.hasValidSession( wsResponse ) ) {
            m_audioSounds.denomUp.play();
            updateData( wsResponse );
            m_rffsm.changeState( eStateNames.IDLE );
        }
    };

    var onSpinResponse = function( wsResponse ) {
        console.log( "SPIN RESPONSE", wsResponse );

        m_data.lastSpinResponse = wsResponse;

        function stopSpinInternal( _wsResponse ) {
            stopSpin( _wsResponse, function() {

                // Falseamos los datos con el cash incrementado que tengamos en el playerData
                _wsResponse.player.cash += m_data.balanceIncreased;

                self.onStopSpin.dispatch( _wsResponse );
                var audiosToChange = [];
                audiosToChange.push( m_audioSounds.bkgBasegame );
                changeAudiosVolumen( SOUND_VOLUME_MAX, audiosToChange );

                displayPrizes( _wsResponse );

            } );
        }

        self.onSpinResponse.dispatch( wsResponse );
        if( SceneUtils.hasValidSession( wsResponse ) ) {
            if( isStateSpinning() ) {
                if( SceneUtils.isValidWSResult( wsResponse ) ) {
                    stopSpinInternal( wsResponse );
                }
                else {
                    console.error( "Error in server response", wsResponse );
                    debugger;
                    SceneUtils.fERROR( wsResponse );
                }
            }
        }

        updateNextReelIdsToSpinArray( wsResponse );
    };

    var onDoubleOrNothingResponse = function( wsResponse ) {
        console.log( "DOUBLE UP RESPONSE", wsResponse );
        SceneUtils.hasValidSession( wsResponse );

        if( isStateTakingWin() ) {
            if( SceneUtils.isValidWSResult( wsResponse ) ) {

                changeStateByServerSceneId( wsResponse.player.currentServerSceneId );

                updateData( wsResponse );

                if( isStateIdle() ) {
                    if( m_autoGameMode ) {
                        m_rffsm.changeState( eStateNames.SPINNING, {}, "Spining through auto game after taking win" );
                    }
                    else if( m_takeWinFromSpinButton && _.get( RFGameConfig, [ "game", "spinWhenTakingWinFromSpinButton" ], false ) ) {
                        m_rffsm.changeState( eStateNames.SPINNING, {}, "Spining through spin button after taking win with it" );
                    }
                    m_takeWinFromSpinButton = false;
                }

            }
            else {
                console.error( "Error in server response", wsResponse );
                debugger;
                SceneUtils.fERROR( wsResponse );
            }
        }
    };

    // #endregion

    var stopSpin = function( spinResponse, fEndSpinCallback ) {
        var rfSymbolsMatrix = SceneUtils.getRFSymbolsMatrix( spinResponse.spinState.reels.reelMatrix );
        SceneUtils.setLastRFSymbolsMatrix( rfSymbolsMatrix );
        // Guardamos los rodillos en el localStorage
        localStorage.setItem( "latestDataBase" + RFGameConfig.gameName, btoa( JSON.stringify( spinResponse.spinState.reels.reelMatrix ) ) );

        // INSUFFICIENT FOUNDS or LIMIT EXCEEDED
        if( spinResponse.result.returnCode === eServerReturnCodes.INSUFFICIENT_FOUNDS ||
            spinResponse.result.returnCode === eServerReturnCodes.LIMIT_EXCEEDED ) {
            console.warn( "Stop now the reels. Limit exceeded or insufficient founds (", spinResponse.result.returnCode, ")." );
            m_reels.setOptions( { playSounds: false } );
            m_reels.setReels( rfSymbolsMatrix );
            m_reels.setOptions( { playSounds: true } );
            if( _.isFunction( fEndSpinCallback ) ) {
                fEndSpinCallback();
            }
        }
        // Valid SPIN
        else {
            console.log( "Tiempo de spin mientras esperábamos respuesta del servidor", m_reels.getSpinningTime() );
            var msMinSpinTime = _.get( RFGameConfig, "game.msMinSpinTime", DEFAULTS.MS_MIN_SPIN_TIME );
            var timeToWait = msMinSpinTime; // todo: contemplar además el tiempo del rebote y tenerlo en cuenta a la hora de construir los rodillos
            if( m_reels.getSpinningTime() < msMinSpinTime ) {
                timeToWait = msMinSpinTime - m_reels.getSpinningTime();
            }
            var prizeHypeobjResponse = SceneUtils.getPrizeHypeObject( spinResponse.spinState );
            self.game.time.events.add( timeToWait, function() {
                m_reels.stopSpin( rfSymbolsMatrix, prizeHypeobjResponse, function( _rfSymbolsMatrix, _prizeHypeObj, _fOnStop ) {
                    fEndSpinCallback( _rfSymbolsMatrix, _prizeHypeObj, _fOnStop );
                } );
            } );
        }
    };

    var displayPrizes = function( spinResponse ) {

        var currentServerSceneId = _.get( spinResponse, [ "player", "currentServerSceneId" ] );
        var prize = _.get( spinResponse, "spinState.prize", 0 );

        // BigPrize
        if( spinResponse.spinState.bigPrize ) {
            if( RFGameConfig.game.showBigPrizesBeforeCarrousel ) {
                var text = String.format( m_texts.CONGRATULATIONS, SceneUtils.formatMoneyOrCredits( prize, RFGlobalData.locale.currencyISO ) );
                drawLabelMain( text );
                displayBigPrize( spinResponse, function() {
                    startCarrousel( spinResponse, function() {
                        console.log( "End of display Big Prize and then, carrousel => change state by serverSceneId", currentServerSceneId );
                        changeStateByServerSceneId( currentServerSceneId );
                        endFirstLoopCarrousel( spinResponse );
                    } );
                } );
            }
            else {
                startCarrousel( spinResponse, function() {
                    drawTotalPrize( prize, 0 );
                    displayBigPrize( spinResponse, function() {
                        console.log( "End of display carrousel and then, big prize => change state by serverSceneId", currentServerSceneId );
                        changeStateByServerSceneId( currentServerSceneId );
                        endFirstLoopCarrousel( spinResponse );
                    } );
                } );
            }
        }
        // no BigPrize
        else {
            startCarrousel( spinResponse, function() {
                console.log( "End of display carrousel, with no big prize => change state by serverSceneId", currentServerSceneId );
                changeStateByServerSceneId( currentServerSceneId );
                endFirstLoopCarrousel( spinResponse );
            } );
        }
    };

    var displayBigPrize = function( spinResponse, fOnBigPrizeEnds ) {

        console.log( "Displaying a BigPrize" );

        fOnBigPrizeEnds = getAFunction( fOnBigPrizeEnds );

        // Si la señal no tiene listeners, se sale directamente
        if( !hasBigPrizeListeners() ) {
            fOnBigPrizeEnds();
        }
        else {
            m_reels.stopAllSounds();
            m_rffsm.changeState( eStateNames.DISPLAYING_BIGPRIZE, {}, "Calling to onBigPrize and wait for the fOnBigPrizeEnds" );
            self.onBigPrize.dispatch( spinResponse, fOnBigPrizeEnds, m_audioSounds.bigPrize );
        }
    };

    var endFirstLoopCarrousel = function( spinResponse ) {

        m_isShowingFirstLoopCarrousel = false;

        var prize = _.get( spinResponse, "spinState.prize", 0 );

        // Check if the totalBet has changed
        if( !RFGlobalData.game.useLocalBets ) {
            if( spinResponse.player.totalBet !== m_data.totalBet ) {
                console.info( "Total bet has changed. We need to update the paytable." );
                updatePayTable( function() {
                    updateData( spinResponse );
                }, SceneUtils.fERROR );
            }
            else {
                updateData( spinResponse );
            }
        }
        else {
            updateData( spinResponse );
        }

        if( prize > 0 ) {
            var twTime = MS_TWEEN_COUNTER_PRIZE_LINES;
            self.game.time.events.add( twTime, function() { m_isShowingPrizes = false; } );
            drawTotalPrize( prize, 0 );
            transferPrizeToCash( twTime );
        }
        else {
            m_isShowingPrizes = false;
        }
        if( m_autoGameMode ) {
            if( isStateDouble() || isStateIdle() || isStatePreIdle() ) {
                var elapsedTimeCarrousel = self.game.time.now - m_data.startCarrouselTime;
                var msMinToStartNewGameInAutoGameWithPrize = _.get(
                    RFGameConfig,
                    "game.msMinToStartNewGameInAutoGameWithPrize",
                    DEFAULTS.MS_MIN_TO_START_NEW_GAME_IN_AUTO_GAME_WITH_PRIZE
                );
                var msMinToStartNewGameInAutoGameWithNoPrize = _.get(
                    RFGameConfig,
                    "game.msMinToStartNewGameInAutoGameWithNoPrize",
                    DEFAULTS.MS_MIN_TO_START_NEW_GAME_IN_AUTO_GAME_WITH_NO_PRIZE
                );
                var waitTime = prize > 0 ? msMinToStartNewGameInAutoGameWithPrize : msMinToStartNewGameInAutoGameWithNoPrize;
                var timeToWait = waitTime;
                if( elapsedTimeCarrousel < waitTime ) {
                    timeToWait = waitTime - elapsedTimeCarrousel;
                }
                scheduleNextSpin( timeToWait );
            }
        }
    };

    var startDisplayingCarrousel = function() {

        // habilitamos el botón de spin si estamos en basegame y no tenemos que esperar a visualizar todas las líneas
        var currentServerSceneId = _.get( m_data.lastSpinResponse, [ "player", "currentServerSceneId" ] );
        var enable = !hasToWaitUntilDisplayAllPrizeLines() && currentServerSceneId === eServerSceneId.BASE_GAME;
        enableSpinButton( enable );

    };

    var startDisplayingBigPrize = function() {
        var enable = canBigPrizeBeSkipped();
        enableSpinButton( enable );
    };

    var startCarrousel = function( spinResponse, fEndFirstLoopCarrousel ) {

        console.log( "Displaying the carrousel" );

        m_rffsm.changeState( eStateNames.DISPLAYING_CARROUSEL, {}, "Showing the carrousel" );
        m_isShowingFirstLoopCarrousel = true;

        fEndFirstLoopCarrousel = getAFunction( fEndFirstLoopCarrousel );

        var extendedWildArray = SceneUtils.getExtendedWildArray( spinResponse.spinState );
        var prizesArray = SceneUtils.getPrizesArray( spinResponse.spinState );
        m_data.startCarrouselTime = self.game.time.now;

        m_isShowingPrizes = true;

        var fEndFirstLoop = function() {
            if( m_isShowingFirstLoopCarrousel ) {
                fEndFirstLoopCarrousel();
            }
            else {
                console.log( "Omiting callback..." );
            }
        };

        var oWinPrizes = {
            prizesArray: prizesArray,
            extendedWildArray: extendedWildArray
        };

        if( hasToDisplayPrizeLines() ) {
            startCarrouselDisplayingLinePrizes( oWinPrizes, fEndFirstLoop );
        }
        else {
            m_reels.startWinPrizesCarrousel( oWinPrizes, fEndFirstLoop );
        }
    };

    var startCarrouselDisplayingLinePrizes = function( oWinPrizes, fCallback, bUpdatePrizeLines, bPlaySound ) {
        var fnName = startCarrouselDisplayingLinePrizes.name + ": ";

        bUpdatePrizeLines = _.isBoolean( bUpdatePrizeLines ) ? bUpdatePrizeLines : true;
        bPlaySound = _.isBoolean( bPlaySound ) ? bPlaySound : true;

        if( !m_reels.isStopped() ) { throw new Error( fnName + "Can't start carrousel if the reels are not stopped" ); }
        assertAllKeys( oWinPrizes, [ "prizesArray", "extendedWildArray" ] );

        var aWinPrizes = oWinPrizes.prizesArray;
        if( !_.isArray( aWinPrizes ) ) { throw new Error( fnName + "oWinPrizes.aWinPrizes is not an array" ); }

        var aExtendedWild = oWinPrizes.extendedWildArray;
        if( !_.isArray( aWinPrizes ) ) { throw new Error( fnName + "oWinPrizes.extendedWildArray is not an array" ); }

        if( aWinPrizes.length > 0 ) {
            m_reels.stopAllSounds();
            m_reels.showExtendedWildPrizes( aExtendedWild, function() { // 1.- Show Extended Wild
                showWinPrizes( aWinPrizes, fCallback, bUpdatePrizeLines, bPlaySound );         // 2.- Show Line prizes
            } );

        } // Only ExtendedWild
        else if( aExtendedWild.length > 0 ) {
            m_reels.stopAllSounds();
            m_reels.showExtendedWildPrizes( aExtendedWild, fCallback );
        }
        else if( _.isFunction( fCallback ) ) {
            fCallback();
        }
    };

    var showWinPrizes = function( aWinPrizes, fOnEndLoopCallback, bUpdatePrizeLines, bPlaySound ) {

        bUpdatePrizeLines = _.isBoolean( bUpdatePrizeLines ) ? bUpdatePrizeLines : true;
        bPlaySound = _.isBoolean( bPlaySound ) ? bPlaySound : true;


        // eslint-disable-next-line max-params
        var showWinPrizeInternal = function( _aWinPrizes, currentIdx, bLoop, playSound, isFirstLoop, _fOnEndLoopCallback ) {
            if( currentIdx >= _aWinPrizes.length ) { // base case
                if( _.isFunction( _fOnEndLoopCallback ) ) {
                    _fOnEndLoopCallback();
                }
                if( bLoop ) {
                    currentIdx = 0;  // loop again
                }
                else {
                    return; // end loop
                }
            }

            var currentPrize = _aWinPrizes[ currentIdx ];

            // mute sound and don't update the display if the carrusel was skipped by SPIN button.
            playSound = m_isShowingFirstLoopCarrousel ? playSound : false;
            bUpdatePrizeLines = m_isShowingFirstLoopCarrousel ? bUpdatePrizeLines : false;

            // draw the information line prize
            if( isFirstLoop && bUpdatePrizeLines ) {
                var linePrize = _.get( currentPrize, "prize" );
                var accumulatedPrize = _.get( currentPrize, "accumulatedPrize" );
                var numLineWinner = Number( _.get( currentPrize, "rfLine.id" ) );
                var text = "";
                var formatedLinePrize = SceneUtils.formatMoneyOrCredits( linePrize, RFGlobalData.locale.currencyISO );
                if( linePrize > 0 ) {
                    var template = numLineWinner > 0 ? m_texts.LINE_PRIZE : m_texts.NO_LINE_PRIZE;
                    text = String.format( template, formatedLinePrize, numLineWinner );
                }
                drawPrize( accumulatedPrize, MS_TWEEN_COUNTER_PRIZE_LINES, text );
                self.onStartPrizeLine.dispatch( linePrize, formatedLinePrize, numLineWinner );
            }

            m_reels.showWinPrize( currentPrize, playSound, function() {
                showWinPrizeInternal( _aWinPrizes, currentIdx + 1, bLoop, playSound, isFirstLoop, _fOnEndLoopCallback );  // recursive call
            } );
        };

        // First loop
        var loop = false;
        var playSound = bPlaySound;
        var isFirstLoop = true;
        showWinPrizeInternal( aWinPrizes, 0, loop, playSound, isFirstLoop, function() {

            // When finish the first loop, we call the callback function
            if( _.isFunction( fOnEndLoopCallback ) ) {
                fOnEndLoopCallback();
            }

            // Nexts loops
            loop = true;
            playSound = false;
            isFirstLoop = false;
            showWinPrizeInternal( aWinPrizes, 0, loop, playSound, isFirstLoop );
        } );
    };

    var changeStateByServerSceneId = function( serverSceneId ) {

        if( !_.hasValue( eServerSceneId, serverSceneId ) && !_.hasValue( eCustomSceneNames, serverSceneId ) ) {
            console.error( "Invalid server scene id: '" + serverSceneId + "'" );
            debugger;
        }

        var timeToWait = 0;

        switch( serverSceneId ) {
            case eServerSceneId.BASE_GAME:
                m_rffsm.changeState( eStateNames.PRE_IDLE, {}, "Server Playing Game: BASEGAME" );
                break;
            case eServerSceneId.FREE_GAME:
                timeToWait = _.get( RFGameConfig.game, "msMinToEnterInFreeGames", DEFAULTS.MS_MIN_TO_ENTER_IN_FREEGAMES );
                m_audioSounds.bonus.play();
                m_rffsm.changeState( eStateNames.EXIT, {
                    sceneName: eSceneNames.FREEGAME,
                    msToWait: timeToWait
                }, "Server Playing Game: " + serverSceneId );
                break;
            case eCustomSceneNames.CUSTOM_GAME_1:
                timeToWait = _.get( RFGameConfig.game, "msMinToEnterInFreeGames", DEFAULTS.MS_MIN_TO_ENTER_IN_FREEGAMES );
                m_audioSounds.bonus.play();
                m_rffsm.changeState( eStateNames.EXIT, {
                    sceneName: eCustomSceneNames.CUSTOM_GAME_1,
                    msToWait: timeToWait
                }, "Server Playing Game: " + eCustomSceneNames.CUSTOM_GAME_1 );
                break;
            case eCustomSceneNames.CUSTOM_GAME_2:
                timeToWait = _.get( RFGameConfig.game, "msMinToEnterInFreeGames", DEFAULTS.MS_MIN_TO_ENTER_IN_FREEGAMES );
                m_audioSounds.bonus.play();
                m_rffsm.changeState( eStateNames.EXIT, {
                    sceneName: eCustomSceneNames.CUSTOM_GAME_2,
                    msToWait: timeToWait
                }, "Server Playing Game: " + eCustomSceneNames.CUSTOM_GAME_2 );
                break;
            case eCustomSceneNames.CUSTOM_GAME_3:
                timeToWait = _.get( RFGameConfig.game, "msMinToEnterInFreeGames", DEFAULTS.MS_MIN_TO_ENTER_IN_FREEGAMES );
                m_audioSounds.bonus.play();
                m_rffsm.changeState( eStateNames.EXIT, {
                    sceneName: eCustomSceneNames.CUSTOM_GAME_3,
                    msToWait: timeToWait
                }, "Server Playing Game: " + eCustomSceneNames.CUSTOM_GAME_3 );
                break;
            default:
                console.warn( "Server state not controlled: '" + serverSceneId + "'" );
                break;
        }
    };

    var scheduleNextSpin = function( msToWait ) {
        if( _.isNumber( msToWait ) ) {
            self.game.time.events.remove( m_waitingToNextAutoGameTimer );
            m_waitingToNextAutoGameTimer = self.game.time.events.add( msToWait, function() {
                if( isStateIdle() || isStateWaitingResidual() ) {
                    m_rffsm.changeState( eStateNames.SPINNING, {}, "Start Spin from Auto Game" );
                }
                else if( isStateDouble() ) {
                    m_rffsm.changeState( eStateNames.TAKING_WIN, {}, "Taking win from Auto Game" );
                }
                else if( isStatePreIdle() ) {
                    var waitTime = 1000;
                    m_rffsm.changeState( eStateNames.IDLE, {}, "From PreIdle" );
                    scheduleNextSpin( waitTime );
                }
                else {
                    console.error( "Logic Error: The state has change (" +
                        m_rffsm.getState() +
                        ") and we must be on Double or Idle. Maybe the m_waitingToNextAutoGameTimer has not removed" );
                }
            } );
        }
        else {
            console.error( "Invalid msToWait scheduling next spin: ", msToWait );
        }
    };

    var stopAutoSbg = function() {
        stopAutoGames();
    };

    var startAutoGames = function( iNumAutoGames ) {
        enableVisibilityOnAutoButtons( true );
        m_autoGameMode = true;
        _.set( RFGlobalData, "game.autoGameMode.enable", true );
        m_numAutoGames = iNumAutoGames;
        drawNumAutoGames( m_numAutoGames );
        showAutoGameMenu( false );
        enableVisibilityOnAutoButtons( true );
    };

    var stopAutoGames = function() {
        enableVisibilityOnAutoButtons( false );
        enableVisibilityOnAutoButtons( false );
        m_autoGameMode = false;
        _.set( RFGlobalData, "game.autoGameMode.enable", false );
        m_numAutoGames = 0;
        drawNumAutoGames( m_numAutoGames );
        showAutoGameMenu( false );
        if( isSBG() ) {
            var m_phaserButtonsStop = SceneUtils.filterByAction( m_phaserButtons, "STOP_AUTO" );
            _.forEach( m_phaserButtonsStop, function( oPhaserButton ) {
                oPhaserButton.visible = false;
            } );
        }
        self.game.time.events.remove( m_waitingToNextAutoGameTimer );
        if( m_data.cash > 0 ) { sendActiveButtons(); }
    };

    var enableVisibilityOnAutoButtons = function( visible ) {
        if( isSBG() ) {
            var m_phaserButtonsStop = SceneUtils.filterByAction( m_phaserButtons, "STOP_AUTO" );
            _.forEach( m_phaserButtonsStop, function( oPhaserButton ) {
                oPhaserButton.visible = visible;
            } );
            var m_phaserButtonsAutoMode = SceneUtils.filterByAction( m_phaserButtons, "AUTO_X" );
            _.forEach( m_phaserButtonsAutoMode, function( oPhaserButton ) {
                oPhaserButton.visible = !visible;
            } );
        }
    };

    var reconnection = function() {
        getGameState( function( wsResponse ) {
            var lastWsResponse = RFWebServiceUtil.createRFWSResponse( wsResponse.gameState );
            console.log( "Reconnection: Last Response", lastWsResponse );

            drawLabelWin( 0, 0 );

            if( lastWsResponse ) {

                changeStateByServerSceneId( lastWsResponse.player.currentServerSceneId );

                switch( RFGlobalData.game.lastScene ) {
                    case eSceneNames.RECONNECTION:
                    case eSceneNames.HELP:
                    case eSceneNames.BASEGAME:
                        if( lastWsResponse instanceof RFWSSpinResponse ) {

                            // update the reels
                            console.log( "Reconnection: Update reels with last spin state" );
                            SceneUtils.setLastRFSymbolsMatrix( SceneUtils.getRFSymbolsMatrix( lastWsResponse.spinState.reels.reelMatrix ) );
                            m_reels.setReels( SceneUtils.getLastRFSymbolsMatrix() );
                            SceneUtils.hideCovers();

                            // update data
                            if( isStateIdle() ) {
                                console.log( "Reconnection: Update data with current data" );
                                updateData( wsResponse );
                            }
                            else if( isStateDouble() ) {
                                console.log( "Reconnection: Update data with last spin response and start carrousel" );
                                updateData( lastWsResponse );
                                startCarrousel( lastWsResponse );
                            }
                            else {
                                updateData( wsResponse );
                                console.warn( "Reconnection: State no controlled to update data. Update with current data" );
                            }

                        }
                        else {
                            console.log( "Reconnection: Last response is not a RFWSSpinResponse. Restore the reels of " + eServerSceneId.BASE_GAME );
                            updateServerReelState( function() {
                                if( isStateIdle() ) {
                                    m_reels.setReels( SceneUtils.getLastRFSymbolsMatrix() );
                                }
                                SceneUtils.hideCovers();
                            } );
                        }
                        break;

                    default:
                        console.log( "Reconnection: Come from " + RFGlobalData.game.lastScene + ". Restore the reels of " + eSceneNames.BASEGAME );
                        drawResidualPrize( lastWsResponse );
                        updateServerReelState( function() {
                            if( isStateIdle() || isStateWaitingResidual() ) {
                                m_reels.setReels( SceneUtils.getLastRFSymbolsMatrix() );
                            }
                            SceneUtils.hideCovers();
                        } );
                        break;
                }

            }
            else {
                console.log( "Reconnection: No previous spin in " + eServerSceneId.BASE_GAME + " => we put a random reels" );
                SceneUtils.setLastRFSymbolsMatrix( SceneUtils.getLastRFSymbolsMatrix() || getRandomMatrixRFSymbols() );
                m_reels.setReels( SceneUtils.getLastRFSymbolsMatrix() );
                SceneUtils.hideCovers();
                m_rffsm.changeState( eStateNames.IDLE, {}, "Reconnection without last response. Start new game" );
            }

            RFGlobalData.game.lastScene = self.myScene;
            // Si venimos de autoJuego
            if( _.get( RFGlobalData, "game.autoGameMode.enable", false ) ) {
                var btn_auto = _.find( m_phaserButtons, function( o ) {
                    return ( ( o.action === "AUTO_X" ) && ( o.parameters.numAuto === SBG_NUM_AUTOGAMES ) );
                } );
                var waitTime = Phaser.Timer.SECOND + Phaser.Timer.HALF;
                self.game.time.events.add( waitTime, function() {
                    onBtnAutoX( btn_auto );
                } );
            }

        }, SceneUtils.fERROR );
    };

    var updateServerReelState = function( fCallbackOk, fCallbackError ) {
        var params = {
            sessionId: RFGlobalData.net.session,
            serverSceneId: eServerSceneId.BASE_GAME
        };
        RFGlobalData.net.wrapper.sendGetGameState( params, function( wsResponse ) {
            var lastWsResponse = RFWebServiceUtil.createRFWSResponse( wsResponse.gameState );
            console.log( "Last Response of updateServerReelState (" + params.serverSceneId + ")", lastWsResponse );
            if( lastWsResponse instanceof RFWSSpinResponse ) {
                SceneUtils.setLastRFSymbolsMatrix( SceneUtils.getRFSymbolsMatrix( lastWsResponse.spinState.reels.reelMatrix ) );
            }
            if( _.isFunction( fCallbackOk ) ) {
                fCallbackOk( wsResponse );
            }
        }, fCallbackError );
    };

    var getGameState = function( fCallbackOk, fCallbackError ) {
        var params = {
            sessionId: RFGlobalData.net.session
        };
        RFGlobalData.net.wrapper.sendGetGameState( params, function( wsResponse ) {
            onGetGameStateResponse( wsResponse );
            if( _.isFunction( fCallbackOk ) ) {
                fCallbackOk( wsResponse );
            }
        }, fCallbackError );
    };

    // #region ON ERROR
    SceneUtils.fERROR = function() {
        // TODO: Chapucilla para que no haya errores de llamadas concurrentes
        // if( response.status !== 429 ) {
        // Ante un error, se sale del juego
        var toSocket = {
            type: "EXIT",
            data: null
        };
        sendToSocket( toSocket );
        // }
    };
    // #endregion

    /**
     * @param {string} strLines número de líneas a incrementar (+), decrementar (-), o a modificar
     */
    var sendSetLines = function( strLines, fCallbackOk, fCallbackError ) {
        var params = {
            sessionId: RFGlobalData.net.session,
            signature: "0",
            lines: strLines
        };
        RFGlobalData.net.wrapper.sendSetLines( params, fCallbackOk, fCallbackError );
    };

    /**
     * @param {string} número de apuesta a incrementar (+), decrementar (-), o a modificar
     */
    var sendSetBetPerLine = function( strBetPerLine, fCallbackOk, fCallbackError ) {
        var params = {
            sessionId: RFGlobalData.net.session,
            signature: "0",
            betPerLine: strBetPerLine
        };
        RFGlobalData.net.wrapper.sendSetBetPerLine( params, fCallbackOk, fCallbackError );
    };

    var addActionsButtons = function( phaserButtons ) {
        _.forEach( phaserButtons, function( oPhaserButton ) {
            oPhaserButton.forceOut = true;
            var theFunction = getOnClickFunction( oPhaserButton.action );
            oPhaserButton.onInputUp.add( theFunction, self );
        } );
    };

    var addActionSkittels = function() {
        _.forEach( m_phaserSkittels, function( oPhaserSkittel ) {
            oPhaserSkittel.onInputOver.add( onSkittelOver, self );
            oPhaserSkittel.onInputOut.add( onSkittelOut, self );
            oPhaserSkittel.onInputDown.add( onSkittelDown, self );
            if( isSBG() ) { oPhaserSkittel.onInputUp.add( onSkittelUp, self ); }
        } );
    };

    var loadScene = function() {

        // Load all the scene
        m_rfLoader = m_super.getRFLoadScenes();
        SceneUtils.loadScene( self.myScene );

        // cacheamos
        m_texts = RFGlobalData.locale.texts;

        m_reels = m_rfLoader.getRFGraphicReels();

        m_phaserLabels = m_rfLoader.getPhaserLabels();
        m_phaserButtons = m_rfLoader.getPhaserButtons();
        m_phaserLabelsAuto = m_rfLoader.getPhaserLabelsAuto();

        m_phaserImagesAuto = m_rfLoader.getPhaserImagesAuto();

        // Para hacer que se pueda pulsar sobre el label que muestra los juegos automáticos
        if( typeof m_phaserImagesAuto.imgLabel !== "undefined" ) {
            m_phaserImagesAuto.imgLabel.inputEnabled = true;
            m_phaserImagesAuto.imgLabel.events.onInputUp.add( stopAutoGames, self );
            m_phaserImagesAuto.imgLabel.input.useHandCursor = true;
        }

        m_phaserButtonsAuto = m_rfLoader.getPhaserButtonsAuto();
        m_phaserSkittels = m_rfLoader.getPhaserSkittels();
        m_phaserLines = m_rfLoader.getPhaserLines();
        m_phaserButtonsDouble = _.union(
            SceneUtils.filterByAction( m_phaserButtons, eActionNames.goDouble ),
            SceneUtils.filterByAction( m_phaserButtons, eActionNames.takeWin )
        );
        m_phaserVolumeButtons = SceneUtils.filterByAction( m_phaserButtons, eActionNames.volumeUp );

        // Ocultamos los botones de denominación cuando se trata de una sbg
        m_phaserButtonsDenom = SceneUtils.filterByAction( m_phaserButtons, eActionNames.changeDenom );
        if( isSBG() ) {
            _.forEach( m_phaserButtonsDenom, function( oButton ) {
                oButton.visible = false;
            } );
        }

        var buttonAuto = SceneUtils.filterByAction( m_phaserButtonsAuto, eActionNames.autoGame );
        if( buttonAuto.length > 1 ) {
            console.warn( "Multiples AUTO_GAME action in this scene" );
        }
        else if( buttonAuto.length === 1 ) {
            m_phaserButtonAuto = _.head( buttonAuto );
        }
        else {
            console.warn( "No AUTO_GAME action in this scene!" );
        }

        m_phaserButtonsAutoX = SceneUtils.filterByAction( m_phaserButtonsAuto, eActionNames.autoX );

        SceneUtils.initSound( m_audioSounds.bonus, eSoundTypes.BONUS );
        SceneUtils.initSound( m_audioSounds.lineUp, eSoundTypes.LINE_UP );
        SceneUtils.initSound( m_audioSounds.betUp, eSoundTypes.BET_UP );
        SceneUtils.initSound( m_audioSounds.bkgBasegame, eSoundTypes.BKG_BASEGAME );
        SceneUtils.initSound( m_audioSounds.denomUp, eSoundTypes.DENOM_UP );
        SceneUtils.initSound( m_audioSounds.limitExceeded, eSoundTypes.LIMIT_EXCEEDED );
        SceneUtils.initSound( m_audioSounds.insufficientFounds, eSoundTypes.INSUFFICIENT_FOUNDS );
        SceneUtils.initSound( m_audioSounds.cantSpin, eSoundTypes.CANT_SPIN );
        SceneUtils.initSound( m_audioSounds.bigPrize, eSoundTypes.BIG_PRIZE );
        SceneUtils.initSound( m_audioSounds.betMax, eSoundTypes.BET_MAX );

        addCounterSoundsToLabels();
        addCounterTweenToLabels();

        refreshVolumeButton();

        SceneUtils.onMasterVolumeChanged.add( refreshVolumeButton );
        SceneUtils.onGameResumed.add( onGameResumed );

        SceneUtils.handleTransitionSymbolsForReels( m_reels );
    };

    var onGameResumed = function() {

        // Bloqueamos el juego para esperar la actualización del nuevo cash actualizado.
        self.game.paused = true;

        m_data.isCashDirty = true;

        RFGlobalData.net.wrapper.sendGetBalance( {}, function( wsResponse ) {
            m_data.cashValueWhenIsDirty = wsResponse.playerBalance;

            // En Idle actualizamos el valor del dinero.
            if( isStateIdle() ) {
                console.log( "GAME RESUMED: se actualiza el valor del cash actual.", m_data.cash, "=>", m_data.cashValueWhenIsDirty );
                m_data.cash = m_data.cashValueWhenIsDirty;
                m_data.isCashDirty = false;
                m_data.cashValueWhenIsDirty = -1;

                drawCash( m_data.cash, 0 );
                checkMoney();
            }
            else {
                console.warn( "GAME RESUMED: Estamos en mitad de algún proceso y cuando se actualice el valor en updateData, se tendrá en cuenta el cash modificado:", m_data.cashValueWhenIsDirty );
            }

            self.game.paused = false;

        }, function( wsResponse ) {
            self.game.paused = false;
            SceneUtils.fERROR( wsResponse );
        } );

    };

    var addCounterSoundsToLabels = function() {
        var soundType = eSoundTypes.COUNTER;
        var rfSounds = m_rfLoader.getRFSounds();
        var filteredSounds = SceneUtils.filterByType( rfSounds, soundType );
        if( filteredSounds.length > 0 ) {

            // init RFAudioSound
            var soundKeys = _.head( filteredSounds ).soundKeys;
            var rfAudioSound = new RFAudioSound( game );
            rfAudioSound.setSounds( soundKeys );

            var creditLabels = SceneUtils.filterByType( m_phaserLabels, eLabelsNames.msgCredits );
            var label = _.head( creditLabels );
            label.setRFAudioSound( rfAudioSound );

            if( filteredSounds.length > 1 ) {
                console.warn( "Multiple sounds defined for this type:", soundType );
            }

        }
        else {
            console.warn( "No sounds found for counters", soundType );
        }
    };

    var addCounterTweenToLabels = function() {
        var applyTimeToFinish = function( oLabels, timeToFinish ) {
            _.forEach( oLabels, function( oLabel ) {
                oLabel.setTimeToFinish( timeToFinish );
            } );
        };
        applyTimeToFinish( SceneUtils.filterByType( m_phaserLabels, eLabelsNames.msgCredits ), MS_TWEEN_COUNTER_CREDITS );
        applyTimeToFinish( lbl_win, MS_TWEEN_COUNTER_WIN );
    };

    var lineVisible = function( buttonOrId ) {
        var value = _.isObject( buttonOrId ) && _.has( buttonOrId, "id" ) ? buttonOrId.id : buttonOrId;
        if( !_.isNumber( value ) ) { throw new Error( "Invalid button Or Id" ); }
        setVisibility( m_phaserLines, value );
    };

    /**
     * Se usa solo para lineas y skittles y cambia la visibilidad del mapa de objectos pasado.
     */
    var setVisibility = function( oPhaserLineOrSkittel, iValue ) {
        _.forEach( oPhaserLineOrSkittel, function( oLineOrSkittel ) {
            if( _.has( oLineOrSkittel, "id" ) ) {
                if( parseInt( oLineOrSkittel.id, 10 ) > iValue ) {
                    oLineOrSkittel.alpha = DISABLE_OPACITY;
                }
                else {
                    oLineOrSkittel.alpha = 1;
                }
            }
        } );
    };

    var hideLines = function() {
        _.forEach( m_phaserLines, function( oPhaserLine ) {
            fadeOutLine( oPhaserLine.id, MS_FADEOUT_LINE );
        } );
    };

    var sendCurrentPayTable = function() {
        var toSocket = {
            type: "PAYTABLE",
            data: {
                currentScene: self.myScene,
                payTable: m_data.payTable
            }
        };
        sendToSocket( toSocket );
    };

    var updateCurrentPayTable = function( rfwsResponse ) {
        if( isSBG() && RFGlobalData.game.useLocalBets ) {
            m_data.payTable = SceneUtils.makePayTable( eServerSceneId.BASE_GAME );
        }
        else if( isSBG() && _.has( rfwsResponse, "payTable.prizeList" ) ) {
            m_data.payTable = rfwsResponse.payTable.prizeList;
        }
    };

    var updateData = function( rfwsResponse ) {
        SceneUtils.updateGlobalData( rfwsResponse );

        var rfwsPlayer = _.get( rfwsResponse, [ "player" ] );

        // Si player no tiene cash y está en la respuesta, se añade a pelo
        if( ( _.has( rfwsResponse, "cash" ) ) && ( !_.has( rfwsResponse, "player.cash" ) ) ) { rfwsResponse.player.cash = rfwsResponse.cash; }

        if( m_data.isCashDirty ) {
            if( m_data.cashValueWhenIsDirty >= 0 ) {
                console.log( "Cash dirty: actualizamos con el valor", m_data.cashValueWhenIsDirty, "en lugar de", rfwsResponse.player.cash );
                m_data.cash = m_data.cashValueWhenIsDirty;
                m_data.isCashDirty = false;
                m_data.cashValueWhenIsDirty = -1;
            }
            else {
                console.error( "El cash es dirty y el valor a actualizar es incorrecto. Dejamos el valor de la respuesta" );
                m_data.cash = rfwsResponse.player.cash;
            }
        }
        else {
            m_data.cash = rfwsResponse.player.cash;
        }

        if( RFGlobalData.game.useLocalBets && SceneUtils.isDirtyLocalBet() ) {
            if( rfwsPlayer ) {
                var oLocalBet = {
                    betPerLine: String( rfwsPlayer.internalBet ),
                    lines: String( rfwsPlayer.numLines ),
                    denom: String( rfwsPlayer.denom ),
                    needsToDrawData: false
                };
                updateLocalBet( oLocalBet );
                SceneUtils.setDirtyLocalBet( false );
            }
        }

        updateCurrentPayTable( rfwsResponse );
        sendCurrentPayTable();
        drawData( rfwsResponse );
        checkMoney( rfwsResponse );
    };

    var updateNextReelIdsToSpinArray = function( data ) {
        console.log( "updateNextReelIdsToSpinArray", data );

        if( data instanceof RFWSSpinResponse ) {
            self.nextReelIdsToSpinArray = _.get( data, [ "spinState", "nextReelIdsToSpinArray" ], undefined );
        }
        else {
            self.nextReelIdsToSpinArray = _.get( data, [ "gameState", "play", "spinState", "spinReelIds" ], undefined );
        }
    };

    var updateCash = function() {
        // Si están girando los rodillos no permito recibir el saldo para que no se pinten cosas raras
        if( !isStateSpinning() ) {
            RFGlobalData.net.wrapper.sendGetBalance( {}, function( wsResponse ) {
                m_data.cash = wsResponse.playerBalance;
                console.log( "WS RESPONSE", wsResponse );
                drawCash( m_data.cash, 0 );
                checkMoney();
            }, SceneUtils.fERROR );
        }

    };

    var updateCashByGS = function() {
        RFGlobalData.net.wrapper.sendGenericRequest( "updateBet", null, function( wsResponse ) {
            console.log( "WS RESPONSE", wsResponse );
            updateData( wsResponse.response );
        }, SceneUtils.fERROR );
    };

    var decreaseCash = function( data ) {
        updateLastPressedButtonToNow();
        var decreasedBalance = data.remainingAmount;
        m_data.cash = decreasedBalance;
        drawCash( m_data.cash, 0 );
    };

    var updateCashBySocket = function( data ) {
        var money = _.get( data, [ "value" ] );
        updateLastPressedButtonToNow();
        if( _.isNumber( money ) ) {
            m_data.cash += money;
            drawCash( m_data.cash, 0 );

            if( m_data.cash > 0 ) { sendActiveButtons(); }
            else { turnOffButtonsWithoutCash(); }

            /* if( playerData.cash === LOTTERY_MINIMUM_CASH ) {
                if( isStateIdle() ) {
                    m_rffsm.changeState( eStateNames.LOTTERY );
                }
            } */
        }
        else {
            console.warn( "Balance no es un número" );
        }

        // Si el estado es SPIN, se suma al playerBalance
        if( isStateSpinning() ) { m_data.balanceIncreased += money; }

        updateCash();

        if( isStateLottery() ) {
            var toSocket = {
                type: "CLOSE_LOTTERY",
                data: null
            };
            sendToSocket( toSocket );
        }

    };

    /* var cket = function( balance, useWrapper ) {
        // El wrapper ya divide el playerBalance. Cuando nos viene la información por socket, no se realiza dicha división.
        var cash = 0;
        if( !useWrapper ) cash = balance / 1000;
        else cash = balance;

        playerData.cash = cash;
        _.forEach( m_phaserLabels, function( oPhaserLabel, key ) {
            switch( oPhaserLabel.type ) {
                case eLabelsNames.msgCredits: oPhaserLabel.setMoneyOrCredits( cash, RFGlobalData.locale.currencyISO, 0 ); break;
            }
        } );

        // Si el dinero introducido es igual que el valor mínimo para entrar al Doble o Nada, se entra en el Doble o Nada
        if( playerData.cash === LOTTERY_MINIMUM_CASH ) {
            if( isStateIdle() ) {
                m_rffsm.changeState( eStateNames.LOTTERY );
            }
        }
    }; */

    var updateLocalBet = function( oLocalBet ) {
        var needsToDrawData = _.get( oLocalBet, [ "needsToDrawData" ], true );
        _.set( oLocalBet, "serverSceneId", eServerSceneId.BASE_GAME );

        SceneUtils.updateLocalBet( oLocalBet, m_data );

        if( needsToDrawData ) {
            drawPlayerData();
        }

        checkMoney();
        updateCurrentPayTable();
        sendCurrentPayTable();

        if( SceneUtils.isCurrentEnvironmentEqualsTo( eEnvironments.HOMOLOGATION ) ) {
            RFGlobalData.net.retailNotificationWrapper.sendMaxBetReachedRequest( { "maxBetReach": SceneUtils.isMaximumBet( eServerSceneId.BASE_GAME, m_data.cash ) } );
        }
    };

    var drawNumAutoGames = function( iNumAutoGames ) {
        _.set( RFGlobalData, "game.autoGameMode.numAutoGames", m_numAutoGames );
        _.forEach( m_phaserLabelsAuto, function( oPhaserLabelsAuto ) {
            oPhaserLabelsAuto.text = iNumAutoGames;
        } );
    };

    var drawData = function( rfwsResponse ) {
        if( _.has( rfwsResponse, "player" ) ) {
            drawPlayerData( rfwsResponse.player, rfwsResponse.spinState );
        }

        if( _.has( rfwsResponse, "spinState" ) ) {
            drawSpinStateData( rfwsResponse.spinState, rfwsResponse.player );
        }

    };

    var drawPlayerData = function( rfwsPlayer, rfWSSpinState ) {

        var betPerLine = RFGlobalData.game.useLocalBets ? m_data.betPerLine : rfwsPlayer.betPerLine;
        var lines = RFGlobalData.game.useLocalBets ? m_data.lines : rfwsPlayer.numLines;
        var denom = RFGlobalData.game.useLocalBets ? m_data.denom : rfwsPlayer.denom;
        var totalBet = RFGlobalData.game.useLocalBets ? m_data.totalBet : rfwsPlayer.totalBet;
        var cash = RFGlobalData.game.useLocalBets ? m_data.cash : rfwsPlayer.cash;

        var prize = _.get( rfWSSpinState, "prize", 0 );

        var textMsgSecondary = String.format( m_texts.PLAYING, lines, betPerLine, lines > 1 ? "s" : "", SceneUtils.formatMoneyOrCredits( betPerLine, RFGlobalData.locale.currencyISO ) );

        lineVisible( lines );

        /* eslint-disable complexity */
        _.forEach( m_phaserLabels, function( oPhaserLabel ) {
            switch( oPhaserLabel.type ) {
                case eLabelsNames.msgTotalBet: oPhaserLabel.setMoneyOrCredits( totalBet, RFGlobalData.locale.currencyISO ); break;
                case eLabelsNames.msgCredits:
                    // If no hay prize, then pinto el cash
                    if( prize === 0 ) {
                        oPhaserLabel.setMoneyOrCredits( cash, RFGlobalData.locale.currencyISO, 0 );
                    }
                    break;
                case eLabelsNames.msgDenom:
                    if( !isSBG() ) { oPhaserLabel.setMoney( denom, RFGlobalData.locale.currencyISO ); }
                    else { oPhaserLabel.visible = false; }
                    break;
                case eLabelsNames.msgLine:
                    // Se oculta cuando se trata de sbg
                    if( !isSBG() ) { oPhaserLabel.setString( lines ); }
                    else { oPhaserLabel.visible = false; }
                    break;
                case eLabelsNames.msgBetPerLine:
                    // Se oculta cuando se trata de sbg
                    if( !isSBG() ) { oPhaserLabel.setMoneyOrCredits( betPerLine, RFGlobalData.locale.currencyISO ); }
                    else { oPhaserLabel.visible = false; }
                    break;
                case eLabelsNames.msgSecundary: oPhaserLabel.setFormattedString( textMsgSecondary ); break;
                default: break;
            }
        } );
        /* eslint-enable */

        // Añadimos a playerData los datos del jugador. Obviamente. Qué pedazo de código.
        m_data.totalBet = totalBet;
        m_data.cash = cash;
    };

    var drawSpinStateData = function( rfWSSpinState ) {
        var prize = rfWSSpinState.prize;

        if( prize > 0 ) {
            if( !hasToDisplayPrizeLines() ) {
                var text = String.format( m_texts.CONGRATULATIONS, SceneUtils.formatMoneyOrCredits( prize, RFGlobalData.locale.currencyISO ) );
                drawPrize( prize, MS_TWEEN_COUNTER_WIN, text );
            }
            else {
                // This draw will be update on the startCarrouselDisplayingLinePrizes function
            }

            winningPizes( rfWSSpinState );
        }
        else {
            // Enviamos que se ha acabado el juego al servidor
            if( isSBG() ) {
                var toSocket = {
                    type: "LOSE",
                    data: null
                };
                sendToSocket( toSocket );
            }

            var currentServerSceneId = _.get( m_data.lastSpinResponse, [ "player", "currentServerSceneId" ] );
            if( currentServerSceneId === eServerSceneId.BASE_GAME ) {
                drawLabelMain( m_texts.TRY );
            }
        }
    };

    var drawPrize = function( prize, twTime, text ) {
        if( prize !== undefined ) {
            drawLabelWin( prize, twTime );
        }
        if( text !== undefined ) {
            drawLabelMain( text );
        }
    };

    var drawLabelMain = function( text ) {
        var mainLabels = SceneUtils.filterByType( m_phaserLabels, eLabelsNames.msgMain );
        _.forEach( mainLabels, function( oPhaserLabel ) {
            oPhaserLabel.setFormattedString( text );
        } );
    };


    var drawLabelWin = function( value, twTime ) {
        var winLabels = SceneUtils.filterByType( m_phaserLabels, eLabelsNames.msgWin );
        _.forEach( winLabels, function( oPhaserLabel ) {
            oPhaserLabel.setMoneyOrCredits( value, RFGlobalData.locale.currencyISO, twTime );
        } );
    };


    var drawCash = function( value, twTime ) {
        var cashLabels = SceneUtils.filterByType( m_phaserLabels, eLabelsNames.msgCredits );
        _.forEach( cashLabels, function( oPhaserLabel ) {
            oPhaserLabel.setMoneyOrCredits( value, RFGlobalData.locale.currencyISO, twTime );
        } );
    };

    var drawTotalPrize = function( prize, twTime ) {
        prize = _.isNumber( prize ) ? prize : _.get( m_data.lastSpinResponse, [ "spinState", "prize" ], 0 );
        var text = String.format( m_texts.CONGRATULATIONS, SceneUtils.formatMoneyOrCredits( prize, RFGlobalData.locale.currencyISO ) );
        drawPrize( prize, twTime, text );
    };

    var transferPrizeToCash = function( twTime ) {
        drawLabelWin( 0, twTime );
        drawCash( m_data.cash, twTime );
    };

    var checkMoney = function( wsResponse ) {

        function checkMoneyInternal() {
            if( isBetLowerThanCash( { cash: cash, totalBet: totalBet } ) ) {
                limitExceeded = true;
            }
        }

        var labelsToShow = getInformationLabels();

        var limitExceeded = false;
        var insufficientFounds = false;

        var cash = m_data.cash;
        var totalBet = m_data.totalBet;

        if( wsResponse !== undefined ) {
            // Server response
            switch( _.get( wsResponse, [ "result", "returnCode" ] ) ) {
                case eServerReturnCodes.INSUFFICIENT_FOUNDS:
                    insufficientFounds = true;
                    break;
                case eServerReturnCodes.LIMIT_EXCEEDED:
                    limitExceeded = true;
                    break;
                default:
                    break;
            }
        }

        if( RFGlobalData.game.useLocalBets ) {
            checkMoneyInternal();
        }
        else {
            // check money with player data
            var rfwsPlayer = _.get( wsResponse, [ "player" ] );
            if( rfwsPlayer instanceof RFWSPlayer ) {
                cash = rfwsPlayer.cash;
                totalBet = rfwsPlayer.totalBet;
                checkMoneyInternal();
            }
        }

        m_canSpin = false;
        if( insufficientFounds ) {
            SceneUtils.setTextAll( labelsToShow, m_texts.INSUFFICIENT_FOUNDS );
            m_audioSounds.insufficientFounds.play();
        }
        else if( limitExceeded ) {
            _.forEach( labelsToShow, function( oPhaserLabel ) {
                oPhaserLabel.setFormattedString( m_texts.LIMIT_EXCEEDED );
            } );
            // if( !isStateLottery() ) { m_audioSounds.limitExceeded.play(); }
        }
        else {
            // eslint-disable-next-line max-len
            var textMsgSecundary = String.format( m_texts.PLAYING, m_data.lines, m_data.betPerLine, m_data.lines > 1 ? "s" : "", SceneUtils.formatMoneyOrCredits( m_data.betPerLine, RFGlobalData.locale.currencyISO ) );
            _.forEach( m_phaserLabels, function( oPhaserLabel ) {
                switch( oPhaserLabel.type ) {
                    case eLabelsNames.msgSecundary: oPhaserLabel.setFormattedString( textMsgSecundary ); break;
                    default: break;
                }
            } );
            m_canSpin = true;
        }

        if( !m_canSpin ) {
            stopAutoGames();
        }

    };

    /** Muestra cosas chachis si se obtiene premio */
    var winningPizes = function( data ) {

        var symbolsArray = [];
        _.forEach( data.winPrizesArray, function( winPrize ) {
            console.log( "SÍMBOLOS", winPrize.line );
            _.forEach( winPrize.line, function( symbol ) {
                symbolsArray.push( { "column": symbol[ 0 ], "row": symbol[ 1 ] } );
            } );
        } );
        if( isSBG() ) {
            var toSocket = {
                type: "WIN",
                data: data
            };
            sendToSocket( toSocket );
        }

    };

    /**
     * Returns an array labels to show information.
     * @param {boolean} isPriority if it's true, return mainLabels. Otherwise, msgSecundary (if exists, or mainLabels if not)
     * @return {array} Array of labels
     */
    var getInformationLabels = function( isPriority ) {

        var mainLabels, secundaryLabels;
        mainLabels = SceneUtils.filterByType( m_phaserLabels, eLabelsNames.msgMain );
        secundaryLabels = SceneUtils.filterByType( m_phaserLabels, eLabelsNames.msgSecundary );

        isPriority = _.isBoolean( isPriority ) ? isPriority : false;

        var labelsToShow;

        if( isPriority ) {
            labelsToShow = ( _.size( mainLabels ) > 0 ) ? mainLabels : secundaryLabels;
        }
        else {
            labelsToShow = ( _.size( secundaryLabels ) > 0 ) ? secundaryLabels : mainLabels;
        }

        if( _.size( labelsToShow ) === 0 ) {
            console.warn( "No labels (main or secundary), to show information" );
        }

        return labelsToShow;
    };

    var enablePlayableButtons = function( bEnable ) {
        _.forEach( m_phaserButtons, function( oButton ) {
            switch( oButton.action ) {
                case eActionNames.goDouble:
                case eActionNames.takeWin:
                    break;
                default:
                    oButton.enable( bEnable );
            }
        } );

        _.forEach( m_phaserButtonsAuto, function( oButton ) {
            oButton.enable( bEnable );
        } );
    };

    var enableSpinButton = function( bEnable ) {
        _.forEach( m_phaserButtons, function( oButton ) {
            if( oButton.action === eActionNames.spin ) {
                oButton.enable( bEnable );
            }
        } );
    };


    // #region UTILS AND HELPERS
    var turnOffButtonsWithoutCash = function() {
        if( m_data.cash === 0 ) {
            // Si el jugador no tiene saldo, encendemos solo el botón de Cambio de juego
            var toSocket = {
                type: "ENABLE_BUTTON_LIST",
                data: {
                    buttonIdList: [ "EXIT" ]
                }
            };
            sendToSocket( toSocket );
        }
    };

    var getLotteryUrl = function() {
        var token = _.get( QueryParameters, "token" );
        if( _.isUndefined( token ) ) { console.warn( "TOKEN ERROR", "Unable to find the user's token in the URL" ); }
        // TODO Cambiar esta chapuza, deberíamos recibir la url del Doble o Nada por el socket
        var urlToLaunch;
        if( !SceneUtils.checkEnvironment( eEnvironments.PRODUCTION ) ) {
            urlToLaunch = "https://demo.rfrancogames.es/lobby/gs/DOUBLEORNOTHING/index.html?token=" + token + "&local=true";
        }
        else {
            urlToLaunch = "https://prod.kolyseo.es/lobby/gs/DOUBLEORNOTHING/index.html?&lang=es&forreal=false&useCredits=false&token=" + token + "&local=true";
        }
        return urlToLaunch;
    };

    var isSBG = function() {
        return _.get( RFGameConfig, "sbg.enable", false );
    };

    var canSpin = function() {
        if( !m_canSpin ) {
            m_audioSounds.cantSpin.play();
        }
        return m_canSpin;
    };

    var isBetLowerThanCash = function( container ) {
        return ( precise( container.cash ) < precise( container.totalBet ) );
    };

    var showAutoGameMenu = function( bShow ) {
        if( _.isBoolean( bShow ) ) {
            if( _.has( m_phaserImagesAuto, "imgBackground" ) ) {
                m_phaserImagesAuto.imgBackground.visible = bShow;
            }
            SceneUtils.setVisibleAll( m_phaserButtonsAutoX, bShow );
            if( isSBG() ) { SceneUtils.setVisibleAll( m_phaserLabelsAuto, false ); }
            else { SceneUtils.setVisibleAll( m_phaserLabelsAuto, m_autoGameMode ); }

            if( isSBG() ) { m_phaserImagesAuto.imgLabel.visible = m_autoGameMode; }

            // Por si no existe el botón del auto
            if( !_.isUndefined ) { m_phaserButtonAuto.visible = !m_autoGameMode; }
            m_autoGameMenuVisible = bShow;
        }
        else {
            console.warn( "showAutoGameMenu, bShow has invalid value", bShow );
        }
    };

    /*
    0.1.75c - para conseguir un fade del volumen
    */
    var changeAudiosVolumen = function( volume, listOfAudios ) {
        _.forEach( listOfAudios, function( oAudio ) {
            var tween = self.game.add.tween( oAudio ).to( { volume: volume }, MS_SOUND_FADE );
            tween.start();
        } );
    };

    var updateLastPressedButtonToNow = function() {
        m_data.msLastPressedButton = self.game.time.now;
    };

    /**
     * Para la reproducción de todos los sonidos de la escena
     */
    var stopAllSounds = function() {
        _.forEach( m_audioSounds, function( audioSound ) {
            audioSound.stop();
        } );
        self.game.sound.stopAll();
    };

    var isForced = function() {
        return m_oForceReels.forceNextGame;
    };

    var isSpecialForced = function() {
        return _.size( m_specialForceReels ) === m_reels.getNumberOfReels();
    };

    var isStateIdle = function() {
        return m_rffsm.getState() === eStateNames.IDLE;
    };

    var isStateCashOut = function() {
        return m_rffsm.getState() === eStateNames.CASH_OUT;
    };

    var isStateLottery = function() {
        return m_rffsm.getState() === eStateNames.LOTTERY;
    };

    var isStatePreIdle = function() {
        return m_rffsm.getState() === eStateNames.PRE_IDLE;
    };

    var isStateWaitingResidual = function() {
        return m_rffsm.getState() === eStateNames.WAITING_RESIDUAL;
    };

    var isStateSpinning = function() {
        return m_rffsm.getState() === eStateNames.SPINNING;
    };

    var isStateDouble = function() {
        return m_rffsm.getState() === eStateNames.DOUBLE;
    };

    var isStateTakingWin = function() {
        return m_rffsm.getState() === eStateNames.TAKING_WIN;
    };

    var isStateDisplayingCarrousel = function() {
        return m_rffsm.getState() === eStateNames.DISPLAYING_CARROUSEL;
    };

    var isStateDisplayingBigPrize = function() {
        return m_rffsm.getState() === eStateNames.DISPLAYING_BIGPRIZE;
    };

    var canBigPrizeBeSkipped = function() {
        return _.get( RFGameConfig, [ "game", "canBigPrizeBeSkipped" ], false );
    };

    var hasToWaitUntilDisplayAllPrizeLines = function() {
        return _.get( RFGameConfig, [ "game", "waitUntilDisplayAllPrizeLines" ], false );
    };

    var hasToDisplayPrizeLines = function() {
        return _.get( RFGameConfig, [ "game", "displayPrizeLines" ], false );
    };

    var hasBigPrizeListeners = function() {
        return self.onBigPrize.getNumListeners() > 0;
    };

    // #endregion

    // ===================================== FSM ===
    var eStateNames = {
        INIT: "INIT",
        PRE_IDLE: "PRE_IDLE",
        IDLE: "IDLE",
        CHANGING_BET: "CHANGING_BET",
        SPINNING: "SPINNING",
        DISPLAYING_CARROUSEL: "DISPLAYING_CARROUSEL",
        DISPLAYING_BIGPRIZE: "DISPLAYING_BIGPRIZE",
        DOUBLE: "DOUBLE",
        TAKING_WIN: "TAKING_WIN",
        WAITING_RESIDUAL: "WAITING_RESIDUAL",
        CASH_OUT: "CASH_OUT",
        LOTTERY: "LOTTERY",
        EXIT: "EXIT"
    };
    Object.freeze( eStateNames );

    var m_fsmInitData = {
        game: game,
        debug: true,
        states: {
            INIT:                 { onStart: startInit,                                                 onExit: exitInit },
            PRE_IDLE:             { onStart: startPreIdle,             onUpdate: updatePreIdle,         onExit: null },
            IDLE:                 { onStart: startIdle,                onUpdate: loopIdle,              onExit: exitIdle },
            CHANGING_BET:         { onStart: null,                                                      onExit: null },
            WAITING_RESIDUAL:     { onStart: startWaitingResidual,     onUpdate: updateWaitingResidual, onExit: exitWaitingResidual },
            SPINNING:             { onStart: startSpinning,                                             onExit: exitSpinning },
            DISPLAYING_CARROUSEL: { onStart: startDisplayingCarrousel,                                  onExit: null },
            DISPLAYING_BIGPRIZE:  { onStart: startDisplayingBigPrize,                                   onExit: null },
            CASH_OUT:             { onStart: startCashingOut,                                           onExit: null },
            TAKING_WIN:           { onStart: startTakingWin,                                            onExit: null },
            LOTTERY:              { onStart: startLottery,                                              onExit: exitLottery },
            EXIT:                 { onStart: startExit,                                                 onExit: null }
        }
    };
    // ===================================== END FSM ===


};

BaseGameScene.prototype = Object.create( BaseScene.prototype );
BaseGameScene.prototype.constructor = BaseGameScene;

//------------------------------------------------------
// Source: src/sbg/game/scenes/rf.scene.boot.js
//------------------------------------------------------

/**
 * @version 0.4.0
 * CHANGELOG:
 * 0.0.2 - Se añade el escalado del juego dentro del boot (en el index el alto y el ancho del juego ha de ser 1),
 *         por compatibilidad con versiones antiguas de iPad
 * 0.0.3 - Se soluciona el problema de escalado en dispositivos iOS.
 * 0.0.4 - Realiza la carga del plugin de comportamiento
 * 0.0.5 - Se añade el pageAlignVertically a true, para que el canvas del juego se centre verticalmente en la página que lo muestra
 * 0.1.0 - Se añade parte protegida para slo2game. Cambia la forma de imprimir por consola el arranque del juego
 *         para que funcione la tarea removelogging del grunt de producción.
 * 0.1.1 - Parametrización de color en el arranque
 * 0.2.0 - Carga el archivo de configuración de entorno y sobreescribe los valores necesarios
 * 0.2.1 - Se añade el fix para arreglar el problema con el audio de Android y Chrome
 * 0.2.2 - Se añade el fix de audio para hacer que el audio se resuma con una interacción del usuario (thanks Chrome)
 * 0.3.0 - Extrae la información de si es móvil o no, desde RFlobalData.
 * 0.4.0 - Se extrae nueva información respecto a los endpotins del env.json.
 */

// eslint-disable-next-line no-implicit-globals
var BootScene = function( game ) {
    "use strict";

    // Call to base Scene Constructor
    var _protected = BaseScene.call( this, game ) || {};

    var m_super = this;
    var self = this;

    var colorGameVersion = RFDebugUtils.getConsoleColors().INFO_GAME_VERSION;
    var gameVersion = RFGameConfig.gameServer.gameUid + " - " +
        RFGameConfig.version + " ~ " + RFGameConfig.build;
    console.info( "%c" + gameVersion, colorGameVersion ); /* RemoveLogging:skip */

    this.preload = function() {

        // prevents stop updating game on navigator focus change
        self.game.stage.disableVisibilityChange = true;

        // Load GameDefinition File
        loadGameDefinitionFile( "js/game/appdata/rf.gamedefinition" + ( RFGlobalData.device.isMobile ? ".mobile" : "" ) + ".json" );

        // Override values with enviroment settings
        var envJson = loadEnvJsonFile( "./conf/env.json?date=" + Date.now() );
        RFGameConfig.gameServer.isForFun = _.get( envJson, [ "game", "forFun" ], RFGameConfig.gameServer.isForFun );
        RFGameConfig.gameServer.url = _.get( envJson, [ "session", "url" ], RFGameConfig.gameServer.url );

        // overwrite with the new configuration of the env.json
        RFGameConfig.gameServer.url = _.get( envJson, [ "endPoints", "gameServer" ], RFGameConfig.gameServer.url );
        RFGameConfig.sbg.rest = _.get( envJson, [ "endPoints", "apiRetail" ], RFGameConfig.sbg.rest );
        RFGameConfig.environment.id = _.get( envJson, [ "environment", "id" ], RFGameConfig.environment.id );

        if( RFGameConfig.environment.id === eEnvironments.HOMOLOGATION ) {
            RFGameConfig.game.autoPlayWithCash = false;
        }

        console.log( "%cENVIRONMENT: %s", RFDebugUtils.getConsoleColors().INFO, RFGameConfig.environment.id );

        // Call to parent
        m_super.preloadBase( { showLoadingBar: false } );
    };

    this.create = function() {

        // Cargamos el plugin de componentes
        if( !self.game.behaviorPlugin ) {
            self.game.behaviorPlugin = self.game.plugins.add( Phaser.Plugin.Behavior );
        }

        // Call to parent
        m_super.createBase();

        if( _.get( RFGameConfig, [ "game", "useTransitions" ], false ) ) {
            self.game.transitions = self.game.plugins.add( Phaser.Plugin.Transition );
        }

        self.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        self.game.scale.pageAlignHorizontally = true;
        self.game.scale.pageAlignVertically = true;
        self.game.scale.setGameSize( RFGameConfig.screen.width, RFGameConfig.screen.height );
        self.game.scale.refresh();

        // Soluciona el problema de reescalado en dispositivos iOS
        self.game.scale.onOrientationChange.add( function() {
            self.game.canvas.style.height = "0px";
            self.game.scale.setGameSize( RFGameConfig.screen.width, RFGameConfig.screen.height );
            self.game.scale.refresh();
        }, self );

        fixAudioProblems();

        self.game.state.start( eSceneNames.PRELOAD );

    };

    // #region Audio Fixes
    var fixAudioProblems = function() {

        // Fix para el sonido en Android y Chrome
        // info: https://github.com/photonstorm/phaser/issues/2913
        function fixAudioAndroidAndChrome() {
            var CHROME_VERSION = 55;
            var SAMPLE_RATE = 22050;

            var hasToFix = self.game.device.android && self.game.device.chrome && self.game.device.chromeVersion >= CHROME_VERSION;
            if( hasToFix ) {
                self.game.sound.touchLocked = true;
                self.game.input.touch.addTouchLockCallback( function() {
                    /* eslint-disable no-invalid-this */
                    if( this.noAudio || !this.touchLocked || this._unlockSource !== null ) {
                        return true;
                    }
                    if( this.usingWebAudio ) {
                        // Create empty buffer and play it
                        // The SoundManager.update loop captures the state of it and then resets touchLocked to false
                        var buffer = this.context.createBuffer( 1, 1, SAMPLE_RATE );
                        this._unlockSource = this.context.createBufferSource();
                        this._unlockSource.buffer = buffer;
                        this._unlockSource.connect( this.context.destination );

                        if( this._unlockSource.start === undefined ) {
                            this._unlockSource.noteOn( 0 );
                        }
                        else {
                            this._unlockSource.start( 0 );
                        }

                        // Hello Chrome 55!
                        if( this._unlockSource.context.state === "suspended" ) {
                            this._unlockSource.context.resume();
                        }
                    }
                    /* eslint-enable */

                    //  We can remove the event because we've done what we needed (started the unlock sound playing)
                    return true;

                }, self.game.sound, true );
            }
        }

        function fixAudioUserInteraction() {
            function resumeAudio() {
                if( game.sound.context.state === "suspended" ) {
                    game.sound.context.resume();
                }
            }
            game.canvas.addEventListener( "touchend", resumeAudio );
            game.canvas.addEventListener( "click", resumeAudio );
            window.document.addEventListener( "keydown", resumeAudio );
        }


        fixAudioAndroidAndChrome();
        fixAudioUserInteraction();
    };
    // #endregion

    self.shutdown = function() {

        // call to parent
        m_super.shutdownBase();

    };

    if( self.constructor !== BootScene ) { return _protected; }
    return undefined;
};


BootScene.prototype = Object.create( BaseScene.prototype );
BootScene.prototype.constructor = BootScene;

//------------------------------------------------------
// Source: src/sbg/game/scenes/rf.scene.freegame.js
//------------------------------------------------------

/* eslint-disable max-lines */

/* jshint -W087 */ // Ignore debugger

/* eslint-disable max-len */
/**
 * @version - CUSTOM SBG 0.3.0c
 * CAMBIOS:
 * 0.3.0c  - Se actualiza el balance ante un onGameResumed
 * 0.2.20c - Se libera el sprite del m_fadeSprite al salir de la escena.
 * 0.2.19c - Se han añadido los métodos para crear la tabla de pagos con la tabla de pago del Servidor.
 * 0.2.18c - Se ha modificado el método stopSpin para que tenga en cuenta el máximo de prizeHype
 *           que pueden aparecer en este spin (para parar a tiempo la estela de la emoción)
 * 0.2.17c - Se inicia el playerData cada vez que se entra en juegos libres
 * 0.2.16c - Se añaden las señales onStopSpin, onStartPrizeLine, onSpinResponse, onStartSpin
 * 0.2.15c - En postExit: var msToShowPoster = _.get( RFGameConfig, "game.msToShowOutroPoster", 0 );
 *           Para que se vea un rato la última partida jugada.
 * -------------------------------------------------------
 * 0.1.5 - Se añade m_acumulatedPrize para evitar que se muestre al jugador un cartel indicando que ha ganado un premio si éste es 0.
 * 0.1.6 - Controla el número de bonos en caso de que tenga que cambiar de escena para no mostrar el cartel del premio obtenido.
 * 0.1.7 - Para el sonido del fondo antes de reproducir el del premio. Accede al nodo correcto para sacar el premio acumulado.
 * 0.1.8 - Se llama a updateGlobalData de SceneUtils y se inicializan las labels para la gestión de moneda y créditos (requiere versión >= 0.0.25 de la BaseScene)
 * 0.1.9 - Se inicializa la visibilidad de las líneas y los skittles.
 * 0.1.10 - Se inicializan los keybindings vía config (requiere versión >= 0.0.27 de BaseScene y >= 0.5.4 de RFGameConfig)
 * 0.2.0  - Se añade parte protegida para slo2game. Se exporta changeStateByServerSceneId,
 *          la máquina de estados, su definición y datos de inicialización.
 *          Cambio del nombre a 'forcedGames' en los keybindings
 * 0.2.1c - var m_myScene = eCustomSceneNames.CUSTOM_GAME_1;
 * 0.2.2c - case eLabelsNames.msgSecundary:  oPhaserLabel.setFormatedString( msgSecundary ); break;
 * 0.2.3c - Se modifica la llamada de setFormatedString a setFormattedString
 * 0.2.4c - Añade la visualización de premios por línea y soluciona algunos bugs
 * 0.2.5c - Se coloca el setFormattedString en la label principal
 * 0.2.6c - En la reconexión se añade drawLabelWin( lastWsResponse.spinState.acumulatedPrize ); para que pinte el último valor del premio obtenido
 * 0.2.7c - Se añade el wrapper de conexión pow websockets
 * 0.2.8c - Cambia la llamada de setEnableButtons a enablePlayableButtons
 * 0.2.9c - Se mandan por socket los botones activos para que se iluminen en la botonera
 * 0.2.10c - Se añade la función turnOffButtons
 * 0.2.11c - Se sobreescribe SceneUtils.fERROR para que el juego salga al lobby ante un error
 *         - Se impide que el juego pinte el dinero del jugador cuando se acaben los juegos libres (se ha de ver en la pantalla principal, para hacer el paripé de que el dinero se incrementa)
 * 0.2.12c - Se añade el número de juegos libres al playerData, para poderlos restar cuando el jugador pulsa el botón de Spin
 * 0.2.13c - Actualiza el valor de los label win cuando se produce un spin, si se está mostrando el carrusel.
 *           Esto evita que el cartel final se visualice con un importe incorrecto.
 * 0.2.14c - Se hace que el label de premio en el cartel final tenga un tween.
 */
/* eslint-enable max-len */

// eslint-disable-next-line no-implicit-globals, max-statements
var FreeGameScene = function( game ) {

    "use strict";

    // Call to base Scene Constructor and get the protected
    var _protected = BaseScene.call( this, game ) || {};

    var m_super = this;
    var SceneUtils = _protected.SceneUtils;

    var self = this;
    var m_rfLoader = null;
    var m_rffsm = new RFFsm();
    var m_phaserButtons = null;
    var m_phaserSkittels = null;
    var m_phaserLines = null;
    var m_phaserLabels = null;
    var m_phaserButtonsSpin = []; // buttons with spin action
    var m_fadeSprite = null;

    // var m_lastRFSymbolsMatrix = null;
    var m_texts = {};
    var m_waitingToNextAutoGameTimer = null;
    var m_reels = null;

    var m_isShowingPrizes = false;
    var m_acumulatedPrize;
    var m_bonus;

    var m_audioSounds = {
        bkgFreegame: new RFAudioSound( game ),
        congrat: new RFAudioSound( game )
    };

    var m_eSoundTypes = {
        CONGRAT: "CONGRAT"
    };
    _.assign( m_eSoundTypes, eSoundTypes );
    Object.freeze( m_eSoundTypes );

    // Force reels
    var m_oForceReels = {
        forceNextGame: false, // next game is forced
        forcedGamesArray: [],
        currentIdx: -1 // current idx of the forcedGamesArray
    };

    var m_data = {
        cash: -1, // Se coloca a -1 para saber que aún no se ha actualizado el dinero que tiene el jugador
        totalBet: 0,
        bonds: 0,
        payTable: null
    };

    var m_specialForceReels = {};

    var socket_handler = null;

    var DEFAULTS = {
        MS_TWEEN_COUNTER_PRIZE_LINES: 500,
        MS_TWEEN_COUNTER_WIN: 500,
        MS_TWEEN_COUNTER_CREDITS: 500,
        MS_TWEEN_COUNTER_RESIDUAL_CREDITS: 1000,
        MS_TO_START_THE_TWEEN_COUNTER_RESIDUAL_CREDITS: 500,
        MS_MIN_SPIN_TIME: 3000,
        MS_STOP_REEL: 300,
        Y_BOUNCE_OFFSET: 20,
        MS_BETWEEN_STOPS: 100,
        MS_WAIT_RESIDUAL: 1000,
        MS_TO_EXIT_WITH_NO_CASH: 30000,
        MS_FADEOUT_LINE: 2000,
        MS_MIN_TO_START_NEW_GAME_IN_AUTO_GAME_WITH_PRIZE: 1000,
        MS_MIN_TO_START_NEW_GAME_IN_AUTO_GAME_WITH_NO_PRIZE: 500,
        MS_MIN_TO_ENTER_IN_FREEGAMES: 1000,
        BET_PER_LINE: 0.2,

        MS_FOR_REEL_START_SPIN: 150,
        OFFSET_REEL_SPIN_SPEED: 100,
        REEL_SPIN_SPEED: 300,

        MS_PRIZE_HYPE_DELAY: 500,
        MS_PRIZE_HYPE_DELAY_MULTIPLIER: 3
    };
    Object.freeze( DEFAULTS );

    var DISABLE_OPACITY = 0.4;
    var MS_TWEEN_COUNTER_PRIZE_LINES = _.get( RFGameConfig, "game.msTweenCounterPrizeLines", DEFAULTS.MS_TWEEN_COUNTER_PRIZE_LINES );
    var MS_TWEEN_COUNTER_OUTRO_PRIZE = _.get( RFGameConfig, "game.msTweenCounterOutroPrize", DEFAULTS.MS_TWEEN_COUNTER_OUTRO_PRIZE );
    var OUTRO_POSTER = "OUTRO_POSTER";

    // #region signals
    this.onStopSpin = new Phaser.Signal();
    this.onStartPrizeLine = new Phaser.Signal();
    this.onSpinResponse = new Phaser.Signal();
    this.onStartSpin = new Phaser.Signal();
    // #endregion

    this.nextReelIdsToSpinArray = [];

    /**
     * Exports an utility function to set force the reels
     *
     * @param {array} array Array of integers with the server reel position
     */
    this._specialForceReels = function( array ) {

        // Check errors
        if( !RFGameConfig.game.canBeForced ) {
            console.error( "FORCE REELS: Can't be forced" ); /* RemoveLogging:skip*/
            return false;
        }
        if( !_.isArray( array ) ) {
            console.error( "FORCE REELS: Needs an array of integers" ); /* RemoveLogging:skip*/
            return false;
        }
        if( array.length !== m_reels.getNumberOfReels() ) {
            console.error( "FORCE REELS: Invalid length of reels" ); /* RemoveLogging:skip*/
            return false;
        }

        // Reset
        m_specialForceReels = {};

        for( var i = 0; i < array.length; ++i ) {
            var value = array[ i ];
            if( !_.isInteger( value ) || value < 0 ) {
                console.error( "FORCE REELS: All values in the array needs to be integer and >= 0" ); /* RemoveLogging:skip*/
                m_specialForceReels = {};
                return false;
            }
            m_specialForceReels[ "reel" + ( i + 1 ) ] = value;

        }

        return true;
    };

    this.shutdown = function() {

        // call to parent
        m_super.shutdownBase();

        var destroyChildren = true;
        var destroyTexture = true;
        m_fadeSprite.destroy( destroyChildren, destroyTexture );

    };

    this.preload = function() {
        // call to parent
        m_super.preloadBase();
    };

    this.create = function() {

        // call to parent
        m_super.createBase();

        // iniciamos la maquina de estados con los estados definidos
        m_rffsm.initialize( m_fsmInitData );

        // ponemos el estado inicial
        m_rffsm.changeState( eStateNames.INIT );

        initializePlayerData();

        this.nextReelIdsToSpinArray = [];

    };// create

    // ---  BEGIN TRANSITIONS ---------------------------------------------
    var transitionOut = function() {
        if( self.game.transitions ) {
            self.game.transitions.animation( {
                animation: "trans_out",
                fps: 21,
                killOnComplete: true,
                scale: 1.7
            } );
        }
        else {
            SceneUtils.fadeOut( m_fadeSprite );
        }
    };
    var transitionIn = function( sceneName ) {
        RFGlobalData.net.wsWrapper.unsubscribe( socket_handler );
        if( self.game.transitions ) {
            self.game.transitions.animation( {
                animation: "trans_in", scale: 1.7, fps: 21,
                onComplete: function() {
                    self.game.state.start( sceneName );
                }
            } );
        }
        else {
            self.game.state.start( sceneName );
        }
    };
    // --- END TRANSITIONS -----------------------------------------------

    var initializePlayerData = function() {
        // Datos del jugador para poder restar el dinero de su cuenta antes de que llegue la respuesta del servidor.
        // También se guarda la última tabla de pagos para poder mandarla a la pantalla superior cuando ésta se conecte
        m_data.cash = -1;  // Se coloca a -1 para saber que aún no se ha actualizado el dinero que tiene el jugador
        m_data.totalBet = 0;
        m_data.bonds = 0;
        m_data.payTable = null;
    };

    var startStarting = function() {

        // creacion de la escena
        loadScene();

        // black screen
        m_fadeSprite = SceneUtils.createFadeSprite();
        if( !self.game.transitions ) {
            SceneUtils.fadeIn( m_fadeSprite, 0 );
        }
        transitionOut();

        // recofemos los botones para añadirle su action
        addActionsButtons( m_phaserButtons );

        // recogemos los skittels para añadirle sus actions
        addActionSkittels();

        // Keyboard bindings
        addKeyBindings();

        // inicialización aleatoria de rodillos
        m_reels.setOptions( {
            msStopReel: _.get( RFGameConfig.game, "msStopReel", DEFAULTS.MS_STOP_REEL ),
            yBounceOffset: _.get( RFGameConfig.game, "yBounceOffset", DEFAULTS.Y_BOUNCE_OFFSET ),
            msBetweenStops: _.get( RFGameConfig.game, "msBetweenStops", DEFAULTS.MS_BETWEEN_STOPS ),

            msForReelStartSpin: _.get( RFGameConfig.game, "msForReelStartSpin", DEFAULTS.MS_FOR_REEL_START_SPIN ),
            offsetReelSpinSpeed: _.get( RFGameConfig.game, "offsetReelSpinSpeed", DEFAULTS.OFFSET_REEL_SPIN_SPEED ),
            reelSpinSpeed: _.get( RFGameConfig.game, "reelSpinSpeed", DEFAULTS.REEL_SPIN_SPEED ),

            symbolWeights: _.get( _.get( RFGameConfig.game, "symbolWeights", null ), "BASE_GAME", null ),
            prizeHype: _.get( RFGameConfig.game, "prizeHype", null ),

            prizeHypeDelay: _.get( RFGameConfig.game, "prizeHypeSoundDelay", DEFAULTS.MS_PRIZE_HYPE_DELAY ),
            prizeHypeDelayMultiplier: _.get( RFGameConfig.game, "prizeHypeDelayMultiplier", DEFAULTS.MS_PRIZE_HYPE_DELAY_MULTIPLIER ),
            prizeHypeFadeAnim: _.get( RFGameConfig.game, "prizeHypeFadeAnim", DEFAULTS.PRIZEHYPEANIM_FADE ),
            prizeHypeDelayToStopAnim: _.get( RFGameConfig.game, "prizeHypeDelayToStopAnim", DEFAULTS.PRIZEHYPEANIM_DELAYTOSTOP ),
            prizeHypeSoundOnlyOne: _.get( RFGameConfig.game, "prizeHypeSoundOnlyOne", false ),

            startExtendedWildsAtSameTime: _.get( RFGameConfig.game, "startExtendedWildsAtSameTime", true ),
            playSounds: true
        } );
        SceneUtils.setLastRFSymbolsMatrix( SceneUtils.getLastRFSymbolsMatrix() || getRandomMatrixRFSymbols() );

        // deshabilitamos los botones
        SceneUtils.setButtonEnable( m_phaserButtons, false );

        // "deshabilitamos" líneas y skittels
        lineVisible( 0 );
        skittelVisible( 0 );

        m_audioSounds.bkgFreegame.playAll();

        if( RFGameConfig.game.canBeForced ) {
            getForcedGames( reconnection );
        }
        else {
            reconnection();
        }

        var wsWrapper = RFGlobalData.net.wsWrapper;
        socket_handler = wsWrapper.subscribe( socketReception );

    };

    var getRandomMatrixRFSymbols = function() {
        var matrix = [];
        var numReels = m_reels.getNumberOfReels();
        var numSymbolsPerReels = m_reels.getNumberOfSymbolsPerReels();
        var mapSymbols = m_rfLoader.getRFSymbols();
        for( var i = 0; i < numReels; ++i ) {
            matrix.push( _.sampleSize( mapSymbols, numSymbolsPerReels ) );
        }
        return matrix;
    };

    var startPreIdle = function( oParams ) {
        m_states.PRE_IDLE.startParams = oParams;
        if( !hasToWaitUntilDisplayAllPrizeLines() ) {
            m_rffsm.changeState( eStateNames.IDLE, m_states.PRE_IDLE.startParams, "From Pre-Idlehasn't to wait until display all prize lines" );
        }
    };

    var sendActiveButtons = function() {
        var btn_auto = _.find( m_phaserButtons, function( btn ) { return btn.action === "STOP_AUTO"; } );
        // Una ñapa para que el botón de autoJuego se desactive si no se está en juego automático
        if( !_.isUndefined( btn_auto ) ) {
            btn_auto.behaviors.get( "RFButtonComp" ).options.enabled = false;
        }
        var buttonList = _.filter( m_phaserButtons, function( btn ) {
            return ( ( btn.behaviors.has( "RFButtonComp" ) ) && ( btn.behaviors.get( "RFButtonComp" ).options.enabled === true ) );
        } );
        var buttonIdList = [];
        _.forEach( buttonList, function( button ) {
            if( button.action === "STOP_AUTO" ) { button.action = "AUTO_X"; }
            buttonIdList.push( button.action );
        } );
        var toSocket = {
            type: "ENABLE_BUTTON_LIST",
            data: {
                buttonIdList: buttonIdList
            }
        };

        sendToSocket( toSocket );
    };
    var turnOffButtons = function() {
        var toSocket = {
            type: "ENABLE_BUTTON_LIST",
            data: {
                buttonIdList: []
            }
        };
        sendToSocket( toSocket );
    };

    var updatePreIdle = function() {
        if( !m_isShowingPrizes ) {
            m_rffsm.changeState( eStateNames.IDLE, m_states.PRE_IDLE.startParams, "From Pre-Idle is not showing prizes" );
        }
    };

    var startIdle = function() {
        enablePlayableButtons( true );
        sendActiveButtons();
    };

    var exitInit = function() {
        // Nothing
    };

    var exitIdle = function() {
        // Nothing
    };

    var startSpinning = function( oStateParams ) {

        enablePlayableButtons( false );

        // Mandamos información al socket para saber que se ha hecho un spin
        if( isSBG() ) {
            var toSocket = {
                type: "SPINNING"
            };
            sendToSocket( toSocket );
        }

        var canSpin = true;
        _.forEach( m_phaserLabels, function( oPhaserLabel ) {
            if( oPhaserLabel.type === eLabelsNames.msgMain ) {
                oPhaserLabel.setText( m_texts.GOODLUCK );
            }
        } );

        if( canSpin ) {
            self.onStartSpin.dispatch();

            hideLines();

            if( m_isShowingPrizes ) {
                drawLabelWin( m_acumulatedPrize );
                m_isShowingPrizes = false;
            }

            m_data.bonds--;
            _.forEach( m_phaserLabels, function( oPhaserLabel ) {
                switch( oPhaserLabel.type ) {
                    case eLabelsNames.msgBonds: oPhaserLabel.setString( m_data.bonds ); break;
                    default: break;
                }

            } );

            m_reels.startSpin( self.nextReelIdsToSpinArray );
            sendActiveButtons();

            var params = {
                sessionId: RFGlobalData.net.session,
                signature: "0",
                serverSceneId: eServerSceneId.FREE_GAME
            };
            if( isForced() ) {
                params.forcedGameId = m_oForceReels.forcedGamesArray[ m_oForceReels.currentIdx ].id;
                m_oForceReels.forceNextGame = false;
                m_oForceReels.currentIdx = -1;
            }
            if( isSpecialForced() ) {
                params.specialForce = m_specialForceReels;
                m_specialForceReels = {};
            }
            _.extend( params, oStateParams ); // add custom params
            RFGlobalData.net.wrapper.sendSpin( params, onSpinResponse, SceneUtils.fERROR );
        }
    };

    var startExit = function( oParams ) {
        m_states.EXIT.startParams = oParams;
        if( !hasToWaitUntilDisplayAllPrizeLines() ) {
            enablePlayableButtons( true, m_phaserButtonsSpin );
        }
    };

    var updateExit = function() {
        if( !m_isShowingPrizes ) {
            m_rffsm.changeState( eStateNames.POST_EXIT, m_states.EXIT.startParams, "EXIT" );
        }
    };

    var startPostExit = function( oParams ) {
        enablePlayableButtons( false );
        var msToShowPoster = _.get( RFGameConfig, "game.msToShowOutroPoster", 0 );
        game.time.events.add( msToShowPoster, function() {
            // Shows the poster only when the player has won any prize and has no bonus to continue
            if( m_acumulatedPrize > 0 && m_bonus === 0 ) {
                m_audioSounds.bkgFreegame.stop();
                m_audioSounds.congrat.play();

                // Reset the final prize
                drawLabelWinOutroPoster( 0, 0 );

                SceneUtils.showOutroPoster();

                // Make the tween effect
                drawLabelWinOutroPoster( m_acumulatedPrize, MS_TWEEN_COUNTER_OUTRO_PRIZE );

                turnOffButtons();
            }

            if( _.has( oParams, "sceneName" ) ) {
                var timeToWait = 0;
                if( _.has( oParams, "msToWait" ) ) {
                    timeToWait = oParams.msToWait;
                }
                if( timeToWait > 0 ) {
                    self.game.time.events.add( timeToWait, function() {
                        stopAllSounds();
                        transitionIn( oParams.sceneName );
                    } );
                }
                else {
                    stopAllSounds();
                    transitionIn( oParams.sceneName );
                }
            }
            else {
                console.warn( "We are on EXIT state without no scene to go next" );
            }
        }, self );
    };

    // funcion que retorna la funcion asociada al strFunctionName si existe
    var getOnClickFunction = function( strFunctionName ) {
        switch( strFunctionName ) {
            case eActionNames.spin:
                return onBtnSpin;
            default:
                console.warn( "Function ", strFunctionName, " not implemented in this scene" );
                return onBtnNotDefined;
        }
    };

    var skittelOver = function( button ) {
        if( isStateIdle() ) {
            hideLines(); // this prevent visible skittles when lost the focus on another window
            m_phaserLines[ button.id ].visible = true;
        }
    };
    var skittelOut = function( button ) {
        m_phaserLines[ button.id ].visible = false;
    };
    var skittelUp = function( /* button */ ) {
        // Nothing to do here
    };

    // #region ON ERROR
    SceneUtils.fERROR = function() {
        // Ante un error, se sale del juego
        var toSocket = {
            type: "EXIT",
            data: null
        };
        sendToSocket( toSocket );
    };
    // #endregion

    var setForceReel = function() {
        if( RFGameConfig.game.canBeForced ) {
            var numForcedGames = _.size( m_oForceReels.forcedGamesArray );
            if( numForcedGames > 0 ) {
                m_oForceReels.currentIdx = ( m_oForceReels.currentIdx + 1 === numForcedGames ? -1 : ( m_oForceReels.currentIdx + 1 ) % numForcedGames );
                m_oForceReels.forceNextGame = m_oForceReels.currentIdx >= 0;
            }
        }
        // eslint-disable-next-line max-len
        var text = "FORCE: " + ( m_oForceReels.forceNextGame ? m_oForceReels.forcedGamesArray[ m_oForceReels.currentIdx ].id + " (" + m_oForceReels.forcedGamesArray[ m_oForceReels.currentIdx ].description + ")" : "Normal play" );
        console.log( text ); /* RemoveLogging:skip*/
    };

    var getForcedGames = function( fEnd ) {
        var params = {
            sessionId: RFGlobalData.net.session,
            serverSceneId: eServerSceneId.FREE_GAME
        };
        RFGlobalData.net.wrapper.sendGetForcedGames( params, function( wsResponse ) {
            console.log( "GET_FORCED_GAMES RESPONSE:", wsResponse );
            if( SceneUtils.hasValidSession( wsResponse ) ) {
                m_oForceReels.forcedGamesArray = wsResponse.forcedGamesArray;

                if( _.isFunction( fEnd ) ) {
                    fEnd( wsResponse );
                }
            }
        }, fEnd );
    };


    // ==============================================================
    // ON BUTTON CLICKS
    // ==============================================================

    var onBtnSpin = function() {
        if( isStateIdle() ) {
            self.game.time.events.remove( m_waitingToNextAutoGameTimer );
            m_rffsm.changeState( eStateNames.SPINNING );
        }
    };

    var onBtnNotDefined = function( button ) {
        console.warn( "Action not defined for this button: ", button );
    };

    // ==============================================================
    // ON BUTTON CLICKS END
    // ==============================================================


    // ==============================================================
    // BEGIN SERVER RESPONSES
    // ==============================================================

    var onGetGameStateResponse = function( wsResponse ) {
        console.log( "GAME STATE RESPONSE", wsResponse );
        if( SceneUtils.hasValidSession( wsResponse ) ) {
            updateData( wsResponse );
        }

        updateNextReelIdsToSpinArray( wsResponse );
    };

    // callback de respuesta correcta para un SPIN
    var onSpinResponse = function( wsResponse ) {
        console.log( "SPIN RESPONSE", wsResponse );

        self.onSpinResponse.dispatch( wsResponse );
        if( SceneUtils.hasValidSession( wsResponse ) ) {
            if( isStateSpinning() ) {
                if( SceneUtils.isValidWSResult( wsResponse ) ) {
                    stopSpin( wsResponse, function() {

                        updateData( wsResponse );

                        _protected.changeStateByServerSceneId( wsResponse.player.currentServerSceneId );

                        startCarrousel( wsResponse );

                        self.onStopSpin.dispatch( wsResponse );
                    } );
                }
                else {
                    // Error
                    console.error( "Error in server response", wsResponse );
                    debugger;
                    SceneUtils.fERROR( wsResponse );
                }
            }
        }
        updateNextReelIdsToSpinArray( wsResponse );
    };

    var updateNextReelIdsToSpinArray = function( data ) {
        console.log( "updateNextReelIdsToSpinArray", data );

        if( data instanceof RFWSSpinResponse ) {
            self.nextReelIdsToSpinArray = _.get( data, [ "spinState", "nextReelIdsToSpinArray" ], undefined );
        }
        else {
            self.nextReelIdsToSpinArray = _.get( data, [ "gameState", "play", "spinState", "spinReelIds" ], undefined );
        }
    };

    var stopSpin = function( spinResponse, fEndSpinCallback ) {
        console.log( "Tiempo de spin mientras esperábamos respuesta del servidor", m_reels.getSpinningTime() );
        var msMinSpinTime = _.has( RFGameConfig.game, "msMinSpinTime" ) ? RFGameConfig.game.msMinSpinTime : DEFAULTS.MS_MIN_SPIN_TIME;
        var timeToWait = msMinSpinTime; // todo: contemplar además el tiempo del rebote y tenerlo en cuenta a la hora de construir los rodillos
        if( m_reels.getSpinningTime() < msMinSpinTime ) {
            timeToWait = msMinSpinTime - m_reels.getSpinningTime();
        }

        var rfSymbolsMatrix = SceneUtils.getRFSymbolsMatrix( spinResponse.spinState.reels.reelMatrix );
        SceneUtils.setLastRFSymbolsMatrix( rfSymbolsMatrix );
        var prizeHypeobjResponse = SceneUtils.getPrizeHypeObject( spinResponse.spinState );
        self.game.time.events.add( timeToWait, function() {
            m_reels.stopSpin( rfSymbolsMatrix, prizeHypeobjResponse, fEndSpinCallback );
        } );
    };

    var startCarrousel = function( spinResponse ) {

        var hasToDisplayPrizeLines = _.get( RFGameConfig, "game.displayPrizeLines", false );
        var extendedWildArray = SceneUtils.getExtendedWildArray( spinResponse.spinState );
        var prizesArray = SceneUtils.getPrizesArray( spinResponse.spinState );
        var startCarrouselTime = self.game.time.now;
        var prize = _.get( spinResponse, "spinState.prize", 0 );
        var accumulatedPrize = _.get( spinResponse, "spinState.acumulatedPrize", 0 );

        var fEndFirstLoop = function() {
            m_isShowingPrizes = false;
            if( hasToDisplayPrizeLines ) {
                if( prize > 0 ) {
                    var twTime = MS_TWEEN_COUNTER_PRIZE_LINES;
                    var text = String.format( m_texts.CONGRATULATIONS, SceneUtils.formatMoneyOrCredits( prize, RFGlobalData.locale.currencyISO ) );
                    drawPrize( accumulatedPrize, twTime, text );
                }
            }
            if( isStateIdle() || isStatePreIdle() ) {
                var elapsedTimeCarrousel = self.game.time.now - startCarrouselTime;
                var waitTime = _.get(
                    RFGameConfig, "game.msMinToStartNewGameInAutoGameWithNoPrize", DEFAULTS.MS_MIN_TO_START_NEW_GAME_IN_AUTO_GAME_WITH_NO_PRIZE
                );
                var timeToWait = waitTime;
                if( elapsedTimeCarrousel < waitTime ) {
                    timeToWait = waitTime - elapsedTimeCarrousel;
                }
                scheduleNextSpin( timeToWait );
            }

        };

        var oWinPrizes = {
            prizesArray: prizesArray,
            extendedWildArray: extendedWildArray
        };

        m_isShowingPrizes = true;
        if( hasToDisplayPrizeLines ) {
            startCarrouselDisplayingLinePrizes( oWinPrizes, fEndFirstLoop );
        }
        else {
            m_reels.startWinPrizesCarrousel( oWinPrizes, fEndFirstLoop );
        }
    };

    var startCarrouselDisplayingLinePrizes = function( oWinPrizes, fCallback ) {
        var fnName = startCarrouselDisplayingLinePrizes.name + ": ";

        if( !m_reels.isStopped() ) { throw new Error( fnName + "Can't start carrousel if the reels are not stopped" ); }
        assertAllKeys( oWinPrizes, [ "prizesArray", "extendedWildArray" ] );

        var aWinPrizes = oWinPrizes.prizesArray;
        if( !_.isArray( aWinPrizes ) ) { throw new Error( fnName + "oWinPrizes.aWinPrizes is not an array" ); }

        var aExtendedWild = oWinPrizes.extendedWildArray;
        if( !_.isArray( aWinPrizes ) ) { throw new Error( fnName + "oWinPrizes.extendedWildArray is not an array" ); }

        if( aWinPrizes.length > 0 ) {
            m_reels.stopAllSounds();
            m_reels.showExtendedWildPrizes( aExtendedWild, function() { // 1.- Show Extended Wild
                showWinPrizes( aWinPrizes, fCallback );         // 2.- Show Line prizes
            } );

        } // Only ExtendedWild
        else if( aExtendedWild.length > 0 ) {
            m_reels.stopAllSounds();
            m_reels.showExtendedWildPrizes( aExtendedWild, fCallback );
        }
        else if( _.isFunction( fCallback ) ) {
            fCallback();
        }
    };

    var showWinPrizes = function( aWinPrizes, fOnEndLoopCallback ) {

        // eslint-disable-next-line max-params
        var showWinPrizeInternal = function( _aWinPrizes, currentIdx, bLoop, playSound, isFirstLoop, _fOnEndLoopCallback ) {
            if( currentIdx >= _aWinPrizes.length ) { // base case
                if( _.isFunction( _fOnEndLoopCallback ) ) {
                    _fOnEndLoopCallback();
                }
                if( bLoop ) {
                    currentIdx = 0;  // loop again
                }
                else {
                    return; // end loop
                }
            }

            var currentPrize = _aWinPrizes[ currentIdx ];

            // draw the information line prize
            if( isFirstLoop ) {
                var linePrize = _.get( currentPrize, "prize" );
                var accumulatedPrize = _.get( currentPrize, "accumulatedPrize" );
                var numLineWinner = Number( _.get( currentPrize, "rfLine.id" ) );
                var text = "";
                var formatedLinePrize = SceneUtils.formatMoneyOrCredits( linePrize, RFGlobalData.locale.currencyISO );
                if( linePrize > 0 ) {
                    var template = numLineWinner > 0 ? m_texts.LINE_PRIZE : m_texts.NO_LINE_PRIZE;
                    text = String.format( template, formatedLinePrize, numLineWinner );
                }
                drawPrize( accumulatedPrize, MS_TWEEN_COUNTER_PRIZE_LINES, text );
                self.onStartPrizeLine.dispatch( linePrize, formatedLinePrize, numLineWinner );
            }

            m_reels.showWinPrize( currentPrize, playSound, function() {
                showWinPrizeInternal( _aWinPrizes, currentIdx + 1, bLoop, playSound, isFirstLoop, _fOnEndLoopCallback );  // recursive call
            } );
        };

        // First loop
        var loop = false;
        var playSound = true;
        var isFirstLoop = true;
        showWinPrizeInternal( aWinPrizes, 0, loop, playSound, isFirstLoop, function() {

            // When finish the first loop, we call the callback function
            if( _.isFunction( fOnEndLoopCallback ) ) {
                fOnEndLoopCallback();
            }

            // Nexts loops
            loop = true;
            playSound = false;
            isFirstLoop = false;
            showWinPrizeInternal( aWinPrizes, 0, loop, playSound, isFirstLoop );
        } );
    };

    _protected.changeStateByServerSceneId = function( serverSceneId ) {

        if( !_.hasValue( eServerSceneId, serverSceneId ) ) {
            console.error( "Invalid server scene id: ", serverSceneId );
        }

        var timeToWait;
        switch( serverSceneId ) {
            case eServerSceneId.FREE_GAME:
                m_rffsm.changeState( eStateNames.PRE_IDLE, {}, "Server Playing Game: FREE_GAME" );
                break;
            case eServerSceneId.BASE_GAME:
                timeToWait = _.get( RFGameConfig.game, [ "msMinToExitFromFreeGames" ], DEFAULTS.MS_MIN_TO_EXIT_FROM_FREEGAMES );
                m_rffsm.changeState( eStateNames.EXIT, {
                    sceneName: eSceneNames.BASEGAME,
                    msToWait: timeToWait
                }, "Server Playing Game: BASEGAME" );
                break;
            case eServerSceneId.ANIMALS_GAME:
                timeToWait = _.get( RFGameConfig.game, [ "msMinToExitFromFreeGames" ], DEFAULTS.MS_MIN_TO_EXIT_FROM_FREEGAMES );
                m_rffsm.changeState( eStateNames.EXIT, {
                    sceneName: eCustomSceneNames.ANIMALS_GAME,
                    msToWait: timeToWait
                }, "Server Playing Game: ANIMALS_GAME" );
                break;
            default:
                if( self.constructor === FreeGameScene ) {
                    console.warn( "Server state not controlled: '" + serverSceneId +
                        "'. You need to implement a CustomBaseGameScene and override changeStateByServerSceneId" );
                }
                break;
        }
    };

    var scheduleNextSpin = function( msToWait ) {
        if( _.isNumber( msToWait ) ) {
            self.game.time.events.remove( m_waitingToNextAutoGameTimer );
            m_waitingToNextAutoGameTimer = self.game.time.events.add( msToWait, function() {
                if( isStateIdle() ) {
                    m_rffsm.changeState( eStateNames.SPINNING, {}, "Start schedule Spin " );
                }
                else {
                    console.error( "Logic Error: The state has change (" + m_rffsm.getState() +
                        ") and we must be Idle. Maybe the m_waitingToNextAutoGameTimer has not removed" );
                }
            } );
        }
        else {
            console.error( "Invalid msToWait scheduling next spin: ", msToWait );
        }
    };

    // ==============================================================
    //   END SERVER RESPONSES
    // ==============================================================

    var reconnection = function() {
        getGameState( function( wsResponse ) {

            var lastWsResponse = RFWebServiceUtil.createRFWSResponse( wsResponse.gameState );
            console.log( "Reconnection: Last Response", lastWsResponse );

            drawLabelWin( 0 );

            if( lastWsResponse ) {

                _protected.changeStateByServerSceneId( lastWsResponse.player.currentServerSceneId );

                if( lastWsResponse instanceof RFWSSpinResponse ) {
                    console.log( "Reconnection: Update data with last spin response" );
                    updateData( lastWsResponse );
                    drawLabelWin( lastWsResponse.spinState.acumulatedPrize );
                    SceneUtils.setLastRFSymbolsMatrix( SceneUtils.getRFSymbolsMatrix( lastWsResponse.spinState.reels.reelMatrix ) );
                    m_reels.setReels( SceneUtils.getLastRFSymbolsMatrix() );
                    startCarrousel( lastWsResponse );
                    SceneUtils.hideCovers();
                    SceneUtils.showIntroPoster();
                }
                else {
                    console.log( "Reconnection: It's not a spin response. Restore last reels, and schedule next spin" );
                    m_reels.setReels( SceneUtils.getLastRFSymbolsMatrix() );
                    SceneUtils.hideCovers();
                    SceneUtils.showIntroPoster();
                    var timeToWait = _.get( RFGameConfig.game, [ "msMinToStartFirstSpinFreeGames" ], DEFAULTS.MS_MIN_TO_START_FIRST_SPIN_FREEGAMES );
                    scheduleNextSpin( timeToWait );
                    m_rffsm.changeState( eStateNames.IDLE, {}, "First spin in freegame" );
                }

            }
            else {
                console.error( "Reconnection: Can't start without a previous play" );
                debugger;
                SceneUtils.fERROR( wsResponse );
            }

            RFGlobalData.game.lastScene = self.myScene;

        }, SceneUtils.fERROR );
    };


    var getGameState = function( fCallbackOk, fCallbackError ) {
        var params = {
            sessionId: RFGlobalData.net.session
        };
        RFGlobalData.net.wrapper.sendGetGameState( params, function( wsResponse ) {
            onGetGameStateResponse( wsResponse );
            if( _.isFunction( fCallbackOk ) ) {
                fCallbackOk( wsResponse );
            }
        }, fCallbackError );
    };

    var addActionsButtons = function( phaserButtons ) {
        _.forEach( phaserButtons, function( oPhaserButton ) {
            var theFunction = getOnClickFunction( oPhaserButton.action );
            oPhaserButton.onInputUp.add( theFunction, self );
        } );
    };

    var addActionSkittels = function() {
        _.forEach( m_phaserSkittels, function( oPhaserSkittel ) {
            oPhaserSkittel.onInputOver.add( skittelOver, self );
            oPhaserSkittel.onInputOut.add( skittelOut, self );
            oPhaserSkittel.onInputUp.add( skittelUp, self );
        } );
    };

    var addKeyBindings = function() {
        if( _.get( RFGameConfig, [ "game", "useKeyBindings" ], true ) ) {
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.forcedGames" ), setForceReel );
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.spin" ), onBtnSpin );
        }
        else if( _.get( RFGameConfig, [ "game", "forceAllowHistoryKeyBinding" ], true ) ) {
            // Nothing
        }
    };

    var loadScene = function() {

        // Load all the scene
        m_rfLoader = m_super.getRFLoadScenes();
        SceneUtils.loadScene( self.myScene );

        // cacheamos
        m_texts = RFGlobalData.locale.texts;
        m_reels = m_rfLoader.getRFGraphicReels();

        m_phaserLabels = m_rfLoader.getPhaserLabels();
        m_phaserButtons = m_rfLoader.getPhaserButtons();
        m_phaserSkittels = m_rfLoader.getPhaserSkittels();
        m_phaserLines = m_rfLoader.getPhaserLines();

        m_phaserButtonsSpin = SceneUtils.filterByAction( m_phaserButtons, eActionNames.spin );

        SceneUtils.initSound( m_audioSounds.bkgFreegame, m_eSoundTypes.FREEGAME );
        SceneUtils.initSound( m_audioSounds.congrat, m_eSoundTypes.CONGRAT );

        // Hide all posters
        SceneUtils.hideAllPosters();

        SceneUtils.handleTransitionSymbolsForReels( m_reels );

        SceneUtils.onGameResumed.add( onGameResumed );
    };

    var onGameResumed = function() {

        // Bloqueamos el juego para esperar la actualización del nuevo cash actualizado.
        self.game.paused = true;

        RFGlobalData.net.wrapper.sendGetBalance( {}, function( wsResponse ) {

            var currentBalance = wsResponse.playerBalance;
            if( currentBalance >= 0 ) {
                console.log( "GAME RESUMED: se actualiza el valor del cash actual.", m_data.cash, "=>", currentBalance );
                m_data.cash = currentBalance;
                drawCash( m_data.cash, 0 );
            }
            else {
                console.warn( "GAME RESUMED: cash inválido:", currentBalance );
            }

            self.game.paused = false;

        }, function( wsResponse ) {
            self.game.paused = false;
            SceneUtils.fERROR( wsResponse );
        } );

    };

    var lineVisible = function( buttonOrId ) {
        var value = _.isObject( buttonOrId ) && _.has( buttonOrId, "id" ) ? buttonOrId.id : buttonOrId;
        if( !_.isNumber( value ) ) { throw new Error( "Invalid button Or Id" ); }
        setVisibility( m_phaserLines, value );
    };


    var skittelVisible = function( buttonOrId ) {
        var value = _.isObject( buttonOrId ) && _.has( buttonOrId, "id" ) ? buttonOrId.id : buttonOrId;
        if( !_.isNumber( value ) ) { throw new Error( "Invalid button Or Id" ); }
        setVisibility( m_phaserSkittels, value );
    };

    /**
     * Se usa solo para lineas y skittles y cambia la visibilidad del mapa de objectos pasado.
     */
    var setVisibility = function( oPhaserLineOrSkittel, iValue ) {
        _.forEach( oPhaserLineOrSkittel, function( oLineOrSkittel ) {
            if( _.has( oLineOrSkittel, "id" ) ) {
                if( parseInt( oLineOrSkittel.id, 10 ) > iValue ) {
                    oLineOrSkittel.alpha = DISABLE_OPACITY;
                }
                else {
                    oLineOrSkittel.alpha = 1;
                }
            }
        } );
    };

    var hideLines = function() {
        _.forEach( m_phaserLines, function( oPhaserLine ) {
            oPhaserLine.visible = false;
        } );
    };

    var updateData = function( rfwsResponse ) {
        SceneUtils.updateGlobalData( rfwsResponse );

        m_acumulatedPrize = _.get( rfwsResponse, "spinState.acumulatedPrize", m_acumulatedPrize );
        m_bonus = _.get( rfwsResponse, "player.bonus", m_bonus );

        // Update the residual prize to draw it in the basegame scene
        RFGlobalData.game.residualPrize = m_acumulatedPrize;

        updateCurrentPayTable( rfwsResponse );
        sendCurrentPayTable();
        drawData( rfwsResponse );
    };

    var drawData = function( rfwsResponse ) {
        if( _.has( rfwsResponse, "player" ) ) {
            drawPlayerData( rfwsResponse.player );
        }
        if( _.has( rfwsResponse, "spinState" ) ) {
            drawSpinStateData( rfwsResponse.spinState );
        }
    };

    var drawPlayerData = function( rfWSPlayer ) {

        var lines = rfWSPlayer.numLines;
        var betPerLine = rfWSPlayer.betPerLine;
        // Así evitamos que el saldo del jugador se actualice al terminar los juegos libres
        if( m_data.cash === -1 ) { m_data.cash = rfWSPlayer.cash; }
        var cash = m_data.cash;
        var denom = rfWSPlayer.denom;
        var totalBet = rfWSPlayer.totalBet;
        var multiplier = rfWSPlayer.multiplier;
        var bonus = rfWSPlayer.bonus;
        m_data.bonds = bonus;
        // eslint-disable-next-line
        var msgSecundary = String.format( m_texts.PLAYING, lines, betPerLine, lines > 1 ? "s" : "", SceneUtils.formatMoneyOrCredits( betPerLine, RFGlobalData.locale.currencyISO ) );

        lineVisible( lines );
        skittelVisible( lines );
        _.forEach( m_phaserLabels, function( oPhaserLabel ) {
            switch( oPhaserLabel.type ) {
                case eLabelsNames.msgTotalBet: oPhaserLabel.setMoneyOrCredits( totalBet, RFGlobalData.locale.currencyISO ); break;
                case eLabelsNames.msgCredits: oPhaserLabel.setMoneyOrCredits( cash, RFGlobalData.locale.currencyISO ); break;
                case eLabelsNames.msgDenom: oPhaserLabel.setMoney( denom, RFGlobalData.locale.currencyISO ); break;
                case eLabelsNames.msgLine: oPhaserLabel.setString( lines ); break;
                case eLabelsNames.msgBetPerLine: oPhaserLabel.setMoneyOrCredits( betPerLine, RFGlobalData.locale.currencyISO ); break;
                case eLabelsNames.msgMult: oPhaserLabel.setString( "x" + multiplier ); break;
                case eLabelsNames.msgBonds: oPhaserLabel.setString( bonus ); break;
                case eLabelsNames.msgSecundary: oPhaserLabel.setFormattedString( msgSecundary ); break;
                default: break;
            }

        } );

    };
    var drawSpinStateData = function( rfWSSpinState ) {
        var prize = rfWSSpinState.prize;
        var acumulatedPrize = rfWSSpinState.acumulatedPrize;
        if( prize > 0 ) {
            if( !_.get( RFGameConfig, "game.displayPrizeLines", false ) ) {
                var twTime = 0;
                var text = String.format( m_texts.CONGRATULATIONS, SceneUtils.formatMoneyOrCredits( prize, RFGlobalData.locale.currencyISO ) );
                drawPrize( acumulatedPrize, twTime, text );
            }
            else {
                // This draw will be update on the startCarrouselDisplayingLinePrizes function
            }

            winningPizes( rfWSSpinState );
        }
        else {
            // Enviamos que se ha acabado el juego al servidor
            if( isSBG() ) {
                var toSocket = {
                    type: "LOSE",
                    data: null
                };
                sendToSocket( toSocket );
            }
            drawLabelMain( m_texts.TRY );
        }
    };

    var drawCash = function( value, twTime ) {
        var cashLabels = SceneUtils.filterByType( m_phaserLabels, eLabelsNames.msgCredits );
        _.forEach( cashLabels, function( oPhaserLabel ) {
            oPhaserLabel.setMoneyOrCredits( value, RFGlobalData.locale.currencyISO, twTime );
        } );
    };

    var drawPrize = function( prize, twTime, text ) {
        if( prize !== undefined ) {
            drawLabelWin( prize, twTime );
        }
        if( text !== undefined ) {
            drawLabelMain( text );
        }
    };

    var drawLabelMain = function( text ) {
        var mainLabels = SceneUtils.filterByType( m_phaserLabels, eLabelsNames.msgMain );
        _.forEach( mainLabels, function( oPhaserLabel ) {
            oPhaserLabel.setFormattedString( text );
        } );
    };

    var drawLabelWin = function( value, twTime ) {
        var winLabels = SceneUtils.filterByType( m_phaserLabels, eLabelsNames.msgWin );
        drawMoneyOrCredits( winLabels, value, twTime );
    };

    var drawLabelWinOutroPoster = function( value, twTime ) {
        var winLabels = SceneUtils.filterByType( m_phaserLabels, eLabelsNames.msgWin );
        winLabels = SceneUtils.filterByParameters( winLabels, "tag", OUTRO_POSTER );
        drawMoneyOrCredits( winLabels, value, twTime );
    };

    var drawMoneyOrCredits = function( labels, value, twTime ) {
        _.forEach( labels, function( oPhaserLabel ) {
            oPhaserLabel.setMoneyOrCredits( value, RFGlobalData.locale.currencyISO, twTime );
        } );
    };

    /** Muestra cosas chachis si se obtiene premio */
    var winningPizes = function( data ) {
        var symbolsArray = [];
        _.forEach( data.winPrizesArray, function( winPrize ) {
            console.log( "SÍMBOLOS", winPrize.line );
            _.forEach( winPrize.line, function( symbol ) {
                symbolsArray.push( { "column": symbol[ 0 ], "row": symbol[ 1 ] } );
            } );
        } );
        if( isSBG() ) {
            var toSocket = {
                type: "WIN",
                data: data
            };
            sendToSocket( toSocket );
        }
        // getSymbolCoords( symbolsArray );
    };

    var enablePlayableButtons = function( bEnable ) {
        _.forEach( m_phaserButtons, function( oButton ) {
            switch( oButton.action ) {
                default:
                    oButton.enable( bEnable );
            }
        } );
    };

    // ---------------------------------------------------//
    // PAYTABLE UTILS
    // ---------------------------------------------------//
    var updateCurrentPayTable = function( rfwsResponse ) {
        if( isSBG() && RFGlobalData.game.useLocalBets ) {
            m_data.payTable = SceneUtils.makePayTable( eServerSceneId.FREE_GAME );
        }
        else if( isSBG() && _.has( rfwsResponse, "payTable.prizeList" ) ) {
            m_data.payTable = rfwsResponse.payTable.prizeList;
        }
    };

    // ---------------------------------------------------//
    // SOCKETS
    // ---------------------------------------------------//

    /**
     * DATOS QUE RECIBIMOS DEL SOCKET
     */
    var socketReception = function( data ) {
        console.log( "DATA FROM SOCKET", data );
        var message = JSON.parse( data.body );

        switch( message.type ) {
            case "UPPER_GAME_CONNECTED":
                sendCurrentPayTable();
                break;
            case "START_SPIN": onBtnSpin();
                break;
            default:
                break;
        }
    };
    var sendToSocket = function( data ) {
        if( isSBG() ) { RFGlobalData.net.wsWrapper.send( data ); }
    };

    var sendCurrentPayTable = function() {
        var toSocket = {
            type: "PAYTABLE",
            data: {
                currentScene: self.myScene,
                payTable: m_data.payTable
            }
        };
        sendToSocket( toSocket );
    };
    // ==============================================================
    //  UTILS AND HELPERS
    // ==============================================================

    var isSBG = function() {
        return _.get( RFGameConfig, "sbg.enable", false );
    };

    var hasToWaitUntilDisplayAllPrizeLines = function() {
        return _.get( RFGameConfig, "game.waitUntilDisplayAllPrizeLines", false );
    };

    /**
     * Para la reproducción de todos los sonidos de la escena
     */
    var stopAllSounds = function() {
        _.forEach( m_audioSounds, function( audioSound ) {
            audioSound.stop();
        } );
        self.game.sound.stopAll();
    };

    var isForced = function() {
        return m_oForceReels.forceNextGame;
    };

    var isSpecialForced = function() {
        return _.size( m_specialForceReels ) === m_reels.getNumberOfReels();
    };

    var isStatePreIdle = function() {
        return m_rffsm.getState() === eStateNames.PRE_IDLE;
    };

    var isStateIdle = function() {
        return m_rffsm.getState() === eStateNames.IDLE;
    };

    var isStateSpinning = function() {
        return m_rffsm.getState() === eStateNames.SPINNING;
    };

    // ===================================== FSM ===
    var eStateNames = {
        INIT: "INIT",
        PRE_IDLE: "PRE_IDLE",
        IDLE: "IDLE",
        SPINNING: "SPINNING",
        EXIT: "EXIT",
        POST_EXIT: "POST_EXIT"
    };

    var m_fsmInitData = {
        game: game,
        debug: true,
        states: {
            INIT: { onStart: startStarting, onExit: exitInit },
            PRE_IDLE: { onStart: startPreIdle, onUpdate: updatePreIdle, onExit: null },
            IDLE: { onStart: startIdle, onExit: exitIdle },
            SPINNING: { onStart: startSpinning, onExit: null },
            EXIT: { onStart: startExit, onUpdate: updateExit, onExit: null },
            POST_EXIT: { onStart: startPostExit, onExit: null }
        }
    }; // m_fsmInitData
    var m_states = m_fsmInitData.states;
    // ===================================== END FSM ===

    // protected export section
    _protected.eStateNames = eStateNames;
    _protected.fsmInitData = m_fsmInitData;
    _protected.rffsm = m_rffsm;

    if( self.constructor !== FreeGameScene ) { return _protected; }

    return undefined;

};// SceneUtils.fERROR

FreeGameScene.prototype = Object.create( BaseScene.prototype );
FreeGameScene.prototype.constructor = FreeGameScene;

//------------------------------------------------------
// Source: src/sbg/game/scenes/rf.scene.help.js
//------------------------------------------------------

/* eslint-disable max-lines */

/* eslint-disable max-len */
/**
 * @version 0.2.1
 * CAMBIOS:
 * 0.2.1 - Checkea si la escena a la que transita es un string y existe en game.
 * 0.2.0 - Se eliminan las referencias a la carga inicial de la tabla de pagos. Ahora se hace en la escena de reconexión.
 * 0.1.19c - Modificamos el como hacemos el flashing de los premios y sacamos dos señales enableFlash y disableFlash para punches.
 * 0.1.18c - Agregamos soporte a varios strIco como objetivos de animación de premio.
 * 0.1.17c - Se modifica el updateScreen para que haga desaparecer de forma correcta la segunda pantalla de ayuda.
 * 0.1.16c - Cuando se recibe un spinning, se vuelve a la pantalla del plan de ganancias
 * ------------------------------------------------------------------
 * 0.1.3 - Se llama a updateGlobalData de SceneUtils y se inicializan las labels para la gestión de moneda y créditos (requiere versión >= 0.0.25 de la BaseScene)
 * 0.1.4 - Añade la inicialización de labels de créditos
 * 0.1.5 - Se inicializan los keybindings vía config (requiere versión >= 0.0.27 de BaseScene y >= 0.5.4 de RFGameConfig)
 * 0.1.6c - Se añade la función connectToSocket para conectarse al socket cuando es una sbg con doble monitor
 * 0.1./c - Se hace que los premios flaseen con un tween
 * 0.1.8c - El falsheo de los premios se hace mediante el alpha. Al recibir un cambio de apuesta, cambio de ayuda, etc..., se desactiva el flasheo de los premios.
 * 0.1.9c - El juego espera a mostrar nada en la pantalla hasta que no recibe por socket un STARTING.
 * 0.1.10c - Se crea initTexts() para que pueda haber labels que recojan el texto a mostrar directamente desde el lang.json
 * 0.1.11c - Añadido SceneUtils.filterByParameters( m_rfLoader.getPhaserImages(), "visibleScreens" ) a UpdateScreens
 * 0.1.12c - El parpadeo de los premios se hace mediante scale, como estaba anteriormente
 * 0.1.13c - Se crea un grupo, cuyo tag es HIDE_ON_START (o algo así), que se muestra hasta que se recibe por socket el START o el PAYTABLE
 * 0.1.14c - El UPPER_GAME_CONNECTED se envía mediante json
 * 0.1.15c - Uso del wrapper de sockets
 */
/* eslint-enable max-len max-lines */

// eslint-disable-next-line no-implicit-globals, max-statements
var HelpScene = function( game ) {
    "use strict";

    // Call to base Scene Constructor
    var _protected = BaseScene.call( this, game );

    var m_super = this;
    var SceneUtils = _protected.SceneUtils;

    var self = this;
    var m_rfLoader = null;
    var m_phaserLabelsMap = {};
    var m_phaserButtonsMap = {};
    var m_aPhaserBackgrounds = [];
    var m_currentScene = 0;
    var m_audioSounds = {
        help: new RFAudioSound( game )
    };
    var lbl_msg = null;

    var socket_handler = null;

    // Array con los tweens del flaseo de premios
    var m_tweens = [];
    this.onEnableFlashing = new Phaser.Signal();
    this.onDisableFlashing = new Phaser.Signal();

    this.shutdown = function() {

        // call to parent
        m_super.shutdownBase();

    };

    this.preload = function() {

        // call to parent
        m_super.preloadBase();

    };

    this.create = function() {

        this.onEnableFlashing.removeAll();
        this.onDisableFlashing.removeAll();

        connectToSocket();
        // call to parent
        m_super.createBase();

        m_currentScene = 0;
        m_aPhaserBackgrounds = [];

        loadScene();

        // Keyboard bindings
        addKeyBindings();

        addActionsButtons();
        addBackgrounds();

        SceneUtils.initSound( m_audioSounds.help, eSoundTypes.HELP );
        m_audioSounds.help.playLooped();

        m_phaserLabelsMap = m_rfLoader.getPhaserLabels();
        m_phaserButtonsMap = m_rfLoader.getPhaserButtons();

        updateScreen( m_currentScene );

        lbl_msg = _.find( m_phaserLabelsMap, function( o ) { return o.parameters.tag === "MESSAGE"; } );

        transitionOut();

    };

    // ---  BEGIN TRANSITIONS ---------------------------------------------
    var transitionOut = function() {
        if( self.game.transitions ) {
            self.game.transitions.animation( {
                animation: "trans_out",
                fps: 21,
                killOnComplete: true,
                scale: 1.7
            } );
        }
    };
    var transitionIn = function( sceneName ) {
        RFGlobalData.net.wsWrapper.unsubscribe( socket_handler );
        if( self.game.transitions ) {
            self.game.transitions.animation( {
                animation: "trans_in", scale: 1.7, fps: 21,
                onComplete: function() {
                    self.game.state.start( sceneName );
                }
            } );
        }
        else {
            self.game.state.start( sceneName );
        }
    };
    // --- END TRANSITIONS -----------------------------------------------

    // ---------------------------------------------------//
    // SOCKETS
    // ---------------------------------------------------//
    var sendToSocket = function( data ) {
        if( isSBG() ) { RFGlobalData.net.wsWrapper.send( data ); }
    };
    // ---------------------------------------------------//
    // END SOCKETS
    // ---------------------------------------------------//

    var isSBG = function() {
        return _.get( RFGameConfig, "sbg.enable", false );
    };

    var drawLose = function() {
        if( !_.isUndefined( lbl_msg ) ) { lbl_msg.text = RFGameConfig.gameName; }
    };

    var startSpinning = function() {
        m_currentScene = 0;
        updateScreen( m_currentScene );
        if( !_.isUndefined( lbl_msg ) ) { lbl_msg.text = RFGlobalData.locale.texts.GOODLUCK; }
        disableFlashingPrizes();
    };

    var disableFlashingPrizes = function() {
        // Se elimina el tween de los premios.
        _.forEach( m_tweens, function( tw ) {
            game.tweens.remove( tw );
        } );
        var phaserLabels = m_rfLoader.getPhaserLabels();
        _.forEach( phaserLabels, function( label ) {
            var scalex = _.get( label, "data.gdObj.scale.x", 1 );
            var scaley = _.get( label, "data.gdObj.scale.y", 1 );
            label.scale.x = scalex;
            label.scale.y = scaley;
        } );

        self.onDisableFlashing.dispatch( );
    };

    var flashingPrizes = function( data ) {
        // Ponemos enhorabuena
        if( !_.isUndefined( lbl_msg ) ) { lbl_msg.text = RFGlobalData.locale.texts.CONGRATULATIONS_SIMPLE; }
        // Parpadeo de premios

        var phElementsToFlashing = m_rfLoader.getPhaserLabels();
        var arrayElementsToFlash = [];
        var twTime = 300;
        _.forEach( data.winPrizesArray, function( prize ) {
            var times = prize.times;
            var symbol = prize.strIco;
            // Agregamos soporte a varios strIco como objetivos de animación de premio.
            _.forEach( phElementsToFlashing, function( element ) {
                var parametersSymbol = _.get( element, "parameters", undefined );
                if( parametersSymbol ) {
                    var keySymbolsReferencesArray = _.get( parametersSymbol, "keySymbolsReferences", [] );
                    if( !_.isEmpty( keySymbolsReferencesArray ) ) {
                        if( _.isArray( keySymbolsReferencesArray ) ) {
                            _.forEach( keySymbolsReferencesArray, function( keyReference ) {
                                var keySymbol = _.get( keyReference, "keySymbol", "" );
                                var numberMatches = _.get( keyReference, "numberMatches", "" );
                                if( keySymbol === symbol && numberMatches === times ) {
                                    arrayElementsToFlash.push( element );
                                    var tw = game.add.tween( element.scale ).to( { x: 1.5, y: 1.5 }, twTime, Phaser.Easing.Linear.None, false, 0, -1, true );
                                    m_tweens.push( tw );
                                }
                            } );
                        }
                    }
                    else {
                        var keySymbol = _.get( parametersSymbol, "keySymbol", "" );
                        var numberMatches = _.get( parametersSymbol, "numberMatches", 0 );
                        if( keySymbol === symbol && numberMatches === times ) {
                            arrayElementsToFlash.push( element );
                            var tw = game.add.tween( element.scale ).to( { x: 1.5, y: 1.5 }, twTime, Phaser.Easing.Linear.None, false, 0, -1, true );
                            m_tweens.push( tw );
                        }
                    }
                }
            } );

        } );
        // start tweens and dispatch signal to attach other flashing effects
        _.forEach( m_tweens, function( tween ) {
            tween.start();
        } );

        self.onEnableFlashing.dispatch( data.winPrizesArray );
    };

    var getOnClickFunction = function( strFunctionName ) {
        switch( strFunctionName ) {
            case eActionNames.exitHelp: return exitHelp;
            case eActionNames.nextPayHelp: return nextPayHelp;
            case eActionNames.prevPayHelp: return prevPayHelp;
            default:
                console.warn( "Function ", strFunctionName, " not implemented in this scene" );
                return onBtnNotDefined;
        }
    };

    var exitHelp = function( sceneName ) {
        sceneName = sceneName || eSceneNames.BASEGAME;
        stopAllSounds();
        transitionIn( sceneName );
    };

    var updateScreen = function( sceneNumber ) {

        // All buttons and Labels with "visibleScreens"
        var allVisibleObjs = _.union(
            SceneUtils.filterByParameters( m_phaserLabelsMap, "visibleScreens" ),
            SceneUtils.filterByParameters( m_phaserButtonsMap, "visibleScreens" ),
            SceneUtils.filterByParameters( m_rfLoader.getPhaserImages(), "visibleScreens" ),
            SceneUtils.filterByParameters( m_rfLoader.getPhaserAnimations(), "visibleScreens" ),
            SceneUtils.filterByParameters( m_rfLoader.getPhaserGroups(), "visibleScreens" )
        );

        // this elements will be visibles
        var visibles = _.filter( allVisibleObjs, function( o ) { return _.hasValue( o.parameters.visibleScreens, sceneNumber ); } );
        visibles.push( m_aPhaserBackgrounds[ sceneNumber ] );

        // invisible elements
        var invisibles = _.difference( allVisibleObjs, visibles );
        _.forEach( m_aPhaserBackgrounds, function( bkg, key ) {
            if( key !== sceneNumber ) {
                invisibles.push( bkg );
            }
        } );

        SceneUtils.setVisibleAll( visibles, true );
        SceneUtils.setVisibleAll( invisibles, false );

    };

    var nextPayHelp = function() {
        disableFlashingPrizes();
        m_aPhaserBackgrounds[ m_currentScene ].visible = false;
        if( m_aPhaserBackgrounds.length - 1 === m_currentScene ) {
            m_currentScene = 0;
        }
        else {
            m_currentScene++;
        }
        updateScreen( m_currentScene );
        m_aPhaserBackgrounds[ m_currentScene ].visible = true;
    };

    var prevPayHelp = function() {

        m_aPhaserBackgrounds[ m_currentScene ].visible = false;
        if( m_currentScene === 0 ) {
            m_currentScene = m_aPhaserBackgrounds.length - 1;
        }
        else {
            m_currentScene--;
        }

        updateScreen( m_currentScene );
        m_aPhaserBackgrounds[ m_currentScene ].visible = true;
    };

    var onBtnNotDefined = function( button ) {
        console.warn( "Action not defined for this button: ", button );
    };

    var drawPayTable = function( data ) {
        disableFlashingPrizes();
        var prizeList = data.payTable;

        console.log( "DRAWING PAYTABLE", prizeList );

        /* eslint-disable  */
        function drawPayTableInternal( _prizeList, type ) {
            _.forEach( _prizeList, function( oi, key1 ) {
                _.forEach( oi, function( or ) {
                    _.forEach( or, function() {
                        var phaserLabels = m_rfLoader.getPhaserLabels();
                        _.forEach( phaserLabels, function( oPhaserLabel ) {
                            if( oPhaserLabel.type === type ) {
                                var parametersSymbol = _.get( oPhaserLabel, "parameters", undefined );
                                if( parametersSymbol ) {
                                    var keySymbolsReferencesArray = _.get( parametersSymbol, "keySymbolsReferences", [] );
                                    if( !_.isEmpty( keySymbolsReferencesArray ) ) {
                                        // take the first one, all of these array has the same prize
                                        if( _.isArray( keySymbolsReferencesArray ) ) {
                                            var keySymbolReference = _.get( keySymbolsReferencesArray[ 0 ], "keySymbol", "" );
                                            var numberMatchesReference = _.get( keySymbolsReferencesArray[ 0 ], "numberMatches", 0 );
                                            if( keySymbolReference === key1 ) {
                                                if( numberMatchesReference > 0 ) {
                                                    oPhaserLabel.setMoneyOrCredits( or[ numberMatchesReference ], RFGlobalData.locale.currencyISO );
                                                }
                                                else {
                                                    console.warn( keySymbolReference + " no has numberMatches declared" );
                                                }
                                            }
                                        }
                                    }
                                    else { // classic way - garantee retrcompatibility
                                        var keySymbol = _.get( parametersSymbol, "keySymbol", "" );
                                        var numberMatches = _.get( parametersSymbol, "numberMatches", 0 );
                                        if( keySymbol === key1 ) {
                                            if( numberMatches > 0 ) {
                                                oPhaserLabel.setMoneyOrCredits( or[ numberMatches ], RFGlobalData.locale.currencyISO );
                                            }
                                            else {
                                                console.warn( keySymbol + " no has numberMatches declared" );
                                            }
                                        }
                                    }
                                }
                                else {
                                    console.warn( parametersSymbol + " has not parameters to build pay table" );
                                }
                            }
                        } );
                    } );
                } );
            } );
            /* eslint-enable  */
        }

        drawPayTableInternal( prizeList, eLabelsNames.symbolPrizeBaseGame );
        drawPayTableInternal( prizeList, eLabelsNames.symbolPrizeFreeGame );

    };

    var onChangeBaseScene = function( changeSceneData ) {
        var currentStateKey = _.get( changeSceneData, [ "currentStateKey" ] );
        var previousStateKey = _.get( changeSceneData, [ "previousStateKey" ] );
        console.log( "On Change Scene received. currentStateKey:", currentStateKey, "previousStateKey:", previousStateKey );

        if( currentStateKey ) {
            var sceneToTransit = _.get( RFGameConfig.game.upper.transitScenes, [ currentStateKey ] );
            if( _.isString( sceneToTransit ) ) {
                console.log( "CHANGE SCENE. BaseSceneKey", currentStateKey, " => ", "UpperSceneKey", sceneToTransit );
                exitHelp( sceneToTransit );
            }
        }
    };

    // ---------------------------------------------------//
    // SOCKETS
    // ---------------------------------------------------//
    var connectToSocket = function() {


        var wsWrapper = RFGlobalData.net.wsWrapper;
        if( !wsWrapper.isConnected() ) {
            wsWrapper.connect( RFGameConfig.sbg.socket.server, {}, function() {
                console.info( "CONNECTED TO SOCKET" );
                subscribeToSocket();
                sendUpperGameConnected();
            }, function( error ) {
                console.error( "SOCKET ERROR:", error );
            } );
        }
        else {
            subscribeToSocket();
            sendUpperGameConnected();
        }
    };

    var subscribeToSocket = function() {
        var wsWrapper = RFGlobalData.net.wsWrapper;
        wsWrapper.setOption( "defaultSendDestination", RFGameConfig.sbg.socket.subscription );
        socket_handler = wsWrapper.subscribe( socketReception );
    };

    var sendUpperGameConnected = function() {
        sendToSocket( {
            type: "UPPER_GAME_CONNECTED",
            data: null
        } );
    };


    /**
     * DATOS QUE RECIBIMOS DEL SOCKET
     */
    var socketReception = function( data ) {
        console.log( "DATA FROM SOCKET", data );
        var message = JSON.parse( data.body );
        switch( message.type ) {
            case "SPINNING":
                console.log( "SPINNING" );
                startSpinning();
                break;
            case "WIN":
                flashingPrizes( message.data );
                break;
            case "PAYTABLE":
                checkIfWeAreInCorrectScene( message.data.currentScene );
                drawPayTable( message.data );
                break;
            case "LOSE":
                drawLose( message.data );
                break;
            case "HELP":
                nextPayHelp();
                break;
            case "CHANGE_BASE_SCENE":
                onChangeBaseScene( message.data );
                break;
            default: console.warn( "UNKNOWN SOCKET MESSAGE", message );
        }
    };

    // ---------------------------------------------------//
    // //SOCKETS
    // ---------------------------------------------------//

    var loadScene = function() {
        m_rfLoader = m_super.getRFLoadScenes();
        SceneUtils.loadScene( self.myScene );
    };
    var addActionsButtons = function() {
        var phaserButtons = m_rfLoader.getPhaserButtons();
        _.forEach( phaserButtons, function( oPhaserButton ) {
            var theFunction = getOnClickFunction( oPhaserButton.action );
            oPhaserButton.onInputUp.add( theFunction, self );
        } );
    };

    var addBackgrounds = function() {
        var phaserBackgroundsMap = m_rfLoader.getPhaserBackgrounds();
        _.forEach( phaserBackgroundsMap, function( oPhaserBackground ) {
            oPhaserBackground.visible = false;
            m_aPhaserBackgrounds.push( oPhaserBackground );
        } );
        m_aPhaserBackgrounds = _.sortBy( m_aPhaserBackgrounds, function( o ) { return o.order; } );
        m_aPhaserBackgrounds[ m_currentScene ].visible = true;
    };
    var addKeyBindings = function() {
        if( _.get( RFGameConfig, [ "game", "useKeyBindings" ], true ) ) {
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.exitHelp" ), exitHelp );
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.previousPage" ), prevPayHelp );
            SceneUtils.addKeyBinding( _.get( RFGameConfig, "game.keyBindings.nextPage" ), nextPayHelp );
        }
        else if( _.get( RFGameConfig, [ "game", "forceAllowHistoryKeyBinding" ], true ) ) {
            // Nothing
        }
    };

    /**
     * Para la reproducción de todos los sonidos de la escena
     */
    var stopAllSounds = function() {
        _.forEach( m_audioSounds, function( audioSound ) {
            audioSound.stop();
        } );
        self.game.sound.stopAll();
    };


    var checkIfWeAreInCorrectScene = function( currentBaseScene ) {
        var currentUpperScene = RFGameConfig.game.upper.transitScenes[ currentBaseScene ];
        if( !_.isString( currentBaseScene ) ) {
            console.log( "Invalid currentBaseScene", currentBaseScene, "Maybe you need to update slo2sbg to >= 1.3.0 version in base game" );
            debugger;
        }
        else if( self.myScene !== currentUpperScene ) {
            if( _.isString( currentUpperScene ) ) {

                if( existsSceneKeyInGame( self.game, currentUpperScene ) ) {
                    console.error( "UpperScene", currentUpperScene, "not instanciated in game. You need to add it using game.state.add method" );
                    debugger;
                }
                else {
                    console.warn( "Paytable from another scene. We are in:", self.myScene, "but we need to change to", currentUpperScene,
                        "[ currentBaseScene:", currentBaseScene, " ]" );

                    exitHelp( currentUpperScene );
                }
            }
            else {
                console.log( "No transition defined. We stay here" );
            }
        }
    };

};

HelpScene.prototype = Object.create( BaseScene.prototype );
HelpScene.prototype.constructor = HelpScene;

//------------------------------------------------------
// Source: src/sbg/game/scenes/rf.scene.preload.js
//------------------------------------------------------

/**
 * @version 0.2.0 - CUSTOM 0.3.0c
 * CAMBIOS:
 * 0.1.2 - Se permite entrar en una escena sin pasar por la escena de RECONNECTION (para testear escenas sin conexión con servidor)
 * 0.1.3 - Elimina la carga del plugin de comportamiento (se carga en el BOOT)
 * 0.2.0  - Se añade parte protegida para slo2game. Se añaden métodos de inicialización para las utilidades de forzado.
 * 0.2.1c  - Init sockets
 * 0.3.0c  - Se usa la nueva opción para enviar comandos al SAS o no. Se pasa la opción de debug a la construcción del wrapper del webSocket.
 */

/* global GameDefinition */
// eslint-disable-next-line no-implicit-globals
var PreloadScene = function( game ) {
    "use strict";

    // Call to base Scene Constructor and get the protected
    var _protected = BaseScene.call( this, game ) || {};

    var m_super = this;
    var SceneUtils = _protected.SceneUtils;

    var self = this;
    // var m_rfLoadScenes = null;

    this.shutdown = function() {

        // call to parent
        m_super.shutdownBase( { stopAsyncLoad: false } );

    };

    this.preload = function () {

        // call to parent
        m_super.preloadBase( { loadAssets: false } );

        // Load the preload scene ( the assets for this scene, must be preloaded in "boot" scene )
        // m_rfLoadScenes = m_super.getRFLoadScenes();
        SceneUtils.loadScene( self.myScene );

        // barra de carga
        m_super._drawLoadingBar();

        // Inicialización del objeto global
        initRFGlobalData();

        // Inicialización de las utilidades
        initUtilities();

        // Start to load assets
        loadAssets();
    };

    this.create = function() {

        // call to parent
        m_super.createBase( { loadAssets: false } );
        m_super.loadAssetsAsync( GameDefinition.assets, "All Game assets" );

        var reconectionEnabled = _.get( RFGameConfig, "reconnection.enable", true );
        if( reconectionEnabled ) {
            console.log( "Entering in Reconnection scene..." );
            self.game.state.start( eSceneNames.RECONNECTION );
        }
        else {
            var sceneToLoad = _.get( RFGameConfig, "reconnection.sceneToLoadIfReconnectionIsDisable", eSceneNames.BASEGAME );
            console.warn( "Reconnection scene disabled. Entering in ", sceneToLoad, " scene..." );
            self.game.state.start( sceneToLoad );
        }
    };

    /**
     * Load assets associated to sceneName
     * @param {string} sceneName
     */
    var loadSceneAssets = function( sceneName ) {
        if( !_.hasValue( eSceneNames, sceneName ) ) {
            console.warn( "Scene", sceneName, "is not defined in eSceneNames", eSceneNames, "Load its assets anyway." );
        }
        var sceneAssetsToLoad = _.get( GameDefinition, [ "scenes", sceneName, "assets" ], null );
        m_super.loadAssets( sceneAssetsToLoad, "Assets for " + sceneName + " scene" );
    };

    /**
     * Load all scene assets included in sceneNamesArray
     * @param {array} sceneNamesArray
     */
    var loadAllScenesAssets = function( sceneNamesArray ) {
        _.forEach( sceneNamesArray, function( sceneName ) {
            loadSceneAssets( sceneName );
        } );
    };

    /**
     * Load all necessary and configurated assets through
     * @param {array} sceneNamesArray
     */
    var loadAssets = function() {

        // Carga de todos los assets del juego (sin asociar a ninguna escena)
        m_super.loadAssets( GameDefinition.assets, "All common game assets" );

        // Obtenemos la sección de configuración para escritorio o por defecto (móvil, iPad,...)
        var assetsConfig = _.get( RFGameConfig, [ "game", "assets", ( self.game.device.desktop ? "desktop" : "default" ) ] );

        // Creamos el array de escenas a cargar en función del fichero de configuración
        var sceneNamesArray = [];
        if( _.get( assetsConfig, [ "loadAllScenesAtPreload" ], false ) ) {
            _.forEach( GameDefinition.scenes, function( scene, sceneName ) {
                sceneNamesArray.push( sceneName );
            } );
        }
        else {
            sceneNamesArray = _.get( assetsConfig, [ "scenesToLoadAtPreloadArray" ], sceneNamesArray );
        }
        loadAllScenesAssets( sceneNamesArray );
    };


    var initRFGlobalData = function() {

        function initLocale() {
            var userlang = navigator.language || navigator.userLanguage;
            if ( QueryParameters.lang !== undefined ) {
                userlang = QueryParameters.lang;
            }
            else {
                userlang = userlang.substring( 0, 2 ).toLocaleLowerCase();
            }
            $.ajax( {
                url: "assets/" + userlang + "/texts/lang.json",
                async: false,
                dataType: "json",
                success: function ( response ) {
                    RFGlobalData.locale.texts = response;
                }
            } );
            RFGlobalData.locale.lang = userlang;
            RFCurrencyUtils.decimalSeparator = RFGlobalData.locale.decimalSeparator;
            RFCurrencyUtils.thousandSeparator = RFGlobalData.locale.thousandSeparator;
        }

        function initNet() {
            if( _.has( RFGameConfig, "gameServer" ) ) {
                RFGlobalData.net.wrapper = new RFGameServerWrapper();
                RFGlobalData.net.wrapper.initialize( RFGameConfig.gameServer );
            }
            else {
                throw new Error( "No server defined in RFGameConfig (gameServer or webService)" );
            }

            if( _.get( RFGameConfig, "sbg.enable" ) ) {
                RFGlobalData.net.wsWrapper = new RFWebSocketWrapper();
                RFGlobalData.net.wsWrapper.setOptions( { debug: RFGameConfig.sbg.socket.debug } );
                if( RFGameConfig.sbg.sas.enable ) {
                    RFGlobalData.net.retailNotificationWrapper = new RFRetailNotificationWrapper();
                }
                else {
                    console.info( "SAS is and HOMOLOGATION is disabled" );
                }
            }
        }

        if( typeof RFGlobalData === "undefined" ) { throw new Error( "RFGlobalData is undefined" ); }
        initLocale();
        initNet();
    };

    var initUtilities = function() {
        if( !_.get( RFGameConfig, "game.allowForceReelsUtility" ) ) {
            if( typeof FORCE_REELS !== "undefined" ) {
                console.info( "Noop the FORCE_REELS utility" );
                window.FORCE_REELS = _.noop;
            }
        }
    };

    if( self.constructor !== PreloadScene ) { return _protected; }

    return undefined;
};

PreloadScene.prototype = Object.create( BaseScene.prototype );
PreloadScene.prototype.constructor = PreloadScene;

//------------------------------------------------------
// Source: src/sbg/game/scenes/rf.scene.reconnection.js
//------------------------------------------------------

/**
 * @version 0.3.0
 * CAMBIOS:
 * 0.3.0 - Controla el inicio del juego en función de si es la pantalla superior o la inferior.
 * ----------------
 *  0.1.2: Se realiza un control para evitar entrar en la escena de reconexión, en caso de que esté deshabilitada desde el archivo de configuración
 *  0.1.3c: Conexión al socket
 *  0.1.4c: Se crean tres eCustomSceneNames.CUSTOM_GAME para transitar
 *  0.1.5c - Uso del wrapper de los sockets
 *  0.1.6c - Ante una desconexión se sale al lobby
 *  0.1.7c - Sobrescribe el archivo de configuración del cliente con los datos del servidor, en caso de que se reciban.
 *  0.1.8c - Elimina una suscripción al socket que era innecesaria.
 *  0.1.9c - Se almacena el objeto gameConfig del server en RFGlobalData
 *  0.1.10c - Soporte para apuestas locales. Gestión de la configuración inicial de la máquina por websocket.
 *  0.1.11c - Procesado de configuración de la configuración de la SBG para mantener apuesta y volumen entre cambio de juegos.
 *  0.1.12c - Cambio para que el valor con el que inicia la sesión lo tome de un valor del RFGameConfig.sbg.
 *  0.1.13c - Se pasa el timeout para el módulo del wrapper de retail.
 *          - Procesa el mute del audio de la configuración.
 *  0.2.0c  - Se comprueba si está creado retailNotificationWrapper antes de usarlo.
 */

// eslint-disable-next-line no-implicit-globals, no-unused-vars
var ReconnectionScene = function( game ) {
    "use strict";

    // Call to base Scene Constructor
    var _protected = BaseScene.call( this, game );

    var m_super = this;
    var SceneUtils = _protected.SceneUtils;

    var self = this;
    var m_storage = null;
    var m_keyData = null;
    var m_rfLoader = null;

    var m_wsStartSessionResponse;
    var m_websocketHandler;
    var m_waitingGameConfiguration;

    this.preload = function() {

        // call to parent
        m_super.preloadBase();

        m_keyData = getSessionStorageKey();
    };

    this.create = function() {

        connectToSocket( function() {

            // call to parent
            m_super.createBase();

            m_websocketHandler = RFGlobalData.net.wsWrapper.subscribe( socketReception );

            RFGlobalData.game.lastScene = eSceneNames.RECONNECTION;

            // We initialize the retailNotificationWrapper.
            if( RFGlobalData.net.retailNotificationWrapper ) {
                RFGlobalData.net.retailNotificationWrapper.initialize( {
                    url: RFGameConfig.sbg.rest,
                    timeout: RFGameConfig.sbg.restTimeout
                } );
            }

            var reconectionEnabled = _.get( RFGameConfig, "reconnection.enable", true );
            if( !reconectionEnabled ) {
                startSceneWithoutReconnection();
            }
            else {
                loadScene();

                if( storageAvailable( "localStorage" ) ) {
                    m_storage = localStorage;
                    RFGlobalData.net.session = _.isEmpty( RFGlobalData.net.session ) ? m_storage.getItem( m_keyData ) : RFGlobalData.net.session;
                }

                switch( RFGameConfig.sbg.screenId ) {
                    case eScreenId.BASE:
                        startBaseScreen();
                        break;
                    case eScreenId.UPPER:
                        startUpperScreen();
                        break;
                    default:
                        throw new Error( "Invalid RFGameConfig.sbg.screenId: " + RFGameConfig.sbg.screenId );
                }
            }
        } );
    };

    this.shutdown = function() {

        if( RFGlobalData.net.wsWrapper ) {
            RFGlobalData.net.wsWrapper.unsubscribe( m_websocketHandler );
        }

        // call to parent
        m_super.shutdownBase();

    };

    SceneUtils.fERROR = function() {
        // Ante un error, se sale del juego
        var toSocket = {
            type: "EXIT",
            data: null
        };
        sendToSocket( toSocket );
    };

    var startUpperScreen = function() {
        sendToSocket( { type: "UPPER_GAME_CONNECTED" } );
    };

    var startBaseScreen = function() {

        startSession( function( wsStartSessionResponse ) {

            m_wsStartSessionResponse = wsStartSessionResponse;

            var clientConfig = _.get( wsStartSessionResponse, [ "clientConfig" ] );
            SceneUtils.overrideConfig( clientConfig );

            processLocalBets( wsStartSessionResponse );

            processPrizeHype( wsStartSessionResponse );

            processSymbolWeights( wsStartSessionResponse );

            // send the configuration to the socket and wait the data to call
            // at startScene method (in socketReception)
            socketGetGameConfiguration();

            // wait for game configuration
            m_waitingGameConfiguration = self.game.time.events.add( RFGameConfig.sbg.msToWaitForTheGameConfiguration, function() {
                RFGlobalData.sbg.enableToSendGameConfigurationToSocket = true;
                console.warn( "Starting the game without the GameConfiguration" );
                startScene( m_wsStartSessionResponse );
            } );

            // Enable autoPlay if you want the user to auto bet when s/he has cash.
            RFGameConfig.game.autoPlayWithCash = _.get( wsStartSessionResponse, "autoPlayWithCash", RFGameConfig.game.autoPlayWithCash );
        }, SceneUtils.fERROR );
    };

    var startSceneWithoutReconnection = function() {
        var sceneToLoad = _.get( RFGameConfig, "reconnection.sceneToLoadIfReconnectionIsDisable", eSceneNames.BASEGAME );
        console.error(
            "Reconnection scene disabled. At this point, we must not be here (probably you need to update " +
            "rf.scene.preload.js to 0.1.2 version). Entering in", sceneToLoad, " scene..."
        );
        self.game.state.start( sceneToLoad );
    };

    var processLocalBets = function( wsStartSessionResponse ) {

        RFGlobalData.game.useLocalBets = false;
        var gameConfig = wsStartSessionResponse.gameConfig;
        var useLocalBets = _.get( RFGameConfig, [ "game", "useLocalBets" ], eUseLocalBets.FALSE );

        switch( useLocalBets ) {
            case eUseLocalBets.TRUE:
                if( gameConfig ) {
                    _.set( RFGlobalData, [ "game", "gameConfig" ], gameConfig );
                    RFGlobalData.game.useLocalBets = true;
                }
                else {
                    throw new Error( "Configured for use local bet, but the server don't send us this information." );
                }
                break;
            case eUseLocalBets.FALSE:
                break;
            case eUseLocalBets.AUTO:
                if( gameConfig ) {
                    _.set( RFGlobalData, [ "game", "gameConfig" ], gameConfig );
                    RFGlobalData.game.useLocalBets = true;
                }
                break;
            default:
                break;
        }

        if( RFGlobalData.game.useLocalBets ) {
            var oLocalBet = {
                betPerLine: String( wsStartSessionResponse.player.internalBet ),
                lines: String( wsStartSessionResponse.player.numLines ),
                denom: String( wsStartSessionResponse.player.denom ),
                serverSceneId: eServerSceneId.BASE_GAME
            };
            var data = {
                cash: precise( wsStartSessionResponse.player.cash ),
                totalBet: 0,
                lines: 0,
                denom: 0,
                internalBet: 0,
                betPerLine: 0
            };
            SceneUtils.updateLocalBet( oLocalBet, data );
        }

        console.info( "Using " + ( RFGlobalData.game.useLocalBets ? "local" : "server" ) + "bets" );
    };

    var processPrizeHype = function( wsStartSessionResponse ) {
        var gameConfig = wsStartSessionResponse.gameConfig;
        RFGameConfig.game.prizeHype = _.get( gameConfig, [ "prizeHype" ], RFGameConfig.game.prizeHype );
    };

    var processSymbolWeights = function( wsStartSessionResponse ) {
        var gameConfig = wsStartSessionResponse.gameConfig;
        RFGameConfig.game.symbolWeights = _.get( gameConfig, [ "dynamicReels" ], RFGameConfig.game.symbolWeights );
    };

    /**
     *
     */
    var loadScene = function() {

        m_rfLoader = m_super.getRFLoadScenes();
        SceneUtils.loadScene( self.myScene );

        var imagesMap = m_rfLoader.getPhaserImages();
        if( _.has( imagesMap, [ "loadingBar" ] ) ) {
            self.game.add.tween( imagesMap.loadingBar ).to( { alpha: 0 }, Phaser.Timer.SECOND, Phaser.Easing.Linear.None, true, 0, -1, false );
        }
    };

    /**
     * @param {RFWSStartSessionResponse} wsStartSessionResponse
     */
    var startScene = function( wsStartSessionResponse ) {


        var wsResponse = wsStartSessionResponse;
        if( !_.hasAllKeys( wsResponse, [ "player", "player.currentServerSceneId" ] ) ) { throw new Error( "Can't start any scene" ); }

        var scene = eSceneNames.BASEGAME;
        switch( wsResponse.player.currentServerSceneId ) {
            case eServerSceneId.BASE_GAME:
                break;
            case eServerSceneId.FREE_GAME:
                scene = eSceneNames.FREEGAME;
                break;
            case eCustomSceneNames.CUSTOM_GAME_1:
                scene = eCustomSceneNames.CUSTOM_GAME_1;
                break;
            case eCustomSceneNames.CUSTOM_GAME_2:
                scene = eCustomSceneNames.CUSTOM_GAME_2;
                break;
            case eCustomSceneNames.CUSTOM_GAME_3:
                scene = eCustomSceneNames.CUSTOM_GAME_3;
                break;
            case "":
                console.log( "No previous scene. Start a new game" );
                break;
            default:
                throw new Error( "Invalid scene '" + wsResponse.player.currentServerSceneId + "'" );
        }


        self.game.state.start( scene );
    };

    // ---------------------------------------------------//
    // SOCKETS
    // ---------------------------------------------------//
    var connectToSocket = function( fOk, fError ) {
        fOk = fOk || _.noop;
        fError = fError || _.noop;
        if( _.has( RFGlobalData, "net.wsWrapper" ) ) {
            var wsWrapper = RFGlobalData.net.wsWrapper;
            wsWrapper.connect( RFGameConfig.sbg.socket.server, {}, function() {
                console.info( "CONNECTED TO SOCKET" );
                wsWrapper.setOption( "defaultSendDestination", RFGameConfig.sbg.socket.subscription );
                fOk();
            }, function( error ) {
                console.error( "SOCKET ERROR:", error );
                fError();
            } );
        }
    };
    var sendToSocket = function( data ) {
        RFGlobalData.net.wsWrapper.send( data );
    };

    var socketReception = function( data ) {

        var message = JSON.parse( data.body );

        switch( RFGameConfig.sbg.screenId ) {
            case eScreenId.BASE:
                socketReceptionBaseScreen( message );
                break;
            case eScreenId.UPPER:
                socketReceptionUpperScreen( message );
                break;
            default:
                throw new Error( "Invalid RFGameConfig.sbg.screenId: " + RFGameConfig.sbg.screenId );
        }
    };

    var socketReceptionUpperScreen = function( message ) {
        switch( message.type ) {

            case "PAYTABLE":
            case "START":
                console.log( "Message type received:", message.type, "We can start the scene", eSceneNames.HELP );
                self.game.state.start( eSceneNames.HELP );
                break;

            default:
                console.warn( "message.type", message.type, "untreated" );
        }
    };

    var socketReceptionBaseScreen = function( message ) {

        switch( message.type ) {
            case "GAME_CONFIGURATION":
                if( _.get( message, [ "data", "method" ] ) === "PUSH" ) {

                    console.log( "GAME_CONFIGURATION received", message );

                    self.game.time.events.remove( m_waitingGameConfiguration );

                    processSBGGameConfiguration( message.data.content );

                    RFGlobalData.sbg.enableToSendGameConfigurationToSocket = true;

                    startScene( m_wsStartSessionResponse );
                }
                break;
            default:
                break;
        }
    };

    var socketGetGameConfiguration = function() {
        sendToSocket( {
            type: "GAME_CONFIGURATION",
            data: { method: "GET" }
        } );

        // Response: GAME_CONFIGURATION with method = PUSH
    };
    // ---------------------------------------------------//
    // //SOCKETS
    // ---------------------------------------------------//

    var processSBGGameConfiguration = function( gameConfiguration ) {

        function processMasterVolume( gameConfig ) {
            var masterVolume = _.get( gameConfig, [ "volume" ], 1 );
            SceneUtils.setSnappedMasterVolume( masterVolume );
        }

        function processMuteAudio( gameConfig ) {
            var mute = Boolean( _.get( gameConfig, [ "mute" ] ) );
            SceneUtils.setMuteAudio( mute );
        }

        function processBet( gameConfig ) {
            var totalBet = _.get( gameConfig, [ "totalBet" ], 1 );

            var localBets = RFGlobalData.game.localBets;
            localBets.currentBetPerLineIdx = SceneUtils.getBetsPerLineArray().length - 1;
            var oBet = {
                betPerLineIdx: localBets.currentBetPerLineIdx,
                linesIdx: localBets.currentLinesIdx,
                denomIdx: localBets.currentDenomIdx,
                serverSceneId: eServerSceneId.BASE_GAME
            };

            for( var idx = oBet.betPerLineIdx; idx >= 0; --idx ) {
                oBet.betPerLineIdx = idx;
                var currentTotalBet = SceneUtils.getTotalBetFrom( oBet );
                if( currentTotalBet <= totalBet ) {
                    localBets.currentBetPerLineIdx = idx;
                    break;
                }
            }
        }

        processMuteAudio( gameConfiguration );
        processMasterVolume( gameConfiguration );
        processBet( gameConfiguration );
    };

    var getSessionStorageKey = function() {
        return "_RFSessionID_" + _.get( RFGameConfig, [ "gameId" ], "-" ) + "_";
    };

    var startSession = function( fOK, fERROR ) {
        var params = {
            sessionId: _.isEmpty( RFGlobalData.net.session ) ? "2" : RFGlobalData.net.session
        };
        RFGlobalData.net.wrapper.sendStartSession( params, function( wsResponse ) {
            console.log( "START SESSION RESPONSE", wsResponse );

            RFGlobalData.net.session = wsResponse.sessionId;
            if( m_storage ) {
                m_storage.setItem( m_keyData, RFGlobalData.net.session );
            }
            if( _.isFunction( fOK ) ) {
                fOK( wsResponse );
            }
        }, fERROR );
    };
};

ReconnectionScene.prototype = Object.create( BaseScene.prototype );
ReconnectionScene.prototype.constructor = ReconnectionScene;

//------------------------------------------------------
// Source: src/sbg/game/utils/rf.gameutils.js
//------------------------------------------------------

/**
 * @version 0.1.2
 * CAMBIOS
 * 0.0.3 - Corrección del mensaje de error
 * 0.1.0 - Realiza la carga del archivo de entorno (de forma bloqueante) y devuelve el JSON leído
 * 0.1.1 - Añade la carga del lang
 * 0.1.2 - Cambia el nombre del método loadJSONAsync por loadJSONSync
 */

/**
 * Exports an utility function to set force the reels globaly
 *
 * @param {any} array of integers
 */

// eslint-disable-next-line no-implicit-globals, no-unused-vars
function FORCE_REELS( array ) {
    "use strict";

    // Check a valid game
    if( !( typeof game  !== "undefined" && isValidGame( game ) ) ) {
        console.error( "FORCE REELS: 'game' is not an Phaser game" ); /* RemoveLogging:skip */
        return;
    }

    // Get current state
    var state = game.state.getCurrentState();

    // Only BaseGame and FreeGame
    if( !( ( state instanceof BaseGameScene ) || ( state instanceof FreeGameScene ) ) ) {
        console.error( "FORCE REELS: Invalid scene to force reels [only BaseGame or FreeGame] " ); /* RemoveLogging:skip */
        return;
    }

    // Check if implement the _specialForceReels function
    if( !_.isFunction( state._specialForceReels ) ) {
        console.warn( "FORCE REELS: The scene needs to implement _specialForceReels" ); /* RemoveLogging:skip */
        return;
    }

    // Send Special Force Reels to current state
    if( state._specialForceReels( array ) ) {
        console.info( "FORCE REELS: Next spin will be forced." ); /* RemoveLogging:skip */
    }
    else {
        console.error( "FORCE REELS: Next spin WILL NOT be forced" ); /* RemoveLogging:skip */
    }

}

/**
 * Load a JSON file synchronously
 *
 * @param {any} urlToFile
 */

// eslint-disable-next-line no-implicit-globals, no-unused-vars
function loadJSONSync( urlToFile, fCallBackError, fCallBackSuccess ) {
    "use strict";
    var jsonFile;
    $.ajax( {
        url: urlToFile,
        async: false,
        dataType: "json",
        success: function( response ) {
            jsonFile = response;
            if( _.isFunction( fCallBackSuccess ) ) {
                fCallBackSuccess( response );
            }
        },
        error: fCallBackError
    } );

    return jsonFile;
}

/**
 * Load the enviroment Json file synchronously
 * @param {string} urlToEnvJsonFile
 * @returns envJson
 */

// eslint-disable-next-line no-implicit-globals, no-unused-vars
function loadEnvJsonFile( urlToEnvJsonFile ) {
    "use strict";
    var envJson;
    envJson = loadJSONSync( urlToEnvJsonFile, function( /* error */ ) {
        console.warn( "No environment file loaded", urlToEnvJsonFile );
    } );

    if( envJson !== undefined ) {
        console.log( "Enviroment file loaded", envJson );
    }

    return envJson;
}

/**
 * Load the lang texts
 * @param {string} urlToLangFile
 * @returns localeTexts
 */
// eslint-disable-next-line no-implicit-globals, no-unused-vars
function loadLangFile( urlToLangFile ) {
    "use strict";

    var langFile;
    langFile = loadJSONSync( urlToLangFile, function( /* error */ ) {
        console.error( "Error loading lang file", urlToLangFile );
    } );

    if( langFile !== undefined ) {
        console.log( "Lang file loaded", langFile );
    }

    return langFile;
}


//------------------------------------------------------
// Source: src/sbg/game/utils/rf.precise.js
//------------------------------------------------------

// eslint-disable-next-line no-implicit-globals, no-unused-vars
function precise( value ) {
    "use strict";
    var LENGTH_OF_WHOLE_PART = -1;
    var decimals = _.get( RFGlobalData, [ "locale", "numberOfDecimals" ], 2 );
    var finalValue = parseFloat( value ).format( decimals, LENGTH_OF_WHOLE_PART );
    return parseFloat( finalValue );
}

//------------------------------------------------------
// Source: src/sbg/slo2sbg/rf.slo2sbg.version.js
//------------------------------------------------------

/**
 * Slo2sbg Version
 */
if( typeof Slo2sbg === "undefined" ) { Slo2sbg = {}; }
Slo2sbg.VERSION = "1.3.1";  // IMPORTANT: Don't change this manually. The version will filled by grunt task
( function() {
    "use strict";
    var color = RFDebugUtils.getConsoleColors().INFO_SLO2;
    console.log( "%cSlo2sbg v" + Slo2sbg.VERSION, color ); /* RemoveLogging:skip */
} )();