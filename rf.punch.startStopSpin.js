// oh yeah
/* eslint-disable no-mixed-operators */
( function() {
    "use strict";
    var m_state = game.state.getCurrentState();
    var num_reels = m_state
        .getRFLoadScenes()
        .getRFGraphicReels()
        .getNumberOfReels();
    var MS_OFFSET = 1200;
    var anim = m_state.getRFLoadScenes().getPhaserAnimations().anm_payline;
    var animShouldStop = false;
    var invisibleAnim = function() {
        anim.visible = false;
    };

    var sound = new RFAudioSound( game );
    var rfSounds = game.state
        .getCurrentState()
        .getRFLoadScenes()
        .getRFSounds();
    var soundClip = _.find( rfSounds, function( o ) {
        return o.type === "PRIZE_SOUND";
    } );
    sound.setSounds( soundClip.soundKeys );

    m_state.onSpinResponse.add( function( wsResponse ) {
        if ( wsResponse.spinState.prize > 0 ) {
            animShouldStop = false;
            _.delay( function() {
                if ( !animShouldStop ) {
                    anim.visible = true;
                    anim.play( "animation" );
                    anim.animations.currentAnim.onComplete.add( invisibleAnim );
                    console.log( "-------------", wsResponse, anim );
                    sound.play();
                }
                else {
                    invisibleAnim();
                }
            }, MS_OFFSET + RFGameConfig.game.msBetweenStops * num_reels );
        }
    } );

    m_state.onStartSpin.add( function() {
        // When start spin, clean prize and stop animation
        animShouldStop = true;

        if ( anim !== null && anim !== undefined ) {
            anim.visible = false;
            anim.animations.stop( null, true );
        }
    } );

    function onShutdown() {
        m_state = null;
        num_reels = null;
        anim = null;
        invisibleAnim = null;
        animShouldStop = null;
        sound = null;
        rfSounds = null;
        soundClip = null;
    }

    m_state.onShutdown.addOnce( onShutdown );
} )();
